<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$errorMessage = "" ; 
$department = new Department();
$htmlElement = new HtmlElement() ; 


$formatValid = $department->isFormatValid($_GET, "getUserId", true);
$formatValidStatus = $formatValid["status"];

$data["regex"] = $department->getRegexJsMessage("createDepartment") ;


if ($formatValidStatus == "1") {

    $data["rtn"] = $department->getInfoWithSearch(array(
        "_id" => $_GET["id"]
    ));

    if (isset($data["rtn"][0]["_id"])) {

        /*
        $roleRegex = $data["role"] ;
        $teamRegex = $data["team"] ;
        $sexRegex = $data["sex"] ;
        $accStatusRegex = $data["accStatus"] ;

        $data["sexInfo"] = $htmlElement->getDropDownList("sex", array(),$sexRegex, $data["rtn"]["data"]["sex"]) ;
        $data["accStatusInfo"] = $htmlElement->getDropDownList("accStatus", array(),$accStatusRegex, $data["rtn"]["data"]["status"]) ;

        $data["roleInfo"] = $htmlElement->getDropDownList("role", $roleInfo["data"],$roleRegex, $data["rtn"]["data"]["type"]) ;
        $data["teamInfo"] = $htmlElement->getDropDownList("team", $teamInfo["data"],$teamRegex , $data["rtn"]["data"]["team"]) ;
        */
        $rtn = $data["rtn"][0] ;



        $data["regex"]["deptName"]["html"]  = $htmlElement->getTextField("deptName", $data["regex"]["deptName"], "Please type department title", $rtn["deptName"], "disabled");
        $data["regex"]["deptAddress"]["html"]  = $htmlElement->getTextField("deptAddress", $data["regex"]["deptAddress"], "Please type department address", $rtn["deptAddress"], "disabled");
        $data["regex"]["courseTitle"]["html"]  = $htmlElement->getTextField("courseTitle", $data["regex"]["courseTitle"], "Please type course title", $rtn["courseTitle"], "disabled");
        $data["regex"]["courseLevel"]["html"]  = $htmlElement->getTextField("courseLevel", $data["regex"]["courseLevel"], "Please type course level", $rtn["courseLevel"], "disabled");
        $data["regex"]["offerYear"]["html"]  = $htmlElement->getTextField("offerYear", $data["regex"]["offerYear"], "Please type offer year", $rtn['offerYear'], "disabled");
        $data["regex"]["classSize"]["html"]  = $htmlElement->getTextField("classSize", $data["regex"]["classSize"], "Please type class size", $rtn["classSize"], "disabled");
        $data["regex"]["availablePlaces"]["html"]  = $htmlElement->getTextField("availablePlaces", $data["regex"]["availablePlaces"], "Please type the available Places", $rtn["availablePlaces"], "disabled");


    }else{
        echo "Error Message : ".$data["msg"] ; 
    }
    
} else {
    echo "Error Message : " . $formatValid["msg"];
}

//print_r_pre($data) ;




$includeThemeFile = "school/details";

$route->requireThemeFile('school/details', $data);

