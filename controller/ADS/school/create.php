<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false)  {  die( "請由正常方法進入") ; }

$department = new Department();

$includeThemeFile = "school/create" ;


$data["regex"] = $department->getRegexJsMessage("createDepartment") ;

$data["dateTime"] = date("Y-m-d h:m:s") ;

//getSymptoms
$htmlElement = new HtmlElement();

$dateTime = date("Y-m-d h:m:s") ;

$data["regex"]["deptName"]["html"]  = $htmlElement->getTextField("deptName", $data["regex"]["deptName"], "Please type department title", "", "required");
$data["regex"]["deptAddress"]["html"]  = $htmlElement->getTextField("deptAddress", $data["regex"]["deptAddress"], "Please type department address", "", "required");
$data["regex"]["courseTitle"]["html"]  = $htmlElement->getTextField("courseTitle", $data["regex"]["courseTitle"], "Please type course title", "", "required");
$data["regex"]["courseLevel"]["html"]  = $htmlElement->getTextField("courseLevel", $data["regex"]["courseLevel"], "Please type course level", "", "required");
$data["regex"]["offerYear"]["html"]  = $htmlElement->getTextField("offerYear1", $data["regex"]["offerYear"], "Please type offer year", $dateTime, "disabled");
$data["regex"]["classSize"]["html"]  = $htmlElement->getTextField("classSize", $data["regex"]["classSize"], "Please type class size", "", "required");
$data["regex"]["availablePlaces"]["html"]  = $htmlElement->getTextField("availablePlaces", $data["regex"]["availablePlaces"], "Please type the available Places", "", "required");



$route->requireThemeFile('school/create',$data) ;

