<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$department = new Department();
$htmlElement = new HtmlElement() ;


$data = array() ; 
$data["courseInfo"] = $department->getInfoWithSearch([]);


$data["regex"] = $department->getRegexJsMessage("searchDepartment") ;

$data["regex"]["deptName"]["html"]  = $htmlElement->getTextField("deptName", $data["regex"]["deptName"], "Please type department title", "", "");
$data["regex"]["deptAddress"]["html"]  = $htmlElement->getTextField("deptAddress", $data["regex"]["deptAddress"], "Please type department address", "", "");
$data["regex"]["courseTitle"]["html"]  = $htmlElement->getTextField("courseTitle", $data["regex"]["courseTitle"], "Please type course title", "", "");
$data["regex"]["courseLevel"]["html"]  = $htmlElement->getTextField("courseLevel", $data["regex"]["courseLevel"], "Please type course level", "", "");
$data["regex"]["offerYear"]["html"]  = $htmlElement->getTextField("offerYear", $data["regex"]["offerYear"], "Please type offer year", "", "");
$data["regex"]["classSize"]["html"]  = $htmlElement->getTextField("classSize", $data["regex"]["classSize"], "Please type class size", "", "");
$data["regex"]["availablePlaces"]["html"]  = $htmlElement->getTextField("availablePlaces", $data["regex"]["availablePlaces"], "Please type the available Places", "", "");

//print_r_pre($data) ; 

$includeThemeFile = "school/view";

$route->requireThemeFile($includeThemeFile, $data);

