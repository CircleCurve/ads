<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false)  {  die( "請由正常方法進入") ; }

$student = new Student() ;
$department = new Department() ;

$courseInfo = $department->getInfoWithSearch([]) ;



$data["regex"] = $student->getRegexJsMessage("createStudent") ;



//getSymptoms
$htmlElement = new HtmlElement();

$data["courseInfo"] = $courseInfo ; 
$data["regex"]["studName"]["html"]  = $htmlElement->getTextField("studName", $data["regex"]["studName"], "Please type student name", "", "required");
$data["regex"]["DOB"]["html"]  = $htmlElement->getTextField("DOB", $data["regex"]["DOB"], "Please type date of birth", "", "required");



$route->requireThemeFile('student/enroll',$data) ;

