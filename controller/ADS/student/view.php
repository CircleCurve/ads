<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$student = new Student() ;
$htmlElement = new HtmlElement() ;


$data = array() ; 
$data["staff"] = $student->getAllInfo();

$data["regex"] = $student->getRegexJsMessage("createStudent") ;

$data["regex"]["studName"]["html"]  = $htmlElement->getTextField("studName", $data["regex"]["studName"], "Please type student name", "", "");
$data["regex"]["DOB"]["html"]  = $htmlElement->getTextField("DOB", $data["regex"]["DOB"], "Please type date of birth", "", "");

//print_r_pre($data) ; 

$includeThemeFile = "student/view";

$route->requireThemeFile($includeThemeFile, $data);

