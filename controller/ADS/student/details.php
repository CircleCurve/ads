<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("Please input from invalid page");
}

$errorMessage = "" ;
$student = new Student() ;
$htmlElement = new HtmlElement() ;


$courseInfo = $student->getCourse($_GET["id"]) ;

$formatValid = $student->isFormatValid($_GET, "getUserId", true);
$formatValidStatus = $formatValid["status"];

$data["regex"] = $student->getRegexJsMessage("createStudent") ;

if ($formatValidStatus == "1") {

    $data["rtn"] = $student->getInfoWithSearch(array(
        "_id" => $_GET["id"],
    ));


    if (isset($data["rtn"][0]["_id"])) {

        /*
        $roleRegex = $data["role"] ;
        $teamRegex = $data["team"] ;
        $sexRegex = $data["sex"] ;
        $accStatusRegex = $data["accStatus"] ;

        $data["sexInfo"] = $htmlElement->getDropDownList("sex", array(),$sexRegex, $data["rtn"]["data"]["sex"]) ;
        $data["accStatusInfo"] = $htmlElement->getDropDownList("accStatus", array(),$accStatusRegex, $data["rtn"]["data"]["status"]) ;

        $data["roleInfo"] = $htmlElement->getDropDownList("role", $roleInfo["data"],$roleRegex, $data["rtn"]["data"]["type"]) ;
        $data["teamInfo"] = $htmlElement->getDropDownList("team", $teamInfo["data"],$teamRegex , $data["rtn"]["data"]["team"]) ;
        */
        $rtn = $data["rtn"][0] ;
        
        $data["courseInfo"] = $courseInfo ;
        $data["regex"]["studName"]["html"]  = $htmlElement->getTextField("studName", $data["regex"]["studName"], "Please type student name", $rtn["studName"], "disabled");
        $data["regex"]["DOB"]["html"]  = $htmlElement->getTextField("DOB", $data["regex"]["DOB"], "Please type date of birth", $rtn["DOB"], "disabled");



    }else{
        echo "Error Message : ".$data["msg"] ; 
    }
    
} else {
    echo "Error Message : " . $formatValid["msg"];
}

//print_r_pre($data) ;




$includeThemeFile = "student/details";

$route->requireThemeFile('student/details', $data);

