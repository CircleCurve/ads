<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false)  {  die( "請由正常方法進入") ; }

$staff = new Staff() ; 

$roleInfo = $staff->getRoleInfo() ; 
$teamInfo = $staff->getTeamInfo() ; 


$includeThemeFile = "usermanagement/createuser" ; 

$data = $staff->getRegexJsMessage("createUser") ; 

$data["data"]["roleInfo"] = $roleInfo["data"] ; 
$data["data"]["teamInfo"] = $teamInfo["data"] ; 

$route->requireThemeFile('usermanagement/createuser',$data) ; 

