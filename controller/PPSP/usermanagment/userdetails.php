<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$errorMessage = "" ; 
$staff = new Staff();
$htmlElement = new HtmlElement() ; 

$roleInfo = $staff->getRoleInfo();
$teamInfo = $staff->getTeamInfo();

$formatValid = $staff->isFormatValid($_GET, "getUserId", true);
$formatValidStatus = $formatValid["status"];

$data = $staff->getRegexJsMessage("createUser") ; 

if ($formatValidStatus == "1") {

    $data["rtn"] = $staff->getUserInfo(array(
        "id" => $_GET["id"]
    ));

    if ($data["rtn"]["status"] == "1") {
        $roleRegex = $data["role"] ; 
        $teamRegex = $data["team"] ; 
        $sexRegex = $data["sex"] ; 
        $accStatusRegex = $data["accStatus"] ; 
        
        $data["sexInfo"] = $htmlElement->getDropDownList("sex", array(),$sexRegex, $data["rtn"]["data"]["sex"]) ; 
        $data["accStatusInfo"] = $htmlElement->getDropDownList("accStatus", array(),$accStatusRegex, $data["rtn"]["data"]["status"]) ; 

        $data["roleInfo"] = $htmlElement->getDropDownList("role", $roleInfo["data"],$roleRegex, $data["rtn"]["data"]["type"]) ; 
        $data["teamInfo"] = $htmlElement->getDropDownList("team", $teamInfo["data"],$teamRegex , $data["rtn"]["data"]["team"]) ;         

        }else{
        echo "Error Message : ".$data["msg"] ; 
    }
    
} else {
    echo "Error Message : " . $formatValid["msg"];
}

//print_r_pre($data) ; 
$includeThemeFile = "usermanagement/userdetails";

$route->requireThemeFile('usermanagement/userdetails', $data);

