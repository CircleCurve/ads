<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$staff = new Staff();
$htmlElement = new HtmlElement() ; 
$roleInfo = $staff->getRoleInfo("all");


$data = array() ; 
$data["staff"] = $staff->getAllStaff(
        array(
            "a.id",
            "a.userName",
            "a.jobTitle",
            "b.name",
            "a.status" , 
            "a.createTime",
            "a.loginTime"
        )
);
$data["regex"] = $staff->getRegexJsMessage("viewUser") ; 

$data["userNameInfo"] = $htmlElement->getTextField("userName", $data["regex"]["userName"], "Please type user name", "", ""); 
$data["roleInfo"] = $htmlElement->getDropDownList("role", $roleInfo["data"],$roleRegex,$data["regex"]["type"], "", ""); 
$data["idInfo"] = $htmlElement->getTextField("id", $data["regex"]["id"], "Please type user id", "", "" ); 
$data["accStatusInfo"] = $htmlElement->getDropDownList("status",array(), $data["regex"]["accStatus"], "" , ""); 

$data["jobTitleInfo"] = $htmlElement->getTextField("jobTitle", $data["regex"]["jobTitle"], "Please type job title", "", ""); 

//print_r_pre($data) ; 

$includeThemeFile = "usermanagement/viewuser";

$route->requireThemeFile('usermanagement/viewuser', $data);

