<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false)  {  die( "請由正常方法進入") ; }

$report = new Report () ; 
$htmlElement = new HtmlElement() ; 

/* Report Data */
$data = array() ; 

$data["regex"] = $report->getRegexJsMessage("viewReport") ;

$data["numberOfSymptoms"] = json_encode($report->getNumberOfSymptoms(array())) ; 
$data["slaSetting"] = json_encode($report->getNumberOfSymptoms(array())) ; 
$data["incidentModelType"] = json_encode($report->getNumberOfSymptoms(array())) ; 


$reportType = $report->getReportType(); 



/*Regular Expression */

$data["startDate"] = $htmlElement->getDateField("startDate", $data["regex"]["startDate"], "Please type start date", "", ""); 
$data["endDate"] = $htmlElement->getDateField("endDate", $data["regex"]["endDate"], "Please type end date", "", ""); 
$data["reportType"] = $htmlElement->getDropDownList("reportType", $reportType,$data["regex"]["reportType"], "", ""); 



$includeThemeFile = "report/viewreport" ; 
$route->requireThemeFile('report/viewreport',$data) ; 

