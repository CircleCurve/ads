<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}


$errorMessage = "";
$incident = new Incident();
$htmlElement = new HtmlElement();

//========getSoftware HardwareInfo 
$deviceInfo = $incident->getDevice(array()) ; 
$symptomInfo = $incident->getSymptom(array()) ; 


//getSymptoms
$data = array(); 

$data["regex"] = $incident->getRegexJsMessage("createIncident");

//print_r_pre($data) ; 

$data["regex"]["title"]["html"]  = $htmlElement->getTextField("title", $data["regex"]["title"], "Please type incident title", "", "required");
$data["regex"]["firstName"]["html"]  = $htmlElement->getTextField("firstName", $data["regex"]["firstName"], "Please type first name", "", "required");
$data["regex"]["lastName"]["html"]  = $htmlElement->getTextField("lastName", $data["regex"]["lastName"], "Please type last name", "", "required");
$data["regex"]["sex"]["html"]  = $htmlElement->getDropDownList("sex", array(),$data["regex"]["sex"],  "", "required");
$data["regex"]["email"]["html"]  = $htmlElement->getTextField("email", $data["regex"]["email"], "Please type email address", "", "required");
$data["regex"]["mobile"]["html"]  = $htmlElement->getTextField("mobile", $data["regex"]["mobile"],"Please type your mobile", "", "required");

$data["regex"]["address"]["html"]  = $htmlElement->getTextArea("address", $data["regex"]["address"], "", "required");
$data["regex"]["serialNo"]["html"]  = $htmlElement->getTextField("serialNo", $data["regex"]["serialNo"], "Please type serialNo", "", "required");
$data["regex"]["serviceType"]["html"]  = $htmlElement->getDropDownList("serviceType",$data["regex"]["serviceType"] ,  array(),"", "required");
$data["regex"]["firmWare"]["html"]  = $htmlElement->getDropDownList("firmWare", array(),$data["regex"]["firmware"], "", "required");

//$data["regex"]["device"]["html"]  = $htmlElement->getDropDownList("device", $data,array("regex"=>"", "message"=>""), "", "");


$data["regex"]["description"]["html"]  = $htmlElement->getTextArea("description",$data["regex"]["description"],"", "required");

//=========Device Info=============

$data["deviceInfo"] = json_encode($deviceInfo) ; 
$data["symptomInfo"] = json_encode($symptomInfo) ; 

/*
$roleRegex = $data["role"];
$teamRegex = $data["team"];
$sexRegex = $data["sex"];
$accStatusRegex = $data["accStatus"];

$data["sexInfo"] = $htmlElement->getDropDownList("sex", array(), $sexRegex, $data["rtn"]["data"]["sex"]);
$data["accStatusInfo"] = $htmlElement->getDropDownList("accStatus", array(), $accStatusRegex, $data["rtn"]["data"]["status"]);

$data["roleInfo"] = $htmlElement->getDropDownList("role", $roleInfo["data"], $roleRegex, $data["rtn"]["data"]["type"]);
$data["teamInfo"] = $htmlElement->getDropDownList("team", $teamInfo["data"], $teamRegex, $data["rtn"]["data"]["team"]);
*/


$includeThemeFile = "incidentmanagement/createincident";

$route->requireThemeFile('incidentmanagement/createincident',$data);

