<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$errorMessage = "";
$incident = new Incident();
$symptomsIssue = new Symptoms() ; 
$htmlElement = new HtmlElement();
$calendarUrl = "?act=incidentmanagement-viewIncident";

//========getSoftware HardwareInfo 
$deviceInfo = $incident->getDevice(array());
$symptomInfo = $incident->getSymptom(array());

$formatValid = $incident->isFormatValid($_GET, "getIncidentId", true);

$formatValidStatus = $formatValid["status"];

$data = array() ; 
$data["regex"] = $incident->getRegexJsMessage("createIncident");
$data["updateIncidentStatus"] =  $incident->getRegexJsMessage("updateIncident");



if ($formatValidStatus == "1") {
    $data["rtn"] = $incident->getIncidentInfo(array(
        "id" => $_GET["id"]
    ));
    //print_r_pre($data["rtn"]) ; 
    $data["symptomsIssue"] = $symptomsIssue->getSymptomsIssue(array(
        "incidentId"=>$_GET["id"],
    )) ;

    if ($data["symptomsIssue"]["status"]=="1"){
        $symptomsIssueInfo = $data["symptomsIssue"]["info"] ; 
        
        $data["regex"]["symptomTable"]["html"] = $htmlElement->getTable("symptomTable", $symptomsIssueInfo) ; 
        
                //print_r_pre($symptomsIssueInfo) ; 
        
    }
    
    
    if ($data["rtn"]["status"] == "1") {
        
        $value = $data["rtn"]["info"][0] ; 
        
        //Progress Information
        $data["regex"]["incidentStatus"]["html"] = $htmlElement->getDropDownList("incidentStatus", array(), $data["updateIncidentStatus"]["incidentStatus"], $value["status"], "required");
        $data["regex"]["comment"]["html"] = $htmlElement->getTextArea("comment", $data["updateIncidentStatus"]["comment"], $value["comment"] ,  "required");
        
                
        $data["regex"]["title"]["html"] = $htmlElement->getTextField("title", $data["regex"]["title"], "Please type incident title", $value["title"], "disabled");
        $data["regex"]["firstName"]["html"] = $htmlElement->getTextField("firstName", $data["regex"]["firstName"], "Please type first name", $value["firstName"], "disabled");
        $data["regex"]["lastName"]["html"] = $htmlElement->getTextField("lastName", $data["regex"]["lastName"], "Please type last name", $value["lastName"], "disabled");
        $data["regex"]["sex"]["html"] = $htmlElement->getDropDownList("sex", array(), $data["regex"]["sex"], $value["sex"], "disabled");
        $data["regex"]["email"]["html"] = $htmlElement->getTextField("email", $data["regex"]["email"], "Please type email address", $value["email"], "disabled");
        $data["regex"]["mobile"]["html"] = $htmlElement->getTextField("mobile", $data["regex"]["mobile"], "Please type your mobile", $value["mobile"], "disabled");

        $data["regex"]["address"]["html"] = $htmlElement->getTextArea("address", $data["regex"]["address"], $value["address"], "disabled");
        $data["regex"]["serialNo"]["html"] = $htmlElement->getTextField("serialNo", $data["regex"]["serialNo"], "Please type serialNo", $value["serialNo"], "disabled");
        $data["regex"]["serviceType"]["html"] = $htmlElement->getDropDownList("serviceType", $data["regex"]["serviceType"], array(), $value["serviceType"], "disabled");
        $data["regex"]["firmWare"]["html"] = $htmlElement->getDropDownList("firmWare", array(), $data["regex"]["firmware"], $value["firmWare"], "disabled");

//$data["regex"]["device"]["html"]  = $htmlElement->getDropDownList("device", $data,array("regex"=>"", "message"=>""), "", "");


        $data["regex"]["description"]["html"] = $htmlElement->getTextArea("description", $data["regex"]["description"], $value["description"], "disabled");
        

    } else {
         

        $incident->displayAlertBox($data["rtn"]["status"]["msg"], $calendarUrl);
    }
} else {
    //echo "Test" ; 
    //die() ; 
    $incident->displayAlertBox($formatValid["msg"], $calendarUrl);
}

//print_r_pre($data) ; 

$includeThemeFile = "incidentmanagement/updateIncident";

$route->requireThemeFile($includeThemeFile, $data);

