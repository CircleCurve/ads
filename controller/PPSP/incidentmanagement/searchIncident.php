<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$incident = new Incident();
$htmlElement = new HtmlElement();
$incidentALLInfo = $incident->getIncidentInfoALL();

$data = array();
//$data["incidentInfo"] = $incidentALLInfo;

$data["incidentInfo"] = $incident->getAllStaff(
        array(
            "b.description",
            "d.userName Fromwho",
            "c.userName Assigned",
            "b.status",
            "b.logTime"
        )
);



$data["regex"] = $incident->getRegexJsMessage("searchIncidentId");

$data["title"] = $htmlElement->getTextField("title", $data["regex"]["title"], "Please type user name", "", "");
$data["assign"] = $htmlElement->getTextField("assign", $data["regex"]["assign"], "Please type input some description", "", "");

$data["description"] = $htmlElement->getTextField("description", $data["regex"]["description"], "Please type input some description", "", "");
$data["incidentId"] = $htmlElement->getTextField("incidentId", $data["regex"]["incidentId"], "Please input Incident ID", "", "");


//print_r_pre($data) ; 

$includeThemeFile = "incidentmanagement/searchIncident";

$route->requireThemeFile($includeThemeFile, $data);

