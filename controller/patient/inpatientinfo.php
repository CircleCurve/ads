<?php

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$patient = new patient();

$includeThemeFile = "Patient/inpatientinfo";

$data = $patient->getRegexJsMessage("inpatientinfo");


$data["physicalCheck"] = $patient->getPhysicalCheckInformation(array(
    "inPatientId" => $_GET["inPatientId"]
        )
);

$data["healthStatus"] = $patient->getHealthStatus() ; 

$route->requireThemeFile($includeThemeFile, $data);

