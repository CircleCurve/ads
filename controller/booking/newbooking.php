<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false)  {  die( "請由正常方法進入") ; }

$booking = new Booking() ; 

$includeThemeFile = "Booking/newbooking" ; 

$data = $booking->getRegexJsMessage("booking") ; 

$service = $booking->getServiceInformation (array()) ; 

$bedType =  $booking->getBedTypeInformation(array()) ;

$data["bedType"] = $bedType["result"] ; 

$route->requireThemeFile('Booking/newbooking' ,$data) ; 

