-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 24, 2016 at 11:58 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `PPSP`
--

-- --------------------------------------------------------

--
-- Table structure for table `calendar_newevent`
--

create database PPSP DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE PPSP ; 

CREATE TABLE IF NOT EXISTS `calendar_newevent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incidentId` int(11) NOT NULL COMMENT '1 to 1 incidentId',
  `title` varchar(50) DEFAULT NULL,
  `start` datetime NOT NULL COMMENT 'event start date',
  `end` datetime NOT NULL COMMENT 'event end date',
  `allDay` int(1) DEFAULT '0' COMMENT 'event all day',
  `color` varchar(50) DEFAULT NULL,
  `repeatedevent` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` text COMMENT 'event description',
  `reminder` varchar(50) DEFAULT NULL,
  `timer` varchar(50) DEFAULT NULL,
  `reminder_number` varchar(50) DEFAULT NULL,
  `public_own` int(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `calendar_newevent`
--

INSERT INTO `calendar_newevent` (`id`, `incidentId`, `title`, `start`, `end`, `allDay`, `color`, `repeatedevent`, `user_id`, `description`, `reminder`, `timer`, `reminder_number`, `public_own`) VALUES
(1, 1, 'incident1', '2016-03-24 17:06:35', '2016-03-24 19:06:35', 0, '78B4B4', NULL, 2, 'Description', NULL, NULL, NULL, 1),
(2, 2, 'CEO computer', '2016-03-24 17:31:59', '2016-03-24 19:31:59', 0, '378006', NULL, 3, 'the computer cover is scratch', NULL, NULL, NULL, 1),
(3, 3, 'CFO computer', '2016-03-24 17:33:52', '2016-03-24 19:33:52', 0, '378006', NULL, 4, 'This computer is purchase from china', NULL, NULL, NULL, 1),
(4, 4, 'CTO computer', '2016-03-24 17:36:28', '2016-03-25 17:36:28', 0, 'ED220C', NULL, 5, 'This computer is very dirty', NULL, NULL, NULL, 1),
(5, 5, 'Manger computer', '2016-03-24 17:38:14', '2016-03-24 19:38:14', 0, 'ED220C', NULL, 6, 'the customer is very angry', NULL, NULL, NULL, 1),
(6, 6, 'Larry Computer', '2016-03-24 17:42:52', '2016-03-24 19:42:52', 0, '378006', NULL, 7, 'Test from Larry', NULL, NULL, NULL, 1),
(7, 7, 'Laptop from Gov', '2016-03-24 17:57:51', '2016-03-24 19:57:51', 0, '378006', NULL, 8, 'this computer is important', NULL, NULL, NULL, 1),
(8, 8, 'Harry Ng notebook', '2016-03-24 18:55:38', '2016-03-24 18:55:38', 0, '378006', NULL, 9, 'no desciption', NULL, NULL, NULL, 1),
(9, 9, 'Testing 2John', '2016-03-24 19:21:32', '2016-03-24 21:21:32', 0, '378006', NULL, 10, 'Hong Kong', NULL, NULL, NULL, 1),
(10, 10, 'Incident Summary', '2016-03-24 19:29:33', '2016-03-24 21:29:33', 0, '378006', NULL, 11, 'larry tam', NULL, NULL, NULL, 1),
(11, 11, 'Incident Summary', '2016-03-24 19:30:34', '2016-03-24 21:30:34', 0, '378006', NULL, 12, 'larry tam', NULL, NULL, NULL, 1),
(12, 12, 'Testing 3', '2016-03-24 19:32:13', '2016-03-24 21:32:13', 0, '378006', NULL, 13, 'Radio', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `calendar_shared_event`
--

CREATE TABLE IF NOT EXISTS `calendar_shared_event` (
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `editEvent` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calendar_shared_event`
--


-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT 'describe the device name ',
  `description` text NOT NULL COMMENT 'describe the  details',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT ' effective',
  `createTime` datetime NOT NULL COMMENT 'Which time of role create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this role',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id`, `name`, `description`, `status`, `createTime`, `createBy`) VALUES
(1, 'Application', '', 1, '2016-03-16 23:13:48', 1),
(2, 'Audio port', '', 1, '2016-03-16 23:13:48', 1),
(3, 'Battery', '', 1, '2016-03-16 23:13:48', 1),
(4, 'Blur-Ray', '', 1, '2016-03-16 23:13:48', 1),
(5, 'Camema', '', 1, '2016-03-16 23:13:48', 1),
(6, 'Card Reader', '', 1, '2016-03-16 23:13:48', 1),
(7, 'DP Port', '', 1, '2016-03-16 23:13:48', 1),
(8, 'DVD Rom', '', 1, '2016-03-16 23:13:48', 1),
(9, 'DVDRW', '', 1, '2016-03-16 23:13:48', 1),
(10, 'DVI port', '', 1, '2016-03-16 23:13:48', 1),
(11, 'Fan', '', 1, '2016-03-16 23:13:48', 1),
(12, 'Hard disk', '', 1, '2016-03-16 23:13:48', 1),
(13, 'HDMI port', '', 1, '2016-03-16 23:13:48', 1),
(14, 'Hinge', '', 1, '2016-03-16 23:13:48', 1),
(15, 'Keybooard', '', 1, '2016-03-16 23:13:48', 1),
(16, 'Mic port', '', 1, '2016-03-16 23:13:48', 1),
(17, 'Modem Port', '', 1, '2016-03-16 23:13:48', 1),
(18, 'Monitor', '', 1, '2016-03-16 23:13:48', 1),
(19, 'Mouse', '', 1, '2016-03-16 23:13:48', 1),
(20, 'NIC Port', '', 1, '2016-03-16 23:13:48', 1),
(21, 'Operating System', '', 1, '2016-03-16 23:13:48', 1),
(22, 'Other', '', 1, '2016-03-16 23:13:48', 1),
(23, 'Physical Damage', '', 1, '2016-03-16 23:13:48', 1),
(24, 'Power', '', 1, '2016-03-16 23:13:48', 1),
(25, 'Power Adapter', '', 1, '2016-03-16 23:13:48', 1),
(26, 'Speaker', '', 1, '2016-03-16 23:13:48', 1),
(27, 'Touch Pad', '', 1, '2016-03-16 23:13:48', 1),
(28, 'USB Port', '', 1, '2016-03-16 23:13:48', 1),
(29, 'VGA port', '', 1, '2016-03-16 23:13:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Incident`
--

CREATE TABLE IF NOT EXISTS `Incident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL COMMENT 'staff firstName',
  `lastName` varchar(50) NOT NULL COMMENT 'staff lastName',
  `sex` int(1) DEFAULT NULL COMMENT 'sex',
  `email` varchar(50) NOT NULL COMMENT 'email',
  `address` text COMMENT 'user address',
  `mobile` int(8) NOT NULL COMMENT 'user mobile number',
  `serialNo` varchar(30) NOT NULL COMMENT 'serial No',
  `serviceType` int(11) NOT NULL DEFAULT '0' COMMENT 'service Type',
  `description` text COMMENT 'description',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'status',
  `logTime` datetime NOT NULL COMMENT 'Log time',
  `completed_time` datetime DEFAULT NULL COMMENT 'completed time',
  `expected_finish` datetime DEFAULT NULL COMMENT 'expected finish',
  `comment` text COMMENT 'comment',
  `createBy` int(11) DEFAULT NULL COMMENT 'createBy',
  PRIMARY KEY (`id`),
  KEY `IDX_MOBILE` (`mobile`),
  KEY `IDX_type` (`serviceType`),
  KEY `IDX_status` (`status`),
  KEY `IDX_logTime` (`logTime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Incident`
--

INSERT INTO `Incident` (`id`, `firstName`, `lastName`, `sex`, `email`, `address`, `mobile`, `serialNo`, `serviceType`, `description`, `status`, `logTime`, `completed_time`, `expected_finish`, `comment`, `createBy`) VALUES
(1, 'larry', 'tam', 1, 'larry@gmail.com', 'sklasmdlkasmd', 12345678, '12345678', 1, 'Description', 1, '2016-03-22 17:06:35', '2016-03-24 17:38:35', '2016-03-24 19:06:35', 'COMPLETED', 1),
(2, 'Jenny', 'LEE', 2, 'jennylee@ooko.com', 'Hong Kong street', 12345678, '23764JB', 1, 'the computer cover is scratch', 2, '2016-03-22 17:31:59', '2016-03-24 17:38:35', '2016-03-24 19:31:59', NULL, 2),
(3, 'Kevin', 'CHAN', 1, 'kevinchan12@fcu.com', 'China', 12345678, '23746JH', 1, 'This computer is purchase from china', 2, '2016-03-18 17:33:52', '2016-03-24 17:38:35', '2016-03-24 19:33:52', NULL, 2),
(4, 'Gigi', 'LAU', 2, 'gigiL@deuor.com', 'Quata', 12345678, '21P4675', 2, 'This computer is very dirty', 3, '2016-03-24 17:36:28', '2016-03-24 17:38:35', '2016-03-24 19:36:28', 'this is test', 2),
(5, 'Sam', 'Kung', 1, 'samk22@abc.com', 'Wan Chai', 12345678, 'RT56494', 1, 'the customer is very angry', 3, '2016-03-24 17:38:14', '2016-03-24 17:38:35', '2016-03-24 19:38:14', 'Being investigat', 2),
(6, 'Larry', 'Tam', 1, 'larry@lol.com', 'Hong Kong', 12345678, 'RT56473', 1, 'Test from Larry', 2, '2016-03-15 17:42:52', NULL, NULL, NULL, 2),
(7, 'Kenneth', 'WAN', 1, 'support@govv.com', 'Aberdeen', 12345678, 'GA345678', 1, 'this computer is important', 2, '2016-03-24 17:57:51', NULL, NULL, NULL, 2),
(8, 'Harry', 'Ng', 1, 'harryng@gmail.com', 'Room 2201 Sin Yat House Yat Tung EST Tung Chung', 65432113, 'STN299902', 3, 'no desciption', 2, '2016-03-09 18:55:38', NULL, NULL, NULL, 1),
(9, 'John', 'DOE', 1, 'johndoe@fff.com', 'HK', 12345678, 'TY78904', 1, 'Hong Kong', 2, '2016-03-24 19:21:32', NULL, NULL, NULL, 1),
(10, 'larry', 'larry', 1, 'larry@gmail.com', 'cityu scope', 12345678, '12345678', 1, 'larry tam', 2, '2016-03-24 19:29:33', NULL, NULL, NULL, 1),
(11, 'larry', 'larry', 1, 'larry@gmail.com', 'cityu scope', 12345678, '12345678', 1, 'larry tam', 2, '2016-03-17 19:30:34', NULL, NULL, NULL, 1),
(12, 'Michael', 'LAM', 1, 'michaellam@go.com', 'Hong Kong', 12345678, 'THYZui7', 1, 'Radio', 2, '2016-03-20 19:32:13', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL COMMENT 'describe usage of each user permission',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Is it effective',
  `createTime` datetime NOT NULL COMMENT 'Which time of permission create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this permission',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `permission`
--


-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT 'describe the role name invoke in this systme',
  `description` text NOT NULL COMMENT 'describe the role details',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'role effective',
  `createTime` datetime NOT NULL COMMENT 'Which time of role create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this role',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `status`, `createTime`, `createBy`) VALUES
(1, 'admin', 'allow to edit all pages', 1, '2016-03-14 19:27:13', 1),
(2, 'SERVICE DECK', 'allow to watch report', 1, '2016-03-14 19:27:13', 1),
(3, 'ENGINEER', 'allow to edit warrant and incident', 1, '2016-03-14 19:27:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Staff`
--

CREATE TABLE IF NOT EXISTS `Staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL COMMENT 'login userName',
  `firstName` varchar(50) NOT NULL COMMENT 'staff firstName',
  `lastName` varchar(50) NOT NULL COMMENT 'staff lastName',
  `password` varchar(32) NOT NULL COMMENT 'password is one way encrypted cannot be decrypted',
  `sex` int(1) DEFAULT NULL COMMENT 'sex',
  `hkid` varchar(8) DEFAULT NULL COMMENT 'hkid',
  `birthday` datetime NOT NULL COMMENT 'birthday',
  `email` varchar(50) NOT NULL COMMENT 'email',
  `directLine` int(20) NOT NULL COMMENT 'direct line phone number',
  `address` text COMMENT 'user address',
  `mobile` int(8) NOT NULL COMMENT 'user mobile number',
  `jobTitle` varchar(50) NOT NULL COMMENT 'job title',
  `team` int(11) NOT NULL COMMENT 'Service team',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT 'staff type maybe patient, doctor,nurse',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'The account status',
  `createTime` datetime NOT NULL COMMENT 'The account created time',
  `loginTime` datetime NOT NULL COMMENT 'The time of user login',
  `roundRobin` int(11) DEFAULT NULL COMMENT 'roundRobin',
  PRIMARY KEY (`id`),
  KEY `IDX_TYPE` (`type`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_LOGIN_TIME` (`loginTime`),
  KEY `IDX_CREATE_TIME` (`createTime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `Staff`
--

INSERT INTO `Staff` (`id`, `userName`, `firstName`, `lastName`, `password`, `sex`, `hkid`, `birthday`, `email`, `directLine`, `address`, `mobile`, `jobTitle`, `team`, `type`, `status`, `createTime`, `loginTime`, `roundRobin`) VALUES
(1, 'patrick', 'Patrick', 'Ng', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, '0000-00-00 00:00:00', 'patrick@gmail.com', 12345678, '12345678', 12345678, 'UnusableMan', 1, 1, 1, '2016-02-04 21:57:20', '2016-03-24 19:06:55', NULL),
(2, 'admin', 'admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 0, 'D886934A', '1987-07-16 00:00:00', 'admin@admin.com', 27867560, 'Rm 19 45F Sea View Maison Causeway Bay HK', 96620499, 'Admin', 1, 1, 1, '2016-03-14 01:05:56', '2016-03-24 19:00:58', NULL),
(3, 'peterpan', 'Peter', 'PAN', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Z044672C', '1987-07-17 00:00:00', '', 27867561, 'Rm 33, 10/F, Jade Garden, Tsim Sha Tsui, Kowloon, HK', 50718405, 'Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '2016-03-24 08:18:55', NULL),
(4, 'ivylo', 'Ivy', 'LO', 'e10adc3949ba59abbe56e057f20f883e', 2, 'Z6673456', '1987-07-18 00:00:00', '', 27867562, 'Rm 02, 1/F, Rich Rich House, Tai Po, NT, HK', 94522862, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '2016-03-24 10:43:43', NULL),
(5, 'albertyip', 'Albert', 'YIP', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Z664298A', '1987-07-19 00:00:00', '', 27867563, 'Rm 202, 15/F, Blue Coast Garden, Tung Chung, NT, HK', 98664527, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '2016-03-24 10:46:18', NULL),
(6, 'carmanfan', 'Carman', 'FAN', 'e10adc3949ba59abbe56e057f20f883e', 2, 'D7781349', '1987-07-20 00:00:00', '', 27867564, 'Rm 14, 80/F, High Hill Maison, Mid Level, Central, HK', 92341038, 'Senior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(7, 'garfieldyuen', 'Garfield', 'YUEN', 'e10adc3949ba59abbe56e057f20f883e', 2, 'G123211A', '1987-07-21 00:00:00', '', 27867565, 'Rm 8A, 16/F, Good View House, Ma on San, NT, HK', 63029374, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(8, 'chrischan', 'Chris', 'CHAN', 'e10adc3949ba59abbe56e057f20f883e', 1, 'K1238795', '1987-07-22 00:00:00', '', 27867566, 'Rm 1881, 1/F, Kings Veune, North Point, HK', 61452138, 'Service Officer', 0, 3, -2, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(9, 'dennislok', 'Dennis', 'LOK', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Z3908901', '1987-07-23 00:00:00', '', 27867567, '6/F, Dargon Slope Mountin, Repluse Bay, HK', 98724688, 'Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(10, 'carloskam', 'Carlos', 'KAM', 'e10adc3949ba59abbe56e057f20f883e', 1, 'A3213213', '1987-07-24 00:00:00', '', 27867568, 'Rm 05, 13/F, Ice age maison, Sai Tin, NT, HK', 53669456, 'Senior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(11, 'mandytai', 'Mandy', 'TAI', 'e10adc3949ba59abbe56e057f20f883e', 2, 'Z3281798', '1987-07-25 00:00:00', '', 27867569, 'Rm1502, 33/F, Yi On House, Tsuen wan, NT, HK', 68798789, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(12, 'jennyma', 'Jenny', 'MA', 'e10adc3949ba59abbe56e057f20f883e', 2, 'D1239049', '1987-07-26 00:00:00', '', 27867570, 'Rm 16, 19/F, Nang Shan House, Jordan, Kowloon, HK', 53644752, 'Senior Engineer', 0, 3, -2, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(13, 'collinso', 'Collin', 'SO', 'e10adc3949ba59abbe56e057f20f883e', 1, 'A3896379', '1987-07-27 00:00:00', '', 27867571, 'Rm10, 34/F, Wah Fei Court, Aberdeen, HK', 64587921, 'Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(14, 'rickylau', 'Ricky', 'LAU', 'e10adc3949ba59abbe56e057f20f883e', 1, 'D0088181', '1987-07-28 00:00:00', '', 27867572, '31 Ma Kei Shin Rd, Kenny Town, HK', 96857432, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(15, 'leokwan', 'Leo', 'KWAN', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Z1729304', '1987-07-29 00:00:00', '', 27867573, '28 Ho Chong Rd, Yuen Long, NT, HK', 96835214, 'Senior Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(16, 'sarahwan', 'Sarah', 'WAN', 'e10adc3949ba59abbe56e057f20f883e', 2, 'G1918937', '1987-07-30 00:00:00', '', 27867574, 'Rm02, 09/F, Shek O Maison, Shek O, HK', 63112212, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(17, 'irenelui', 'Irene', 'LUI', 'e10adc3949ba59abbe56e057f20f883e', 2, 'K1329095', '1987-07-31 00:00:00', '', 27867575, 'Rm 17, Diamond Hill House, Diamond Hill, Kowloon, HK', 55352639, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(18, 'larryjohnson', 'Larry', 'Johnson', 'e10adc3949ba59abbe56e057f20f883e', 1, 'D7367391', '1987-08-01 00:00:00', '', 27867576, 'Gold Coast Maison, 268 Ching Shan Rd, NT, HK', 96455525, 'Senior Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(19, 'mariasiu', 'Maria', 'SIU', 'e10adc3949ba59abbe56e057f20f883e', 2, 'G1239103', '1987-08-02 00:00:00', '', 27867577, 'Rm 12, 1/F, Tai Chong house, Mei Foo, Kowloon, HK', 69778451, 'Senior Engineer', 0, 3, -2, '2016-03-14 01:05:57', '0000-00-00 00:00:00', NULL),
(20, 'larry', 'larry', 'tam', 'e10adc3949ba59abbe56e057f20f883e', 0, 'A1234567', '1992-11-18 00:00:00', 'larry@gmail.com', 12345678, 'Some address', 12345671, 'SeniorLarry', 1, 1, -1, '2016-03-14 02:19:17', '2016-03-15 21:02:51', NULL),
(21, 'joe', 'Joe', 'Lai', 'e10adc3949ba59abbe56e057f20f883e', 1, 'A1234567', '2016-02-29 00:00:00', 'JOELAI@gmail.com', 12345678, 'some address', 12345678, 'SeniorJoe', 1, 0, -2, '2016-03-14 19:50:51', '0000-00-00 00:00:00', NULL),
(27, 'larry1', 'larry', 'tam', 'e10adc3949ba59abbe56e057f20f883e', 1, 'A1234567', '1992-11-18 00:00:00', 'larry@gmail.com', 12345678, 'some address', 12345678, 'SeniorPat', 1, 1, -2, '2016-03-14 23:02:29', '0000-00-00 00:00:00', NULL),
(28, 'larryj', 'Larry', 'Johnson', 'e10adc3949ba59abbe56e057f20f883e', 0, 'Z1234567', '1983-10-06 00:00:00', 'larry2@jka.com', 12345678, 'rm1111', 88888888, 'SmallPotato', 2, 3, 1, '2016-03-19 13:55:21', '0000-00-00 00:00:00', NULL),
(29, 'kit', 'kit', 'lam', 'e10adc3949ba59abbe56e057f20f883e', 0, 'A1234567', '1992-11-18 17:00:00', 'kit@gmail.com', 12345678, 'shittyu', 12345678, 'Senior', 1, 1, 1, '2016-03-19 15:27:18', '2016-03-19 18:31:47', NULL),
(30, 'chris56hk', 'Cheng', 'ChunKIT', 'e10adc3949ba59abbe56e057f20f883e', 1, 'z1234567', '1988-05-01 00:00:00', 'acb@abc.com', 12345678, 'abc', 12345678, 'cccc', 1, 1, 1, '2016-03-20 20:06:04', '0000-00-00 00:00:00', NULL),
(31, 'michaeljordan', 'Michael', 'Jordan', 'e10adc3949ba59abbe56e057f20f883e', 0, 'Y1904567', '1975-01-01 00:00:00', 'mj@abc.com', 78905421, 'USA', 78905432, 'RepairTeamLeader', 4, 3, 1, '2016-03-23 20:42:39', '2016-03-23 21:18:25', NULL),
(32, 'larrytam', 'Larry', 'Tam', 'e10adc3949ba59abbe56e057f20f883e', 1, 'Y6688254', '1980-01-03 00:00:00', 'larrytam@abc.com', 12345678, 'HK', 12345678, 'ServiceCounterOffier', 2, 2, 1, '2016-03-24 19:02:18', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `StaffPermission`
--

CREATE TABLE IF NOT EXISTS `StaffPermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT 'User use which permission',
  `permissionId` int(11) NOT NULL DEFAULT '0' COMMENT 'JSON for saving serval permission',
  `last_effectiveTime` datetime NOT NULL COMMENT 'permission last effective time',
  PRIMARY KEY (`id`),
  KEY `IDX_UID` (`uid`),
  KEY `IDX_PERMISSIONID` (`permissionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `StaffPermission`
--


-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE IF NOT EXISTS `symptoms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mainCategory` int(11) NOT NULL COMMENT 'main category',
  `subCategory` varchar(30) NOT NULL COMMENT 'sub category',
  `symptoms` varchar(50) NOT NULL COMMENT 'description',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'The account status',
  PRIMARY KEY (`id`),
  KEY `IDX_mainCategory` (`mainCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `symptoms`
--

INSERT INTO `symptoms` (`id`, `mainCategory`, `subCategory`, `symptoms`, `status`) VALUES
(1, 1, '24', 'No Power', 1),
(2, 1, '24', 'Suddenly Shut Down', 1),
(3, 1, '24', 'Automatically Power on', 1),
(4, 1, '18', 'No display', 1),
(5, 1, '18', 'Display dim', 1),
(6, 1, '18', 'Display Blinking', 1),
(7, 1, '18', 'Display flickering', 1),
(8, 1, '18', 'Dead spot, White spot', 1),
(9, 1, '18', 'Bezel is loosen', 1),
(10, 1, '18', 'Display has shadow', 1),
(11, 1, '5', 'No image', 1),
(12, 1, '5', 'Black spot on LENs', 1),
(13, 1, '14', 'Hinge is loosen', 1),
(14, 1, '11', 'Fan error', 1),
(15, 1, '11', 'Fan is noisy', 1),
(16, 1, '15', 'Keys has no response', 1),
(17, 1, '15', 'Stuck Key', 1),
(18, 1, '15', 'Key is loosen', 1),
(19, 1, '27', 'Touch Pad has no response', 1),
(20, 1, '27', 'Touch Pad button has no response', 1),
(21, 1, '19', 'Button related problem', 1),
(22, 1, '19', 'Scolling wheel problem', 1),
(23, 1, '19', 'Mouse pointer has no response', 1),
(24, 1, '26', 'Speaker has no sound', 1),
(25, 1, '26', 'Strange sound or noisy', 1),
(26, 1, '12', 'Hard disk error', 1),
(27, 1, '12', 'Strange sound or noisy', 1),
(28, 1, '8', 'Unable to read DVD Disc', 1),
(29, 1, '8', 'Unable to read CD Disc', 1),
(30, 1, '9', 'Unable to read DVD Disc', 1),
(31, 1, '9', 'Unable to read CD Disc', 1),
(32, 1, '9', 'Unable to burn DVD Disc', 1),
(33, 1, '9', 'Unable to Burn CD Disc', 1),
(34, 1, '4', 'Unable to read DVD Disc', 1),
(35, 1, '4', 'Unable to read CD Disc', 1),
(36, 1, '4', 'Unable to read Blur-Ray disc', 1),
(37, 1, '4', 'Unable to burn DVD Disc', 1),
(38, 1, '4', 'Unable to burn CD Disc', 1),
(39, 1, '4', 'Unable to burn Blur-Ray disc', 1),
(40, 1, '20', 'Pin is Loosen', 1),
(41, 1, '20', 'NIC not work', 1),
(42, 1, '17', 'Pin is loosen', 1),
(43, 1, '17', 'Modem not work', 1),
(44, 1, '28', 'USB port not work', 1),
(45, 1, '28', 'USB device unrecognized', 1),
(46, 1, '28', 'USB device speed degradation', 1),
(47, 1, '6', 'Card Reader not work', 1),
(48, 1, '29', 'VGA port is loosen', 1),
(49, 1, '29', 'VGA port not work', 1),
(50, 1, '13', 'HDMI port is loosen', 1),
(51, 1, '13', 'HDMI port not work', 1),
(52, 1, '7', 'DP port is loosen', 1),
(53, 1, '7', 'DP port not work', 1),
(54, 1, '10', 'DVI port is loosen', 1),
(55, 1, '10', 'DVI port not work', 1),
(56, 1, '2', 'Audio port is loosen', 1),
(57, 1, '2', 'Audio port no signal', 1),
(58, 1, '2', 'Audio port only one side has signal', 1),
(59, 1, '16', 'Mic port not work', 1),
(60, 1, '16', 'Mic port is loosen', 1),
(61, 1, '25', 'No Power', 1),
(62, 1, '25', 'Unable to charge up battery', 1),
(63, 1, '3', 'No Power', 1),
(64, 1, '3', 'Unable to be charged up', 1),
(65, 1, '23', 'Machine water spilled', 1),
(66, 1, '23', 'Machine fire damage', 1),
(67, 1, '23', 'Monitor water spilled', 1),
(68, 1, '23', 'Monitor fire damage', 1),
(69, 1, '23', 'Monitor Broken', 1),
(70, 1, '23', 'Monitor Bezel Broken', 1),
(71, 1, '23', 'Monitor Cover broken', 1),
(72, 1, '23', 'Camera broken', 1),
(73, 1, '23', 'Hinge broken', 1),
(74, 1, '23', 'Keyboard key broken', 1),
(75, 1, '23', 'keyboard water spilled', 1),
(76, 1, '23', 'keyboard fire damage', 1),
(77, 1, '23', 'Keyboard bezel broken', 1),
(78, 1, '23', 'Touch Pad broken', 1),
(79, 1, '23', 'Touch Pad button broken', 1),
(80, 1, '23', 'Mouse broken', 1),
(81, 1, '23', 'Mouse button broken', 1),
(82, 1, '23', 'Speaker broken', 1),
(83, 1, '23', 'Hard disk broken', 1),
(84, 1, '23', 'Rom drive broken', 1),
(85, 1, '23', 'NIC port borken', 1),
(86, 1, '23', 'Modem port broken', 1),
(87, 1, '23', 'USB port broken', 1),
(88, 1, '23', 'Card Reader port broken', 1),
(89, 1, '23', 'VGA port broken', 1),
(90, 1, '23', 'HDMI port broken', 1),
(91, 1, '23', 'DP port broken', 1),
(92, 1, '23', 'DVI port broken', 1),
(93, 1, '23', 'Audio port broken', 1),
(94, 1, '23', 'Base cover broken', 1),
(95, 2, '21', 'System Recovery', 1),
(96, 2, '21', 'Driver Installation', 1),
(97, 2, '21', 'Virus Malware', 1),
(98, 2, '1', 'Application problem', 1),
(99, 2, '22', 'Bios update', 1);

-- --------------------------------------------------------

--
-- Table structure for table `SymptomsIssue`
--

CREATE TABLE IF NOT EXISTS `SymptomsIssue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incidentId` int(11) NOT NULL COMMENT 'incidentId',
  `symptomsId` int(11) NOT NULL COMMENT 'symptomsId',
  `status` int(11) NOT NULL COMMENT 'status',
  PRIMARY KEY (`id`),
  KEY `IDX_ID` (`id`),
  KEY `IDX_INCIDENTID` (`incidentId`),
  KEY `IDX_symptoms` (`symptomsId`),
  KEY `IDX_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `SymptomsIssue`
--

INSERT INTO `SymptomsIssue` (`id`, `incidentId`, `symptomsId`, `status`) VALUES
(1, 1, 54, 1),
(2, 1, 55, 1),
(3, 1, 14, 1),
(4, 1, 15, 1),
(5, 1, 26, 1),
(6, 1, 27, 1),
(7, 2, 27, 1),
(8, 2, 16, 1),
(9, 2, 40, 1),
(10, 2, 49, 1),
(11, 3, 32, 1),
(12, 3, 64, 1),
(13, 3, 45, 1),
(14, 4, 59, 1),
(15, 4, 17, 1),
(16, 4, 55, 1),
(17, 4, 15, 1),
(18, 4, 27, 1),
(19, 5, 7, 1),
(20, 5, 62, 1),
(21, 5, 53, 1),
(22, 6, 28, 1),
(23, 6, 63, 1),
(24, 6, 46, 1),
(25, 7, 26, 1),
(26, 7, 16, 1),
(27, 7, 6, 1),
(28, 7, 57, 1),
(29, 7, 61, 1),
(30, 8, 4, 1),
(31, 8, 62, 1),
(32, 8, 15, 1),
(33, 9, 13, 1),
(34, 9, 60, 1),
(35, 9, 59, 1),
(36, 10, 26, 1),
(37, 10, 27, 1),
(38, 10, 14, 1),
(39, 10, 15, 1),
(40, 11, 26, 1),
(41, 11, 27, 1),
(42, 11, 14, 1),
(43, 11, 15, 1),
(44, 12, 54, 1),
(45, 12, 27, 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'team auto increment id ',
  `name` varchar(20) NOT NULL COMMENT 'team name',
  `description` text NOT NULL COMMENT 'describe the team details',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'team status',
  `createTime` datetime NOT NULL COMMENT 'Which time of role create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this role',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `description`, `status`, `createTime`, `createBy`) VALUES
(1, 'admin', 'allow to edit all pages', 1, '2016-03-14 19:34:46', 1),
(2, 'helpdeck', 'helpdeck', 1, '2016-03-14 19:34:46', 1),
(3, 'Express', 'express', 1, '2016-03-14 19:34:46', 1),
(4, 'Standard', 'standard', 1, '2016-03-14 19:34:46', 1),
(5, 'Percall', 'percall', 1, '2016-03-14 19:34:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Warranty`
--

CREATE TABLE IF NOT EXISTS `Warranty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serialNo` varchar(50) NOT NULL COMMENT 'serial No',
  `deviceType` int(11) NOT NULL COMMENT 'device type',
  `modelNo` varchar(20) NOT NULL COMMENT 'model no',
  `warrantyStart` datetime NOT NULL COMMENT 'email',
  `warrantyPeriod` datetime NOT NULL COMMENT 'user address',
  `warrantyType` int(11) NOT NULL COMMENT 'warrant type',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'The account status',
  PRIMARY KEY (`id`),
  KEY `IDX_deviceType` (`deviceType`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_warrantyType` (`warrantyType`),
  KEY `IDX_warrantyPeriod` (`warrantyPeriod`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Warranty`
--

INSERT INTO `Warranty` (`id`, `serialNo`, `deviceType`, `modelNo`, `warrantyStart`, `warrantyPeriod`, `warrantyType`, `status`) VALUES
(1, 'CN1234ABC', 1, 'Y300', '2013-03-16 00:00:00', '2016-03-16 00:00:00', 1, 0),
(2, 'CB88642BH', 2, 'N100', '2013-09-02 00:00:00', '2016-09-02 00:00:00', 2, 0),
(3, 'JK9786FBX', 3, 'V6000', '2012-12-18 00:00:00', '2013-12-18 00:00:00', 1, 0),
(4, 'SF6672FEQ', 1, 'H310', '2014-05-20 00:00:00', '2015-05-20 00:00:00', 2, 0),
(5, 'AE9902TYP', 2, 'N200', '2014-06-28 00:00:00', '2016-06-28 00:00:00', 1, 0),
(6, 'MV6432QAS', 3, 'X1000', '2015-01-21 00:00:00', '2016-01-21 00:00:00', 2, 0);
