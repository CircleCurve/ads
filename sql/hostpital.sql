create database hostpital DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE hostpital ; 
delimiter ; 
CREATE TABLE IF NOT EXISTS `Staff` (
`id` int(11) NOT NULL auto_increment,
`userName` varchar(20) NOT NULL COMMENT 'login userName' , 
`firstName` varchar(50) NOT NULL COMMENT 'staff firstName',
`lastName` varchar(50) NOT NULL COMMENT 'staff lastName',
`password` varchar (32) NOT NULL COMMENT 'password is one way encrypted cannot be decrypted' , 
`type` INT(11) NOT NULL DEFAULT '0' COMMENT 'staff type maybe patient, doctor,nurse', 
`status` INT(11) NOT NULL DEFAULT '0' COMMENT 'The account status', 
`createTime` datetime NOT NULL  COMMENT 'The account created time' , 
`loginTime` datetime NOT NULL COMMENT 'The time of user login',
PRIMARY KEY  (`id`), 
KEY `IDX_TYPE` (`type`),
KEY `IDX_STATUS` (`status`), 
KEY `IDX_LOGIN_TIME` (`loginTime`), 
KEY `IDX_CREATE_TIME` (`createTime`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `StaffPermission` (
`id` int(11) NOT NULL auto_increment,
`uid` int(11) NOT NULL COMMENT 'User use which permission' , 
`permissionId` INT(11) NOT NULL DEFAULT '0' COMMENT 'JSON for saving serval permission', 
`last_effectiveTime` datetime NOT NULL  COMMENT 'permission last effective time' , 
PRIMARY KEY  (`id`), 
KEY `IDX_UID` (`uid`),
KEY `IDX_PERMISSIONID` (`PERMISSIONID`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `permission` (
`id` int(11) NOT NULL auto_increment,
`description` TEXT NOT NULL COMMENT 'describe usage of each user permission' , 
`status` INT(11) NOT NULL DEFAULT '0' COMMENT 'Is it effective', 
`createTime` datetime NOT NULL  COMMENT 'Which time of permission create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this permission',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL auto_increment,
`name` varchar(20) NOT NULL 'describe the role name invoke in this systme', 
`description` TEXT NOT NULL COMMENT 'describe the role details' , 
`status` INT(11) NOT NULL DEFAULT '1' COMMENT 'role effective', 
`createTime` datetime NOT NULL  COMMENT 'Which time of role create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this role',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL COMMENT 'staff firstName',
  `lastName` varchar(50) NOT NULL COMMENT 'staff lastName',
  `hkid` varchar(11) NOT NULL COMMENT 'HKID CARD NUMBERS' , 
  `dateOfBirth` datetime NOT NULL COMMENT 'date of birth' ,
  `address` varchar (100) NOT NULL COMMENT 'patient address' , 
  `phone` int(11) NOT NULL COMMENT 'phone number',
  `emContactPerson` varchar (50) NOT NULL COMMENT 'contact person if cannot contact patient', 
  `emContactPhone` int(11) NOT NULL COMMENT 'contact person phone number if cannot contact patient',
  `status` INT(11) NOT NULL COMMENT 'record enablement', 
  `in_time` datetime NOT NULL COMMENT 'The time of patient come in hostpital',
  `out_time` datetime NOT NULL COMMENT 'The time of patient leave hostpital',
  PRIMARY KEY (`id`),
  KEY `IDX_IN_TIME` (`in_time`),
  KEY `IDX_OUT_TIME` (`out_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Raymond','YAN', 'D886934A', '1987-07-16', 'Rm 19, 45/F, Sea View Maison, Causeway Bay, HK', '96620499','Karen YAN','96650489','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Victor','YIP', 'Z044672C', '1985-04-02', 'Rm 33, 10/F, Jade Garden, Tsim Sha Tsui, Kowloon, HK', '50718405','Fed TING','96435296','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'John','MA', 'Z6673456', '1977-03-20', 'Rm 02, 1/F, Rich Rich House, Tai Po, NT, HK', '94522862','Elaine KOO','94282670','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Kimmy','LAU', 'Z664298A', '1991-02-14', 'Rm 202, 15/F, Blue Coast Garden, Tung Chung, NT, HK', '98664527','Jimmy LAU','94565321','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Ken','CHAN', 'D7781349', '1990-06-12', 'Rm 14, 80/F, High Hill Maison, Mid Level, Central, HK', '92341038','Ray CHAN','69874123','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'David','YEUNG', 'G123211A', '1967-08-05', 'Rm 8A, 16/F, Good View House, Ma on San, NT, HK', '63029374','Jay YEUNG','63989752','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Denny','HUI', 'K1238795', '1979-05-29', 'Rm 1881, 1/F, King Veune, North Point, HK', '61452138','Lemon WU','93366455','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Fiona','NG', 'Z3908901', '1992-11-18', '6/F, Dargon Slope Mountin, Repluse Bay, HK', '98724688','Cindy NG','53996987','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Candy','MOK', 'A3213213', '1986-12-01', 'Rm 05, 13/F, Ice age maison, Sai Tin, NT, HK', '53669456','Joe IP','69695686','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Winnie','YIU', 'Z3281798', '1992-06-13', 'Rm1502, 33/F, Yi On House, Tsuen wan, NT, HK', '68798789','Sam LUI','53214587','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Sammy','LO', 'D1239049', '1989-11-30', 'Rm 16, 19/F, Nang Shan House, Jordan, Kowloon, HK', '53644752','Dan Ryan','53241758','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Stella','CHEUNG', 'A3896379', '1990-07-15', 'Rm10, 34/F, Wah Fei Court, Aberdeen, HK', '64587921','Wick LAM','91814735','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Rachel','GI', 'D0088181', '1994-05-13', '31 Ma Kei Shin Rd, Kenny Town, HK', '96857432','Rose GI','65872452','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'King','MA', 'Z1729304', '1993-03-15', '28 Ho Chong Rd, Yuen Long, NT, HK', '96835214','Ben LAM','52364123','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Thomas','LAM', 'G1918937', '1986-09-17', 'Rm02, 09/F, Shek O Maison, Shek O, HK', '63112212','Wing LAM','96896832','1');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Lion','KING', 'K1329095', '1988-04-21', 'Rm 17, Diamond Hill House, Diamond Hill, Kowloon, HK', '55352639','Harris KING','96852125','0');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Moon','LAM', 'D7367391', '1989-04-14', 'Gold Coast Maison, 268 Ching Shan Rd, NT, HK', '96455525','Bunny LAM','54636964','0');
INSERT INTO `hostpital`.`patient` ( `firstName`, `lastName`, `hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`status`) VALUES ( 'Joe','CHU', 'G1239103', '1989-06-04', 'Rm 12, 1/F, Tai Chong house, Mei Foo, Kowloon, HK', '69778451','Don LI','68279869','0');



CREATE TABLE IF NOT EXISTS `healthStatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'healthStatus id ',
  `name` varchar(30) NOT NULL COMMENT 'healthStatus name',
  `status` varchar(50) NOT NULL COMMENT 'Active or deactive for soft deleteion ',
  `description` TEXT NOT NULL COMMENT 'describe each health status' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `hostpital`.`healthStatus` ( `name`, `status`, `description`) VALUES ( 'Undetermined','1', 'Patient is awaiting assessment');
INSERT INTO `hostpital`.`healthStatus` ( `name`, `status`, `description`) VALUES ( 'Good','1', 'Vital signs are stable and within normal limits. Patient is conscious and comfortable. Indicators are excellent.');
INSERT INTO `hostpital`.`healthStatus` ( `name`, `status`, `description`) VALUES ( 'Fair','1', 'Vital signs are stable and within normal limits. Patient is conscious but may be uncomfortable. Indicators are favorable.');
INSERT INTO `hostpital`.`healthStatus` ( `name`, `status`, `description`) VALUES ( 'Serious','1', 'Vital signs may be unstable and not within normal limits. Patient is acutely ill. Indicators are questionable.');
INSERT INTO `hostpital`.`healthStatus` ( `name`, `status`, `description`) VALUES ( 'Critical','1', 'Vital signs are unstable and not within normal limits. Patient may be unconscious. Indicators are not favorable.');


CREATE TABLE IF NOT EXISTS `disease` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'disease id ',
  `name` varchar(30) NOT NULL COMMENT 'disease name',
  `status` varchar(50) NOT NULL COMMENT 'Active or deactive for soft deleteion ',
  `description` TEXT NOT NULL COMMENT 'describe disease' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Cancer','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Autoimmune conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Diabetes','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Genetic conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Chromosome conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Viral infections','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Bacterial infections','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Parasitic Infections','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Worm conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Physical conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Prion diseases','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Nutritional deficiencies','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Vitamin deficiencies','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Mineral deficiencies','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Mitochondrial diseases','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Accidents','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Sexually Transmitted Diseases','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Pregnancy Conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Breastfeeding Conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Birth defects','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Male conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Female conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Infant conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Childhood conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Adolescent conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Immune disorders','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Balance disorders','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Pain','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Systemic disorders','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Blood conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Blood vessel conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Nerve conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Muscle conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Heart conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Back conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Neck conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Spinal disorders','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Eye conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Brain conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Mental conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Nose conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Mouth conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Dental conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Foot conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Leg conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Knee conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Elbow conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Finger conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Wrist conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Shoulder conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Ear conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Lung conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Liver conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Kidney conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Gall bladder conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Pancreas conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Digestive conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Prostate conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Male genital conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Obstetrical conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Gynaecological conditions','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Thyroid disorders','1');
INSERT INTO `hostpital`.`disease` ( `name`, `status`) VALUES ( 'Hearing disorders','1');


CREATE TABLE IF NOT EXISTS `drug` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'disease id ',
  `name` varchar(30) NOT NULL COMMENT 'disease name',
  `price` INT(11) NOT NULL COMMENT 'drug price for each',
  `qty` int(11) NOT NULL COMMENT 'drug qty for each', 
  `status` varchar(50) NOT NULL COMMENT 'Active or deactive for soft deleteion ',
  `description` TEXT NOT NULL COMMENT 'describe drug' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Hycamtin (topotecan)','210', '654', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Zanosar (streptozocin)','560', '718', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Docefrez (docetaxel)','400', '756', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Adriamycin (doxorubicin)','560', '513', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Rheumatrex (methotrexate)','560', '271', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Avastin (bevacizumab)','210', '961', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Mustargen (mechlorethamine)','560', '139', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Platinol (cisplatin)','560', '446', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Paraplatin (carboplatin)','400', '720', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Cytoxan (cytoxan)','400', '531', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Abitrexate (methotrexate injection)','400', '205', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Platinol (cisplatin injection)','400', '491', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Abraxane (paclitaxel injection)','560', '810', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Oncovin (vincristine)','210', '664', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Velban (vinblastine)','210', '344', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Iressa (gefitinib)','210', '168', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Velban (vinblastine)','210', '795', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Xalkori (crizotinib)','400', '268', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Navelbine (vinorelbine)','400', '550', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Tarceva (erlotinib)','400', '749', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Gemzar (gemcitabine)','560', '757', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Alimta (pemetrexed)','210', '676', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Gilotrif (afatinib)','210', '356', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Zykadia (ceritinib)','210', '260', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Matulane (procarbazine)','210', '173', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Hycamtin (topotecan)','210', '421', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Cytoxan Injection (cyclophosphamide injection)','400', '721', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Etopophos (etoposide injection)','560', '311', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Cytoxan (cytoxan)','560', '872', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Oncovin (vincristine)','210', '806', '1');
INSERT INTO `hostpital`.`drug` ( `name`, `price`,`qty`,`status`) VALUES ( 'Vepesid (etoposide)','210', '454', '1');


CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'disease id ',
  `name` varchar(30) NOT NULL COMMENT 'disease name',
  `type` varchar(50) NOT NULL COMMENT 'service type foriegn key to service type', 
  `price` INT(11) NOT NULL COMMENT 'drug price for each',
  `status` varchar(50) NOT NULL COMMENT 'Active or deactive for soft deleteion ',
  `description` TEXT NOT NULL COMMENT 'describe drug' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Head','General X-ray Examination', '630', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Thorax','General X-ray Examination', '210', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Abdomen','General X-ray Examination', '210', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Spine','General X-ray Examination', '420', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Extremities','General X-ray Examination', '420', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Done Age (Each)','General X-ray Examination', '210', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Skeletal Survey','General X-ray Examination', '2940', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Renal Function Test','PATHOLOGY', '580', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Liver Function Test','PATHOLOGY', '580', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Lipid Profile','PATHOLOGY', '825', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Arthritis Profile','PATHOLOGY', '685', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Cardiac Profile','PATHOLOGY', '1005', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Antenatal Screening+HIV Profile','PATHOLOGY', '1775', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Fertility Profile','PATHOLOGY', '1340', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Coronary Risk Profile','PATHOLOGY', '1235', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Thyroid Function Profile','PATHOLOGY', '910', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Immunoglobulin Profile','PATHOLOGY', '1175', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Coagulation Profile','PATHOLOGY', '635', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Pre-operation Package','PATHOLOGY', '320', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Rapid Test for Flu A & B','PATHOLOGY', '470', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Respiratory Virus Panel','PATHOLOGY', '1595', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Gastroenteritis Package','PATHOLOGY', '1465', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Diabetes Profile','PATHOLOGY', '590', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Inhalant Allergens (Infant)','PATHOLOGY', '850', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Food Allergens (Infant)','PATHOLOGY', '1215', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'STD Package','PATHOLOGY', '2525', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Complete Blood Picture','PATHOLOGY', '170', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'VDRL','PATHOLOGY', '155', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Tumor Marker (Female)','PATHOLOGY', '5315', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Tumor Marker (Male)','PATHOLOGY', '4150', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'ABO Group & Rh(D) Type','PATHOLOGY', '200', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Glucose','PATHOLOGY', '150', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Culture & Sensitivity Test','PATHOLOGY', '420', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Urine Routine','PATHOLOGY', '145', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Stool Routine','PATHOLOGY', '140', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Helicobacter pylori breath test','PATHOLOGY', '850', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Abdomen','ULTRASOUND SCANNING', '3000', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Bladder','ULTRASOUND SCANNING', '920', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Brain (neonatal)','ULTRASOUND SCANNING', '1430', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Breasts','ULTRASOUND SCANNING', '950', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Carotid Doppler (bilateral)','ULTRASOUND SCANNING', '3000', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Doppler (each vein)','ULTRASOUND SCANNING', '1280', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Bones or Limbs','ULTRASOUND SCANNING', '1210', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Kidneys','ULTRASOUND SCANNING', '920', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Liver & GB','ULTRASOUND SCANNING', '920', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Pelvis/Obs + T.V.S.','ULTRASOUND SCANNING', '900', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Prostate','ULTRASOUND SCANNING', '1200', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'COLONOSCOPY','Endoscopy', '21020', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'GASTROSCOPY','Endoscopy', '22680', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'CYSTPSCOPY','Endoscopy', '22760', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'BRONCHOSCOPY','Endoscopy', '26200', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'ERCP + BASIC MATERIALS','Endoscopy', '83710', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Circumcision (G.A.)','General Surgery', '19060', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Excision of Sebaceous Cyst (G.A.)','General Surgery', '24010', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Haemorrhoidectomy / H.D.S.','General Surgery', '38960', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Excision of Sebaceous Cyst (L.A.)','General Surgery', '17730', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Hernia','General Surgery', '32800', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Breast Biopsy (G.A.)','General Surgery', '29430', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Thyroidectomy','General Surgery', '55020', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Excision of Lipoma (G.A.)','General Surgery', '23590', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Abscess / I & D (G.A.)','General Surgery', '29670', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Total Thyroidectomy','General Surgery', '78200', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Excision of Tumour (G.A.)','General Surgery', '29330', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Fistual in Anal / Fia / Fistulectomy','General Surgery', '37010', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'General Physiotherapy Consultation with Treatment','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Cardiopulmonary Physiotherapy Consultation with Treatment','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Musculoskeletal Physiotherapy Consultation with Treatment','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Functional/ADL Physiotherapy Consultation and Training','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Sports and Fitness Physiotherapy Consultation and Training','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Balance and Coordination Evaluation and Training','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Shock Wave Evaluation and Treatment','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Gait Evaluation and Training','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Incontinence Consultation and Treatment','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Neurological/ Stroke Physiotherapy Consultation with Treatment','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Torticollis Physiotherapy Consultation with Treatment','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Lymphedema Management','Physiotherapy', '840', '1');
INSERT INTO `hostpital`.`service` ( `name`, `type`,`price`,`status`) VALUES ( 'Therapeutic Exercise Prescription','Physiotherapy', '840', '1');

CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'disease id ',
  `type` varchar(30) NOT NULL COMMENT 'service type foriegn key to service type', 
  `name` varchar(30) NOT NULL COMMENT 'disease name',
  `location` varchar(30) NOT NULL COMMENT 'room floor', 
  `status` varchar(50) NOT NULL COMMENT 'Active or deactive for soft deleteion ',
  `description` TEXT NOT NULL COMMENT 'describe room' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Premium Suite', '5/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Premium Suite', '5/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Private Room', '3/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Private Room', '3/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Private Room', '3/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Private Room', '3/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Stardard Room', '2/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Stardard Room', '2/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Stardard Room', '2/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Stardard Room', '2/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Paediatric Ward', '1/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Paediatric Ward', '1/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Paediatric Ward', '1/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Patient Room','Paediatric Ward', '1/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Operation Treatre','Operation Treatre 1', '6/F', '1');
INSERT INTO `hostpital`.`room` ( `type`,`name`,`location`,`status`) VALUES ( 'Operation Treatre','Operation Treatre 2', '6/F', '1');


CREATE TABLE IF NOT EXISTS `bed` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'bed id ',
  `rood_id` INT(11) NOT NULL COMMENT 'foriegn key room_id', 
  `name` varchar(30) NOT NULL COMMENT 'bed name',
  `type` varchar(30) NOT NULL COMMENT 'service type foriegn key to service type', 
  `price` INT(11) NOT NULL COMMENT 'price for each bed',
  `status` varchar(50) NOT NULL COMMENT 'Active or deactive for soft deleteion ',
  `description` TEXT NOT NULL COMMENT 'describe bed' ,
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_PRICE` (`price`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;




INSERT INTO `hostpital`.`bed` ( `name`,`type`,`price`,`status`) VALUES ( 'Premium Suite','Patient Bed', '10800', '1');
INSERT INTO `hostpital`.`bed` ( `name`,`type`,`price`,`status`) VALUES ( 'Private Bed','Patient Bed', '1900', '1');
INSERT INTO `hostpital`.`bed` ( `name`,`type`,`price`,`status`) VALUES ( 'Stardard Bed','Patient Bed', '550', '1');
INSERT INTO `hostpital`.`bed` ( `name`,`type`,`price`,`status`) VALUES ( 'Paediatric Bed','Patient Bed', '1600', '1');
INSERT INTO `hostpital`.`bed` ( `name`,`type`,`price`,`status`) VALUES ( 'Operation Bed 1','Operation Bed', '3000', '1');
INSERT INTO `hostpital`.`bed` ( `name`,`type`,`price`,`status`) VALUES ( 'Operation Bed 2','Operation Bed', '3000', '1');




