create database PPSP DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE PPSP ; 
delimiter ; 
CREATE TABLE IF NOT EXISTS `Staff` (
`id` int(11) NOT NULL auto_increment,
`userName` varchar(20) NOT NULL COMMENT 'login userName' , 
`firstName` varchar(50) NOT NULL COMMENT 'staff firstName',
`lastName` varchar(50) NOT NULL COMMENT 'staff lastName',
`password` varchar (32) NOT NULL COMMENT 'password is one way encrypted cannot be decrypted' , 
`sex` int(1) NULL COMMENT 'sex', 
`hkid` varchar(8) COMMENT 'hkid', 
`birthday` datetime NOT NULL COMMENT 'birthday', 
`email` varchar(50) NOT NULL COMMENT 'email',  
`directLine` int(20) NOT NULL COMMENT 'direct line phone number' , 
`address` TEXT NULL COMMENT 'user address' , 
`mobile` int(8) NOT NULL COMMENT 'user mobile number' , 
`jobTitle` varchar(50) NOT NULL COMMENT 'job title' , 
`team` int(11) NOT NULL COMMENT 'Service team' , 
`type` INT(11) NOT NULL DEFAULT '0' COMMENT 'staff type maybe patient, doctor,nurse', 
`status` INT(11) NOT NULL DEFAULT '0' COMMENT 'The account status', 
`createTime` datetime NOT NULL  COMMENT 'The account created time' , 
`loginTime` datetime NOT NULL COMMENT 'The time of user login',
PRIMARY KEY  (`id`), 
KEY `IDX_TYPE` (`type`),
KEY `IDX_STATUS` (`status`), 
KEY `IDX_LOGIN_TIME` (`loginTime`), 
KEY `IDX_CREATE_TIME` (`createTime`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL auto_increment,
`name` varchar(20) NOT NULL COMMENT 'describe the role name invoke in this systme', 
`description` TEXT NOT NULL COMMENT 'describe the role details' , 
`status` INT(11) NOT NULL DEFAULT '1' COMMENT 'role effective', 
`createTime` datetime NOT NULL  COMMENT 'Which time of role create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this role',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `team` (
`id` int(11) NOT NULL auto_increment COMMENT 'team auto increment id ',
`name` varchar(20) NOT NULL COMMENT 'team name' ,
`description` TEXT NOT NULL COMMENT 'describe the team details' , 
`status` INT(11) NOT NULL DEFAULT '1' COMMENT 'team status', 
`createTime` datetime NOT NULL  COMMENT 'Which time of role create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this role',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; 

CREATE TABLE IF NOT EXISTS `Incident` (
`id` int(11) NOT NULL auto_increment,
`firstName` varchar(50) NOT NULL COMMENT 'staff firstName',
`lastName` varchar(50) NOT NULL COMMENT 'staff lastName',
`sex` int(1) NULL COMMENT 'sex', 
`email` varchar(50) NOT NULL COMMENT 'email',  
`address` TEXT NULL COMMENT 'user address' , 
`mobile` int(8) NOT NULL COMMENT 'user mobile number' , 
`serialNo` varchar(30) NOT NULL COMMENT 'serial No' , 
`serviceType` INT(11) NOT NULL DEFAULT '0' COMMENT 'service Type', 
`description` TEXT NULL COMMENT 'description', 
`status` INT(11) NOT NULL DEFAULT '0' COMMENT 'status', 
`logTime` datetime NOT NULL COMMENT 'Log time',
`createBy` INT(11) NOT NULL comment 'create incident person',
PRIMARY KEY  (`id`), 
KEY `IDX_MOBILE` (`mobile`),
KEY `IDX_type` (`serviceType`), 
KEY `IDX_status` (`status`), 
KEY `IDX_logTime` (`logTime`) 
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 

CREATE TABLE IF NOT EXISTS `SymptomsIssue` (
`id` int(11) NOT NULL auto_increment,
`incidentId` int(11) NOT NULL COMMENT 'incidentId',
`symptomsId` int(11) NOT NULL COMMENT 'symptomsId',
`status` int(11) NOT NULL COMMENT 'status', 
PRIMARY KEY  (`id`), 
KEY `IDX_ID` (`id`),
KEY `IDX_INCIDENTID` (`incidentId`), 
KEY `IDX_symptoms` (`symptomsId`), 
KEY `IDX_status` (`status`) 
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 

CREATE TABLE IF NOT EXISTS `Warranty` (
`id` int(11) NOT NULL auto_increment,
`serialNo` varchar(50) NOT NULL COMMENT 'serial No',
`deviceType` int(11) NOT NULL COMMENT 'device type',
`modelNo` varchar(20) NOT NULL COMMENT 'model no', 
`warrantyStart` datetime NOT NULL COMMENT 'email',  
`warrantyPeriod` int(11) NOT NULL  COMMENT 'user address' , 
`warrantyType` int(11) NOT NULL COMMENT 'warrant type' , 
`status` INT(11) NOT NULL DEFAULT '0' COMMENT 'The account status'
PRIMARY KEY  (`id`), 
KEY `IDX_TYPE` (`type`),
KEY `IDX_deviceType` (`deviceType`) , 
KEY `IDX_STATUS` (`status`), 
KEY `IDX_warrantyType` (`warrantyType`), 
KEY `IDX_warrantyPeriod` (`warrantyPeriod`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `symptoms` (
`id` int(11) NOT NULL auto_increment,
`mainCategory` int(11) NOT NULL COMMENT 'main category',
`subCategory` varchar(30) NOT NULL COMMENT 'sub category',
`symptoms` varchar(50) NOT NULL COMMENT 'description', 
`status` INT(11) NOT NULL DEFAULT '0' COMMENT 'The account status', 
PRIMARY KEY  (`id`), 
KEY `IDX_mainCategory` (`mainCategory`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `team` (
`id` int(11) NOT NULL auto_increment COMMENT 'team auto increment id ',
`name` varchar(20) NOT NULL COMMENT 'team name' ,
`description` TEXT NOT NULL COMMENT 'describe the team details' , 
`status` INT(11) NOT NULL DEFAULT '1' COMMENT 'team status', 
`createTime` datetime NOT NULL  COMMENT 'Which time of role create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this role',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; 

CREATE TABLE IF NOT EXISTS `device` (
`id` int(11) NOT NULL auto_increment,
`name` varchar(20) NOT NULL COMMENT 'describe the device name ', 
`description` TEXT NOT NULL COMMENT 'describe the  details' , 
`status` INT(11) NOT NULL DEFAULT '1' COMMENT ' effective', 
`createTime` datetime NOT NULL  COMMENT 'Which time of role create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this role',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*

CREATE TABLE IF NOT EXISTS `device` (
`id` int(11) NOT NULL auto_increment,
`name` varchar(20) NOT NULL COMMENT 'describe the device name ', 
`description` TEXT NOT NULL COMMENT 'describe the  details' , 
`status` INT(11) NOT NULL DEFAULT '1' COMMENT ' effective', 
`createTime` datetime NOT NULL  COMMENT 'Which time of role create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this role',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `service` (
`id` int(11) NOT NULL auto_increment,
`name` varchar(20) NOT NULL COMMENT 'describe the device name ', 
`description` TEXT NOT NULL COMMENT 'describe the  details' , 
`status` INT(11) NOT NULL DEFAULT '1' COMMENT ' effective', 
`createTime` datetime NOT NULL  COMMENT 'Which time of role create' , 
`createBy` int(11) NOT NULL COMMENT 'who create the this role',
PRIMARY KEY  (`id`), 
KEY `IDX_STATUS` (`status`),
KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=Innodb  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

*/
`createBy` INT(11) NOT NULL comment 'create incident person',


ALTER TABLE Staff
ADD COLUMN `roundRobin` INT(11) COMMENT 'roundRobin'  AFTER `roundrobin` ;


ALTER TABLE Staff
ADD COLUMN `roundRobin` INT(11) COMMENT 'roundRobin'  AFTER `roundrobin` ;

ALTER TABLE Incident
ADD COLUMN `createBy` INT(11) COMMENT 'createBy'  AFTER `loginTime` ;

ALTER TABLE Incident
ADD COLUMN `completed_time` datetime COMMENT 'completed time'  AFTER `loginTime` ;

ALTER TABLE Incident
ADD COLUMN `comment` INT(11) COMMENT 'coment'  AFTER `loginTime` ;

ALTER TABLE Incident
ADD COLUMN `expected_finish` datetime COMMENT 'expected finish'  AFTER `loginTime` ;




ALTER TABLE Staff 
ADD COLUMN `hkid` varchar(8) COMMENT 'hkid'  AFTER `sex`  ,
ADD COLUMN `birthday` datetime NOT NULL COMMENT 'birthday',  AFTER `hkid`  ,
ADD COLUMN `email` varchar(50) NOT NULL COMMENT 'email'  AFTER `birthday`  ,
ADD COLUMN `directLine` int(20) NOT NULL COMMENT 'direct line phone number'  AFTER `email`  ,
ADD COLUMN `address` TEXT NULL COMMENT 'user address'  AFTER `directLine`  ,
ADD COLUMN `mobile` int(8) NOT NULL COMMENT 'user mobile number'  AFTER `address`  ,
ADD COLUMN `jobTitle` varchar(50) NOT NULL COMMENT 'job title'  AFTER `mobile`  ,
ADD COLUMN `team` int(11) NOT NULL COMMENT 'Service team'   AFTER `jobTitle`  ; 



#Staff db
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','admin','admin','D886934A','1987-07-16 00:00:00','Rm 19, 45/F, Sea View Maison, Causeway Bay, HK','27867560','96620499','Admin', 'admin', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Peter','PAN','Z044672C','1987-07-17 00:00:00','Rm 33, 10/F, Jade Garden, Tsim Sha Tsui, Kowloon, HK','27867561','50718405','Service Officer', 'peterpan', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Ivy','LO','Z6673456','1987-07-18 00:00:00','Rm 02, 1/F, Rich Rich House, Tai Po, NT, HK','27867562','94522862','Junior Engineer', 'ivylo', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Albert','YIP','Z664298A','1987-07-19 00:00:00','Rm 202, 15/F, Blue Coast Garden, Tung Chung, NT, HK','27867563','98664527','Junior Engineer', 'albertyip', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Carman','FAN','D7781349','1987-07-20 00:00:00','Rm 14, 80/F, High Hill Maison, Mid Level, Central, HK','27867564','92341038','Senior Engineer', 'carmanfan', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Garfield','YUEN','G123211A','1987-07-21 00:00:00','Rm 8A, 16/F, Good View House, Ma on San, NT, HK','27867565','63029374','Junior Engineer', 'garfieldyuen', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Chris','CHAN','K1238795','1987-07-22 00:00:00','Rm 1881, 1/F, Kings Veune, North Point, HK','27867566','61452138','Service Officer', 'chrischan', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Dennis','LOK','Z3908901','1987-07-23 00:00:00','6/F, Dargon Slope Mountin, Repluse Bay, HK','27867567','98724688','Service Officer', 'dennislok', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Carlos','KAM','A3213213','1987-07-24 00:00:00','Rm 05, 13/F, Ice age maison, Sai Tin, NT, HK','27867568','53669456','Senior Engineer', 'carloskam', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Mandy','TAI','Z3281798','1987-07-25 00:00:00','Rm1502, 33/F, Yi On House, Tsuen wan, NT, HK','27867569','68798789','Junior Engineer', 'mandytai', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Jenny','MA','D1239049','1987-07-26 00:00:00','Rm 16, 19/F, Nang Shan House, Jordan, Kowloon, HK','27867570','53644752','Senior Engineer', 'jennyma', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Collin','SO','A3896379','1987-07-27 00:00:00','Rm10, 34/F, Wah Fei Court, Aberdeen, HK','27867571','64587921','Service Officer', 'collinso', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Ricky','LAU','D0088181','1987-07-28 00:00:00','31 Ma Kei Shin Rd, Kenny Town, HK','27867572','96857432','Junior Engineer', 'rickylau', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Leo','KWAN','Z1729304','1987-07-29 00:00:00','28 Ho Chong Rd, Yuen Long, NT, HK','27867573','96835214','Senior Service Officer', 'leokwan', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Sarah','WAN','G1918937','1987-07-30 00:00:00','Rm02, 09/F, Shek O Maison, Shek O, HK','27867574','63112212','Junior Engineer', 'sarahwan', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Irene','LUI','K1329095','1987-07-31 00:00:00','Rm 17, Diamond Hill House, Diamond Hill, Kowloon, HK','27867575','55352639','Junior Engineer', 'irenelui', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('1','Larry','Johnson','D7367391','1987-08-01 00:00:00','Gold Coast Maison, 268 Ching Shan Rd, NT, HK','27867576','96455525','Senior Service Officer', 'larryjohnson', '1',NOW());
INSERT INTO Staff (`sex`,  `firstName`,`lastName`,`hkid`,`birthday`,`address`,`directLine`,`mobile`,`jobTitle`,`userName`,`status`,`createTime`) VALUES ('2','Maria','SIU','G1239103','1987-08-02 00:00:00','Rm 12, 1/F, Tai Chong house, Mei Foo, Kowloon, HK','27867577','69778451','Senior Engineer', 'mariasiu', '1',NOW());


#Role DB
INSERT INTO role (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('admin' , 'allow to edit all pages', '1' , NOW(), '1') ; 
INSERT INTO role (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('SERVICE DECK' , 'allow to watch report', '1' , NOW(), '1') ; 
INSERT INTO role (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('ENGINEER' , 'allow to edit warrant and incident', '1' , NOW(), '1') ; 

#Team DB
INSERT INTO team (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('admin' , 'allow to edit all pages', '1' , NOW(), '1') ; 
INSERT INTO team (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('helpdeck' , 'helpdeck', '1' , NOW(), '1') ; 
INSERT INTO team (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Express' , 'express', '1' , NOW(), '1') ; 
INSERT INTO team (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Standard' , 'standard', '1' , NOW(), '1') ; 
INSERT INTO team (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Percall' , 'percall', '1' , NOW(), '1') ; 

#For WarrantDB


INSERT INTO Warranty ('serialNo','deviceType' , 'modelNo', 'warrantyStart', 'warrantyPeriod', 'warrantyType') VALUES ('CN1234ABC' , '1' , 'Y300' , '2013-03-16 00:00:00' , '3' , '1');
INSERT INTO Warranty ('serialNo','deviceType' , 'modelNo', 'warrantyStart', 'warrantyPeriod', 'warrantyType') VALUES ('CB88642BH' , '2' , 'N100' , '2013-09-02 00:00:00' , '3' , '2');
INSERT INTO Warranty ('serialNo','deviceType' , 'modelNo', 'warrantyStart', 'warrantyPeriod', 'warrantyType') VALUES ('JK9786FBX' , '3' , 'V6000' , '2012-12-18 00:00:00' , '1' , '1');
INSERT INTO Warranty ('serialNo','deviceType' , 'modelNo', 'warrantyStart', 'warrantyPeriod', 'warrantyType') VALUES ('SF6672FEQ' , '1' , 'H310' , '2014-05-20 00:00:00' , '1' , '2');
INSERT INTO Warranty ('serialNo','deviceType' , 'modelNo', 'warrantyStart', 'warrantyPeriod', 'warrantyType') VALUES ('AE9902TYP' , '2' , 'N200' , '2014-06-28 00:00:00' , '2' , '1');
INSERT INTO Warranty ('serialNo','deviceType' , 'modelNo', 'warrantyStart', 'warrantyPeriod', 'warrantyType') VALUES ('MV6432QAS' , '3' , 'X1000' , '2015-01-21 00:00:00' , '1' , '2');


INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Power', 'No Power');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Power', 'Suddenly Shut Down');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Power', 'Automatically Power on');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Monitor', 'No display');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Monitor', 'Display dim');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Monitor', 'Display Blinking');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Monitor', 'Display flickering');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Monitor', 'Dead spot, White spot');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Monitor', 'Bezel is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Monitor', 'Display has shadow');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Camema', 'No image');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Camema', 'Black spot on LENs');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Hinge', 'Hinge is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Fan', 'Fan error');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Fan', 'Fan is noisy');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Keybooard', 'Keys has no response');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Keybooard', 'Stuck Key');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Keybooard', 'Key is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Touch Pad', 'Touch Pad has no response');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Touch Pad', 'Touch Pad button has no response');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Mouse', 'Button related problem');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Mouse', 'Scolling wheel problem');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Mouse', 'Mouse pointer has no response');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Speaker', 'Speaker has no sound');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Speaker', 'Strange sound or noisy');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Hard disk', 'Hard disk error');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Hard disk', 'Strange sound or noisy');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVD Rom', 'Unable to read DVD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVD Rom', 'Unable to read CD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVDRW', 'Unable to read DVD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVDRW', 'Unable to read CD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVDRW', 'Unable to burn DVD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVDRW', 'Unable to Burn CD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Blur-Ray', 'Unable to read DVD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Blur-Ray', 'Unable to read CD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Blur-Ray', 'Unable to read Blur-Ray disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Blur-Ray', 'Unable to burn DVD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Blur-Ray', 'Unable to burn CD Disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Blur-Ray', 'Unable to burn Blur-Ray disc');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'NIC Port', 'Pin is Loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'NIC Port', 'NIC not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Modem Port', 'Pin is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Modem Port', 'Modem not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'USB Port', 'USB port not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'USB Port', 'USB device unrecognized');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'USB Port', 'USB device speed degradation');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Card Reader', 'Card Reader not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'VGA port', 'VGA port is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'VGA port', 'VGA port not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'HDMI port', 'HDMI port is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'HDMI port', 'HDMI port not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DP Port', 'DP port is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DP Port', 'DP port not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVI port', 'DVI port is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'DVI port', 'DVI port not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Audio port', 'Audio port is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Audio port', 'Audio port no signal');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Audio port', 'Audio port only one side has signal');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Mic port', 'Mic port not work');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Mic port', 'Mic port is loosen');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Power Adapter', 'No Power');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Power Adapter', 'Unable to charge up battery');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Battery', 'No Power');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Battery', 'Unable to be charged up');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Machine water spilled');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Machine fire damage');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Monitor water spilled');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Monitor fire damage');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Monitor Broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Monitor Bezel Broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Monitor Cover broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Camera broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Hinge broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Keyboard key broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'keyboard water spilled');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'keyboard fire damage');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Keyboard bezel broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Touch Pad broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Touch Pad button broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Mouse broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Mouse button broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Speaker broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Hard disk broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Rom drive broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'NIC port borken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Modem port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'USB port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Card Reader port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'VGA port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'HDMI port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'DP port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'DVI port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Audio port broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('1' , 'Physical Damage', 'Base cover broken');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('2' , 'Operating System', 'System Recovery');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('2' , 'Operating System', 'Driver Installation');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('2' , 'Operating System', 'Virus Malware');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('2' , 'Application', 'Application problem');
INSERT INTO symptoms (`mainCategory`, `subCategory`, `symptoms`) VALUES ('2' , 'Other', 'Bios update');







#Laptop = 1
#Desktop = 2 
#Mobile =  3 

#device
INSERT INTO device (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Laptop' , 'Laptop', '1' , NOW(), '1') ; 
INSERT INTO device (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Desktop' , 'Desktop', '1' , NOW(), '1') ; 
INSERT INTO device (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Mobile' , 'Mobile', '1' , NOW(), '1') ; 

#Express = 1
#Standard = 2 
#Per-CALL = 3 

#service
INSERT INTO service (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Express' , 'Express', '1' , NOW(), '1') ; 
INSERT INTO service (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Standard' , 'Standard', '1' , NOW(), '1') ; 
INSERT INTO service (`name`, `description`, `status`, `createTime` , `createBy`) VALUES ('Per-Call' , 'Per-Call', '1' , NOW(), '1') ; 
