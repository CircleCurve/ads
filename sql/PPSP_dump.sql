-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 17, 2016 at 11:05 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `PPSP`
--

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT 'describe the device name ',
  `description` text NOT NULL COMMENT 'describe the  details',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT ' effective',
  `createTime` datetime NOT NULL COMMENT 'Which time of role create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this role',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id`, `name`, `description`, `status`, `createTime`, `createBy`) VALUES
(1, 'Application', '', 1, '2016-03-16 23:13:48', 1),
(2, 'Audio port', '', 1, '2016-03-16 23:13:48', 1),
(3, 'Battery', '', 1, '2016-03-16 23:13:48', 1),
(4, 'Blur-Ray', '', 1, '2016-03-16 23:13:48', 1),
(5, 'Camema', '', 1, '2016-03-16 23:13:48', 1),
(6, 'Card Reader', '', 1, '2016-03-16 23:13:48', 1),
(7, 'DP Port', '', 1, '2016-03-16 23:13:48', 1),
(8, 'DVD Rom', '', 1, '2016-03-16 23:13:48', 1),
(9, 'DVDRW', '', 1, '2016-03-16 23:13:48', 1),
(10, 'DVI port', '', 1, '2016-03-16 23:13:48', 1),
(11, 'Fan', '', 1, '2016-03-16 23:13:48', 1),
(12, 'Hard disk', '', 1, '2016-03-16 23:13:48', 1),
(13, 'HDMI port', '', 1, '2016-03-16 23:13:48', 1),
(14, 'Hinge', '', 1, '2016-03-16 23:13:48', 1),
(15, 'Keybooard', '', 1, '2016-03-16 23:13:48', 1),
(16, 'Mic port', '', 1, '2016-03-16 23:13:48', 1),
(17, 'Modem Port', '', 1, '2016-03-16 23:13:48', 1),
(18, 'Monitor', '', 1, '2016-03-16 23:13:48', 1),
(19, 'Mouse', '', 1, '2016-03-16 23:13:48', 1),
(20, 'NIC Port', '', 1, '2016-03-16 23:13:48', 1),
(21, 'Operating System', '', 1, '2016-03-16 23:13:48', 1),
(22, 'Other', '', 1, '2016-03-16 23:13:48', 1),
(23, 'Physical Damage', '', 1, '2016-03-16 23:13:48', 1),
(24, 'Power', '', 1, '2016-03-16 23:13:48', 1),
(25, 'Power Adapter', '', 1, '2016-03-16 23:13:48', 1),
(26, 'Speaker', '', 1, '2016-03-16 23:13:48', 1),
(27, 'Touch Pad', '', 1, '2016-03-16 23:13:48', 1),
(28, 'USB Port', '', 1, '2016-03-16 23:13:48', 1),
(29, 'VGA port', '', 1, '2016-03-16 23:13:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL COMMENT 'describe usage of each user permission',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'Is it effective',
  `createTime` datetime NOT NULL COMMENT 'Which time of permission create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this permission',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `permission`
--


-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT 'describe the role name invoke in this systme',
  `description` text NOT NULL COMMENT 'describe the role details',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'role effective',
  `createTime` datetime NOT NULL COMMENT 'Which time of role create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this role',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `status`, `createTime`, `createBy`) VALUES
(1, 'admin', 'allow to edit all pages', 1, '2016-03-14 19:27:13', 1),
(2, 'SERVICE DECK', 'allow to watch report', 1, '2016-03-14 19:27:13', 1),
(3, 'ENGINEER', 'allow to edit warrant and incident', 1, '2016-03-14 19:27:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Staff`
--

CREATE TABLE IF NOT EXISTS `Staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) NOT NULL COMMENT 'login userName',
  `firstName` varchar(50) NOT NULL COMMENT 'staff firstName',
  `lastName` varchar(50) NOT NULL COMMENT 'staff lastName',
  `password` varchar(32) NOT NULL COMMENT 'password is one way encrypted cannot be decrypted',
  `sex` int(1) DEFAULT NULL COMMENT 'sex',
  `hkid` varchar(8) DEFAULT NULL COMMENT 'hkid',
  `birthday` datetime NOT NULL COMMENT 'birthday',
  `email` varchar(50) NOT NULL COMMENT 'email',
  `directLine` int(20) NOT NULL COMMENT 'direct line phone number',
  `address` text COMMENT 'user address',
  `mobile` int(8) NOT NULL COMMENT 'user mobile number',
  `jobTitle` varchar(50) NOT NULL COMMENT 'job title',
  `team` int(11) NOT NULL COMMENT 'Service team',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT 'staff type maybe patient, doctor,nurse',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'The account status',
  `createTime` datetime NOT NULL COMMENT 'The account created time',
  `loginTime` datetime NOT NULL COMMENT 'The time of user login',
  PRIMARY KEY (`id`),
  KEY `IDX_TYPE` (`type`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_LOGIN_TIME` (`loginTime`),
  KEY `IDX_CREATE_TIME` (`createTime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `Staff`
--

INSERT INTO `Staff` (`id`, `userName`, `firstName`, `lastName`, `password`, `sex`, `hkid`, `birthday`, `email`, `directLine`, `address`, `mobile`, `jobTitle`, `team`, `type`, `status`, `createTime`, `loginTime`) VALUES
(1, 'patrick', 'Patrick', 'Ng', 'e10adc3949ba59abbe56e057f20f883e', 0, NULL, '0000-00-00 00:00:00', 'patrick@gmail.com', 12345678, '12345678', 12345678, 'Admin1', 1, 1, 1, '2016-02-04 21:57:20', '2016-03-17 11:40:44'),
(2, 'admin', 'admin', 'admin', '', 1, 'D886934A', '1987-07-16 00:00:00', '', 27867560, 'Rm 19, 45/F, Sea View Maison, Causeway Bay, HK', 96620499, 'Admin', 0, 3, 1, '2016-03-14 01:05:56', '0000-00-00 00:00:00'),
(3, 'peterpan', 'Peter', 'PAN', '', 1, 'Z044672C', '1987-07-17 00:00:00', '', 27867561, 'Rm 33, 10/F, Jade Garden, Tsim Sha Tsui, Kowloon, HK', 50718405, 'Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(4, 'ivylo', 'Ivy', 'LO', '', 2, 'Z6673456', '1987-07-18 00:00:00', '', 27867562, 'Rm 02, 1/F, Rich Rich House, Tai Po, NT, HK', 94522862, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(5, 'albertyip', 'Albert', 'YIP', '', 1, 'Z664298A', '1987-07-19 00:00:00', '', 27867563, 'Rm 202, 15/F, Blue Coast Garden, Tung Chung, NT, HK', 98664527, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(6, 'carmanfan', 'Carman', 'FAN', '', 2, 'D7781349', '1987-07-20 00:00:00', '', 27867564, 'Rm 14, 80/F, High Hill Maison, Mid Level, Central, HK', 92341038, 'Senior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(7, 'garfieldyuen', 'Garfield', 'YUEN', '', 2, 'G123211A', '1987-07-21 00:00:00', '', 27867565, 'Rm 8A, 16/F, Good View House, Ma on San, NT, HK', 63029374, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(8, 'chrischan', 'Chris', 'CHAN', '', 1, 'K1238795', '1987-07-22 00:00:00', '', 27867566, 'Rm 1881, 1/F, Kings Veune, North Point, HK', 61452138, 'Service Officer', 0, 3, -2, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(9, 'dennislok', 'Dennis', 'LOK', '', 1, 'Z3908901', '1987-07-23 00:00:00', '', 27867567, '6/F, Dargon Slope Mountin, Repluse Bay, HK', 98724688, 'Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(10, 'carloskam', 'Carlos', 'KAM', '', 1, 'A3213213', '1987-07-24 00:00:00', '', 27867568, 'Rm 05, 13/F, Ice age maison, Sai Tin, NT, HK', 53669456, 'Senior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(11, 'mandytai', 'Mandy', 'TAI', '', 2, 'Z3281798', '1987-07-25 00:00:00', '', 27867569, 'Rm1502, 33/F, Yi On House, Tsuen wan, NT, HK', 68798789, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(12, 'jennyma', 'Jenny', 'MA', '', 2, 'D1239049', '1987-07-26 00:00:00', '', 27867570, 'Rm 16, 19/F, Nang Shan House, Jordan, Kowloon, HK', 53644752, 'Senior Engineer', 0, 3, -2, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(13, 'collinso', 'Collin', 'SO', '', 1, 'A3896379', '1987-07-27 00:00:00', '', 27867571, 'Rm10, 34/F, Wah Fei Court, Aberdeen, HK', 64587921, 'Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(14, 'rickylau', 'Ricky', 'LAU', '', 1, 'D0088181', '1987-07-28 00:00:00', '', 27867572, '31 Ma Kei Shin Rd, Kenny Town, HK', 96857432, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(15, 'leokwan', 'Leo', 'KWAN', '', 1, 'Z1729304', '1987-07-29 00:00:00', '', 27867573, '28 Ho Chong Rd, Yuen Long, NT, HK', 96835214, 'Senior Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(16, 'sarahwan', 'Sarah', 'WAN', '', 2, 'G1918937', '1987-07-30 00:00:00', '', 27867574, 'Rm02, 09/F, Shek O Maison, Shek O, HK', 63112212, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(17, 'irenelui', 'Irene', 'LUI', '', 2, 'K1329095', '1987-07-31 00:00:00', '', 27867575, 'Rm 17, Diamond Hill House, Diamond Hill, Kowloon, HK', 55352639, 'Junior Engineer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(18, 'larryjohnson', 'Larry', 'Johnson', '', 1, 'D7367391', '1987-08-01 00:00:00', '', 27867576, 'Gold Coast Maison, 268 Ching Shan Rd, NT, HK', 96455525, 'Senior Service Officer', 0, 3, 1, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(19, 'mariasiu', 'Maria', 'SIU', '', 2, 'G1239103', '1987-08-02 00:00:00', '', 27867577, 'Rm 12, 1/F, Tai Chong house, Mei Foo, Kowloon, HK', 69778451, 'Senior Engineer', 0, 3, -2, '2016-03-14 01:05:57', '0000-00-00 00:00:00'),
(20, 'larry', 'larry', 'tam', '8df32286deed62bae5bf746672805c78', 0, 'A1234567', '1992-11-18 00:00:00', 'larry@gmail.com', 12345678, 'Some address', 12345671, 'SeniorLarry', 1, 1, -1, '2016-03-14 02:19:17', '2016-03-15 21:02:51'),
(21, 'joe', 'Joe', 'Lai', '8df32286deed62bae5bf746672805c78', 1, 'A1234567', '2016-02-29 00:00:00', 'JOELAI@gmail.com', 12345678, 'some address', 12345678, 'SeniorJoe', 1, 0, -2, '2016-03-14 19:50:51', '0000-00-00 00:00:00'),
(27, 'larry1', 'larry', 'tam', '8df32286deed62bae5bf746672805c78', 1, 'A1234567', '1992-11-18 00:00:00', 'larry@gmail.com', 12345678, 'some address', 12345678, 'SeniorPat', 1, 1, -2, '2016-03-14 23:02:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `StaffPermission`
--

CREATE TABLE IF NOT EXISTS `StaffPermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT 'User use which permission',
  `permissionId` int(11) NOT NULL DEFAULT '0' COMMENT 'JSON for saving serval permission',
  `last_effectiveTime` datetime NOT NULL COMMENT 'permission last effective time',
  PRIMARY KEY (`id`),
  KEY `IDX_UID` (`uid`),
  KEY `IDX_PERMISSIONID` (`permissionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `StaffPermission`
--


-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE IF NOT EXISTS `symptoms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mainCategory` int(11) NOT NULL COMMENT 'main category',
  `subCategory` varchar(30) NOT NULL COMMENT 'sub category',
  `symptoms` varchar(50) NOT NULL COMMENT 'description',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'The account status',
  PRIMARY KEY (`id`),
  KEY `IDX_mainCategory` (`mainCategory`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `symptoms`
--

INSERT INTO `symptoms` (`id`, `mainCategory`, `subCategory`, `symptoms`, `status`) VALUES
(1, 1, '24', 'No Power', 1),
(2, 1, '24', 'Suddenly Shut Down', 1),
(3, 1, '24', 'Automatically Power on', 1),
(4, 1, '18', 'No display', 1),
(5, 1, '18', 'Display dim', 1),
(6, 1, '18', 'Display Blinking', 1),
(7, 1, '18', 'Display flickering', 1),
(8, 1, '18', 'Dead spot, White spot', 1),
(9, 1, '18', 'Bezel is loosen', 1),
(10, 1, '18', 'Display has shadow', 1),
(11, 1, '5', 'No image', 1),
(12, 1, '5', 'Black spot on LENs', 1),
(13, 1, '14', 'Hinge is loosen', 1),
(14, 1, '11', 'Fan error', 1),
(15, 1, '11', 'Fan is noisy', 1),
(16, 1, '15', 'Keys has no response', 1),
(17, 1, '15', 'Stuck Key', 1),
(18, 1, '15', 'Key is loosen', 1),
(19, 1, '27', 'Touch Pad has no response', 1),
(20, 1, '27', 'Touch Pad button has no response', 1),
(21, 1, '19', 'Button related problem', 1),
(22, 1, '19', 'Scolling wheel problem', 1),
(23, 1, '19', 'Mouse pointer has no response', 1),
(24, 1, '26', 'Speaker has no sound', 1),
(25, 1, '26', 'Strange sound or noisy', 1),
(26, 1, '12', 'Hard disk error', 1),
(27, 1, '12', 'Strange sound or noisy', 1),
(28, 1, '8', 'Unable to read DVD Disc', 1),
(29, 1, '8', 'Unable to read CD Disc', 1),
(30, 1, '9', 'Unable to read DVD Disc', 1),
(31, 1, '9', 'Unable to read CD Disc', 1),
(32, 1, '9', 'Unable to burn DVD Disc', 1),
(33, 1, '9', 'Unable to Burn CD Disc', 1),
(34, 1, '4', 'Unable to read DVD Disc', 1),
(35, 1, '4', 'Unable to read CD Disc', 1),
(36, 1, '4', 'Unable to read Blur-Ray disc', 1),
(37, 1, '4', 'Unable to burn DVD Disc', 1),
(38, 1, '4', 'Unable to burn CD Disc', 1),
(39, 1, '4', 'Unable to burn Blur-Ray disc', 1),
(40, 1, '20', 'Pin is Loosen', 1),
(41, 1, '20', 'NIC not work', 1),
(42, 1, '17', 'Pin is loosen', 1),
(43, 1, '17', 'Modem not work', 1),
(44, 1, '28', 'USB port not work', 1),
(45, 1, '28', 'USB device unrecognized', 1),
(46, 1, '28', 'USB device speed degradation', 1),
(47, 1, '6', 'Card Reader not work', 1),
(48, 1, '29', 'VGA port is loosen', 1),
(49, 1, '29', 'VGA port not work', 1),
(50, 1, '13', 'HDMI port is loosen', 1),
(51, 1, '13', 'HDMI port not work', 1),
(52, 1, '7', 'DP port is loosen', 1),
(53, 1, '7', 'DP port not work', 1),
(54, 1, '10', 'DVI port is loosen', 1),
(55, 1, '10', 'DVI port not work', 1),
(56, 1, '2', 'Audio port is loosen', 1),
(57, 1, '2', 'Audio port no signal', 1),
(58, 1, '2', 'Audio port only one side has signal', 1),
(59, 1, '16', 'Mic port not work', 1),
(60, 1, '16', 'Mic port is loosen', 1),
(61, 1, '25', 'No Power', 1),
(62, 1, '25', 'Unable to charge up battery', 1),
(63, 1, '3', 'No Power', 1),
(64, 1, '3', 'Unable to be charged up', 1),
(65, 1, '23', 'Machine water spilled', 1),
(66, 1, '23', 'Machine fire damage', 1),
(67, 1, '23', 'Monitor water spilled', 1),
(68, 1, '23', 'Monitor fire damage', 1),
(69, 1, '23', 'Monitor Broken', 1),
(70, 1, '23', 'Monitor Bezel Broken', 1),
(71, 1, '23', 'Monitor Cover broken', 1),
(72, 1, '23', 'Camera broken', 1),
(73, 1, '23', 'Hinge broken', 1),
(74, 1, '23', 'Keyboard key broken', 1),
(75, 1, '23', 'keyboard water spilled', 1),
(76, 1, '23', 'keyboard fire damage', 1),
(77, 1, '23', 'Keyboard bezel broken', 1),
(78, 1, '23', 'Touch Pad broken', 1),
(79, 1, '23', 'Touch Pad button broken', 1),
(80, 1, '23', 'Mouse broken', 1),
(81, 1, '23', 'Mouse button broken', 1),
(82, 1, '23', 'Speaker broken', 1),
(83, 1, '23', 'Hard disk broken', 1),
(84, 1, '23', 'Rom drive broken', 1),
(85, 1, '23', 'NIC port borken', 1),
(86, 1, '23', 'Modem port broken', 1),
(87, 1, '23', 'USB port broken', 1),
(88, 1, '23', 'Card Reader port broken', 1),
(89, 1, '23', 'VGA port broken', 1),
(90, 1, '23', 'HDMI port broken', 1),
(91, 1, '23', 'DP port broken', 1),
(92, 1, '23', 'DVI port broken', 1),
(93, 1, '23', 'Audio port broken', 1),
(94, 1, '23', 'Base cover broken', 1),
(95, 2, '21', 'System Recovery', 1),
(96, 2, '21', 'Driver Installation', 1),
(97, 2, '21', 'Virus Malware', 1),
(98, 2, '1', 'Application problem', 1),
(99, 2, '22', 'Bios update', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'team auto increment id ',
  `name` varchar(20) NOT NULL COMMENT 'team name',
  `description` text NOT NULL COMMENT 'describe the team details',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'team status',
  `createTime` datetime NOT NULL COMMENT 'Which time of role create',
  `createBy` int(11) NOT NULL COMMENT 'who create the this role',
  PRIMARY KEY (`id`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_CREATE_BY` (`createBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `description`, `status`, `createTime`, `createBy`) VALUES
(1, 'admin', 'allow to edit all pages', 1, '2016-03-14 19:34:46', 1),
(2, 'helpdeck', 'helpdeck', 1, '2016-03-14 19:34:46', 1),
(3, 'Express', 'express', 1, '2016-03-14 19:34:46', 1),
(4, 'Standard', 'standard', 1, '2016-03-14 19:34:46', 1),
(5, 'Percall', 'percall', 1, '2016-03-14 19:34:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Warranty`
--

CREATE TABLE IF NOT EXISTS `Warranty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serialNo` varchar(50) NOT NULL COMMENT 'serial No',
  `deviceType` int(11) NOT NULL COMMENT 'device type',
  `modelNo` varchar(20) NOT NULL COMMENT 'model no',
  `warrantyStart` datetime NOT NULL COMMENT 'email',
  `warrantyPeriod` int(11) NOT NULL COMMENT 'user address',
  `warrantyType` int(11) NOT NULL COMMENT 'warrant type',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT 'The account status',
  PRIMARY KEY (`id`),
  KEY `IDX_deviceType` (`deviceType`),
  KEY `IDX_STATUS` (`status`),
  KEY `IDX_warrantyType` (`warrantyType`),
  KEY `IDX_warrantyPeriod` (`warrantyPeriod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `Warranty`
--

