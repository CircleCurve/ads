CREATE TABLE `calendar_newevent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
   `incidentId` int(11) NOT NULL COMMENT '1 to 1 incidentId' , 
  `title` varchar(50) DEFAULT NULL,
  `start` datetime DEFAULT NOT NULL COMMENT 'event start date',
  `end` datetime DEFAULT NOT NULL COMMENT 'event end date',
  `allDay` int(1) DEFAULT '0' COMMENT 'event all day',
  `color` varchar(50) DEFAULT NULL,
  `repeatedevent` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` TEXT  NULL COMMENT 'event description',
  `reminder` varchar(50) DEFAULT NULL,
  `timer` varchar(50) DEFAULT NULL,
  `reminder_number` varchar(50) DEFAULT NULL,
  `public_own` int(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=Innodb AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ; 


CREATE TABLE `calendar_shared_event` (
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `editEvent` int(2) NOT NULL
) ENGINE=Innodb DEFAULT CHARSET=utf8 ; 