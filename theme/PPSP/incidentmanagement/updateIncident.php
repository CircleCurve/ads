<?
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>
<!DOCTYPE html>
<html>
    <head>
        <? include_once $path . '/include/header.php'; ?>
        <script type="text/javascript">
            function printDiv(inbox) {
                //Get the HTML of div
                var divElements = document.getElementById(inbox).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head><title></title></head><body><div><h2 align='center' ><b><u>Computer Claim Form</u></b></h2></div>" +
                        divElements + "<div><h3>Customer Signture: __________________</h3><br><h3>Date: _____________</h3></div></body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;


            }
            
            function printDiv1(inbox) {
                //Get the HTML of div
                var divElements = document.getElementById(inbox).innerHTML;
                //Get the HTML of whole page
                var oldPage = document.body.innerHTML;

                //Reset the page's HTML with div's HTML only
                document.body.innerHTML =
                        "<html><head><title></title></head><body><div><h2 align='center' ><b><u>Repair Service Report</u></b></h2></div>" +
                        divElements + "<div><h3>Customer Signture: __________________</h3><br><h3>Date: _____________</h3></div></body>";

                //Print Page
                window.print();

                //Restore orignal HTML
                document.body.innerHTML = oldPage;


            }

            $(function () {
                //var device = JSON.parse('<?= $data["deviceInfo"] ?>');
                //var symptom = JSON.parse('<?= $data["symptomInfo"] ?>');
                //var symptoms = [];

                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', maxDate: new Date()});
                /*
                 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                 //var target = $(e.target).attr("href") // activated tab
                 //alert(target);
                 $('ul.nav-pills li.active').removeClass('active');
                 
                 $(this).parent('li').addClass('active');
                 
                 $('div.active').removeClass('active');
                 
                 $("#incidentStatus").css("display", "");
                 });
                 */
                //callback



                var firmWareCallBack = function () {
                    $('#symptom').empty();

                    firmWare = $("#firmWare").val();
                    //$("#firmWare").wrap('<span/>')
                    $('#device').empty();
                    for (var i = 0; i < device.length; i++) {
                        if (device[i]["mainCategory"] == firmWare) {
                            $('#device')
                                    .append($("<option></option>")
                                            .attr("value", device[i]["subCategory"])
                                            .text(device[i]["name"]));
                        }

                    }
                };
                //firmWareCallBack();

                var deviceCallBack = function () {

                    deviceType = $("#device").val();

                    console.log("deviceType : " + deviceType);
                    //$("#firmWare").wrap('<span/>')
                    $('#symptom').empty();
                    for (var i = 0; i < symptom.length; i++) {
                        if (symptom[i]["subCategory"] == deviceType) {
                            $('#symptom')
                                    .append($("<option></option>")
                                            .attr("value", symptom[i]["id"])
                                            .text(symptom[i]["symptoms"]));
                        }

                    }
                };

                $("#firmWare").change(function () {
                    firmWareCallBack();

                }
                );

                $("#device").change(function () {
                    deviceCallBack();

                }
                );

                $(".removeSymptom").click(function () {
                    console.log($(this).attr('value'));

                });

                var removeSymptomCallBack = function (row) {
                    console.log("row value :" + row.attr('value'));
                    var value = row.attr('value');
                    var removeRow;
                    for (var i = 0; i < symptoms.length; i++) {
                        if (symptoms[i] === value) {
                            console.log("value1111");
                            symptoms.splice(i, 1);
                            removeRow = "#symptomRow" + value;
                            $(removeRow).remove();
                        }
                    }

                }

                var symptomTableCallBack = function () {

                    //$("#symptomTable").find("tr:gt(0)").remove();
                    var table = document.getElementById("symptomTable");
                    var rowCount = table.getElementsByTagName("tr").length - 1;
                    var i = symptoms.length - 1;
                    var row, cell;
                    row = table.insertRow(rowCount + 1);
                    $(row).attr("id", "symptomRow" + symptoms[i]);
                    $(row).attr("value", symptoms[i]);

                    //console.log("i value :" + symptoms[i]);

                    row.insertCell(0).innerHTML = '<div style="cursor:pointer"  value="' + symptoms[i] + '" class="label label-danger glyphicon glyphicon-trash removeSymptom" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Remove</button></td>';
                    row.insertCell(0).innerHTML = symptom[(symptoms[i] - 1)]["symptoms"];

                    var deviceTypeId = "#device option[value='" + deviceType + "']";
                    row.insertCell(0).innerHTML = $(deviceTypeId).text();

                    //row.insertCell(0).innerHTML = device[deviceType]["name"];
                    var firmWareId = "#firmWare option[value='" + firmWare + "']";
                    row.insertCell(0).innerHTML = $(firmWareId).text();

                    row.insertCell(0).innerHTML = symptoms[i];

                    $(".removeSymptom").click(function () {
                        removeSymptomCallBack($(this));

                    });
                    //j = 0;


                };

                function onChange() {
                    console.log("onChange");
                }




                $("#symptom").change(function () {

                    var symptomIssue = [$("#symptom").val()];
                    for (var i = 0; i < symptomIssue.length; i++) {
                        if ($.inArray(symptomIssue[i], symptoms) === -1) {

                            symptoms.push(symptomIssue[i]);
                            symptomTableCallBack();



                        }

                    }
                    $("#symptoms").val(symptoms);


                    //symptomTableCallBack();
                    //console.log($("#symptom").val()) ; 
                });


                //$('#submitcreateuserform').confirmation('show');
                $('#submitincidentform').confirmation({
                    //popout : true, 
                    "onConfirm": function () {

                        $("#symptoms").val(JSON.stringify(symptoms));


                        var successCallBack = function (response) {
                            console.log(response);
                            var response = JSON.parse(response);

                            if (response["status"] === "1") {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                                location.href = "index.php?act=incidentmanagement-viewincident";
                            }
                            else {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            }

                        }

                        ajaxForm("webservice/incident/createIncident.php", $("#createincidentform").serialize(), false, successCallBack);

                    }

                });

                $('#updateIncidentButton').confirmation({
                    //popout : true, 
                    "onConfirm": function () {



                        var successCallBack = function (response) {
                            console.log(response);
                            var response = JSON.parse(response);

                            if (response["status"] === "1") {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                                location.href = "index.php?act=incidentmanagement-viewincident";
                            }
                            else {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            }

                        }

                        ajaxForm("webservice/incident/updateIncident.php", $("#updateIncidentForm").serialize(), false, successCallBack);

                    }

                });


            });
        </script>
    </head>


    <body>
        <div class="container">

        <div class="row" style="padding-top:10px;">
            <!---left side--->
            <div class="col-xs-3" >
                <center>
                    <!--box+menu-->
                    <? include $path . '/include/welcomebox.php'; ?>
                    <? include $path . '/include/menu.php'; ?>
                </center>
            </div>

            <!---right side-->
            <div class="col-xs-9">
                <!---heading-->
                <div class="row">
                    <div class="page-header">
                        <h1>Incident Management<small> - Update Incident</small></h1>
                        <!--logout home button-->
                        <? include $path . '/include/homebutton.php'; ?>
                    </div>
                </div>
                <h2 style="margin-bottom:-10px; margin-top:-30px;" ><small>* = Request</small></h2>
                <!---content-->
                <div class="row" style="padding-top:20px;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li  class="active">
                            <a href="#incidentSummary" role="#tab" data-toggle="tab">Incident Summary</a>
                        </li>
                        <li >
                            <a href="#incidentStatusForm" role="#tab" data-toggle="tab">Progress Information</a>
                        </li>
                        <li>
                             <button type="button" class="btn btn-primary" value="Print 1st Div" onclick="javascript:printDiv('incidentSummary')" />Print Claim <span class="glyphicon glyphicon-print"></span></button>
                        </li>
                        <li>
                             <button type="button" class="btn btn-primary" value="Print 1st Div" onclick="javascript:printDiv1('incidentStatusForm')" />Print Report <span class="glyphicon glyphicon-print"></span></button>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div  id="incidentStatusForm" class=" tab-pane  panel panel-warning" >
                            <div class="panel-heading">
                                <form id="updateIncidentForm" data-toggle="validator"  method="post" onsubmit="return false">
                                    <input type="hidden" id="id" name="id" value="<?= $_GET["id"] ?>">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label>Incident Status</label>
                                            <?= $data["regex"]["incidentStatus"]["html"] ?> 
                                        </div>

                                        
                                    </div>
                                    <div class="form-group col-xs-12">
                                            <label >Comment</label>
                                            <?= $data["regex"]["comment"]["html"] ?> 
                                        </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <button id="updateIncidentButton" type="submit" class="btn btn-default" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Update Progress</button>
                                        </div>


                                    </div>
                                </form>

                            </div>

                        </div> 

                        <div id="incidentSummary" class="tab-pane active panel panel-warning">
                            <div class="panel-heading">
                                <!----create form--->
                                <form id="createincidentform" data-toggle="validator"  method="post" onsubmit="return false">

                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label >Incident Summary *</label>
                                            <?= $data["regex"]["title"]["html"] ?> 
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >First Name *</label>
                                            <?= $data["regex"]["firstName"]["html"] ?> 
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Last Name *</label>
                                            <?= $data["regex"]["lastName"]["html"] ?> 
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-2">
                                            <label >Title *</label>
                                            <?= $data["regex"]["sex"]["html"] ?> 

                                        </div>
                                        <div class="form-group col-xs-5">
                                            <div class="form-group ">
                                                <label >Email Address *</label>
                                                <?= $data["regex"]["email"]["html"] ?> 
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-5">
                                            <div class="form-group ">
                                                <label >Mobile</label>
                                                <?= $data["regex"]["mobile"]["html"] ?> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label >Address</label>
                                            <?= $data["regex"]["address"]["html"] ?> 
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-3">
                                            <div class="form-group">
                                                <label >Device Serial No *</label>
                                                <?= $data["regex"]["serialNo"]["html"] ?> 
                                            </div>
                                        </div>

                                        <div class="form-group col-xs-6">
                                            <div class="form-group">
                                                <label >Warranty Status *</label>
                                                <input type="text" id="warrantyStatus" name="warrantyStatus"" class="form-control"   placeholder="Warranty Status"  disabled>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-3">
                                            <div class="form-group">
                                                <label >Service type *</label>
                                                <?= $data["regex"]["serviceType"]["html"] ?> 


                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label >Symptoms Issue</label>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <?= $data["regex"]["symptomTable"]["html"] ?>

                                                    <!--
                                                    <table id="symptomTable" class="table table-bordered table-striped">
                                                        <tr class="success topbar">
                                                            <td> ID</td>
                                                            <td>Firmware</td>
                                                            <td>Device</td>
                                                            <td>Symptom</td>
                                                            <td></td>
                                                        </tr>


                                                    </table>-->
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label >Description</label>
                                            <?= $data["regex"]["description"]["html"] ?> 
                                        </div>
                                    </div>


                                    <!--
                                    <button id="submitincidentform" type="submit" class="btn btn-default" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Update Incident</button>
                                    -->
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</body>
</html>