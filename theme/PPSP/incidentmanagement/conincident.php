<?
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>
<!DOCTYPE html>
<html>
    <head>
        <? include_once $path . '/include/header.php'; ?>
    </head>


    <body>
        <div class="container">

            <div class="row" style="padding-top:10px;">
                <!---left side--->
                <div class="col-xs-3" >
                    <center>
                        <!--box+menu-->
                        <? include $path . '/include/welcomebox.php'; ?>
                        <? include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---right side-->
                <div class="col-xs-9">
                    <!---heading-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Incident Management<small> - Confirm Incident</small></h1>
                           <!--logout home button-->
							<? include $path.'/include/homebutton.php';?>
                        </div>
                    </div>
                    <!---content-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <!----create form--->
                                <form id="conincidentform" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >First Name *</label>
											<input type="text" id="firstName" name="firstName" class="form-control" placeholder="First Name"  disabled>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Last Name *</label>
											<input type="text" id="lastName" name="lastName" class="form-control"   placeholder="Last Name"  disabled>
                                        </div>
                                    </div>

                                    <div class="row">
									<div class="form-group col-xs-2">
                                            <label >Title *</label>
											<select class="form-control" disabled>
											<option>Mr.</option>
											<option>Mrs.</option></select>
                                        </div>
                                        <div class="form-group col-xs-5">
                                            <div class="form-group ">
                                            <label >Email Address *</label>
											<input type="text" id="emailAddress" name="emailAddress" class="form-control"   placeholder="Email Address"  disabled>
                                        </div>
                                        </div>
										<div class="form-group col-xs-5">
                                            <div class="form-group ">
                                            <label >Mobile</label>
											<input type="text" id="mobile" name="mobile" class="form-control" placeholder="Mobile"  disabled>
                                        </div>
                                        </div>
                                    </div>
									
									<div class="row">
                                        <div class="form-group col-xs-12">
										<label >Address</label>
                                            <textarea class="form-control" rows="1" disabled></textarea>
                                        </div>
                                        </div>
                                    
									 <div class="row">
                                        <div class="form-group col-xs-3">
                                        <div class="form-group">
                                            <label >Device Serial No *</label>
											<input type="text" id="deviceSerialNo" name="deviceSerialNo" class="form-control"   placeholder="Device Serial No"  disabled>
                                        </div>
                                        </div>
										<div class="form-group col-xs-3 label label-danger">
										<label style="font-size:14px;" >Incident Number</label>
											<input type="text" id="incidentNumber" name="incidentNumber" class="form-control"  value="testing123"  disabled>										
                                        </div>
										<div class="form-group col-xs-3">
                                        <div class="form-group">
                                            <label >Warranty Status *</label>
											<input type="text" id="warrantyStatus" name="warrantyStatus"" class="form-control"   placeholder="Warranty Status"  disabled>
                                        </div>
                                        </div>
										<div class="form-group col-xs-3">
                                        <div class="form-group">
                                            <label >Service type *</label>
											<select class="form-control" disabled>
											<option>Express</option>
											<option>Standard</option>
											<option>Per-Call</option></select>
                                        </div>
                                        </div>
                                    </div>
									
									<div class="row">
									<div class="form-group col-xs-3">
                                            <label >Symptom Selection *</label>
											<select class="form-control" disabled>
											<option>Hardware</option>
											<option>Software</option></select>
                                     </div>
									 <div class="form-group col-xs-1">
									 <label ></label>
										<button type="button" class="btn btn-success" style="margin-top:5px;"><span class="glyphicon glyphicon-share-alt"></span></button>
                                     </div>
									<div class="form-group col-xs-3">
									<select multiple class="form-control" disabled>
									<option>Power</option>
									<option>Monitor</option>
									<option>Camema</option>
									<option>Hinge</option>
									<option>Fan</option>
									</select>
									</div>
									<div class="form-group col-xs-1">
									 <label ></label>
										<button type="button" class="btn btn-success" style="margin-top:5px;"><span class="glyphicon glyphicon-share-alt"></span></button>
                                     </div>
									 <div class="form-group col-xs-4">
									<select multiple class="form-control" disabled>
									<option>Unable to read CD Disc</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									</select>
									</div>
									</div>
									
									<div class="row">
                                        <div class="form-group col-xs-12">
										<label >Description</label>
                                            <textarea class="form-control" rows="2" disabled></textarea>
                                        </div>
                                        </div>
									
                                    <button id="submitconincidentmanagementform" type="submit" class="btn btn-default">Confirm</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>