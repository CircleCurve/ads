<style>
.list-group-item { background: #F2F5A9;
					color:#5E610B;
					cursor: pointer;
					font-weight:bold;
					font-size:13px;
}
.list-group-item:hover { background: #FFFFFF;
					color:#5E610B;
					cursor: pointer;
					font-weight:bold;
					font-size:13px;
}
.list-group-item a { color:black;
text-decoration: none;
}
.list-group-item:hover a { color:white;
}
.submenu1 { background: #FFFFFF;
					cursor: pointer;
					font-weight:bold;
					font-size:14px; 
					padding-left:50px;
}
.submenu1:hover { background: #F2F2F2;
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:15px; 
					
}
.submenu2 { background: #FFFFFF;
					cursor: pointer;
					font-weight:bold;
					font-size:14px; 
					padding-left:40px;
}
.submenu2:hover { background: #F2F2F2;
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:14px; 
}
.submenu3 { background: #FFFFFF;
					cursor: pointer;
					font-weight:bold;
					font-size:13px; 
					padding-left:50px;
}
.submenu3:hover { background: #F2F2F2;
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:14px; 
}
.submenu4 { background: #FFFFFF;
					cursor: pointer;
					font-weight:bold;
					font-size:14px; 
					padding-left:50px;
}
.submenu4:hover { background: #F2F2F2;
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:15px; 
}
.submenu5 { background: #FFFFFF;
					cursor: pointer;
					font-weight:bold;
					font-size:14px; 
					padding-left:50px;
}
.submenu5:hover { background: #F2F2F2;
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:15px; 
}
.submenu6 { background: #FFFFFF;
			cursor: pointer;
			font-weight:bold;
			font-size:14px; 
			padding-left:50px;
}
.submenu6:hover { background: #F2F2F2;
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:15px; 
}
.submenu1{display:none;}
.submenu2{display:none;}
.submenu3{display:none;}
.submenu4{display:none;}
.submenu5{display:none;}
.submenu6{display:none;}

</style>
<script>
$(function() {
    $('#bigmenu1').click(function(){
      $('.submenu1').slideToggle();
    });
});
$(function() {
    $('#bigmenu2').click(function(){
      $('.submenu2').slideToggle();
	  
    });
});
$(function() {
    $('#bigmenu3').click(function(){
      $('.submenu3').slideToggle();
    });
});
$(function() {
    $('#bigmenu4').click(function(){
      $('.submenu4').slideToggle();
    });
});
$(function() {
    $('#bigmenu5').click(function(){
      $('.submenu5').slideToggle();
    });
});
$(function() {
    $('#bigmenu6').click(function(){
      $('.submenu6').slideToggle();
    });
});
</script>
<div class="panel panel-warning" style="-webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;">
	 <div class="panel-heading ">
<ul class="list-group" style="list-style-type: none;">
  <li class="list-group-item" id="bigmenu1" ><span class="glyphicon glyphicon-expand pull-left"></span> User Management<img src="<?= $path; ?>/image/User Management.png" class="img-responsive pull-right" width="40" style="margin-top:-10px;"></img></li>
   <li class="list-group-item submenu1 text-left" onClick="window.location='index.php?act=usermanagment-createuser'" ><span class="glyphicon glyphicon-expand"></span> Create User</li>
   <li class="list-group-item submenu1 text-left" onClick="window.location='index.php?act=usermanagment-viewuser'"><span class="glyphicon glyphicon-expand"></span> View User</li>
</ul>
<ul class="list-group" style="list-style-type: none;">
  <li class="list-group-item" id="bigmenu2" ><span class="glyphicon glyphicon-expand pull-left"></span> System Setting<img src="<?= $path; ?>/image/System Setting.png" class="img-responsive pull-right" width="30" style="margin-top:-5px;"></img></li>
     <li class="list-group-item submenu2 text-left" onClick="window.location='index.php?act=systemsetting-slasetting';"><span class="glyphicon glyphicon-expand"></span> incident distribution</li>
</ul>
<ul class="list-group" style="list-style-type: none;">
  <li class="list-group-item" id="bigmenu3" ><span class="glyphicon glyphicon-expand pull-left"></span>Incident Management<img src="<?= $path; ?>/image/Incident Management.png" class="img-responsive pull-right" width="30"></img></li>
     <li class="list-group-item submenu3 text-left" onClick="window.location='index.php?act=incidentmanagement-createincident';"><span class="glyphicon glyphicon-expand"></span> Create Incident</li>
     <li class="list-group-item submenu3 text-left" onClick="window.location='index.php?act=incidentmanagement-viewincident';"><span class="glyphicon glyphicon-expand"></span> View Incident</li>
     <li class="list-group-item submenu3 text-left" onClick="window.location='index.php?act=incidentmanagement-searchIncident';"><span class="glyphicon glyphicon-expand"></span> Search Incident</li>

</ul>
<ul class="list-group" style="list-style-type: none;">
     <li class="list-group-item" id="bigmenu4" ><span class="glyphicon glyphicon-expand pull-left"></span> Stock Management<img src="<?= $path; ?>/image/Stock Management.png" class="img-responsive pull-right" width="30" style="margin-top:-5px;"></img></li> 
     <li class="list-group-item submenu4 text-left" ><span class="glyphicon glyphicon-expand "></span>Create stock Item</li>
     <li class="list-group-item submenu4 text-left" ><span class="glyphicon glyphicon-expand "></span>View stock Item</li>
</ul>
<ul class="list-group" style="list-style-type: none;">
  <li class="list-group-item" id="bigmenu5" ><span class="glyphicon glyphicon-expand pull-left"></span> Warranty<img src="<?= $path; ?>/image/Warranty.png" class="img-responsive pull-right" width="40" style="margin-top:-5px;"></img></li>
     <li class="list-group-item submenu5 text-left"  data-toggle="modal" data-target="#checkwarranty"><span class="glyphicon glyphicon-expand"></span> Warranty Lookup</li>
</ul>
<ul class="list-group" style="list-style-type: none;">
  <li class="list-group-item" id="bigmenu6" ><span class="glyphicon glyphicon-expand pull-left"></span> Reporting<img src="<?= $path; ?>/image/report.png" class="img-responsive pull-right" width="40" style="margin-top:-10px;"></img></li>
     <li class="list-group-item submenu6 text-left" onClick="window.location='index.php?act=report-viewreport';"><span class="glyphicon glyphicon-expand"></span> View Report</li>
</ul>
    </div>
	</div>
<!--include check warranty box--->
<? include $path.'/include/checkwarranty.php'; ?>