<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>
        <script type="text/javascript">
            $(function () {

                $("#search").click(function () {

                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);

                        $("#userInfoTable").find("tr:gt(0)").remove();
                        var table = document.getElementById("userInfoTable");
                        var row, cell;

                        for (var i = 0, j = 0; i < response.length; i++) {
                            row = table.insertRow(i + 1);
                            for (var key in response[i]) {

                                row.insertCell(j).innerHTML = response[i][key];
                                j++;
                            }
                            row.insertCell(j++).innerHTML = '<button value="' + response[i]["id"] + '" class="label label-success glyphicon glyphicon-info-sign details"> Detail</button>';

                            row.insertCell(j++).innerHTML = '<button value="' + response[i]["id"] + '" class="label label-danger glyphicon glyphicon-trash remove" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Remove</button></td>';
                            ;

                            j = 0;
                        }

                        $(".details").click(function () {
                            console.log("details");
                            var detailsId = $(this).attr('value');

                            location.href = "index.php?act=usermanagment-userdetails&id=" + detailsId;
                        });
                        $('.remove').confirmation({
                            "onConfirm": function () {

                                removeId($(this).attr('value'));

                            }

                        });

                    }
                    //var removeUid = $(this).attr('value');

                    ajaxForm("webservice/core/info.php", $("#viewUserTableSortForm").serialize(), false, successCallBack);


                });



                $('.remove').confirmation({
                    "onConfirm": function () {

                        removeId($(this).attr('value'));

                    }

                });

                function removeId(removeUid) {
                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            location.href = "index.php?act=usermanagment-viewuser";
                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }

                    ajaxForm("webservice/core/deleteUser.php", {"id": removeUid}, false, successCallBack);

                }

                $(".details").click(function () {
                    console.log("details");
                    var detailsId = $(this).attr('value');

                    location.href = "index.php?act=usermanagment-userdetails&id=" + detailsId;
                });

                /*
                 $(".remove").click(function() {
                 var bid = $(this).attr('value');
                 console.log(bid) ; 
                 }*/
            });
        </script>

        <style>
            a:hover { text-decoration: none; }
        </style>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---left side-->
                <div class="col-xs-3" >
                    <center>
                        <!--box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---rightside--->
                <div class="col-xs-9">
                    <!---heading-->
                    <div class="row">
                        <div class="page-header">
                            <h1>User Management<small> - View User</small></h1>
                            <!--logout home button-->
                            <?php include $path . '/include/homebutton.php'; ?>
                        </div>
                    </div>
                    <!---sort box-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form id="viewUserTableSortForm" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-4">
                                            <label >User Name</label>
                                            <?= $data["userNameInfo"] ?>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <label >Role</label>
                                            <?= $data["roleInfo"] ?>
                                        </div>

                                        <div class="form-group col-xs-4">
                                            <label >Job Title</label>
                                            <?= $data["jobTitleInfo"] ?>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >ID Info</label>
                                            <?= $data["idInfo"] ?>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Account Status</label>
                                            <?= $data["accStatusInfo"] ?>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12" >
                                            <button id="search" type="submit" class="btn btn-success" >
                                                Search
                                            </button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!---table box-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <table id="userInfoTable" class="table table-bordered table-striped">
                                    <tr class="success topbar">
                                        <td> ID</td>
                                        <td>User Name</td>
                                        <td>Job Title</td>
                                        <td>Role</td>
                                        <td>Status</td>
                                        <td>Create Time</td>
                                        <td>Last Login Date</td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <?
                                    echo '<tr>';
                                    for ($i = 0; $i < count($data["staff"]); $i++) {
                                        echo '<tr>';
                                        foreach ($data["staff"][$i] as $key => $value) {
                                            echo '<td>' . $data["staff"][$i][$key] . '</td>';
                                        }
                                        echo '<td><button value="' . $data["staff"][$i]["id"] . '" class="label label-success glyphicon glyphicon-info-sign details"> Detail</button></td>';
                                        echo '<td>'
                                        . '<button  value="' . $data["staff"][$i]["id"] . '" class="label label-danger glyphicon glyphicon-trash remove" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Remove</button></td>';

                                        echo '</tr>';
                                    }
                                    ?>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</body>

</html>