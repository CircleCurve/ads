<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
//print_r_pre($data) ; 
//value="<?= substr_replace($data["data"][0]['hkid'],"****", -4); 
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {

                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', maxDate: new Date()});

                //$('#submitcreateuserform').confirmation('show');
                $('#editInfo').click(function () {
                    $("#firstName").removeAttr('disabled').attr("required", "");
                    $("#lastName").removeAttr('disabled').attr("required", "");
                    //$("#userPassword").removeAttr('disabled').attr("required","");;
                    $("#sex").removeAttr('disabled').attr("required", "");
                    $("#email").removeAttr('disabled').attr("required", "");
                    $("#directLine").removeAttr('disabled').attr("required", "");
                    $("#address").removeAttr('disabled').attr("required", "");
                    $("#mobile").removeAttr("disabled").attr("required", "");
                    $("#jobTitle").removeAttr('disabled').attr("required", "");

                    $("#accStatus").removeAttr('disabled').attr("required", "");
                    $("#role").removeAttr('disabled').attr("required", "");
                    $("#team").removeAttr('disabled').attr("required", "");
                    $("#sex").removeAttr('disabled').attr("required", "");



                    $("#editInfo").css("display", "none");
                    $("#confirm").css("display", '');
                    $('#myForm').validator();

                });

                $("#confirm").click(function () {
                    var successCallBack = function (response) {
                        console.log(response);

                        var response = JSON.parse(response);

                        if (response["status"] === "1") {

                            $("#firstName").attr("disabled", "");
                            $("#lastName").attr("disabled", "");
                            //$("#userPassword").removeAttr('disabled').attr("required","");;
                            $("#sex").attr("disabled", "");
                            $("#email").attr("disabled", "");
                            $("#directLine").attr("disabled", "");
                            $("#address").attr("disabled", "");
                            $("#mobile").attr("disabled", "");
                            $("#jobTitle").attr("disabled", "");

                            $("#accStatus").attr("disabled", "");
                            $("#role").attr("disabled", "");
                            $("#team").attr("disabled", "");
                            $("#sex").attr("disabled", "");
                            $("#id").attr('readonly', true);

                            $("#editInfo").css("display", "");

                            $("#confirm").css("display", "none");


                        } else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }
                    ajaxForm("webservice/core/updateUserInfo.php", $("#createuserform").serialize(), false, successCallBack);


                });

                $("#deleteUser").click(function () {
                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            location.href = "index.php?act=usermanagment-viewuser";
                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }
                    ajaxForm("webservice/core/deleteUser.php", {"id": '<?= $data["rtn"]["data"]["id"] ?>'}, false, successCallBack);

                });

                $('#submitcreateuserform').confirmation({
                    //popout : true, 
                    "onConfirm": function () {

                        var successCallBack = function (response) {
                            console.log(response);
                            var response = JSON.parse(response);

                            if (response["status"] === "1") {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                                location.href = "index.php?act=usermanagment-viewuser";
                            }
                            else {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            }

                        }

                        ajaxForm("webservice/core/register.php", $("#createuserform").serialize(), false, successCallBack);

                    }

                });


            });
        </script>
    </head>


    <body>
        <div class="container">

            <div class="row" style="padding-top:10px;">
                <!---left side--->
                <div class="col-xs-3" >
                    <center>
                        <!--box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---right side-->
                <div class="col-xs-9">
                    <!---heading-->
                    <div class="row">
                        <div class="page-header">
                            <h1>User Management<small> - Create User</small></h1>
                            <!--logout home button-->
                            <?php include $path . '/include/homebutton.php'; ?>
                        </div>
                    </div>
                    <h2 style="margin-bottom:-10px; margin-top:-30px;" ><small>* = Request</small></h2>
                    <!---content-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <!----create form--->

                                <form id="createuserform" data-toggle="validator"  method="post" onsubmit="return false">
                                    <input type='hidden' id='id' name="id" value='<?= $data["rtn"]["data"]["id"] ?>' >
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >First Name *</label>
                                            <input type="text" id="firstName" name="firstName" class="form-control" placeholder="First Name" value="<?= $data["rtn"]["data"]["firstName"] ?>" pattern="<?= $data["firstName"]["regex"] ?>" data-error="<?= $data["firstName"]["message"] ?>"  disabled="">
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Last Name *</label>
                                            <input type="text" id="lastName" name="lastName" class="form-control"   placeholder="Last Name" value="<?= $data["rtn"]["data"]["lastName"] ?>"   pattern="<?= $data["lastName"]["regex"] ?>" data-error="<?= $data["lastName"]["message"] ?>"  disabled> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >User Name *</label>
                                            <input type="text" id="userName" name="userName" class="form-control" placeholder="User Name" value="<?= $data["rtn"]["data"]["userName"] ?>"  pattern="<?= $data["userName"]["regex"] ?>" data-error="<?= $data["userName"]["message"] ?>"  disabled> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Password *</label>
                                            <input type="password" id="userPassword" name="userPassword" class="form-control"   placeholder="Password" pattern="<?= $data["userPassword"]["regex"] ?>" data-error="<?= $data["userPassword"]["message"] ?>"  disabled> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-2">
                                            <label >Title *</label>
                                            <?= $data["sexInfo"] ?>

                                        </div>
                                        <div class="form-group col-xs-4">
                                            <label >HKID *</label>
                                            <input type="text" id="HKID" name="HKID" class="form-control"  value="<?= substr_replace($data["rtn"]["data"]["hkid"], "****", -4); ?>"   placeholder="HKID" pattern="<?= $data["HKID"]["regex"] ?>" data-error="<?= $data["HKID"]["message"] ?>"  disabled> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Date of Birth *</label>
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' class="form-control" id="birthday" name="birthday" id="dateOfBirth" value="<?= $data["rtn"]["data"]["birthday"] ?>" pattern="<?= $data["birthday"]["regex"] ?>" data-error="<?= $data["birthday"]["message"] ?>"  disabled> 
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <div class="form-group ">
                                                <label >Email Address *</label>
                                                <input type="text" id="email" name="email" class="form-control"  value="<?= $data["rtn"]["data"]["email"] ?>" placeholder="Email Address" pattern="<?= $data["email"]["regex"] ?>" data-error="<?= $data["email"]["message"] ?>"  disabled> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <div class="form-group ">
                                                <label >Direct line *</label>
                                                <input type="text" id="directLine" name="directLine" class="form-control" value="<?= $data["rtn"]["data"]["directLine"] ?>"  placeholder="Direct Line"  pattern="<?= $data["directLine"]["regex"] ?>" data-error="<?= $data["directLine"]["message"] ?>"  disabled> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label >Address *</label>
                                            <textarea id = "address" name="address" class="form-control" rows="1" placeholder="Address"   pattern="<?= $data["address"]["regex"] ?>" data-error="<?= $data["address"]["message"] ?>"  disabled><?= $data["rtn"]["data"]["address"] ?></textarea>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-4">
                                            <div class="form-group ">
                                                <label >Mobile *</label>
                                                <input type="text" id="mobile" name="mobile" class="form-control" value="<?= $data["rtn"]["data"]["mobile"] ?>" placeholder="Mobile"  pattern="<?= $data["mobile"]["regex"] ?>" data-error="<?= $data["mobile"]["message"] ?>"  disabled> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <div class="form-group">
                                                <label >Job Title *</label>
                                                <input type="text" id="jobTitle" name="jobTitle" class="form-control" value="<?= $data["rtn"]["data"]["jobTitle"] ?>" placeholder="Job Title" pattern="<?= $data["jobTitle"]["regex"] ?>" data-error="<?= $data["jobTitle"]["message"] ?>"  disabled> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <div class="form-group">
                                                <label >Account Status *</label>
                                                <?= $data["accStatusInfo"] ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-3">
                                            <div class="form-group ">
                                                <label >User Role *</label>
                                                <?= $data["roleInfo"]; ?>

                                            </div>
                                        </div>
                                        <div class="form-group col-xs-3">
                                            <div class="form-group">
                                                <label >Service Team *</label>
                                                <?= $data["teamInfo"] ?> 

                                            </div>
                                        </div>
                                        <div class="form-group col-xs-5 col-xs-offset-1">
                                            <div class="form-group">
                                                <label >Skill *</label><br>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="desktop" id="desktop" value="desktop" >
                                                    Desktop
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="laptop" id="laptop" value="laptop" >
                                                    Laptop
                                                </label >
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="mobileDevice" id="mobileDevice" value="mobileDevice" >
                                                    Mobile device
                                                </label>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <button id="confirm" type="submit" class="btn btn-success" style="display:none">
                                        Confirm
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>

                                    <button id="editInfo" class="btn btn-primary" href="?act=patient-editinfo" role="button">
                                        Edit Info 
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>	
                                    <button id="deleteUser" type="submit" class="btn btn-danger" >
                                        Delete User
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>