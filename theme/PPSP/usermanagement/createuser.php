<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
$roleInfo = $data["data"]["roleInfo"];
$teamInfo = $data["data"]["teamInfo"];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {

                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', maxDate: new Date()});

                //$('#submitcreateuserform').confirmation('show');
                $('#submitcreateuserform').confirmation({
                    //popout : true, 
                    "onConfirm": function () {

                        var successCallBack = function (response) {
                            console.log(response);
                            var response = JSON.parse(response);

                            if (response["status"] === "1") {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                                location.href = "index.php?act=usermanagment-viewuser";
                            }
                            else {
                                alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            }

                        }

                        ajaxForm("webservice/core/register.php", $("#createuserform").serialize(), false, successCallBack);

                    }

                });


            });
        </script>
    </head>


    <body>
        <div class="container">

            <div class="row" style="padding-top:10px;">
                <!---left side--->
                <div class="col-xs-3" >
                    <center>
                        <!--box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---right side-->
                <div class="col-xs-9">
                    <!---heading-->
                    <div class="row">
                        <div class="page-header">
                            <h1>User Management<small> - Create User</small></h1>
                            <!--logout home button-->
                            <?php include $path . '/include/homebutton.php'; ?>
                        </div>
                    </div>
                    <h2 style="margin-bottom:-10px; margin-top:-30px;" ><small>* = Request</small></h2>
                    <!---content-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <!----create form--->

                                <form id="createuserform" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >First Name *</label>
                                            <input type="text" id="firstName" name="firstName" class="form-control" placeholder="First Name" pattern="<?= $data["firstName"]["regex"] ?>" data-error="<?= $data["firstName"]["message"] ?>"  required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Last Name *</label>
                                            <input type="text" id="lastName" name="lastName" class="form-control"   placeholder="Last Name"  pattern="<?= $data["lastName"]["regex"] ?>" data-error="<?= $data["lastName"]["message"] ?>"  required> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >User Name *</label>
                                            <input type="text" id="userName" name="userName" class="form-control" placeholder="User Name" pattern="<?= $data["userName"]["regex"] ?>" data-error="<?= $data["userName"]["message"] ?>"  required> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Password *</label>
                                            <input type="password" id="userPassword" name="userPassword" class="form-control"   placeholder="Password" pattern="<?= $data["userPassword"]["regex"] ?>" data-error="<?= $data["userPassword"]["message"] ?>"  required> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-2">
                                            <label >Title *</label>
                                            <select class="form-control" name="sex" pattern="<?= $data["sex"]["regex"] ?>" data-error="<?= $data["sex"]["message"] ?>"  required>
                                                <option value="Mr">Mr.</option>
                                                <option value="Mrs">Mrs.</option>
                                            </select>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <label >HKID *</label>
                                            <input type="text" id="HKID" name="HKID" class="form-control"   placeholder="HKID" pattern="<?= $data["HKID"]["regex"] ?>" data-error="<?= $data["HKID"]["message"] ?>"  required> 
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Date of Birth *</label>
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' class="form-control" name="birthday" id="dateOfBirth" pattern="<?= $data["birthday"]["regex"] ?>" data-error="<?= $data["birthday"]["message"] ?>"  required> 
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <div class="form-group ">
                                                <label >Email Address *</label>
                                                <input type="text" id="email" name="email" class="form-control"   placeholder="Email Address" pattern="<?= $data["email"]["regex"] ?>" data-error="<?= $data["email"]["message"] ?>"  required> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <div class="form-group ">
                                                <label >Direct line *</label>
                                                <input type="text" id="directLine" name="directLine" class="form-control"   placeholder="Direct Line"  pattern="<?= $data["directLine"]["regex"] ?>" data-error="<?= $data["directLine"]["message"] ?>"  required> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label >Address *</label>
                                            <textarea name="address" class="form-control" rows="1" placeholder="Address"  pattern="<?= $data["address"]["regex"] ?>" data-error="<?= $data["address"]["message"] ?>"  required></textarea>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-4">
                                            <div class="form-group ">
                                                <label >Mobile *</label>
                                                <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Mobile"  pattern="<?= $data["mobile"]["regex"] ?>" data-error="<?= $data["mobile"]["message"] ?>"  required> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <div class="form-group">
                                                <label >Job Title *</label>
                                                <input type="text" id="jobTitle" name="jobTitle" class="form-control" placeholder="Job Title" pattern="<?= $data["jobTitle"]["regex"] ?>" data-error="<?= $data["jobTitle"]["message"] ?>"  required> 
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <div class="form-group">
                                                <label >Account Status *</label>
                                                <select class="form-control" name ="accStatus"  pattern="<?= $data["accStatus"]["regex"] ?>" data-error="<?= $data["accStatus"]["message"] ?>"  required>
                                                    <option value="1">enable</option>
                                                    <option value="-1">disable</option>
                                                </select>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-3">
                                            <div class="form-group ">
                                                <label >User Role *</label>
                                                <select class="form-control" name="role" pattern="<?= $data["role"]["regex"] ?>" data-error="<?= $data["role"]["message"] ?>"  required>
                                                    <option value="" disabled="disabled">choose</option>

                                                    <?
                                                    for ($i = 0; $i < count($roleInfo); $i++) {

                                                        echo '<option value="' . ($i + 1) . '">' . $roleInfo[$i]["name"] . '</option>';
                                                    }
                                                    ?>

                                                </select>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-3">
                                            <div class="form-group">
                                                <label >Service Team *</label>
                                                <select class="form-control" name="team"  pattern="<?= $data["team"]["regex"] ?>" data-error="<?= $data["team"]["message"] ?>"   required>
                                                    <option value="" disabled="disabled">choose</option>


                                                    <?
                                                    for ($i = 0; $i < count($teamInfo); $i++) {

                                                        echo '<option value="' . ($i + 1) . '">' . $teamInfo[$i]["name"] . '</option>';
                                                    }
                                                    ?>

                                                </select>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-5 col-xs-offset-1">
                                            <div class="form-group">
                                                <label >Skill *</label><br>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="desktop" id="desktop" value="desktop" >
                                                    Desktop
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="laptop" id="laptop" value="laptop" >
                                                    Laptop
                                                </label >
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="mobileDevice" id="mobileDevice" value="mobileDevice" >
                                                    Mobile device
                                                </label>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                    </div>


                                    <button id="submitcreateuserform" type="submit" class="btn btn-default" data-toggle="confirmation" data-btn-ok-label="Confirm!!" data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success" data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle" data-btn-cancel-class="btn-danger">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>