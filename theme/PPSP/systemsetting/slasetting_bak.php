<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>
	<style>
	a:hover { text-decoration: none; }
	</style>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---left side-->
                <div class="col-xs-3" >
                    <center>
                        <!--box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---rightside--->
                <div class="col-xs-9">
                    <!---heading-->
                    <div class="row">
                        <div class="page-header">
                            <h1>System Setting<small> - SLA Setting</small></h1>
                            <!--logout home button-->
							<?php include $path.'/include/homebutton.php';?>
                        </div>
                    </div>
                    <!---sort box-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form id="viewUserTableSortForm" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
									<form id="changeslaform">
                                        <div class="form-group col-xs-6 col-xs-offset-3" >
                                            <label class="radio-inline">
											<input type="radio" name="lessIncidentFirst" id="lessIncidentFirst" value="option1" checked="checked" > Less Incident First
											</label>
											<label class="radio-inline" style="cursor: default;">
											<input type="radio" name="roundRobin" id="roundRobin" value="option2" disabled> Round-Robin
											</label>
											<label class="radio-inline" style="cursor: default;">
											<input type="radio" name="highSlaFirst" id="highSlaFirst" value="option3" disabled> High SLA First
											</label>
                                        </div>
										</div>
										<div class="row" >
									<button id="submitchangeslaform" type="submit" class="btn btn-default col-xs-offset-5" >Change</button>
                                    </div>
									</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</body>

</html>