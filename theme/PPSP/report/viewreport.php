<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#startDatedatetimepicker2,#endDatedatetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', maxDate: new Date()});


                var numberOfSymptoms = JSON.parse('<?= $data["numberOfSymptoms"] ?>');

                /*
                 $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});
                 $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});
                 $('#searchreport').click(function () {
                 
                 ajaxForm("webservice/report/checknormal.php", $("#reportsearch").serialize(), false, $("#reportsearchreslut"));
                 });*/
                //====jqplot========
                /*
                 
                 var s1 = [[2002, 112000], [2003, 122000], [2004, 104000], [2005, 99000], [2006, 121000],
                 [2007, 148000], [2008, 114000], [2009, 133000], [2010, 161000], [2011, 173000]];
                 var s2 = [[2002, 10200], [2003, 10800], [2004, 11200], [2005, 11800], [2006, 12400],
                 [2007, 12800], [2008, 13200], [2009, 12600], [2010, 13100]];
                 */
                /*
                 var labels = ["standard", , "ex", "per call"];
                 var line1 = [6.5, 9.2, 14, 19.65, 26.4, 35, 51, 10, 100, 50, 11, 40];
                 var line2 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120];
                 var line3 = [120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10];
                 var monthLine1 = [];
                 var monthLine2 = [];
                 var monthLine3 = [];
                 var temp;
                 for (var i = 0; i < line1.length; i++) {
                 monthLine1[i] = [i + 1, line1[i]];
                 monthLine2[i] = [i + 1, line2[i]];
                 monthLine3[i] = [i + 1, line3[i]];
                 }
                 // [ line3,line2,line1]*/


                //var line1 = numberOfSymptoms["lineValue"]["DVI port"] ;
                //var line2 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120];
                //var line3 = [120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10];


                var labels = numberOfSymptoms["lineName"];


                somePlot = null;

                togglePlot = function (seriesIdx) {

                    var someData = somePlot.getData();
                    someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;
                    somePlot.setData(someData);
                    somePlot.draw();
                }

                var plotOptions = {
                    title: 'Number of Symptoms',
                    animate: true,
                    animateReplot: true,
                    legend: {
                        renderer: $.jqplot.EnhancedLegendRenderer,
                        show: true,
                        labels: labels,
                        //location: 'e',
                        showSwatches: true,
                        placement: 'outsideGrid'
                    },
                    cursor: {
                        show: true,
                        zoom: true,
                        looseZoom: true,
                        showTooltip: false
                    },
                    series: [
                        {label: 'Process A', color: 'red', pointLabels: {show: true}},
                        {label: 'Process B', color: 'blue', pointLabels: {show: true}},
                    ],
                    axes: {
                        xaxis: {
                            renderer:$.jqplot.DateAxisRenderer,
                                    //renderer: $.jqplot.CategoryAxisRenderer,
                                    tickOptions: {
                                        angle: -30
                                    },
                            tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                            label: 'Day',
                            labelOptions: {
                                /*
                                 fontFamily: 'verdana',
                                 fontSize: '14pt'*/
                            },
                            labelRenderer: $.jqplot.CanvasAxisLabelRenderer
                        },
                        yaxis: {
                            renderer: $.jqplot.LogAxisRenderer,
                            tickOptions: {
                                labelPosition: 'middle',
                                angle: -30
                            },
                            tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                            labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                            labelOptions: {
                                fontFamily: 'Helvetica',
                                fontSize: '14pt'
                            },
                            label: 'Incident'
                        }
                    }
                };
                somePlot = $.jqplot('chart1', numberOfSymptoms["lineValue"], plotOptions);


                $("#search").click(function () {



                    var successCallBack = function (response) {

                        console.log(response);
                        var response = JSON.parse(response);

                        somePlot.destroy();
                        somePlot = $.jqplot('chart1', response["lineValue"], plotOptions);




                    }
                    //var removeUid = $(this).attr('value');

                    ajaxForm("webservice/report/info.php", $("#viewReportForm").serialize(), false, successCallBack);



                });



            });</script>
        <style>
            .submenu1{display:none;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:block;}
            .submenu5{display:none;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Report<small> - View report</small></h1>
                            <!--logout home button-->
                            <?php include $path . '/include/homebutton.php'; ?>
                        </div>
                    </div>
                    <!---內容-->

                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form id="viewReportForm" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >Start Date</label>
                                            <?= $data["startDate"] ?>
                                        </div>


                                        <div class="form-group col-xs-6">
                                            <label >End Date</label>
                                            <?= $data["endDate"] ?>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label >Report Type</label>
                                            <?= $data["reportType"] ?>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12" >
                                            <button id="search" type="submit" class="btn btn-success" >
                                                Search
                                            </button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <!--report here-->

                                <div  id="chart1"  ></div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </body>

</html>


<script type="text/javascript" src="<?= $path ?>/js/jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.cursor.min.js"></script>
<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.enhancedLegendRenderer.min.js"></script>

<script type="text/javascript" src="<?= $path ?>/js/jqplot/plugins/jqplot.pointLabels.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $path ?>/js/jqplot/jquery.jqplot.min.css" />

