<?php $path='';?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?= $path;?>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= $path;?>/bootstrap/css/datetimepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?= $path;?>/styles.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<?= $path;?>/jquery-2.1.0.js"></script>
<script src="<?= $path;?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= $path;?>/bootstrap/js/moment.js"></script>
<script src="<?= $path;?>/bootstrap/js/datetimepicker.min.js"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({ format: 'YYYY-MM-DD',minDate:new Date() });
            });
			 $(function () {
                $('#datetimepicker3').datetimepicker({ format: 'YYYY-MM-DD',minDate:new Date() });
            });
</script>
<style>
.submenu1{display:none;}
.submenu2{display:block;}
.submenu3{display:none;}
.submenu4{display:none;}
.submenu5{display:none;}
</style>
<title>Staff portal | DBP Hospital</title>
</head>


<body>
<div class="container">

<div class="row">
<!---左邊--->
<div class="col-xs-3" >
<center>
<!--插入歡迎box+menu-->
<?php include'../welcomebox.php'; ?>
	<?php include '../menu.php'; ?>
  </center>
</div>

<!---右邊--->
<div class="col-xs-9">
<!---標題-->
<div class="row">
<div class="page-header">
  <h1>Edit Invoice<small> - invoice</small></h1>
  <div class="pull-right">
  <a class="btn btn-success " href="<?= $path;?>/index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
  <a class="btn btn-danger" href="<?= $path;?>/login.php" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
  </div>
</div>
</div>
<!---內容-->
<div class="row" style="padding-top:20px;">
<div class="panel panel-warning">
  <div class="panel-heading">
  <form >
  <div class="row">
  <div class="form-group col-xs-6">
    <label for="exampleInputEmail1">Invoice ID</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Patient ID">
  </div>
  <div class="col-xs-2" style="padding-top:25px;">
    <a style="cursor:pointer" onClick="a0911.style.display=a0911.style.display=='none'?'':'none'" class="btn btn-default">Search</a>
  </div>
</div>
</form>
  </div>
</div>
</div>

<div class="row" style="padding-top:20px;">
<div class="panel panel-success">
  <div class="panel-heading">
  <div class="page-header">
  <h3>Invoice infortmation<small> - search result</small></h3>
  </div>
  </div>
  <div class="panel-body">
  <div id="a0911" style="display:none">
   <form >
<div class="row">
<div class="form-group col-xs-6">
    <label for="exampleInputEmail1">Invoice ID</label>
    <input type="email" class="form-control"  placeholder="Invoice ID" disabled>
  </div>
</div>
<div class="row">
<table class="table" border="1" width="700" >
<tr align="center" class="info" bgcolor="lightblue"><td><b>Product</b></td><td colspan="2"><b>Description</b></td><td><b>Price($)</b></td></tr>
<tr align="center" height="30"><td><input type="email" class="form-control"  Value="Room" ></td>
<td colspan="2"><input type="email" class="form-control" Value="Room:b01 , Type:High Class , Day:3" ></td>
<td><input type="email" class="form-control"  Value="4000" ></td></tr>
<tr align="center" height="30"><td><input type="email" class="form-control"  Value="Drug" ></td>
<td colspan="2"><input type="email" class="form-control"  Value="none" ></td>
<td><input type="email" class="form-control"  Value="0" ></td></tr>
<tr align="center" height="30"><td><input type="email" class="form-control"  Value="Surgery" ></td>
<td colspan="2"><input type="email" class="form-control"  Value="Big Surgery" ></td>
<td><input type="email" class="form-control"  Value="20000" ></td></tr>
<tr align="center" height="30"><td><input type="email" class="form-control"  Value="" ></td><td colspan="2"><input type="email" class="form-control"  Value="" ></td><td><input type="email" class="form-control"  Value="" ></td></tr>
<tr align="center" height="30"><td><input type="email" class="form-control"  Value="" ></td><td colspan="2"><input type="email" class="form-control"  Value="" ></td><td><input type="email" class="form-control"  Value="" ></td></tr>
<tr align="center" height="30"><td><input type="email" class="form-control"  Value="" ></td><td colspan="2"><input type="email" class="form-control"  Value="" ></td><td><input type="email" class="form-control"  Value="" ></td></tr>
</table>
</div>
<div class="row">
<a type="submit" class="btn btn-default" href="edit.php">SAVE</a>
</div>
</form>
  </div>
  </div>
</div>
  </div>
</div>
</div>


</div>
</div>
</div>
</body>

</html>