<?php 

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
?><!DOCTYPE html>
<html>
<head>
        <?php include_once $path . '/include/header.php'; ?>

<script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({ format: 'YYYY-MM-DD',minDate:new Date() });
                $('#datetimepicker3').datetimepicker({ format: 'YYYY-MM-DD',minDate:new Date() });
           
			 $('#searchinoviceId').click(function(){
                    

                });
		   
		   });
</script>
<style>
.submenu1{display:none;}
.submenu2{display:block;}
.submenu3{display:none;}
.submenu4{display:none;}
.submenu5{display:none;}
</style>
<title>Staff portal | DBP Hospital</title>
</head>


<body>
<div class="container">

<div class="row">
<!---左邊--->
<div class="col-xs-3" >
<center>
<!--插入歡迎box+menu-->
<?php include $path.'/include/welcomebox.php'; ?>
	<?php include $path.'/include/menu.php'; ?>
  </center>
</div>

<!---右邊--->
<div class="col-xs-9">
<!---標題-->
<div class="row">
<div class="page-header">
  <h1>Check Invoice<small> - Invoice</small></h1>
  <div class="pull-right">
  <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
  <a class="btn btn-danger" href="<?= $path;?>/login.php" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
  </div>
</div>
</div>
<!---內容-->
<div class="row" style="padding-top:20px;">
<div class="panel panel-warning">
  <div class="panel-heading">
  <form id="invoiceIdSearch" data-toggle="validator"  method="post" onsubmit="return false">
  <div class="row">
  <div class="form-group col-xs-6">
    <label for="exampleInputEmail1">Invoice ID</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Patient ID">
  </div>
  <div class="col-xs-2" style="padding-top:25px;">
     <a style="cursor:pointer" onClick="a0912.style.display=a0912.style.display=='none'?'':'none'" class="btn btn-default">Search</a>
  </div>
</div>
</form>
</div>
</div>
</div>

<div class="row" style="padding-top:20px;">
<div class="panel panel-success">
  <div class="panel-heading">
  <div class="page-header">
  <h3>Invoice<small> - search result</small></h3>
    <div class="pull-right">
  <a class="btn btn-danger " href="<?= $path;?>/invoice/check.php" role="button" >Print it out <span class="glyphicon glyphicon-print"></span></a>
  </div>
  </div>
  </div>
  <div class="panel-body">
  <div id="a0912" style="display:none">
 <?php include $path.'/invoice/invoice.php';?>
 </div>
 </div>
 </div>
 </div>
 </div>

</div>
</div>
</body>

</html>