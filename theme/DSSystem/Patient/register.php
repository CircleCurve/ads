<?php
//$path = '/DSSystem/theme'; 

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$systemUser = new SystemUser();
?>


<!DOCTYPE html>
<html>
    <head>

        <?php include_once $path . '/include/header.php'; ?>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD'});
                
                
                $('#submit').click(function(){
                    
                   ajax("webservice/patient/register.php" , $("#patientRegisterForm").serialize() , false) ; 

                });
            });

        </script>

        <style>
            .submenu1{display:none;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:block;}
        </style>
        <title>Staff portal | DBP Hospital</title>

    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>New Register<small> - Patient</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="?act=main-logout" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form  id="patientRegisterForm" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="inputName">First Name</label>
                                            <input type="text" name="patientFirstName" pattern="<?= $data["patientFirstName"]["regex"] ?>" data-error="<?= $data["patientFirstName"]["message"] ?>" class="form-control" id="FirstName" placeholder="First Name" required/>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="inputName">Last Name</label>
                                            <input type="text" name="patientLastName" pattern="<?= $data["patientLastName"]["regex"] ?>" data-error="<?= $data["patientLastName"]["message"] ?>" class="form-control" id="LastName" placeholder="Last Name" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="hkid">HKID Card Number</label>
                                            <input type="text" name="HKID" pattern="<?= $data["HKID"]["regex"] ?>" data-error="<?= $data["HKID"]["message"] ?>" class="form-control" id="hkid" placeholder="HKID Card Number" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="phoneNumber">Phone Number</label>
                                            <input type="text" name="phoneNumber" pattern="<?= $data["phoneNumber"]["regex"] ?>" data-error="<?= $data["phoneNumber"]["message"] ?>" class="form-control" id="phone" placeholder="Phone Number" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <label for="address">Address</label>
                                            <input type="text" name="address" pattern="<?= $data["address"]["regex"] ?>" data-error="<?= $data["address"]["message"] ?>" class="form-control" id="address" placeholder="Address" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="emContactPerson">Emergency Contact Person</label>
                                            <input type="text" name="emContactPerson" pattern="<?= $data["emContactPerson"]["regex"] ?>" data-error="<?= $data["emContactPerson"]["message"] ?>" class="form-control" id="ecperson" placeholder="Emergency Contact Person" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="emContactPhone">Emergency Contact Phone</label>
                                            <input type="text" name="emContactPhone" pattern="<?= $data["emContactPhone"]["regex"] ?>" data-error="<?= $data["emContactPhone"]["message"] ?>"  class="form-control" id="ecphone" placeholder="Emergency Contact Phone" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="birthday">Birthday Date</label>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type="text" name="birthday" pattern="<?= $data["birthday"]["regex"] ?>" data-error="<?= $data["birthday"]["message"] ?>" class="form-control" required/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>			
                                    <button id="submit" type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>

</html>