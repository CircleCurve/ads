<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$systemUser = new SystemUser();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});

                $('#submit').click(function () {


                   ajax("webservice/patient/addPhysicalCheck.php", $("#physicalCheckForm").serialize(), false);

                }
                );
            });

        </script>
        <style>
            .submenu1{display:none;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:block;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Import physical check<small> - Patient</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="?act=main-logout" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->
                    <form  id="physicalCheckForm" data-toggle="validator"  method="post" onsubmit="return false">

                        <div class="row" style="padding-top:20px;">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <table class="table table-bordered">
                                        <tr class="info">
                                            <td>InPatient ID</td>
                                            <td>Date time</td>
                                            <td>Height(cm)</td>
                                            <td>Weight(kg)</td>
                                            <td>Systolic</td>
                                            <td>Diastolic</td>
                                            <td>Staff</td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <input id="patientId" name="patientId" type="text" class="form-control">
                                            </td>
                                            <td width="30%">
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input id="startDate" name="startDate" type='text' class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </td>
                                            <td width="10%">
                                                <input id="height" name="height" class="form-control">
                                            </td>
                                            <td width="10%">
                                                <input id="weight" name="weight" class="form-control">
                                            </td>
                                            <td width="10%">
                                                <input id="systolic" name="systolic" class="form-control">
                                            </td>
                                            <td width="10%">
                                                <input id="diastolic" name="diastloic" class="form-control">
                                            </td>
                                            <td width="10%">
                                                <input id="staffId" name="staffid" value="<?= $systemUser->getSession("userName") ?>"  class="form-control" readonly>
                                            </td>
                                        </tr>
                                    </table>

                                    <button class="btn btn-primary" id="submit"  role="button">Save 
                                        <span class="glyphicon glyphicon-floppy-save"></span>
                                    </button>			

                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>


        </div>
    </div>
</div>
</body>

</html>