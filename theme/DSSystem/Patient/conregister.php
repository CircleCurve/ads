<?php $path='/DSSystem/theme';?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?= $path;?>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= $path;?>/bootstrap/css/datetimepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?= $path;?>/styles.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<?= $path;?>/jquery-2.1.0.js"></script>
<script src="<?= $path;?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= $path;?>/bootstrap/js/moment.js"></script>
<script src="<?= $path;?>/bootstrap/js/datetimepicker.min.js"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({ format: 'YYYY-MM-DD' });
            });
			
</script>

<style>
.submenu1{display:none;}
.submenu2{display:none;}
.submenu3{display:none;}
.submenu4{display:none;}
.submenu5{display:block;}
</style>
<title>Staff portal | DBP Hospital</title>

</head>


<body>
<div class="container">

<div class="row">
<!---左邊--->
<div class="col-xs-3" >
<center>
<!--插入歡迎box+menu-->
<?php //include'../welcomebox.php'; ?>
	<?php //include '../menu.php'; ?>
  </center>
</div>

<!---右邊--->
<div class="col-xs-9">
<!---標題-->
<div class="row">
<div class="page-header">
  <h1>Confirm Register<small> - Patient</small></h1>
  <div class="pull-right">
  <a class="btn btn-success " href="<?= $path;?>/index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
  <a class="btn btn-danger" href="<?= $path;?>/login.php" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
  </div>
</div>
</div>
<!---內容-->
<div class="row" style="padding-top:20px;">
<div class="panel panel-warning">
  <div class="panel-heading">
  <form>
<div class="row">
  <div class="form-group col-xs-6">
    <label for="exampleInputEmail1">First Name</label>
    <input class="form-control" id="FirstName" placeholder="<?php echo'data';?>" disabled>
  </div>
  <div class="form-group col-xs-6">
    <label for="exampleInputPassword1">Last Name</label>
    <input class="form-control" id="LastName" placeholder="<?php echo'data';?>" disabled >
  </div>
</div>
  <div class="row">
  <div class="form-group col-xs-6">
    <label for="exampleInputEmail1">HKID Card Number</label>
    <input class="form-control" id="hkid" placeholder="<?php echo'data';?>"disabled>
  </div>
  <div class="form-group col-xs-6">
    <label for="exampleInputPassword1">Phone Number</label>
    <input class="form-control" id="phone" placeholder="<?php echo'data';?>" disabled>
  </div>
</div>

  <div class="row">
  <div class="form-group col-xs-12">
    <label for="exampleInputEmail1">Address</label>
    <input class="form-control" id="address" placeholder="<?php echo'data';?>" disabled>
  </div>
</div>

 <div class="row">
  <div class="form-group col-xs-6">
    <label for="exampleInputEmail1">Emergency Contact Person</label>
    <input class="form-control" id="ecperson" placeholder="<?php echo'data';?>" disabled>
  </div>
    <div class="form-group col-xs-6">
    <label for="exampleInputPassword1">Emergency Contact Phone</label>
    <input class="form-control" id="ecphone" placeholder="<?php echo'data';?>" disabled>
  </div>
</div>
 <div class="row">
<div class="form-group col-xs-6">
				<label for="exampleInputPassword1">Birthday Date</label>
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" placeholder="<?php echo'data';?>" disabled/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
			
			<div class="form-group col-xs-6">
				<label for="exampleInputPassword1">Patient ID</label>
                    <input type='text' class="form-control" placeholder="<?php echo'data';?>" disabled/>
            </div>
</div>

  <a class="btn btn-primary" href="<?= $path;?>/Patient/register.php" role="button">Add another patient <span class="glyphicon glyphicon-plus"></span></a>			
</form>
  </div>
</div>
</div>
</div>
</body>

</html>