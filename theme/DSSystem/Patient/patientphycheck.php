<?php


if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$systemUser = new SystemUser();
?>
<!DOCTYPE html>
<html>
<head>
        <?php include_once $path . '/include/header.php'; ?>

<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({ format: 'YYYY-MM-DD',minDate:new Date() });
            });

</script>
<style>
.submenu1{display:none;}
.submenu2{display:none;}
.submenu3{display:none;}
.submenu4{display:none;}
.submenu5{display:block;}
</style>
<title>Staff portal | DBP Hospital</title>
</head>


<body>
<div class="container">

<div class="row">
<!---左邊--->
<div class="col-xs-3" >
<center>
<!--插入歡迎box+menu-->
<?php include $path.'/include/welcomebox.php'; ?>
	<?php include $path.'/include/menu.php'; ?>
  </center>
</div>

<!---右邊--->
<div class="col-xs-9">
<!---標題-->
<div class="row">
<div class="page-header">
  <h1>Import physical check<small> - Patient</small></h1>
  <div class="pull-right">
  <a class="btn btn-success " href="<?= $path;?>/index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
  <a class="btn btn-danger" href="<?= $path;?>/login.php" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
  </div>
</div>
</div>
<!---內容-->
<div class="row" style="padding-top:20px;">
<div class="panel panel-warning">
  <div class="panel-heading">
  <table class="table table-bordered">
<tr class="info"><td>Patient ID</td><td>Date time</td><td>Height(cm)</td><td>Weight(kg)</td><td>Staff</td></tr>
<tr><td width="20%"><input class="form-control"></td>
<td width="20%"><div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div></td><td width="20%"><input class="form-control"></td><td width="20%"><input class="form-control"></td><td width="20%"><input class="form-control"></td></tr>
</table>

  <a class="btn btn-primary" href="<?= $path;?>/Patient/Inportphycheck.php" role="button">Save <span class="glyphicon glyphicon-floppy-save"></span></a>			

  </div>
</div>
</div>

</div>
</div>


</div>
</div>
</div>
</body>

</html>