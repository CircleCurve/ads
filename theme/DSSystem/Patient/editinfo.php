<?php
//$path = '/DSSystem/theme'; 

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$systemUser = new SystemUser();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});
                $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});


                $('#searchPatientId').click(function(){
                    
                   ajaxForm("webservice/patient/info.php" , $("#patientIdSearch").serialize() , false,$("#patientSearchResult")) ; 

                });
            });

        </script>
        <style>
            .submenu1{display:none;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:block;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Edit Patient information<small> - Patient</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="?act=main-logout" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->

                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>Patient infortmation<small> - search result</small></h3>
                                </div>
                            </div>
                            <div class="panel-body">
                                    <form id ="#patientInfoResult">
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputEmail1">First Name</label>

                                                <input class="form-control" id="FirstName" placeholder="<?php echo'data'; ?>"  >
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Last Name</label>
                                                <input class="form-control" id="LastName" placeholder="<?php echo'data'; ?>"   >
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputEmail1">HKID Card Number</label>
                                                <input class="form-control" id="hkid" placeholder="<?php echo'data'; ?>" >
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Phone Number</label>
                                                <input class="form-control" id="phone" placeholder="<?php echo'data'; ?>"  >
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12">
                                                <label for="exampleInputEmail1">Address</label>
                                                <input class="form-control" id="address" placeholder="<?php echo'data'; ?>"  >
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputEmail1">Emergency Contact Person</label>
                                                <input class="form-control" id="ecperson" placeholder="<?php echo'data'; ?>"  >
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Emergency Contact Phone</label>
                                                <input class="form-control" id="ecphone" placeholder="<?php echo'data'; ?>"  >
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Birthday Date</label>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' class="form-control" placeholder="<?php echo'data'; ?>"  />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Patient ID</label>
                                                <input type='text' class="form-control" placeholder="<?php echo'data'; ?>"  disabled/>
                                            </div>
                                        </div>
									<button class="btn btn-primary" role="button" ><span class="glyphicon glyphicon-floppy-save"></span> Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</body>

</html>