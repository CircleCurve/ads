<?php
//$path = '/DSSystem/theme'; 

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$systemUser = new SystemUser();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD'});
                $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});


                $('#editInfo').click(function () {
                    $("#firstName").removeAttr('disabled');
                    $("#lastName").removeAttr('disabled');

                    $("#phone").removeAttr('disabled');
                    $("#address").removeAttr('disabled');

                    $("#dateOfBirth").removeAttr("disabled");
                    $("#emContactPerson").removeAttr('disabled');
                    $("#emContactPhone").removeAttr('disabled');

                    $("#editInfo").css("display", "none");
                    $("#confirm").css("display", '');


                });

                $("#confirm").click(function () {
                    var successCallBack = function (response) {
                        console.log(response);

                        var response = JSON.parse(response);

                        if (response["status"] === "1") {

                            //$(key).val(response["result"][key]); 
                            $("#firstName").attr('disabled', true);
                            $("#lastName").attr('disabled', true);

                            $("#phone").attr('disabled', true);
                            $("#dateOfBirth").attr('disabled', true);
                            $("#address").attr("disabled", true);
                            $("#emContactPerson").attr('disabled', true);
                            $("#emContactPhone").attr('disabled', true);
                            $("#id").attr('readonly', true);

                            $("#editInfo").css("display", "");

                            $("#confirm").css("display", "none");


                        } else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }
                    ajaxForm("webservice/patient/updatePatientInfo.php", $("#patientInfoResult").serialize(), false, successCallBack);


                });

                $('#deletePatient').click(function () {
                    console.log("deleteBooking");

                    var successCallBack = function (response) {
                        console.log(response);

                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            $("#patientSearchResult").css("display", 'none');

                            alert("status : " + response["status"] + "  Message: " + response["msg"]);


                            /*
                             $("#LastName").val("") ; 
                             $("#hkid").val("") ; 
                             $("phone").val("") ; */

                        } else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }
                    ajaxForm("webservice/patient/deletePatient.php", $("#patientInfoResult").serialize(), false, successCallBack);

                });

                $('#searchPatientId').click(function () {
                    var successCallBack = function (response) {
                        console.log(response);

                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            $("#patientSearchResult").css("display", '');

                            for (var key in response["result"]) {

                                //$(key).val(response["result"][key]); 
                                if (document.getElementById(key) !== null)
                                    document.getElementById(key).value = response["result"][key];
                            }

                            var htmltr = "", htmlKey = "", loop = '<tr class="info"><td>Booking id</td><td>InPatientId id</td><td>into the hospital date</td><td>Leave the hospital date</td><td>booking time</td><td>details</td></tr>';
                            for (var historyCount in response["history"]) {
                                loop += "<tr class='InpatientHtml"+historyCount+"'>" ; 
                                for (var historyKey in response["history"][historyCount]) {
                                    htmlKey = historyKey + "Html";

                                        loop += "<td id='" + htmlKey + "'>" + response["history"][historyCount][historyKey] + "</td>";
                                        //document.getElementById("InpatientHtml").innerHTML = loop;
                                }
                                loop += "</tr>" ; 

                            }
                            document.getElementById("patientHistory").innerHTML = loop ; 
                            
                            /*
                             $("#LastName").val("") ; 
                             $("#hkid").val("") ; 
                             $("phone").val("") ; */

                        } else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }


                    ajaxForm("webservice/patient/info.php", $("#patientIdSearch").serialize(), false, successCallBack);

                });
            });

        </script>
        <style>
            .submenu1{display:none;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:block;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Patient information<small> - Patient</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="?act=main-logout" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form  id="patientIdSearch" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="inputId">Patient ID</label>
                                            <input type="text" name="patientId" pattern="<?= $data["patientId"]["regex"] ?>"  data-error="<?= $data["patientId"]["message"] ?>"   class="form-control" id="patientId" placeholder="Patient ID" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>

                                        <div class="col-xs-2" style="padding-top:25px;">
                                            <!-- <a id="searchPatientId" style="cursor:pointer" onClick="a0911.style.display = a0911.style.display == 'none' ? '' : 'none'" class="btn btn-default">Search</a>
                                            -->
                                            <button id="searchPatientId" type="submit" class="btn btn-default">Search</button>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>Patient infortmation<small> - search result</small></h3>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div  id="patientSearchResult" style="display:none">
                                    <form  id="patientInfoResult" data-toggle="validator"  method="post" onsubmit="return false">
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputEmail1">First Name</label>

                                                <input class="form-control" id="firstName" name="firstName" placeholder="<?php echo'data'; ?>"  disabled>
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Last Name</label>
                                                <input class="form-control" id="lastName" name="lastName" placeholder="<?php echo'data'; ?>" disabled >
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputEmail1">HKID Card Number</label>
                                                <input class="form-control" id="hkid" name="hkid" placeholder="<?php echo'data'; ?>"disabled>
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Phone Number</label>
                                                <input class="form-control" id="phone" name="phone" placeholder="<?php echo'data'; ?>" disabled>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12">
                                                <label for="exampleInputEmail1">Address</label>
                                                <input class="form-control" id="address" name="address" placeholder="<?php echo'data'; ?>" disabled>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputEmail1">Emergency Contact Person</label>
                                                <input class="form-control" id="emContactPerson" name="emContactPerson" placeholder="<?php echo'data'; ?>" disabled>
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Emergency Contact Phone</label>
                                                <input class="form-control" id="emContactPhone" name="emContactPhone" placeholder="<?php echo'data'; ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Birthday Date</label>
                                                <div class='input-group date' id='datetimepicker2'>
                                                    <input type='text' class="form-control" id="dateOfBirth" name="dateOfBirth" placeholder="<?php echo'data'; ?>" disabled/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Patient ID</label>
                                                <input type='text' class="form-control" id="id" name="id" placeholder="<?php echo'data'; ?>" readonly=""/>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label for="exampleInputPassword1">Status</label>
                                                <input type='text' id="healthStatus" name="healthStatus" class="form-control" placeholder="<?php echo'Hospitalized'; ?>" disabled/>
                                            </div>
                                        </div>
                                        <button id="confirm" class="btn btn-success" style="display:none">
                                            Confirm
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>

                                        <button id="editInfo" class="btn btn-primary" href="?act=patient-editinfo" role="button">
                                            Edit Info 
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>	
                                        <button id="deletePatient" type="submit" class="btn btn-danger" >
                                            Delete Patient
                                        </button>
                                    </form>
                                    <div style="padding-top:20px;"></div>
                                    <table class="table table-bordered" id="patientHistory">


                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</body>

</html>