<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$systemUser = new SystemUser();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});
                $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});

                $('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
                
                var isChecked = "" ; 
                $('#doctordis').click(function(){
                   
                    isChecked = $('#doctordis').is(":checked") ; 
                    
                    if (isChecked == true){
                        $("#dis").removeAttr("disabled") ; 
                    }else{
                        $("#dis").attr('disabled', true);
                    }

                }) ; 
                
                $("#SavePaymentStatus").click(function(){
                    console.log("Test") ; 
                    alert ("Discharge Success") ; 
                }); 
            });
        </script>
        <style>
            .submenu1{display:none;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:block;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Inpatient information details<small> - Patient</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="?act=main-logout" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>All physical check by inpatient date <? echo 'date'; ?> </h3>
                                </div>
                                <table class="table table-bordered">
                                    <tr class="info" align="center">
                                        <td>Date time</td>
                                        <td>Height(cm)</td>
                                        <td>Weight(kg)</td>
                                        <td>Diastolic</td>
                                        <td>Systolic</td>
                                        <td>Staff</td>
                                    </tr>

                                    <?
                                    if (isset($data["physicalCheck"]["result"])) {
                                        for ($i = 0; $i < count($data["physicalCheck"]["result"]); $i++) {
                                            echo '<tr align="center">';

                                            echo '<td>' . $data["physicalCheck"]["result"][$i]["checkTime"] . '</td>';
                                            echo '<td>' . $data["physicalCheck"]["result"][$i]["height"] . '</td>';

                                            echo '<td>' . $data["physicalCheck"]["result"][$i]["weight"] . '</td>';

                                            echo '<td>' . $data["physicalCheck"]["result"][$i]["systolic"] . '</td>';
                                            echo '<td>' . $data["physicalCheck"]["result"][$i]["diastolic"] . '</td>';
                                            echo '<td>' . $data["physicalCheck"]["result"][$i]["userName"] . '</td>';
                                        
                                            echo '</tr>';

                                        }
                                    }
                                    ?>

                                </table>
                            </div>
                        </div>

                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>Inpatient Record  <? echo 'date'; ?> </h3>
                                </div>
                                <table class="table table-bordered">
                                    <tr class="info" align="center">
                                        <td>Service Name</td>
                                        <td>Service type</td>
                                        <td>Description</td>
                                        <td>Quantity</td>
                                        <td>Price</td>
                                        <td>Delete</td>
                                    </tr>
                                    <tr align="center">
                                        <td> Big Surgery</td>
                                        <td>surgery</td>
                                        <td>.....</td>
                                        <td>1</td>
                                        <td>10500</td>
                                        <td >
                                            <a href="" >
                                                <span class="label label-danger glyphicon glyphicon-trash" style="font-size:14px;"> </span>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <button class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-plus"></span> Add service</button>
                            </div>
                        </div>


                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>Payment Status  <? echo 'date'; ?> </h3>
                                </div>
                                <table class="table table-bordered">
                                    <tr class="info" align="center">
                                        <td>Payment Method</td>
                                        <td>Doctor SignOff</td>
                                        <td>Discharge</td>
                                    </tr>
                                    <tr align="center">
                                        <td><select class="form-control" id="payment">
                                                <option></option>
                                                <option>Cash</option>
                                                <option>PayPal</option>
                                            </select>
                                        </td>
                                        <td><input type="checkbox" id="doctordis" value="option1" >
                                        </td>
                                        <td><input type="checkbox" id="dis" value="option2" disabled>
                                        </td></tr>
                                </table>
                                <button id="SavePaymentStatus" class="btn btn-primary" role="button" >
                                    <span class="glyphicon glyphicon-floppy-save"></span> Save</button>
                            </div>
                        </div>

                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>Health Status Details<? echo 'date'; ?> </h3>
                                </div>
                                <table class="table table-bordered">
                                    <tr class="info" align="center">
                                        <td>Health Status</td>
                                        <td>Start time</td>
                                    </tr>
                                    <tr align="center">
                                        <td>GOOD</td>
                                        <td>09:00</td>
                                    </tr>
                                </table>

                                <button class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus"></span> Add service</button>
                            </div>
                        </div>

                    </div>


                </div>


            </div>
        </div>


    </div>
</div>
</div>
</body>


<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add inpatient service</h4>
            </div>
            <div class="modal-body">
                <form id="" data-toggle="validator"  method="post" onsubmit="return false" style="padding-bottom:10px;">
                    <table class="table table-bordered">
                        <tr class="info" align="center"><td>Service Name</td><td>Service type</td><td>Description</td><td>Quantity</td><td>Price</td></tr>
                        <tr align="center">
                            <td><input name=""/></td>
                            <td><input name=""/></td>
                            <td><input name="" /></td>
                            <td><input name="" /></td>
                            <td><input name="" /></td>
                        </tr>
                    </table>
                    <button class="btn btn-primary" id="" type="submit">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Save
                    </button>
                </form >
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Health status</h4>
            </div>
            <div class="modal-body">
                <form id="" data-toggle="validator"  method="post" onsubmit="return false" style="padding-bottom:10px;">
                    <table class="table table-bordered">
                        <tr class="info" align="center">
                            <td>Health Status</td>
                            <td>Start time</td></tr>
                        <tr align="center">
                            <td><input id="healthStatus" name="healthStatus"/></td>
                            <td>
                                <div class="row">
                                    <div class='col-sm-12'>
                                        <div class="form-group">
                                            <div class='input-group date' id='datetimepicker4'>
                                                <input id="healthStatusStartTime" name="healthStatusStartTime" type='text' class="form-control" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <button class="btn btn-primary" id="" type="submit"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                </form >
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

</html>