<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});
                $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});

                $('#addBooking').click(function () {

                    var successCallBack = function (response) {
                        console.log (response) ; 
                        var response = JSON.parse(response);

                        alert("status : " + response["status"] + "  Message: " + response["msg"]);

                        if (response["status"] === "1") {
                            location.href="index.php?act=booking-info&bookingId="+response["result"]["bookingId"];

                        }
                        
                    };
                    ajaxForm("webservice/booking/newbooking.php", $("#newbookingform").serialize(), false, successCallBack);

                });


                $('#searchbooking').click(function () {

                    var successCallBack = function (response) {

                        console.log(response);

                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            $("#bookingfindresult").css("display", '');

                            for (var i = 0; i < response["result"].length; i++) {
                                for (var key in response["result"][i]) {

                                    //$(key).val(response["result"][key]); 
                                    console.log(key);
                                    if (document.getElementById(key) !== null) {
                                        document.getElementById(key).innerHTML = response["result"][i][key];
                                    }
                                }

                            }
                        }else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }
                        
                    };
                            ajaxForm("webservice/booking/searchBooking.php", $("#newbookingform").serialize(), false, successCallBack);

                });

                $('#searchpatientid').click(function () {
                    //$("#searchpatientidresult").css("display", '');

                    var successCallBack = function (response) {
                        console.log(response);

                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            $("#searchpatientidresult").css("display", '');

                            for (var key in response["result"]) {

                                //$(key).val(response["result"][key]); 
                                console.log(key);
                                if (document.getElementById(key) !== null) {
                                    if (key === "patientId") {
                                        document.getElementById(key).value = response["result"][key];
                                        document.getElementById("searchPatientIdText").innerHTML = response["result"][key];
                                    } else {
                                        document.getElementById(key).innerHTML = response["result"][key];
                                    }
                                }
                            }

                            /*
                             $("#LastName").val("") ; 
                             $("#hkid").val("") ; 
                             $("phone").val("") ; */

                        } else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }

                    ajaxForm("webservice/patient/searchPatientId.php", $("#searchpatientidform").serialize(), false, successCallBack);

                });

            });
        </script>

        <style>
            .submenu1{display:block;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:none;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row" style="padding-top:10px;">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>New Booking<small> - Booking</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="?act=main-logout" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <!----個INFO表--->
                                <form id="newbookingform" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >Booking Date</label>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" name="bookingdate" pattern="<?= $data["bookingdate"]["regex"] ?>" data-error="<?= $data["bookingdate"]["message"] ?>" value="<?php echo date("Y-m-d"); ?>" readonly/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >Income Date</label>
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' class="form-control" name="incomedate" id="incomedate" pattern="<?= $data["incomedate"]["regex"] ?>" data-error="<?= $data["incomedate"]["message"] ?>" required />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label >Out Date</label>
                                            <div class='input-group date' id='datetimepicker3'>
                                                <input type='text' class="form-control" name="outdate" pattern="<?= $data["outdate"]["regex"] ?>" data-error="<?= $data["outdate"]["message"] ?>" required/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="form-group col-xs-10">
                                            <label >Patient ID</label>
                                            <input type="text" id="patientId" name="patientId" class="form-control"  pattern="<?= $data["patientId"]["regex"] ?>" data-error="<?= $data["patientId"]["message"] ?>"  placeholder="Patient ID" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="form-group col-xs-2" style="padding-top:22px;">
                                            <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-search"></span></a>
                                        </div>
                                    </div>
                                    <? /*
                                      <div class="row">
                                      <div class="form-group col-xs-12">
                                      <label >Service</label>
                                      <ul class="nav nav-tabs">
                                      <li class="active" data-toggle="tab">
                                      <a href="#">Home</a>
                                      </li>
                                      <li><a href="#" data-toggle="tab">Menu 1</a></li>
                                      <li><a href="#" data-toggle="tab">Menu 2</a></li>
                                      <li><a href="#" data-toggle="tab">Menu 3</a></li>
                                      </ul>


                                      <select class="form-control" name="servicetype" id="servicetype" pattern="<?= $data["servicetype"]["regex"] ?>" data-error="<?= $data["servicetype"]["message"] ?>" >
                                      <option>Big Surgery</option>
                                      <option>Normal Surgery</option>
                                      <option>Small Surgery</option>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                      <span class="help-block with-errors"></span>
                                      </div>
                                      </div> */ ?>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >Bed type</label>
                                            <select class="form-control" name="bedType" pattern="<?= $data["bedType"]["regex"] ?>" data-error="<?= $data["bedType"]["message"] ?>">
                                                <?
                                                for ($i = 0; $i < count($data["bedType"]); $i++) {
                                                    echo '<option value="' . $data["bedType"][$i]["bed_type_id"] . '">';
                                                    echo $data["bedType"][$i]["name"];
                                                    echo '</option>';
                                                }
                                                ?>
                                            </select>  
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                    </div>
                                    <button id="searchbooking" type="submit" class="btn btn-default">Search all available</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!----hard code 既 結果--->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>Booking available<small> - search result</small></h3>
                                </div>
                                <div id="bookingfindresult" style="display:none">
                                    <table class="table">
                                        <tr class="info">
                                            <td>In date</td>
                                            <td>Out date</td>
                                            <td>bed type</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td id="in_date"></td>
                                            <td id="out_date"></td>
                                            <td id="bedName"></td>
                                            <td> 
                                                <button id="addBooking" type="submit" class="btn btn-default">Click to Book</button>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>

    <!-- 彈出黎 search patient id -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Search Patient ID</h4>
                </div>
                <div class="modal-body">
                    <form id="searchpatientidform" data-toggle="validator"  method="post" onsubmit="return false">
                        <div class="row">
                            <div class="form-group col-xs-6">
                                <label >HKID Card Number</label>                                
                                <input type="text" name="HKID" pattern="<?= $data["HKID"]["regex"] ?>" data-error="<?= $data["HKID"]["message"] ?>" class="form-control" id="hkid" placeholder="HKID Card Number" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group col-xs-6" style="padding-top:25px;">
                                <button id="searchpatientid" type="submit" class="btn btn-default">search</button>
                            </div>
                        </div>
                    </form>
                    <div>
                        <table class="table" id="searchpatientidresult" style="display:none;">
                            <tr >
                                <td>First Name</td>
                                <td>Last Name</td>
                                <td>Patient ID</td>
                            </tr>
                            <tr>
                                <td id="firstName"></td>

                                <td id="lastName"></td>
                                <td id="searchPatientIdText"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</html>