<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});

                $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});


                var userBookInfo = <?= json_encode($data) ?>;


                if (typeof userBookInfo["dbInfo"] !== "undefined") {

                    if (userBookInfo["dbInfo"]["status"] === "1") {

                        console.log("display");

                        var bookingResult = userBookInfo["dbInfo"]["result"];
                        $('#Searchbookingresult').css("display", '');
                        for (var key in bookingResult) {

                            //$(key).val(response["result"][key]); 
                            console.log(key);
                            if (document.getElementById(key) !== null) {
                                document.getElementById(key).value = userBookInfo["dbInfo"]["result"][key];

                            }
                        }

                    }

                }


                $('#searchbookingId').click(function () {
                    //$("#searchpatientidresult").css("display", '');

                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);
                        var userBookInfo = response;
                        
                        if (userBookInfo["status"] === "1") {


                            var bookingResult = userBookInfo["result"];
                            $('#Searchbookingresult').css("display", '');
                            for (var key in bookingResult) {

                                //$(key).val(response["result"][key]); 
                                if (document.getElementById(key) !== null) {
                                    document.getElementById(key).value = userBookInfo["result"][key];

                                }
                            }

                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }

                    ajaxForm("webservice/booking/searchBookingId.php", $("#BookingIdSearch").serialize(), false, successCallBack);

                });
                
                $("#deleteBooking").click(function(){
                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);
                        var userBookInfo = response;
                        
                        if (userBookInfo["status"] === "1") {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            location.href="index.php?act=booking-info";
                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }
                    ajaxForm("webservice/booking/deleteBooking.php", $("#SearchbookingForm").serialize(), false, successCallBack);

                });

            });
        </script>
        <style>
            .submenu1{display:block;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:none;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Booking information<small> - Booking</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="<?= $path; ?>/index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="<?= $path; ?>/login.php" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form id="BookingIdSearch" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >Booking ID</label>
                                            <input type='text' pattern="<?= $data["bookingId"]["regex"] ?>" data-error="<?= $data["bookingId"]["message"] ?>" name="bookingId" class="form-control" value="<?= $data["GET"]["bookingId"] ?>" placeholder="Booking ID" required>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>
                                        <div class="col-xs-2" style="padding-top:25px;">
                                            <button id="searchbookingId" type="submit" class="btn btn-default">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="page-header">
                                    <h3>Booking infortmation<small> - search result</small></h3>
                                </div>
                                <form id="SearchbookingForm" data-toggle="validator"  method="post" onsubmit="return false">

                                    <div id="Searchbookingresult" style="display:none">
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label >Booking Id</label>
                                                <input type='text' name="bookingId" id="bookingId" class="form-control"  readonly/>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>


                                            <div class="form-group col-xs-6">
                                                <label >Booking Date</label>
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' name="bookingDate" id="bookingDate" class="form-control"  disabled/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label >Income Date</label>
                                                <div class='input-group date' id='datetimepicker2'>
                                                    <input name="incomedate" id="incomedate" type='text' class="form-control" disabled/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <label >Out Date</label>
                                                <div class='input-group date' id='datetimepicker3'>
                                                    <input name="outdate" type='text' id="outdate" class="form-control" disabled/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                <span class="help-block with-errors"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label >Patient FullName</label>
                                                <input class="form-control" name="fullName" id="fullName" placeholder="Patient Name" disabled>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>

                                            <div class="form-group col-xs-4">
                                                <label >Patient ID</label>
                                                <input class="form-control" name="patientId" id="patientId" placeholder="Patient ID" disabled>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-6">
                                                <label >Room Type</label>
                                                <input class="form-control" name="roomType" id="roomType" placeholder="Room Type" disabled>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>

                                            <div class="form-group col-xs-6">
                                                <label >Floor</label>
                                                <input class="form-control" name="floor" id="floor" placeholder="Room floor" disabled>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>

                                        </div>

                                        <div class="row">
                                            <div class="form-group col-xs-12">
                                                <label >Bed type</label>
                                                <input class="form-control" name="bedType" id="bedType" placeholder="Bed type" disabled>
                                            </div>
                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                            <span class="help-block with-errors"></span>
                                            

                                        </div>
                                        <button type="submit" id="deleteBooking" class="btn btn-danger" >Delete Booking</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</body>

</html>