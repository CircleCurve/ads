<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD'});
            });

        </script>

        <style>
            .submenu1{display:block;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:none;}
        </style>
        <title>Staff portal | DBP Hospital</title>

    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Confirm Register<small> - Booking</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="<?= $path; ?>/index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="<?= $path; ?>/login.php" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form >
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputPassword1">Booking Date</label>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' class="form-control" value="<?php echo date("Y-m-d"); ?>" disabled/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputPassword1">Income Date</label>
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' class="form-control" disabled/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputPassword1">Out Date</label>
                                            <div class='input-group date' id='datetimepicker3'>
                                                <input type='text' class="form-control" disabled/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Patient Name</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Patient Name" disabled>
                                        </div>
                                        <div class="form-group col-xs-4">
                                            <label for="exampleInputPassword1">Patient ID</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Patient ID" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Booking ID</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Booking ID" disabled>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Bed Reservation Number</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Bed Reservation Number" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Room Number</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Room Number" disabled>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Service Name</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Service Name" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Service Price</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Service Price" disabled>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Service Name</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Room Number" disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Bed type</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Bed type" disabled>
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for="exampleInputEmail1">Invoice ID</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Invoice ID" disabled>
                                        </div>
                                    </div>

                                    <a class="btn btn-primary" href="?act=booking-newbooking" role="button">Add another Booking <span class="glyphicon glyphicon-plus"></span></a>			
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>

</html>