<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>

        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});
                $('#datetimepicker3').datetimepicker({format: 'YYYY-MM-DD', minDate: new Date()});



                $('#changePatientStatus').click(function () {
                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);
                        var userBookInfo = response;


                        alert("status : " + response["status"] + "  Message: " + response["msg"] + "\n Invoice id : 0001");
                        
                    }

                    ajaxForm("webservice/booking/inHostpital.php", $("#bookingIdCheckInResult").serialize(), false, successCallBack);

                });


                $('#searchbookingId').click(function () {
                    //$("#searchpatientidresult").css("display", '');

                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);
                        var userBookInfo = response;

                        if (userBookInfo["status"] === "1") {


                            var bookingResult = userBookInfo["result"];
                            $("#checkInBooking").css("display", "");
                            for (var key in bookingResult) {

                                //$(key).val(response["result"][key]); 
                                if (document.getElementById(key) !== null) {
                                    document.getElementById(key).value = userBookInfo["result"][key];

                                }
                            }

                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }

                    ajaxForm("webservice/booking/searchBookingIdFromCheckIn.php", $("#searchBookingIdForm").serialize(), false, successCallBack);

                });

            });
        </script>
        <style>
            .submenu1{display:block;}
            .submenu2{display:none;}
            .submenu3{display:none;}
            .submenu4{display:none;}
            .submenu5{display:none;}
        </style>
        <title>Staff portal | DBP Hospital</title>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3" >
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Booking check-in<small> - Booking</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="<?= $path; ?>/index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="<?= $path; ?>/login.php" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div>
                        </div>
                    </div>
                    <!---內容-->


                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form id="searchBookingIdForm" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label>Booking ID</label>
                                            <input id="bookingId" type="text" name="bookingId" class="form-control"  placeholder="Booking ID">
                                        </div> 
                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        <span class="help-block with-errors"></span>
                                        <div class="col-xs-2" style="padding-top:15px;">
                                            <button id="searchbookingId" type="submit" class="btn btn-default">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <form id="bookingIdCheckInResult" data-toggle="validator"  method="post" onsubmit="return false">

                        <div class="row" style="padding-top:20px;">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <div  id="checkInBooking" style="display:none">
                                        <table class="table table-bordered">
                                            <tr class="info">
                                                <td>Booking id </td>

                                                <td>Patient id </td>
                                                <td>Into hospital Date</td>
                                                <td>Out of hospital Date</td>
                                                <td>Patient Name</td>
                                                <td>Bed Type</td>
                                                <td>status</td>
                                            </tr>
                                            <tr>

                                                <td>
                                                    <input type="text" name="bookingIdForm" id="bookingIdForm"  class="form-control" readonly="">
                                                </td>
                                                <td>
                                                    <input type="text" name="patientId" id="patientId"  class="form-control" readonly="">
                                                </td>
                                                <td>
                                                    <input type="text" name="incomedate" id="incomedate"  class="form-control" readonly="">

                                                </td>
                                                <td>
                                                    <input type="text" name="outdate" id="outdate"  class="form-control" readonly="">

                                                </td>

                                                <td>
                                                    <input type="text" name="fullName" id="fullName"  class="form-control" readonly="">

                                                </td>
                                                <td>
                                                    <input type="text" name="bedType" id="bedType"  class="form-control" readonly="">

                                                </td>
                                                <td>

                                                    <input type="checkbox" name="inPatientStatus" id="inPatientStatus">
                                                </td>
                                            </tr>
                                        </table>
                                        <button id="changePatientStatus" class="btn btn-primary" >
                                            Save 
                                            <span class="glyphicon glyphicon-floppy-save"></span>
                                        </button>			
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </body>

</html>