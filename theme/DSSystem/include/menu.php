
<style>
.list-group-item:hover { background: #F3F781;
					color:#5E610B;
					cursor: pointer;
					font-weight:bold;
					font-size:16px;}
.list-group-item a { color:black;
text-decoration: none;
}
.list-group-item:hover a { color:white;
}
.submenu1 { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu1:hover { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu2 { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu2:hover { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu3 { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu3:hover { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu4 { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu4:hover { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu5 { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
.submenu5:hover { background: rgba(0, 0, 0, 0.1);
					color:#2E2E2E;
					cursor: pointer;
					font-weight:bold;
					font-size:16px; 
}
</style>
<script>
$(function() {
    $('#bigmenu1').click(function(){
      $('.submenu1').slideToggle();
    });
});
$(function() {
    $('#bigmenu2').click(function(){
      $('.submenu2').slideToggle();
    });
});
$(function() {
    $('#bigmenu3').click(function(){
      $('.submenu3').slideToggle();
    });
});
$(function() {
    $('#bigmenu4').click(function(){
      $('.submenu4').slideToggle();
    });
});
$(function() {
    $('#bigmenu5').click(function(){
      $('.submenu5').slideToggle();
    });
});
</script>
<div class="panel panel-warning" style="-webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;">
	 <div class="panel-heading ">
<ul class="list-group">
  <li class="list-group-item" id="bigmenu1" ><span class="glyphicon glyphicon-expand pull-left"></span> Booking</li>
   <li class="list-group-item submenu1" onClick="window.location='index.php?act=booking-newbooking'"><span class="glyphicon glyphicon-expand pull-left"></span> New Booking</li>
   <li class="list-group-item submenu1" onClick="window.location='index.php?act=booking-info'"><span class="glyphicon glyphicon-expand pull-left"></span> Booking information</li>
   <li class="list-group-item submenu1" onClick="window.location='index.php?act=booking-income';"><span class="glyphicon glyphicon-expand pull-left"></span> Booking Check-in</li>
   
  <li class="list-group-item" id="bigmenu2" ><span class="glyphicon glyphicon-expand pull-left"></span> Invoice</li>
     <li class="list-group-item submenu2" onClick="window.location='index.php?act=invoice-check';"><span class="glyphicon glyphicon-expand pull-left"></span> Check invoice</li>
  <!--- <li class="list-group-item submenu2" onClick="window.location='<?= $path?>/Invoice/edit.php';"><span class="glyphicon glyphicon-expand pull-left"></span> Edit Invoice</li>-->
   
   
  <!---<li class="list-group-item" id="bigmenu3" ><span class="glyphicon glyphicon-expand pull-left"></span> Room information</li> 
     <li class="list-group-item submenu3" onClick="window.location='';"><span class="glyphicon glyphicon-expand pull-left"></span> testing456</li>
   <li class="list-group-item submenu3" onClick="window.location='';"><span class="glyphicon glyphicon-expand pull-left"></span> testing456</li>-->

     <li class="list-group-item" id="bigmenu4" ><span class="glyphicon glyphicon-expand pull-left"></span> Report</li> 
     <li class="list-group-item submenu4" onClick="window.location='index.php?act=report-check';"><span class="glyphicon glyphicon-expand pull-left"></span> Normal report</li>
   
      <li class="list-group-item" id="bigmenu5" ><span class="glyphicon glyphicon-expand pull-left"></span> Patient</li> 
  <li class="list-group-item submenu5" onClick="window.location='index.php?act=patient-register';"><span class="glyphicon glyphicon-expand pull-left"></span> New register</li>
   <li class="list-group-item submenu5" onClick="window.location='index.php?act=patient-info';"><span class="glyphicon glyphicon-expand pull-left"></span> Patient information</li>
   <li class="list-group-item submenu5" onClick="window.location='index.php?act=patient-inportphycheck';"><span class="glyphicon glyphicon-expand pull-left"></span> Physical check import</li>
  </ul>
    </div>
	</div>