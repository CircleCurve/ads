<?php //$path = '/DSSystem/theme'; 

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$verify = new Verify() ; 
?>
<!DOCTYPE html>
<html>
    <head>

    <?php include 'include/header.php' ; ?>
        
    </head>
   
    <style>
        .submenu1{display:none;}
        .submenu2{display:none;}
        .submenu3{display:none;}
        .submenu4{display:none;}
        .submenu5{display:none;}
    </style>

    <body>
        <div class="container">

            <div class="row">
                <!---左邊--->
                <div class="col-xs-3">
                    <center>
                        <!--插入歡迎box+menu-->
                        <?php include'include/welcomebox.php'; ?>
                        <?php include 'include/menu.php'; ?>
                    </center>
                </div>

                <!---右邊--->
                <div class="col-xs-9">
                    <!---標題-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Staff portal | DBP Hospital<small> (Please logout when you leave)</small></h1>
                            <div class="pull-right">
                                <a class="btn btn-success " href="index.php" role="button" >Home <span class="glyphicon glyphicon-home"></span></a>
                                <a class="btn btn-danger" href="?act=main-logout" role="button">logout <span class="glyphicon glyphicon-log-out"></span></a>
                            </div></div>
                    </div>
                    <!---Scroll box-->
                    <div class="row">
                        <div class="col-xs-6">
                            <marquee behavior="alternate" ><h3>
                                    <?php
                                    echo "Today is " . date("Y/m/d");
                                    echo " (" . date("l") . ")";
                                    ?></h3></marquee>
                        </div>
                    </div>
                    <!---床位-->
                    <div class="row">
                        <div class="media col-xs-6">
                            <div class="media-left media-middle">
                                <img class="media-object" src="<?= $path; ?>/image/bed.png"  width="100" alt="...">
                            </div>
                            <div class="media-body">
                                <center><h3 class="media-heading" style="color:#f0ad4e;">The remaining beds</h3><br>
                                    <span class="label label-warning" style="font-size:20px;"><?= $verify->getSession("bedAmount"); ?></span></center>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </body>

</html>