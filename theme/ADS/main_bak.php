<?php //$path = '/DSSystem/theme'; 

if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}

$verify = new Verify() ; 
?>
<!DOCTYPE html>
<html>
    <head>

    <?php include 'include/header.php' ; ?>
        
    </head>
    <body>
        <div class="container">

            <div class="row">
                <!---left box--->
                <div class="col-xs-3">
                    <center>
                        <!--welcome and menu box-->
                        <?php include'include/welcomebox.php'; ?>
                        <?php include 'include/menu.php'; ?>
                    </center>
                </div>

                <!---right box--->
                <div class="col-xs-9">
                    <!---heading-->
                    <div class="row">
                        <div class="page-header" ><button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#inbox" >
						<span class="glyphicon glyphicon-envelope"></span> Inbox  &nbsp;<span class="badge"><span class="glyphicon glyphicon-exclamation-sign" style="color:#d9534f; font-size:16px;"></span></span></button>
                            <h1>Staff portal | Laptop Repair Centre<small><br> (Please logout when you leave)</small> </h1>
                            <!--logout home button-->
							<?php include $path.'/include/homebutton.php';?>
							</div>
                    </div>
                    <!---Auto time Scroll box-->
                    <div class="row">
                        <div class="col-xs-6">
                            <marquee behavior="alternate" ><h3>
                                    <?php
                                    echo "Today is " . date("Y/m/d");
                                    echo " (" . date("l") . ")";
                                    ?></h3></marquee>
                        </div>
                    </div>
                    <!---time schedule-->
                    <div class="row">
                      <img src="<?= $path; ?>/image/carlender.png" class="img-responsive"></img>
                    </div>


                </div>
            </div>
        </div>
    </body>

<!---inbox popup-->
<div class="modal fade" id="inbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning text-warning" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Staff Inbox</h4>
      </div>
      <div class="modal-body">
	  
	  <!---inbox save form--->
	  <form>
	  
	  <!---heading--->
        <table class="table">
		<tr class="info" align="center" style="font-weight:bold;"><td width="5%">Read</td><td  width="30%">Sender</td><td  width="50%">Massage Titile</td><td  width="15%">Received time</td></tr>
		</table>
		
	<!---looping box data scroll box--->
		<div  style="height: 500px; overflow: scroll;  overflow-x:hidden;" >
		<table class="table table-striped table-bordered">
		<tr align="center"><td width="5%"><input type="checkbox" /></td><td width="30%">testing</td><td width="50%">1</td><td width="15%"><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		<tr align="center"><td><input type="checkbox" /></td><td>testing</td><td>1</td><td><?php echo date("Y/m/d") . "<br>" . date("h:i a");?></td></tr>
		</table>
		</div>
		
		
      </div>
      <div class="modal-footer bg-warning text-warning">
        <button type="button" class="btn btn-primary">Save changes</button>
	   </form>
      </div>
    </div>
  </div>
</div>
</html>