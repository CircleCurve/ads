<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();

?>
<!DOCTYPE html>
<html>
<head>
    <?php include_once $path . '/include/header.php'; ?>

    <script type="text/javascript">
        $(function () {

            $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', maxDate: new Date()});

            //$('#submitcreateuserform').confirmation('show');
            $('#submitcreateuserform').confirmation({
                //popout : true,
                "onConfirm": function () {

                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            location.href = "index.php?act=school-view";
                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }

                    ajaxForm("webservice/school/createDepartment.php", $("#createuserform").serialize(), false, successCallBack);

                }

            });


        });
    </script>
</head>


<body>
<div class="container">

    <div class="row" style="padding-top:10px;">
        <!---left side--->
        <div class="col-xs-3">
            <center>
                <!--box+menu-->
                <?php include $path . '/include/welcomebox.php'; ?>
                <?php include $path . '/include/menu.php'; ?>
            </center>
        </div>

        <!---right side-->
        <div class="col-xs-9">
            <!---heading-->
            <div class="row">
                <div class="page-header">
                    <h1>Course Management
                        <small> - Create Department</small>
                    </h1>
                    <!--logout home button-->
                    <?php include $path . '/include/homebutton.php'; ?>
                </div>
            </div>
            <h2 style="margin-bottom:-10px; margin-top:-30px;">
                <small>* = Request</small>
            </h2>
            <!---content-->
            <div class="row" style="padding-top:20px;">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <!----create form--->
                        <form id="createuserform" data-toggle="validator" method="post" onsubmit="return false">
                            <input type="hidden" name="offerYear" value="<?= $data["dateTime"] ?>" >

                            <div class="row">
                                <div class="form-group col-xs-6">
                                    <label>Department Name *</label>

                                    <?= $data["regex"]["deptName"]["html"] ?>

                                </div>
                                <div class="form-group col-xs-6">
                                    <label>Department Location *</label>
                                    <?= $data["regex"]["deptAddress"]["html"] ?>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-xs-6">
                                    <label>Course Title*</label>

                                    <?= $data["regex"]["courseTitle"]["html"] ?>

                                </div>
                                <div class="form-group col-xs-6">
                                    <label>Course Level*</label>
                                    <?= $data["regex"]["courseLevel"]["html"] ?>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-xs-6">
                                    <label>Offer Year *</label>

                                    <?= $data["regex"]["offerYear"]["html"] ?>

                                </div>
                                <div class="form-group col-xs-6">
                                    <label>Class size *</label>
                                    <?= $data["regex"]["classSize"]["html"] ?>

                                </div>
                            </div>
                            


                            <button id="submitcreateuserform" type="submit" class="btn btn-default"
                                    data-toggle="confirmation" data-btn-ok-label="Confirm!!"
                                    data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success"
                                    data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                                    data-btn-cancel-class="btn-danger">Create
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>