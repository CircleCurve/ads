<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
//print_r_pre($data) ; 
//value="<?= substr_replace($data["data"][0]['hkid'],"****", -4); 
?>
<!DOCTYPE html>
<html>
<head>
    <?php include_once $path . '/include/header.php'; ?>

    <script type="text/javascript">
        $(function () {

            $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', maxDate: new Date()});

            //$('#submitcreateuserform').confirmation('show');
            $('#editInfo').click(function () {
                $("#studName").removeAttr('disabled').attr("required", "");
                $("#DOB").removeAttr('disabled').attr("required", "");
                $(".enrollBox").removeAttr('disabled');


                $("#editInfo").css("display", "none");
                $("#confirm").css("display", '');
                $('#myForm').validator();

            });

            $("#confirm").click(function () {
                var successCallBack = function (response) {
                    console.log(response);

                    var response = JSON.parse(response);

                    if (response["status"] === true) {

                        $("#studName").attr("disabled", "");
                        $("#DOB").attr("disabled", "");
                        $(".enrollBox").attr("disabled", "");


                        $("#id").attr('readonly', true);

                        $("#editInfo").css("display", "");

                        $("#confirm").css("display", "none");
                        location.reload();


                    } else {
                        alert("status : " + response["status"] + "  Message: " + response["msg"]);
                    }

                }
                ajaxForm("webservice/student/update.php", $("#createuserform").serialize(), false, successCallBack);


            });

            $("#deleteUser").click(function () {
                var successCallBack = function (response) {
                    console.log(response);
                    var response = JSON.parse(response);

                    if (response["status"] === true) {
                        alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        location.href = "index.php?act=student-view";
                    }
                    else {
                        alert("status : " + response["status"] + "  Message: " + response["msg"]);
                    }

                }
                ajaxForm("webservice/student/delete.php", {"id": '<?= $data["rtn"][0]["_id"] ?>'}, false, successCallBack);

            });

            $('#submitcreateuserform').confirmation({
                //popout : true,
                "onConfirm": function () {

                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            location.href = "index.php?act=student-view";
                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }

                    ajaxForm("webservice/student/register.php", $("#createuserform").serialize(), false, successCallBack);

                }

            });


        });
    </script>
</head>


<body>
<div class="container">

    <div class="row" style="padding-top:10px;">
        <!---left side--->
        <div class="col-xs-3">
            <center>
                <!--box+menu-->
                <?php include $path . '/include/welcomebox.php'; ?>
                <?php include $path . '/include/menu.php'; ?>
            </center>
        </div>

        <!---right side-->
        <div class="col-xs-9">
            <!---heading-->
            <div class="row">
                <div class="page-header">
                    <h1>Student Management
                        <small> - Update Student</small>
                    </h1>
                    <!--logout home button-->
                    <?php include $path . '/include/homebutton.php'; ?>
                </div>
            </div>
            <h2 style="margin-bottom:-10px; margin-top:-30px;">
                <small>* = Request</small>
            </h2>
            <!---content-->
            <div class="row" style="padding-top:20px;">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <!----create form--->

                        <form id="createuserform" data-toggle="validator" method="post" onsubmit="return false">
                            <input type='hidden' id='id' name="id" value='<?= $data["rtn"][0]["_id"] ?>'>

                            <div class="row">
                                <div class="form-group col-xs-6">
                                    <label>Student Name *</label>

                                    <?= $data["regex"]["studName"]["html"] ?>

                                </div>
                                <div class="form-group col-xs-6">
                                    <label>Date of Birth *</label>
                                    <?= $data["regex"]["DOB"]["html"] ?>

                                </div>
                            </div>

                            <div class="row" style="padding-top:20px;">
                                <div class="panel panel-success">
                                    <div class="panel-body">
                                        <table id="userInfoTable" class="table table-bordered table-striped">
                                            <tr class="success topbar">
                                                <td>Enroll</td>
                                                <td>Department Name</td>
                                                <td>Department Location</td>
                                                <td>Course Title</td>
                                                <td>Course Level</td>
                                                <td>Offer Year</td>
                                                <td>Class Size</td>
                                                <td>Available Places</td>

                                            </tr>

                                            <?
                                            
                                            echo '<tr>';
                                            for ($i = 0; $i < count($data["courseInfo"]); $i++) {
                                                echo '<input type="hidden" name="courseId[]" value="'. $data["courseInfo"][$i]["courseId"]. '">';

                                                echo '<input type="hidden" name="offerYear[]" value="'. $data["courseInfo"][$i]["offerYear"]. '">';

                                                echo '<tr>';
                                                echo '<td><input  class="enrollBox" type="checkbox" name="enroll[]" value="'.$data["courseInfo"][$i]["_id"] .'"  '. (isset($data["courseInfo"][$i]["checked"]) ?  $data["courseInfo"][$i]["checked"] : "") .'  disabled></td>';
                                                echo '<td>' . $data["courseInfo"][$i]["deptName"] . '</td>';
                                                echo '<td>' . $data["courseInfo"][$i]["deptAddress"] . '</td>';
                                                echo '<td>' . $data["courseInfo"][$i]["courseTitle"] . '</td>';
                                                echo '<td>' . $data["courseInfo"][$i]["courseLevel"] . '</td>';
                                                echo '<td>' . $data["courseInfo"][$i]["offerYear"] . '</td>';
                                                echo '<td>' . $data["courseInfo"][$i]["classSize"] . '</td>';
                                                echo '<td>' . $data["courseInfo"][$i]["availablePlaces"] . '</td>';

                                                echo '</tr>';
                                            }
                                            ?>

                                        </table>

                                    </div>

                                </div>
                            </div>


                            <button id="confirm" type="submit" class="btn btn-success" style="display:none">
                                Confirm
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>

                            <button id="editInfo" class="btn btn-primary" href="?act=patient-editinfo" role="button">
                                Edit Info
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                            <button id="deleteUser" type="submit" class="btn btn-danger">
                                Delete User
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>