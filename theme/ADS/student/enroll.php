<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();

?>
<!DOCTYPE html>
<html>
<head>
    <?php include_once $path . '/include/header.php'; ?>

    <script type="text/javascript">
        $(function () {

            $('#datetimepicker2').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss', maxDate: new Date()});

            //$('#submitcreateuserform').confirmation('show');
            $('#submitcreateuserform').confirmation({
                //popout : true,
                "onConfirm": function () {

                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);

                        if (response["status"] === "1") {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                            location.href = "index.php?act=student-view";
                        }
                        else {
                            alert("status : " + response["status"] + "  Message: " + response["msg"]);
                        }

                    }

                    ajaxForm("webservice/student/enroll.php", $("#createuserform").serialize(), false, successCallBack);

                }

            });


        });
    </script>
</head>


<body>
<div class="container">

    <div class="row" style="padding-top:10px;">
        <!---left side--->
        <div class="col-xs-3">
            <center>
                <!--box+menu-->
                <?php include $path . '/include/welcomebox.php'; ?>
                <?php include $path . '/include/menu.php'; ?>
            </center>
        </div>

        <!---right side-->
        <div class="col-xs-9">
            <!---heading-->
            <div class="row">
                <div class="page-header">
                    <h1>Course Management
                        <small> - Create Student</small>
                    </h1>
                    <!--logout home button-->
                    <?php include $path . '/include/homebutton.php'; ?>
                </div>
            </div>
            <h2 style="margin-bottom:-10px; margin-top:-30px;">
                <small>* = Request</small>
            </h2>
            <!---content-->
            <form id="createuserform" data-toggle="validator" method="post" onsubmit="return false">

            <div class="row" style="padding-top:20px;float: left">

                <button id="submitcreateuserform" type="submit" class="btn btn-default"
                        data-toggle="confirmation" data-btn-ok-label="Confirm!!"
                        data-btn-ok-icon="glyphicon glyphicon-share-alt" data-btn-ok-class="btn-success"
                        data-btn-cancel-label="Cancel" data-btn-cancel-icon="glyphicon glyphicon-ban-circle"
                        data-btn-cancel-class="btn-danger">Confirm
                </button>
            </div>

            <div class="row" style="padding-top:20px;">
                <div class="panel panel-success">
                    <div class="panel-body">
                        <table id="userInfoTable" class="table table-bordered table-striped">
                            <tr class="success topbar">
                                <td>Enroll</td>
                                <td>Department Name</td>
                                <td>Department Location</td>
                                <td>Course Title</td>
                                <td>Course Level</td>
                                <td>Offer Year</td>
                                <td>Class Size</td>
                                <td>Available Places</td>

                            </tr>

                            <?
                            echo '<tr>';
                            for ($i = 0; $i < count($data["courseInfo"]); $i++) {
                                echo '<tr>';
                                echo '<td><input type="checkbox" name="enroll[]" value="'.$data["courseInfo"][$i]["_id"] .'"></td>';
                                echo '<td>' . $data["courseInfo"][$i]["deptName"] . '</td>';
                                echo '<td>' . $data["courseInfo"][$i]["deptAddress"] . '</td>';
                                echo '<td>' . $data["courseInfo"][$i]["courseTitle"] . '</td>';
                                echo '<td>' . $data["courseInfo"][$i]["courseLevel"] . '</td>';
                                echo '<td>' . $data["courseInfo"][$i]["offerYear"] . '</td>';
                                echo '<td>' . $data["courseInfo"][$i]["classSize"] . '</td>';
                                echo '<td>' . $data["courseInfo"][$i]["availablePlaces"] . '</td>';

                                echo '</tr>';
                            }
                            ?>

                        </table>

                    </div>

                </div>
            </div>
            </form>


        </div>
    </div>
</div>
</body>
</html>