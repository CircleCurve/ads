<?php
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once $path . '/include/header.php'; ?>
        <script type="text/javascript">
            $(function () {

                $("#search").click(function () {

                    var successCallBack = function (response) {
                        console.log(response);
                        var response = JSON.parse(response);

                        $("#userInfoTable").find("tr:gt(0)").remove();
                        var table = document.getElementById("userInfoTable");
                        var row, cell;

                        for (var i = 0, j = 0; i < response.length; i++) {
                            row = table.insertRow(i + 1);
                            for (var key in response[i]) {

                                row.insertCell(j).innerHTML = response[i][key];
                                j++;
                            }
                            row.insertCell(j++).innerHTML = '<button value="' + response[i]["id"] + '" class="label label-success glyphicon glyphicon-info-sign details"> Detail</button>';


                            j = 0;
                        }

                        $(".details").click(function () {
                            var detailsId = $(this).attr('value');

                            location.href = "index.php?act=incidentmanagement-updateIncident&id=" + detailsId;
                        });


                    }
                    //var removeUid = $(this).attr('value');

                    ajaxForm("webservice/incident/info.php", $("#viewIncidentTableSortForm").serialize(), false, successCallBack);


                });





                $(".details").click(function () {
                    var detailsId = $(this).attr('value');
                    location.href = "index.php?act=incidentmanagement-updateIncident&id=" + detailsId;
                });

                /*
                 $(".remove").click(function() {
                 var bid = $(this).attr('value');
                 console.log(bid) ; 
                 }*/
            });
        </script>

        <style>
            a:hover { text-decoration: none; }
        </style>
    </head>


    <body>
        <div class="container">

            <div class="row">
                <!---left side-->
                <div class="col-xs-3" >
                    <center>
                        <!--box+menu-->
                        <?php include $path . '/include/welcomebox.php'; ?>
                        <?php include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <!---rightside--->
                <div class="col-xs-9">
                    <!---heading-->
                    <div class="row">
                        <div class="page-header">
                            <h1>Search Incident<small> - Search Incident</small></h1>
                            <!--logout home button-->
                            <?php include $path . '/include/homebutton.php'; ?>
                        </div>
                    </div>
                    <!---sort box-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <form id="viewIncidentTableSortForm" data-toggle="validator"  method="post" onsubmit="return false">
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >Incident Summary</label>
                                            <?= $data["title"] ?>
                                        </div>


                                        <div class="form-group col-xs-6">
                                            <label >Description</label>
                                            <?= $data["description"] ?>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-6">
                                            <label >ID Info</label>
                                            <?= $data["incidentId"] ?>
                                        </div>
                                        
                                                                                
                                        <div class="form-group col-xs-6">
                                            <label >Assign</label>
                                            <?= $data["assign"] ?>
                                        </div>
                                    </div>

                                    
                                    <div class="row">
                                        <div class="form-group col-xs-12" >
                                            <button id="search" type="submit" class="btn btn-success" >
                                                Search
                                            </button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!---table box-->
                    <div class="row" style="padding-top:20px;">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <table id="userInfoTable" class="table table-bordered table-striped">
                                    <tr class="success topbar">
                                        <td> ID</td>
                                        <td>Incident Summary</td>
                                        <td>Description</td>
                                        <td>From</td>
                                        <td>Assign to</td>
                                        <td>Status</td>
                                        <td>Create Time</td>
                                        <td></td>
                                    </tr>

                                    <?
                                    $incidentInfo = $data["incidentInfo"];
                                    echo '<tr>';
                                    for ($i = 0; $i < count($incidentInfo); $i++) {
                                        echo '<tr>';
                                        foreach ($incidentInfo[$i] as $key => $value) {
                                            echo '<td>' . $incidentInfo[$i][$key] . '</td>';
                                        }
                                        echo '<td><button value="' . $incidentInfo[$i]["eventId"] . '" class="label label-success glyphicon glyphicon-info-sign details"> Detail</button></td>';

                                        echo '</tr>';
                                    }
                                    ?>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
</body>

</html>