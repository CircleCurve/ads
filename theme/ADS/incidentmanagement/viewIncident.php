<?
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
$verify = new Verify();
?>

<!DOCTYPE html>
<html>
    <head>
        <?
        include_once $path . '/include/header.php';
        include_once $path . '/include/fullcalendar.php';
        ?>

        <script>
            var eventInfo = JSON.parse('<?= json_encode($data["event"]) ?>');
            $(document).ready(function () {

                $('#calendar').fullCalendar({
                    theme: true,
                    googleCalendarApiKey: 'AIzaSyAFHROnSTwFBCseKwh741Z9fJKCrpvGrws',
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'

                    },
                    selectable: true,
                    dragOpacity: 0.5,
                    weekNumbers: true,
                    //weekMode: 'variable',
                    editable: true,
                    //height: "1000px", 

                    eventRender: function (event, element) {

                        element.attr('title', event.description);
                    },
                    eventClick: function(calEvent, jsEvent, view) {
                        //alert ("eventClcik  :" +calEvent.id) ;
                        location.href="<?= $data['calendarUrl'] ?>&id="+calEvent.id ; 
                        
                        
                    }, 
                    //defaultDate: '2016-01-12',
                    //editable: true,
                    //       eventLimit: true, // allow "more" link when too many events
                    //events: 'webservice/incident/getIncident.php',
                    eventSources: [
                        
                        {
                            googleCalendarId: 'hong_kong__zh_tw@holiday.calendar.google.com',
                            backgroundColor :  "#ff0000"
                        },
                        {
                            googleCalendarId: 'p#weather@group.v.calendar.google.com',
                            backgroundColor :  "#FF0000"
                        }, 
                        
                    ],
                    events: eventInfo

                });


            });

        </script>
        <style>

            body {
                /*
                margin: 40px 10px;
                padding: 0;
                font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
                font-size: 14px;*/
            }

            #calendar {
                max-width: 900px;
                height:100%;  
                margin: 0 auto;
                margin-bottom: 200px ; 
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="row" style="padding-top:10px;">
                <!---left side--->
                <div class="col-xs-3" >
                    <center>
                        <!--box+menu-->
                        <? include $path . '/include/welcomebox.php'; ?>
                        <? include $path . '/include/menu.php'; ?>
                    </center>
                </div>

                <div class="col-xs-9">
                    <div class="row">
                        <div class="page-header">
                            <h1>Incident Management<small> - View Incident</small></h1>
                            <!--logout home button-->
                            <? include $path . '/include/homebutton.php'; ?>

                        </div>

                    </div>
                    <div class="row" style="padding-top:30px; ">


                        <div id="calendar" ></div>


                    </div>



                </div>
            </div>

        </div>



    </body>
</html>
