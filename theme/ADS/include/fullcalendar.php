<link href='<?= $path; ?>js/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?= $path; ?>js/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />

<link rel="stylesheet" type="text/css" href="<?= $path; ?>js/fullcalendar/css/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?= $path; ?>js/fullcalendar/ColorPicker/js/jquery.miniColors.css" />


<script src='<?= $path; ?>js/fullcalendar/js/jquery-ui-1.10.2.custom.min.js'></script>

<script src='<?= $path; ?>js/fullcalendar/lib/moment.min.js'></script>
<script src='<?= $path; ?>js/fullcalendar/fullcalendar.min.js'></script>


<script src='<?= $path; ?>js/fullcalendar/ColorPicker/js/jquery.miniColors.js'></script>


<script src="<?= $path; ?>js/fullcalendar/ui/jquery.ui.position.js"></script>
<script src="<?= $path; ?>js/fullcalendar/ui/jquery.ui.resizable.js"></script>
<script src="<?= $path; ?>js/fullcalendar/ui/jquery.ui.button.js"></script>
<script src="<?= $path; ?>js/fullcalendar/ui/jquery.ui.dialog.js"></script>

<script type='text/javascript' src="<?= $path; ?>js/fullcalendar/gcal.js"></script>