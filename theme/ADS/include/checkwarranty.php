<div class="modal fade" id="checkwarranty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning text-warning">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Warranty Lookup</h4>
      </div>
      <div class="modal-body">
							<form id="checkwarranty">
								<div class="row">
                                        <div class="form-group col-xs-4 col-xs-offset-4">
                                        <div class="form-group">
                                            <label >Device Serial No *</label>
											<input type="text" id="deviceSerialNo" name="deviceSerialNo" class="form-control"   placeholder="Device Serial No"  >
                                        </div>
                                        </div>
								</div>
						<button type="button" class="btn btn-success" style="margin-top:-20px;"><span class="glyphicon glyphicon-chevron-down"></span></button>
								<div class="row">
										<div class="form-group col-xs-4" style="margin-left:185px;">
										<label ></label>
										<button type="submit" class="btn btn-info" id="submitcheckwarranty" > Search Warranty Status <span class="glyphicon glyphicon-search "></span></button>
                                        </div>
								</div>
						<button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-chevron-down"></span></button>
								<div class="row">
										<div class="form-group col-xs-4 col-xs-offset-2">
                                        <div class="form-group">
                                            <label >Warranty Status</label>
											<input type="text" id="warrantyStatus" name="warrantyStatus"" class="form-control"   placeholder="Warranty Status"  disabled>
                                        </div>
                                        </div>
										<div class="form-group col-xs-4">
                                        <div class="form-group">
                                            <label >Service type</label>
											<input type="text" id="serviceType" name="serveiceType"" class="form-control"   placeholder="Service type"  disabled>
                                        </div>
                                        </div>
                                    </div>
									</form>
      </div>
      <div class="modal-footer bg-warning text-warning">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>