<?php date_default_timezone_set("Asia/Hong_Kong"); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?= $path; ?>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= $path; ?>/bootstrap/css/datetimepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?= $path; ?>/styles.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<?= $path; ?>/jquery-1.9.1.js"></script>

<script src="<?= $path; ?>/bootstrap/js/bootstrap.min.js"></script>


<script src="<?= $path; ?>/bootstrap/js/moment.js"></script>
<script src="<?= $path; ?>/bootstrap/js/datetimepicker.min.js"></script>


<script src="<?= $path; ?>/bootstrap/js/bootstrap-confirmation2.js"></script>

<!-- Validator.js -->
<script src="<?= $path; ?>/js/validator.min.js"></script>
<!-- Validator.js -->
<!-- Ajax.js---->
<script type="text/javascript" src="<?= $path; ?>/js/ajax.js"></script>


<title>Advance Database</title>

