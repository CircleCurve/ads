<style>
    .list-group-item {
        background: #F2F5A9;
        color: #5E610B;
        cursor: pointer;
        font-weight: bold;
        font-size: 13px;
    }

    .list-group-item:hover {
        background: #FFFFFF;
        color: #5E610B;
        cursor: pointer;
        font-weight: bold;
        font-size: 13px;
    }

    .list-group-item a {
        color: black;
        text-decoration: none;
    }

    .list-group-item:hover a {
        color: white;
    }

    .submenu1 {
        background: #FFFFFF;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
        padding-left: 50px;
    }

    .submenu1:hover {
        background: #F2F2F2;
        color: #2E2E2E;
        cursor: pointer;
        font-weight: bold;
        font-size: 15px;

    }

    .submenu2 {
        background: #FFFFFF;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
        padding-left: 40px;
    }

    .submenu2:hover {
        background: #F2F2F2;
        color: #2E2E2E;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
    }

    .submenu3 {
        background: #FFFFFF;
        cursor: pointer;
        font-weight: bold;
        font-size: 13px;
        padding-left: 50px;
    }

    .submenu3:hover {
        background: #F2F2F2;
        color: #2E2E2E;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
    }

    .submenu4 {
        background: #FFFFFF;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
        padding-left: 50px;
    }

    .submenu4:hover {
        background: #F2F2F2;
        color: #2E2E2E;
        cursor: pointer;
        font-weight: bold;
        font-size: 15px;
    }

    .submenu5 {
        background: #FFFFFF;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
        padding-left: 50px;
    }

    .submenu5:hover {
        background: #F2F2F2;
        color: #2E2E2E;
        cursor: pointer;
        font-weight: bold;
        font-size: 15px;
    }

    .submenu6 {
        background: #FFFFFF;
        cursor: pointer;
        font-weight: bold;
        font-size: 14px;
        padding-left: 50px;
    }

    .submenu6:hover {
        background: #F2F2F2;
        color: #2E2E2E;
        cursor: pointer;
        font-weight: bold;
        font-size: 15px;
    }

    .submenu1 {
        display: none;
    }

    .submenu2 {
        display: none;
    }

    .submenu3 {
        display: none;
    }

    .submenu4 {
        display: none;
    }

    .submenu5 {
        display: none;
    }

    .submenu6 {
        display: none;
    }

</style>
<script>
    $(function () {
        $('#bigmenu1').click(function () {
            $('.submenu1').slideToggle();
        });
    });
    $(function () {
        $('#bigmenu2').click(function () {
            $('.submenu2').slideToggle();

        });
    });
    $(function () {
        $('#bigmenu3').click(function () {
            $('.submenu3').slideToggle();
        });
    });
    $(function () {
        $('#bigmenu4').click(function () {
            $('.submenu4').slideToggle();
        });
    });
    $(function () {
        $('#bigmenu5').click(function () {
            $('.submenu5').slideToggle();
        });
    });
    $(function () {
        $('#bigmenu6').click(function () {
            $('.submenu6').slideToggle();
        });
    });
</script>
<div class="panel panel-warning" style="-webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;">
    <div class="panel-heading ">
        <ul class="list-group" style="list-style-type: none;">
            <li class="list-group-item" id="bigmenu1"><span class="glyphicon glyphicon-expand pull-left"></span> Course
                Management<img src="<?= $path; ?>/image/User Management.png" class="img-responsive pull-right"
                               width="40" style="margin-top:-10px;"></img></li>
            <li class="list-group-item submenu1 text-left"
                onClick="window.location='index.php?act=school-create'"><span
                    class="glyphicon glyphicon-expand"></span> Create Course
            </li>
            <li class="list-group-item submenu1 text-left"
                onClick="window.location='index.php?act=school-view'"><span
                    class="glyphicon glyphicon-expand"></span> View Course
            </li>
        </ul>

        <ul class="list-group" style="list-style-type: none;">
            <li class="list-group-item" id="bigmenu2"><span class="glyphicon glyphicon-expand pull-left"></span> Student
                Management<img src="<?= $path; ?>/image/User Management.png" class="img-responsive pull-right"
                               width="40" style="margin-top:-10px;"></img></li>
            <li class="list-group-item submenu2 text-left"
                onClick="window.location='index.php?act=student-create'"><span
                    class="glyphicon glyphicon-expand"></span> Create Student
            </li>
            <li class="list-group-item submenu2 text-left"
                onClick="window.location='index.php?act=student-view'"><span
                    class="glyphicon glyphicon-expand"></span> View Student
            </li>

        </ul>




    </div>
</div>
