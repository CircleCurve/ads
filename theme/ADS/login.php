
<?
if (!isset($BE_INCLUDE) || $BE_INCLUDE == false) {
    die("請由正常方法進入");
}
?>
<!DOCTYPE html>
<html>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <!-- Latest compiled and minified CSS -->
    <script src="<?= $path ?>/jquery-1.9.1.js"></script>

    <link rel="stylesheet" href="<?= $path ?>/bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?= $path ?>/bootstrap/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?= $path ?>/bootstrap/js/bootstrap.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>


    <script type="text/javascript">
        $(document).ready(function () {

            $("#check").click(function () {
                $.ajax({
                    url: "controller/webservice/result.php",
                    data: "data1=1&data2=2",
                    method: "POST",
                    success: function (response) {
                        response = JSON.parse(response);

                        $("#loginName").val(response["data1"]);
                        //your code here
                    },
                    error: function (response) {
                        //your code here
                    }
                });
            });
        });


    </script>

    <head>
        <title>Advance Database</title>
        <style>
            .container {
                /* Margin/padding copied from Bootstrap */
                margin-left: auto;
                margin-right: auto;
                padding-left: 15px;
                padding-right: 15px;

                /* Set width to your desired site width */
                width: 1170px;
            }
        </Style>
    </head>


    <body>
        <div class="container">

            <!--speace-->
            <div style="padding-top:20px;"></div>

            <!--banner-->
            <center><img src="<?= $path ?>/image/banner.png" class="img-responsive" width="650" style="padding-bottom:10px; border-radius: 25px;"></img></center>


            <!--yellow box-->
            <div class="col-xs-8 col-xs-offset-2">
                <div class="panel panel-warning">
                    <div class="panel-heading ">
                        <center>
						<!---tiltle box-->
						<div class="row">
						<div class="label label-warning col-xs-4 col-xs-offset-4" >
						<h4><strong>Advance Database</strong></h4>
						</div>
						
						</div>
						<div class="row">
						
						<!--people icon-->
						<div class="col-xs-4" style="padding-top:20px;">
						 <img src="<?= $path ?>/image/loginicon.png" class="img-responsive img-circle pull-right" width="90" ></img>
						 </div>
						 
						 <!--login form-->
						 <div class="col-xs-8">
                            <form  class="form-horizontal" id="comment_form" action="?act=login-confirm" method="post" style="padding-top:20px;">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-3  control-label">Login Name:</label>
                                    <div class="col-xs-5">
                                        <input type="name" name="userName" class="form-control" id="loginName" placeholder="Login Name" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-xs-3  control-label">Passowrd:</label>
                                    <div class="col-xs-5">
                                        <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
								<div class="col-xs-4">
                                    <?php include $path . '/include/googleRecapcha.php';  ?>
								</div>
								</div>
								<div class="form-group" >
								<div class="col-xs-2 col-xs-offset-2" style="margin-top:-30px;">
                                    <input type="submit" name="submit" value="Login" class="btn btn-default">
                                </div>
								</div>
                            </form>
                        </center>

                    </div>
					</div>
                </div>
            </div>

            <center></center>

        </div>
    </body>

</html>