<?php

function requireFile($BE_INCLUDE , $entirePath, $rootPath, $errorPage, $controllerPath) {

    $fullPath = "$rootPath" . "$controllerPath" . $entirePath;

//echo "$rootPath"."$controllerPath"."/"."$entirePath" ; 
    if (is_file($fullPath)) {

        require_once $fullPath;

    } else {
        require_once "$rootPath" . "$controllerPath" . "/" . "$errorPage";
    }

}

function getFileDirectory($act) {


    $route = explode("-", $act);
///print_r($route) ; 
    $fileDir = $route[0];
    $destFile = $route [1];

    return "/" . $fileDir . "/" . $destFile . ".php";
}


function print_r_pre ($arr) {
    
    echo '<pre>' ; 
    print_r($arr) ; 
    echo '</pre>' ; 
    
}