<?php

class Joedb extends VerifyWS {
    
    private $db = null  ; 
    private $queryParam = array() ; 
    
    private $rtnMessage = array(

        "isPatientExisits" => array(
            "isPatientExisits" => array("status" => "2001", "msg" => "Patient is Exists Status : %s" ),
        ),
        "addPatient" => array(
            "isAddSuccess"=> array("status"=> "2002" , "msg" => "Patient is not add successfuly to database")  
        ),
        "funcName" => array(
            "ErrorMessage" => array("status" => "xxxx" , "msg" => "errorMessage") 
         )
        
    );
    
    public function __construct() {
        $this->db = new db();
        $setting = new Setting() ; 
        $dbInfo = $setting->getDBInfo() ; 
        $this->db->setDBSetting($dbInfo) ; 
    }
   
    public function setQueryParam ($queryParam){
        $this->queryParam = $queryParam ; 
    }
   
    public function getQueryParam($key) {
        return $this->queryParam[$key] ; 
    }
    
    public function addPatient () {        
        $query = "INSERT INTO `patient` (`firstName`,`lastName`,`hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`in_time`)"; 
        $query.="VALUES (?,?,?,?,?,?,?,?,NOW())"; 
        
        $parameter = array(
            $this->getQueryParam("patientFirstName"), 
            $this->getQueryParam("patientLastName"), 
            $this->getQueryParam("HKID"), 
            $this->getQueryParam("birthday"), 
            $this->getQueryParam("address"), 
            $this->getQueryParam("phoneNumber"), 
            $this->getQueryParam("emContactPerson"), 
            $this->getQueryParam("emContactPhone"), 
        ); 
        $patientInfo = $this->db->getResult($query, $parameter) ; 
        
        $count = $this->db->getCount() ; 
        if ($count>0){
            return true; 
        }
        
        return $this->displayResult("addPatient", "isAddSuccess") ; 
        

    }


    public function isPatientExisits () {
        
        $query = "SELECT `id` from patient where hkid=?" ; 
        $parameter = array($this->getQueryParam("HKID")) ; 
        
        $userInfo = $this->db->getResult($query, $parameter) ; 
                
        if (isset($userInfo[0] ) && $userInfo[0]["id"] > 0) {
            $this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
            
        }
        
        return $this->addPatient() ; 
    }
    
    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->rtnMessage["$func"]["$status"];
    }




    
}