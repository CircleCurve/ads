<?php

class Servicedb extends VerifyWS {
    

    
    private $db = null  ; 
    private $queryParam = array() ; 
    
    private $rtnMessage = array(
        
        "getServiceType" => array(
            "serviceNotExists" => array("status" => "4001", "msg" => "Service Type not exists : %s" ),
            "serviceGetSuccess"=>array("status"=>"1", "msg"=>"Service type get successfully") 
        ),
        
        "getServiceName" => array(
            "serviceNotExists" => array("status" => "4002", "msg" => "Service Name not exists : %s" ),
            "serviceGetSuccess"=>array("status"=>"1", "msg"=>"Service type get successfully") 
        ),
        "addPatient" => array(
            "isAddSuccess"=> array("status"=> "2002" , "msg" => "Patient is not add successfuly to database")  
        )
    );
    
    private $successMessage = array(
        "getPatientInformation" => array(
            "patientInfo" => array("status" => "1" , "msg" => "Success to get patient information", "result"=>array())
        ),
    ); 
    
    public function __construct() {
        $this->db = new db();
        $setting = new Setting() ; 
        $dbInfo = $setting->getDBInfo() ; 
        $this->db->setDBSetting($dbInfo) ; 
    }
   
    public function setQueryParam ($queryParam){
        $this->queryParam = $queryParam ; 
    }
   
    public function getQueryParam($key) {
        return $this->queryParam[$key] ; 
    }
    
    public function getServiceType () {
        $query = "SELECT `id`,  `type` from service group by `type` order by `id` " ; 
        $parameter = array() ; 
        
        $serviceType = $this->db->getResult($query, $parameter) ; 
                
        if (isset($serviceType[0] ) && $serviceType[0]["id"] > 0) {

            $info =  $this->getErrorMessage("getServiceType", "serviceGetSuccess") ;
            $info["result"] = $serviceType ; 
            
            return $info ; 
        //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
            
        }
        
        $this->displayResult("getServiceType", "serviceNotExists" , array("-1")) ;
    }
    
    public function getServiceName () {
        $query = "SELECT `id`, `type`,`name` from service " ; 
        $parameter = array() ; 
        
        $serviceType = $this->db->getResult($query, $parameter) ; 
                
        
        if (isset($serviceType[0] ) && $serviceType[0]["id"] > 0) {

            $info =  $this->getErrorMessage("getServiceName", "serviceGetSuccess") ;
            $info["result"] = $serviceType ; 
            
            return $info ; 
        //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
            
        }
        
        $this->displayResult("getServiceName", "serviceNotExists" , array("-1")) ;
    }
    
    
    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->rtnMessage["$func"]["$status"];
    }




    
}