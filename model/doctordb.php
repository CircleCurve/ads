<?php

class doctordb extends VerifyWS {
    
    private $db = null  ; 
    private $queryParam = array() ; 
    
    private $rtnMessage = array(

        "isPatientExisits" => array(
            "isPatientExisits" => array("status" => "2001", "msg" => "Patient is Exists Status : %s" ),
        ),
        "addPatient" => array(
            "isAddSuccess"=> array("status"=> "2002" , "msg" => "Patient is not add successfuly to database")  
        )
    );
    
    
    public function __construct() {
        $this->db = new db();
        $setting = new Setting() ; 
        $dbInfo = $setting->getDBInfo() ; 
        $this->db->setDBSetting($dbInfo) ; 
    }
    
    
    public function setQueryParam ($queryParam){
        $this->queryParam = $queryParam ; 
    }
   
    public function getQueryParam($key) {
        return $this->queryParam[$key] ; 
    }
    
    
    
    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->rtnMessage["$func"]["$status"];
    }
    
}
