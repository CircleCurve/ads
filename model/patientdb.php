<?php

class Patientdb extends VerifyWS {

    private $db = null;
    private $queryParam = array();
    private $rtnMessage = array(
        "isPatientExisits" => array(
            "isPatientExisits" => array("status" => "2001", "msg" => "Patient is Exists Status : %s"),
        ),
        "isPatientIdExists" => array(
            "patientIdNotExists" => array("status" => "2004", "msg" => "PatientId is Exists Status : %s"),
        ),
        "addInPatientRecord" => array(
            "isAddSuccess" => array("status" => "2005", "msg" => "In Patient Record is not add successfuly to database"),
            "addSuccess" => array("status" => "1", "msg" => "Patient add successfully")
        ),
        "addPatient" => array(
            "isAddSuccess" => array("status" => "2002", "msg" => "Patient is not add successfuly to database")
        ),
        "getPatientInformation" => array(
            "patientIdNotExists" => array("status" => "2003", "msg" => "Patient is not Exists Status : %s"),
            "patientInfo" => array("status" => "1", "msg" => "Success to get patient information", "result" => array())
        ),
        "getPatientId" => array(
            "patientIdNotExists" => array("status" => "2003", "msg" => "Patient is not Exists Status : %s"),
            "success" => array("status" => "1", "msg" => "Success to get patient id and name", "result" => array())
        ),
        "isInPatientNow" => array(
            "inPatientExists" => array("status" => "2003", "msg" => "Patient is not Exists Status : %s"),
            "success" => array("status" => "1", "msg" => "Success to get patient id and name", "result" => array())
        ),
        "setInPatientStatus" => array(
            "updateFail" => array("status" => "2006", "msg" => "InPatient status is failed  : %s"),
            "updateSuccess" => array("status" => "1", "msg" => "Successful to update inpatient status", "result" => array())
        ),
        "addPhyCheck" => array(
            "isAddSuccess" => array("status" => "2005", "msg" => "Physical Check does not add successfuly to database"),
        ), 
        "getPatientHistory" => array(
             "success" => array("status" => "1", "msg" => "Success to get patient history", "result" => array()), 
             "fail" => array("status" => "2007", "msg" => "Fail to get patient history")
            
        )
    );

    public function __construct() {
        $this->db = new db();
        $setting = new Setting();
        $dbInfo = $setting->getDBInfo();
        $this->db->setDBSetting($dbInfo);
    }

    public function setQueryParam($queryParam) {
        $this->queryParam = $queryParam;
    }

    public function getQueryParam($key) {
        return $this->queryParam[$key];
    }

    public function addPatient() {
        $query = "INSERT INTO `patient` (`firstName`,`lastName`,`hkid`,`dateOfBirth`,`address`,`phone`,`emContactPerson`,`emContactPhone`,`in_time`)";
        $query.="VALUES (?,?,?,?,?,?,?,?,NOW())";

        $parameter = array(
            $this->getQueryParam("patientFirstName"),
            $this->getQueryParam("patientLastName"),
            $this->getQueryParam("HKID"),
            $this->getQueryParam("birthday"),
            $this->getQueryParam("address"),
            $this->getQueryParam("phoneNumber"),
            $this->getQueryParam("emContactPerson"),
            $this->getQueryParam("emContactPhone"),
        );
        $patientInfo = $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();
        if ($count > 0) {
            return true;
        }

        return $this->displayResult("addPatient", "isAddSuccess");
    }

    public function addInPatientRecord() {
        $query = "INSERT INTO `Inpatient_Record` (`InpRec_Patient_id`,`bookingId`,`InpRec_Staff_id`,`InpRec_DateTimeIn`,`InpRec_DateTimeOut`,`InpRec_log_time`,`InpRec_Status` ) "
                . "VALUES (?,?,?,?,?,NOW(),?) ";

        $parameter = array(
            $this->getQueryParam("patientId"),
            $this->getQueryParam("bookingId"),
            $this->getQueryParam("staffId"),
            $this->getQueryParam("incomedate"),
            $this->getQueryParam("outdate"),
            $this->getQueryParam("status")
        );
        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();

        if ($count > 0) {
            return $this->getErrorMessage("addInPatientRecord", "addSuccess");
        }

        return $this->displayResult("addInPatientRecord", "isAddSuccess");
    }

    public function isInPatientNow() {
        $query = "SELECT `InpRec_id` from Inpatient_Record where bookingId=?";
        $parameter = array($this->getQueryParam("bookingId"));


        $inPatientId = $this->db->getResult($query, $parameter);


        if (isset($inPatientId[0]) && $inPatientId[0]["InpRec_id"] > 0) {

            return $this->setInPatientStatus();
        }

        return array("status" => false);
    }

    public function setInPatientStatus() {
        $query = "Update Inpatient_Record set InpRec_Status = ? where bookingId = ? ";
        $parameter = array(
            $this->getQueryParam("status"),
            $this->getQueryParam("bookingId"),
                );

        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();

        return array("status" => "1", "msg" => "update Successful");

        // $this->displayResult("setInPatientStatus", "updateFail" , array("-1")) ;
    }

    public function isPatientIdExists() {
        $query = "SELECT `id` from patient where id=?";
        $parameter = array($this->getQueryParam("patientId"));

        $userInfo = $this->db->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            return $userInfo;
        }
        $this->displayResult("isPatientIdExists", "patientIdNotExists", array("-1"));
    }

    public function setPatientStatus() {

        $this->isPatientIdExists();


        $query = "Update patient set status = ? where id = ? and status != '-1'";
        $parameter = array(
            "-1",
            $this->getQueryParam("patientId"),
                );

        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();

        if ($count > 0) {
            return array("status" => "1", "msg" => "update Successful");
        }

        return array("status" => "1", "msg" => "update Unsuccessful");

        // $this->displayResult("setInPatientStatus", "updateFail" , array("-1")) ;
    }

    public function isPatientExisits() {

        $query = "SELECT `id` from patient where hkid=? and status != '-1'";
        $parameter = array($this->getQueryParam("HKID"));

        $userInfo = $this->db->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {
            $this->displayResult("isPatientExisits", "isPatientExisits", array("-1"));
        }

        return $this->addPatient();
    }

    public function getPatienInformation() {

        $query = "SELECT * from patient where id=? and status != '-1'; ";
        $parameter = array($this->getQueryParam("patientId"));

        $userInfo = $this->db->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            $info = $this->getErrorMessage("getPatientInformation", "patientInfo");
            $info["result"] = $userInfo[0];

            return $info;
            //return $userInfo ; 
            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        return $this->displayResult("getPatientInformation", "patientIdNotExists", array("-2"));
        // return $this->addPatient() ; 
    }

    public function getPatientId() {
        $query = 'SELECT id patientId ,firstName,lastName from patient where hkid = ? and status != "-1"; ';
        $parameter = array($this->getQueryParam("HKID"));

        $userInfo = $this->db->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["patientId"] > 0) {

            $info = $this->getErrorMessage("getPatientId", "success");
            $info["result"] = $userInfo[0];

            return $info;
            //return $userInfo ; 
            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        return $this->displayResult("getPatientInformation", "patientIdNotExists", array("-2"));
        // return $this->addPatient() ; 
    }

    public function updatePersonInformation() {
        $query = "Update patient set firstName = ?, lastName=?,dateOfBirth=?,address=?,phone=?,emContactPerson=?,emContactPhone=? where id = ? ";
        $parameter = array(
            $this->getQueryParam("firstName"),
            $this->getQueryParam("lastName"),
            $this->getQueryParam("dateOfBirth"),
            $this->getQueryParam("address"),
            $this->getQueryParam("phone"),
            $this->getQueryParam("emContactPerson"),
            $this->getQueryParam("emContactPhone"),
            $this->getQueryParam("id")
                );


        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();

        if ($count > 0) {
            return array("status" => "1", "msg" => "update Successful");
        }

        return array("status" => "-1", "msg" => "update UnSuccessful");

        // $this->displayResult("setInPatientStatus", "updateFail" , array("-1")) ;
    }

    public function addPhysicalCheck() {

        //$recordExists = $this->isPhysicalRecordExists();
        
        //if ($recordExists["status"] == "1"){
          //  return $this->setPhysicalCheck() ; 
        //}
        
        return $this->insertPhysicalCheck() ; 
    }

    public function getPhysicalCheckInformation () {
        
        $query = 'SELECT b.id,b.userName,a.PhyCk_Height height,a.PhyCk_Weight weight, a.PhyCk_Systolic systolic , a.PhyCk_Diastolic diastolic,a.PhyCk_DateTime checkTime from  Physical_Check '
                . ' a left join Staff b on a.PhyCk_Staff_id = b.id '
                . ' where a.patientId = ? and a.PhyCk_Status != "-1"; ';

        $parameter = array($this->getQueryParam("inPatientId"));

        
        $userInfo = $this->db->getResult($query, $parameter);
        
        
        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            $info = $this->getErrorMessage("getPatientId", "success");
            

            return array("status"=>"1" , "result" =>$userInfo);
            //return $userInfo ; 
            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }
        return array("status"=>"-1" , "result" =>array()) ; 
        
    }
    
    public function insertPhysicalCheck() {
        $query = "INSERT INTO Physical_Check (`PhyCk_Staff_id`,`patientId`,  `PhyCk_Height`,`PhyCk_Weight`,`PhyCk_Systolic`,`PhyCk_Diastolic`,`PhyCk_DateTime`,`PhyCk_Status`) "
                . "VALUES (?,?,?,?,?,?,NOW(),'1') ; ";

        $parameter = array(
            $this->getSession("uid"),
            $this->getQueryParam("patientId"),
            $this->getQueryParam("height"),
            $this->getQueryParam("weight"),
            $this->getQueryParam("systolic"),
            $this->getQueryParam("diastloic"),
        );

        $addPhyCheck = $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();
        if ($count > 0) {
            return array("status" => "1", "msg" => "insert  Successful");
        }

        return $this->displayResult("addPhyCheck", "isAddSuccess");
    }

    public function setPhysicalCheck() {
        $query = "Update Physical_Check set PhyCk_Staff_id = ?, PhyCk_Height=?,PhyCk_Weight=?,PhyCk_Systolic=?,PhyCk_Diastolic=?,PhyCk_DateTime=NOW() where patientId = ? ";
        $parameter = array(
            $this->getSession("uid"),
            $this->getQueryParam("height"),
            $this->getQueryParam("weight"),
            $this->getQueryParam("systolic"),
            $this->getQueryParam("diastolic"),
            $this->getQueryParam("patientId")
                );


        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();

        if ($count > 0) {
            return array("status" => "1", "msg" => "update Successful");
        }

        return array("status" => "-1", "msg" => "update UnSuccessful");
    }

    public function isPhysicalRecordExists() {



        $query = "SELECT PhyCk_id from `Physical_Check` where patientId= ? ";
        $parameter = array(
            $this->getQueryParam("patientId")
                );


        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();

        if ($count > 0) {
            return array("status" => "1", "msg" => "Record Exists");
        }

        return array("status" => "-1", "msg" => "Record not exists");
    }

    
    public function getPhysicalRecord () {
        
        $query = "SELECT PhyCk_id from `Physical_Check` where patientId= ? ";
        $parameter = array(
            $this->getQueryParam("patientId")
                );


        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();

        if ($count > 0) {
            return array("status" => "1", "msg" => "Record Exists");
        }

        return array("status" => "-1", "msg" => "Record not exists");
        
    }
    
    public function getPatientHistory (){
        $query = "SELECT bookingId , InpRec_id InPatientId, InpRec_DateTimeIn incomedate, InpRec_DateTimeOut outdate,InpRec_log_time bookingtime from Inpatient_Record "
                . "where InpRec_Patient_id = ? ";
        $parameter = array(
            $this->getQueryParam("patientId")
        ); 


        $patientHistory = $this->db->getResult($query, $parameter);
        

        $info = $this->getErrorMessage("getPatientHistory", "fail");

        if (isset($patientHistory[0]) && $patientHistory[0]["bookingId"] > 0) {

            $info = $this->getErrorMessage("getPatientHistory", "success");
            $info["result"] = $patientHistory;

            return $info;
        }

        $info["result"] = array() ; 
        return $info;
    }
    
    
    public function getHealthStatus () {
        //getHealthStatus
    }
    
    
    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->rtnMessage["$func"]["$status"];
    }

}
