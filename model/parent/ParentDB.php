<?php

class ParentDB {

    private $queryParam = array();
    private $db = null;
    public $rtnMessage = array() ; 

    public function __construct() {
        $this->db = new db();
        $setting = new Setting();
        $dbInfo = $setting->getDBInfo();
        $this->db->setDBSetting($dbInfo);
    }

    public function setQueryParam($queryParam) {
        $this->queryParam = $queryParam;
    }
    
    public function setQueryParamWithKey ($key , $value){
        $this->queryParam["$key"] = $value ; 
    }

    public function getQueryParamALL() {
        return $this->queryParam;
    }

    public function getQueryParam($key) {
        return $this->queryParam[$key];
    }

    public function getDb() {
        return $this->db;
    }

    public function getRtnMessage($func, $status, $vsprintf = array()) {
        if (!isset($this->rtnMessage ["$func"]["$status"]["msg"])){
            return ; ;
        }
        
        $message = $this->rtnMessage ["$func"]["$status"]["msg"];

        if (!empty($vsprintf)) {
            $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
        }


        return $this->rtnMessage["$func"]["$status"];
    }

}
