<?php

class Bookingdb extends VerifyWS {

    private $db = null;
    private $queryParam = array();
    private $rtnMessage = array(
        "addBooking" => array(
            "addSuccess" => array("status" => "1", "msg" => "Add Booking successfully", "result" => array()),
            "isAddSuccess" => array("status" => "2002", "msg" => "Patient is not add successfuly to database"),
        ),
        "addPatient" => array(
            "addSuccess" => array("status" => "1", "msg" => "Add Booking successfully"),
            "isAddSuccess" => array("status" => "2002", "msg" => "Patient is not add successfuly to database")
        ),
        "isBookingBefore" => array(
            "bookingBefore" => array("status" => "3003", "msg" => "Booking is exists at start date : %s and end date  : %s Bed type is %s"),
            "bookingNotExists" => array("status" => "1", "msg" => "Booking Not Exists")
        ),
        "getBookingInfo" => array(
            "bookingNotExists" => array("status" => "3002", "msg" => "Booking not exists : %s"),
            "bookingGetSuccess" => array("status" => "1", "msg" => "Booking successfully")
        ),
        "getBedTypeInformation" => array(
            "bedTypeNotExists" => array("status" => "3001", "msg" => "Bed Type not exists : %s"),
            "bedTypeGetSuccess" => array("status" => "1", "msg" => "bed type get successfully")
        ),
        "addPatient" => array(
            "isAddSuccess" => array("status" => "2002", "msg" => "Patient is not add successfuly to database")
        ),
        "getPatientBookingInformation" => array(
            "getSuccess" => array("status" => "1", "msg" => "Patient Information is get successfully"),
            "bookingNotExists" => array("status" => "3004", "msg" => "Booking Not Exists")
        ),
        "setBookingStatus" => array(
            "updateSuccess" => array("status" => "1", "msg" => "Booking Status is changed successfully"),
            "updateFail" => array("status" => "3005", "msg" => "Booking Status does not update successfully")
        )
    );
    private $successMessage = array(
        "getPatientInformation" => array(
            "patientInfo" => array("status" => "1", "msg" => "Success to get patient information", "result" => array())
        ),
    );
    
   

    public function __construct() {
        $this->db = new db();
        $setting = new Setting();
        $dbInfo = $setting->getDBInfo();
        $this->db->setDBSetting($dbInfo);
    }

    public function setQueryParam($queryParam) {
        $this->queryParam = $queryParam;
    }

    public function getQueryParam($key) {
        return $this->queryParam[$key];
    }

    public function getBedTypeInformation() {
        $query = "SELECT bed_type_id,name,price,amount from bed_type  where amount>0;";
        $parameter = array();

        $bedType = $this->db->getResult($query, $parameter);

        if (isset($bedType[0]) && $bedType[0]["bed_type_id"] > 0) {

            $info = $this->getErrorMessage("getBedTypeInformation", "bedTypeGetSuccess");
            $info["result"] = $bedType;

            return $info;
            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        $this->displayResult("getBedTypeInformation", "bedTypeNotExists", array("-1"));
    }

    public function getBookingInfo() {

        $query = "SELECT `bed_type_id`,`name` bedName, amount from bed_type where bed_type_id = ?";
        $parameter = array($this->getQueryParam("bedType"));

        $bedAmount = $this->db->getResult($query, $parameter);

        if (isset($bedAmount[0]) && $bedAmount[0]["bed_type_id"] > 0) {

            $info = $this->getErrorMessage("getBookingInfo", "bookingGetSuccess");
            $info["result"] = $bedAmount;

            return $info;
            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        $this->displayResult("getBookingInfo", "bookingNotExists", array("-1"));
    }

    public function getPatientBookingInformation() {

        $query = "SELECT a.BedRes_id bookingId,a.BedRes_DateTimeIn incomedate,a.BedRes_ReserveDateTime outdate,a.BedRes_Logtime bookingDate,"
                . "a.BedRes_Patient_id patientId,CONCAT(p.firstName,' ',p.lastName) fullName,  "
                . "a.BedRes_Status bookingStatus,"
                . "b.name bedType, d.type roomType,d.location floor "
                . "from patient p "
                . "left join bed_reservation a on p.id = a.BedRes_Patient_id "
                . "left join bed_type b on a.BedRes_Bed_id = b.bed_type_id "
                . "left join bed c on a.BedRes_Bed_id = c.rood_id "
                . "left join room d  on c.rood_id = d.id "
                . "where a.BedRes_id= ? and a.BedRes_Status = '1'";


        $parameter = array(
            $this->getQueryParam("bookingId"),
        );

        
        
        $isBooking = $this->db->getResult($query, $parameter);

        if (isset($isBooking[0]) && $isBooking[0]["bookingId"] > 0) {

            $info = $this->getErrorMessage("getPatientBookingInformation", "getSuccess");
            $info["result"] = $isBooking[0];

            return $info;

            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        $info = $this->getErrorMessage("getPatientBookingInformation", "bookingNotExists");
        $info["result"] = array();

        return $info;
        //$this->displayResult("isBookingBefore", "bookingNotExists" , array()) ;
    }

    public function isBookingBefore() {

        /* SELECT a.BedRes_id, b.name from bed_reservation a 
          left join `bed_type` b on a.BedRes_Bed_id = b.bed_type_id
          where a.BedRes_Bed_id = '1' and a.BedRes_Patient_id= '22' and
          (a.BedRes_DateTimeIn >= '2015-12-04' and a.BedRes_ReserveDateTime <= '2015-12-04' ) */

        $query = "SELECT a.BedRes_id, b.name from bed_reservation a "
                . "left join `bed_type` b on a.BedRes_Bed_id = b.bed_type_id "
                . " where a.BedRes_Bed_id = ? and a.BedRes_Patient_id=? and "
                . "(a.BedRes_DateTimeIn >= ? and a.BedRes_ReserveDateTime <= ? )";
        $parameter = array(
            $this->getQueryParam("bedType"),
            $this->getQueryParam("patientId"),
            $this->getQueryParam("incomedate"),
            $this->getQueryParam("outdate")
        );

        $isBooking = $this->db->getResult($query, $parameter);
        

        if (isset($isBooking[0]) && $isBooking[0]["BedRes_id"] > 0) {

            $this->displayResult(
                    "isBookingBefore", "bookingBefore", array(
                $this->getQueryParam("incomedate"),
                $this->getQueryParam("outdate"),
                $isBooking[0]["name"]
                    )
            );

            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        return true;
        //$this->displayResult("isBookingBefore", "bookingNotExists" , array()) ;
    }

    public function addBooking() {
        $query = "INSERT INTO `bed_reservation` (`BedRes_Bed_id`,`BedRes_Patient_id`,`BedRes_DateTimeIn`, `BedRes_ReserveDateTime`,`BedRes_Status`, `BedRes_Logtime`)";
        $query.="VALUES (?,?,?,?,'1', NOW())";

        $parameter = array(
            $this->getQueryParam("bedType"),
            $this->getQueryParam("patientId"),
            $this->getQueryParam("incomedate"),
            $this->getQueryParam("outdate")
        );
        $this->db->getResult($query, $parameter);

        $count = $this->db->getCount();
        $lastInsertId = $this->db->getLastInsertId();
        if ($count > 0) {

            $info = $this->getErrorMessage("addBooking", "addSuccess");
            $info["result"]["bookingId"] = $lastInsertId;

            return $info;
        }

        return $this->displayResult("addBooking", "isAddSuccess");
    }

    public function isPatientExisits() {

        $query = "SELECT `id` from patient where hkid=?";
        $parameter = array($this->getQueryParam("HKID"));

        $userInfo = $this->db->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {
            $this->displayResult("isPatientExisits", "isPatientExisits", array("-1"));
        }

        return $this->addPatient();
    }

    public function getBookinginformation() {

        $query = "SELECT * from patient where id=?; ";
        $parameter = array($this->getQueryParam("patientId"));

        $userInfo = $this->db->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {


            return $userInfo;
            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        return $this->displayResult("isPatientExisits", "isPatientExisits", array("-2"));
        // return $this->addPatient() ; 
    }
    
    
    public function setBookingStatus () {
        $query = "Update `bed_reservation` set BedRes_Status =? where BedRes_id = ? limit 1 ;  "; 
        $parameter = array(
            $this->getQueryParam("status") ,
            $this->getQueryParam("bookingId")
        );

        $bookingStatus = $this->db->getResult($query, $parameter);
        
        $count = $this->db->getCount();
        

        $info = $this->getErrorMessage("setBookingStatus", "updateFail",array());
        
        if ($count > 0) {


            $info = $this->getErrorMessage("setBookingStatus", "updateSuccess");

            return $info;
            //$this->displayResult("isPatientExisits", "isPatientExisits" , array("-1")) ;  
        }

        
        return $info ; 
    }
    
    public function getBookingIdWithStatus () {
        
    }
    

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->rtnMessage["$func"]["$status"];
    }

}
