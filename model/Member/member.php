<?php

//class Member extends VerifyWS {
class Member extends ParentDB {

    public $rtnMessage = array(
        "addUser" => array(
            "addSuccess" => array("status" => "1", "msg" => "User:%s add successfully"),
            "addError" => array("status" => "2003", "msg" => "user add to db that have connection error"),
        ),
        "isUserExisits" => array(
            "userNotExists" => array("status" => "2001", "msg" => "UserName or UserPassword is wrong Status : %s"),
        ),
        "getUserInfo" => array(
            "userExists" => array("status" => "2004", "uid" => "", "msg" => "User is Exisits.Please use the their user name"),
            "userNotExists" => array("status" => "2005", "msg" => "User does not Exists"),
            "accBan" => array("status" => "2006", "msg" => "Account has been banned status : %s"),
        ),
        "isUidValid" => array(
            "userExists" => array("status" => "1", "uid" => "", "msg" => "User Exisits"),
            "accDelete" => array("status" => "2008", "msg" => "Account has been deleted status : %s"),
        ),
        "updateUserInfo" => array(
            "updateSuccess" => array("status" => "1", "msg" => "User update information successfully"),
            "updateFail" => array("status" => "2009", "msg" => "Please edit information before update information"),
        ),
        "setUserStatus" => array(
            "updateSuccess" => array("status" => "1", "msg" => "User delete successfully"),
            "updateError" => array("status" => "2007", "msg" => "Delete user that have connection error"),
        ),
    );
    private $sex = array(
        "Mr" => "1",
        "Mrs" => "2"
    );

    public function addUser() {
        $password = $this->getQueryParam("userPassword");
        $encryptPassword = $this->getEncryptPassword($password);

        $query = "INSERT INTO `Staff` ("
                . "`userName`,"
                . "`firstName`,"
                . "`lastName`,"
                . "`password`,"
                . "`sex`,"
                . "`hkid`,"
                . "`birthday`,"
                . "`email`,"
                . "`directLine`,"
                . "`address`,"
                . "`mobile`,"
                . "`jobTitle`,"
                . "`team`,"
                . "`type`,"
                . "`status`,"
                . "`createTime`)";
        $query.="VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW());";

        $parameter = array(
            $this->getQueryParam("userName"),
            $this->getQueryParam("firstName"),
            $this->getQueryParam("lastName"),
            $encryptPassword,
            $this->sex[$this->getQueryParam("sex")],
            $this->getQueryParam("HKID"),
            $this->getQueryParam("birthday"),
            $this->getQueryParam("email"),
            $this->getQueryParam("directLine"),
            addslashes($this->getQueryParam("address")),
            $this->getQueryParam("mobile"),
            $this->getQueryParam("jobTitle"),
            $this->getQueryParam("team"),
            $this->getQueryParam("role"),
            $this->getQueryParam("accStatus"),
        );
        //echo "parameter" ; 
        //print_r_pre($parameter) ; 

        $userInfo = $this->getDb()->getResult($query, $parameter);

        $count = $this->getDb()->getCount();

        if ($count > 0) {
            return $this->getRtnMessage("addUser", "addSuccess", $this->getQueryParam("userName"));
        }


        return $this->getRtnMessage("addUser", "addError");
    }

    public function getUserInfo() {
        $query = "SELECT * from Staff where userName=?";
        $parameter = array(
            $this->getQueryParam("userName")
        );

        $userInfo = $this->getDb()->getResult($query, $parameter);


        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] == "1") {

                $rtnMsg = $this->getRtnMessage("getUserInfo", "userExists");
                $rtnMsg["uid"] = $userInfo[0]["id"];
                return $rtnMsg;
            }

            return $this->getRtnMessage("getUserInfo", "accBan", array($userInfo[0]["status"]));
        }


        return $this->getRtnMessage("getUserInfo", "userNotExists", array("-1"));
    }

    public function getAllUserInformation($start, $end) {

        $attr = 'a.*';
        $parameter = array();
        $query = "SELECT %s ,b.name from Staff a left join role b on a.type=b.id"
                . " where a.status != '-2' order by a.createTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();
        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                $parameter1 .= $value;
                if ($count != ($i + 1)) {
                    $parameter1 .= ",";
                }
                $i++;
            }
            $query = vsprintf($query, $parameter1);
        } else {
            $query = vsprintf($query, array($attr));
        }

        $userInfo = $this->getDb()->getResult($query, $parameter);

        return $userInfo;
    }

    public function isUidValid() {
        $query = "SELECT * from Staff where id=? ";
        $parameter = array(
            $this->getQueryParam("id")
        );

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] != "-2") {

                $rtnMsg = $this->getRtnMessage("isUidValid", "userExists", $this->getQueryParam("id"));
                $rtnMsg["uid"] = $userInfo[0]["id"];
                $rtnMsg["data"] = $userInfo[0];
                return $rtnMsg;
            }

            return $this->getRtnMessage("isUidValid", "accDelete", array($userInfo[0]["status"]));
        }


        return $this->getRtnMessage("getUserInfo", "userNotExists", array("-1"));
    }

    public function setUserStatus() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set status = ? where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"),
            $this->getQueryParam("status")
        );

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);

        /*
          if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
          return $this->getRtnMessage("setUserStatus", "updateSuccess");
          } */

        return $this->getRtnMessage("setUserStatus", "updateSuccess");
    }

    public function isUserExisits($username, $password) {
        $encryptPassword = $this->getEncryptPassword($password);

        $query = "SELECT `id`, `status`,`jobTitle` from Staff where userName=? and password = ?";
        $parameter = array($username, $encryptPassword);

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] == "1") {
                $this->updateLoginTime($userInfo[0]["id"]);

                return array("status" => "1", "uid" => $userInfo[0]["id"], "jobTitle" => $userInfo[0]["jobTitle"]);
            }

            return $this->getRtnMessage("getUserInfo", "accBan", array($userInfo[0]["status"]));
        }

        return $this->getRtnMessage("isUserExisits", "userNotExists", array("-1"));
    }

    public function updateUserInfo() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set "
                . "firstName=?,"
                . "lastName=?,"
                . "sex=?,"
                . "email=?,"
                . "directLine=?,"
                . "address=?,"
                . "mobile=?,"
                . "jobTitle=?,"
                . "type=?,"
                . "team=?,"
                . "status=? "
                . " where id = @id limit 1; ";

        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"),
            $this->getQueryParam("firstName"),
            $this->getQueryParam("lastName"),
            $this->getQueryParam("sex"),
            $this->getQueryParam("email"),
            $this->getQueryParam("directLine"),
            $this->getQueryParam("address"),
            $this->getQueryParam("mobile"),
            $this->getQueryParam("jobTitle"),
            $this->getQueryParam("role"),
            $this->getQueryParam("team"),
            $this->getQueryParam("accStatus")
        );


        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);

        $verify = new Verify();
        $verify->setSession("jobTitle", $this->getQueryParam("jobTitle"));
        $count = $this->getDb()->getCount();


        return $this->getRtnMessage("updateUserInfo", "updateSuccess");
    }

    public function getUserInfoWithCustomQuery($start, $end) {

        $numericField = array("id", "status");
        $parameter = array();
        $query = "SELECT a.* ,b.name from Staff a left join role b on a.type=b.id"
                . " where %s a.status != '-2' order by a.createTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();

        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                
                if (in_array($key, $numericField)) {
                    $parameter1 .= "a." . $key . "='" . $value . "'";
                } else {
                    $parameter1 .= "a." . $key . " like '%" . $value . "%'";
                }
                
                $parameter1 .= " AND ";
                $i++;
            }
            $query = vsprintf($query, array($parameter1));
        } else {
            $query = vsprintf($query, array($attr));
        }
        //echo $query ; 
        $userInfo = $this->getDb()->getResult($query, $parameter);

        if ((isset($userInfo[0]) && $userInfo[0]["id"] > 0)) {
            return array("status" => "1", "userInfo" => $userInfo);
        }

        return array("status" => "-1");
    }

    private function updateLoginTime($id) {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set loginTime = NOW() where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array($id);

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);


        if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
            return array("status" => "1", "msg" => "更改時間成功");
        }

        return array("status" => "-1", "msg" => "更改失敗");
    }

    private function getEncryptPassword($password) {

        return md5($password);
    }

    /*
      public function getRtnMessage($func, $status, $vsprintf = array()) {
      $message = $this->rtnMessage ["$func"]["$status"]["msg"];

      if (!empty($vsprintf)) {
      $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
      }


      return $this->rtnMessage["$func"]["$status"];
      } */
    /*
      private function displayResult($func, $status, $vsprintf = array()) {

      return $this->getRtnMessage($func, $status, $vsprintf);
      }
     */
}
