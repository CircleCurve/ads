<?php

class TeamDB extends ParentDB {
    
    public $rtnMessage = array(
        "getTeamInfo"=> array(
           "teamNotExists" =>  array("status" => "4000", "msg" => "team does not exists"),
           "teamNotEnable" =>  array("status" => "4001", "msg" => "team does not enable to select"),
            "teamExists" => array("status" => "1" , "msg" => "team Exists ", "data"=>array())
        ), 
        
    );
    
    public function getTeamInfo () {
        
        $query = "SELECT * from team where status = '1'";
        $parameter = array(
            //$this->getQueryParam("id")
        ); 

        $teamInfo = $this->getDb()->getResult($query, $parameter);
        
        if (isset($teamInfo[0]) && $teamInfo[0]["id"] > 0) {
            
            if ($teamInfo[0]["status"] == "1"){
                
                $rtnMsg = $this->getRtnMessage("getTeamInfo", "teamExists") ; 
                $rtnMsg["data"] = $teamInfo ; 
                return $rtnMsg ; 
            }
            
            return  $this->getRtnMessage("getTeamInfo","teamNotEnable",array($teamInfo[0]["status"]) );
        }
        
        
        return  $this->getRtnMessage("getTeamInfo","teamNotExists",array("-1") );
        
        
    }
    
    
    public function getTeamInfoWithId () {
        
        $query = "SELECT * from team where id=? and status = '1'";
        $parameter = array(
            $this->getQueryParam("team")
            //'20'
        ); 
        

        $teamInfo = $this->getDb()->getResult($query, $parameter);
        
        if (isset($teamInfo[0]) && $teamInfo[0]["id"] > 0) {
            
            if ($teamInfo[0]["status"] == "1"){
                
                $rtnMsg = $this->getRtnMessage("getTeamInfo", "teamExists") ; 
                $rtnMsg["id"] = $teamInfo[0]["id"] ; 
                return $rtnMsg ; 
            }
            
            return  $this->getRtnMessage("getTeamInfo","teamNotEnable",array($teamInfo[0]["status"]) );
        }
        
        
        return  $this->getRtnMessage("getTeamInfo","teamNotExists",array("-1") );
        
        
    }
    

    
}