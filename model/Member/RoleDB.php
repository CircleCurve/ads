<?php

class RoleDB extends ParentDB {
    
    public $rtnMessage = array(
        "getRoleInfo"=> array(
           "roleNotExists" =>  array("status" => "3000", "msg" => "Role does not exists"),
           "roleNotEnable" =>  array("status" => "3001", "msg" => "Role does not enable to select"),
            "roleExists" => array("status" => "1" , "msg" => "Role Exists ", "data"=>array())
        ), 
        
    );
    
    public function getRoleInfo () {
        
        $query = "SELECT * from role where status = '1'";
        $parameter = array(
            //$this->getQueryParam("id")
        ); 

        $roleInfo = $this->getDb()->getResult($query, $parameter);
        
        if (isset($roleInfo[0]) && $roleInfo[0]["id"] > 0) {
            
            if ($roleInfo[0]["status"] == "1"){
                
                $rtnMsg = $this->getRtnMessage("getRoleInfo", "roleExists") ; 
                $rtnMsg["data"] = $roleInfo ; 
                return $rtnMsg ; 
            }
            
            return  $this->getRtnMessage("getRoleInfo","roleNotEnable",array($roleInfo[0]["status"]) );
        }
        
        
        return  $this->getRtnMessage("getRoleInfo","roleNotExists",array("-1") );
        
        
    }
    
    
    public function getRoleInfoWithId () {
        
        $query = "SELECT * from role where id=? and status = '1'";
        $parameter = array(
            $this->getQueryParam("role")
            //'20'
        ); 
        

        $roleInfo = $this->getDb()->getResult($query, $parameter);
        
        if (isset($roleInfo[0]) && $roleInfo[0]["id"] > 0) {
            
            if ($roleInfo[0]["status"] == "1"){
                
                $rtnMsg = $this->getRtnMessage("getRoleInfo", "roleExists") ; 
                $rtnMsg["id"] = $roleInfo[0]["id"] ; 
                return $rtnMsg ; 
            }
            
            return  $this->getRtnMessage("getRoleInfo","roleNotEnable",array($roleInfo[0]["status"]) );
        }
        
        
        return  $this->getRtnMessage("getRoleInfo","roleNotExists",array("-1") );
        
        
    }
    

    
}