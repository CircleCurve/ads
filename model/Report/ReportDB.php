<?php

//class Member extends VerifyWS {
class ReportDB extends ParentDB {

    public $rtnMessage = array(
        "getNumberOfSymptoms" => array(
            "getSuccess" => array("status" => "1", "info" => array(), "msg" => "Get successfully"),
            "getError" => array("status" => "2003", "msg" => "Get Error"),
        ),
        "isIncidentExisits" => array(
            "userNotExists" => array("status" => "2001", "msg" => "UserName or UserPassword is wrong Status : %s"),
        ),
        "getEventInfo" => array(
            "exists" => array("status" => "1", "info" => array(), "msg" => "Event is Exisits."),
            "notExists" => array("status" => "2005", "info" => array(), "msg" => "Event does not Exists"),
        ),
    );
    private $sex = array(
        "Mr" => "1",
        "Mrs" => "2"
    );
    private $serviceType = array(
        array("id" => "1", "name" => "Express"),
        array("id" => "2", "name" => "Standard"),
        array("id" => "3", "name" => "Per-Call"),
    );

    public function getNumberOfSymptoms() {
        $query = 'SELECT a.symptomsId, d.name, day (c.logTime) month, COUNT( 1 ) amount ' .
                'FROM SymptomsIssue a ' .
                'LEFT JOIN symptoms b ON a.symptomsId = b.id ' .
                'LEFT JOIN Incident c ON c.id = a.IncidentId ' .
                'LEFT JOIN device d ON b.subCategory = d.id ';

        $queryParam = $this->getQueryParamALL() ; 
        
        
        if (empty($queryParam)) {

            $query .= 'WHERE YEAR(c.logTime) = ? ';

            $parameter = array(
                //$this->getQueryParam("uid")
                '2016'
            );
        } else {

            $query .= "WHERE logTime >= ? AND logTime < ? ";

            $parameter = array(
                $this->getQueryParam("startDate"), 
                $this->getQueryParam("endDate")
            );
        }
        $query.= 'GROUP BY month , d.name'; 

        
        // . 'WHERE YEAR(c.logTime) = ? logTime'.
        //   'GROUP BY month , d.name';




        $info = $this->getDb()->getResult($query, $parameter);


        if (isset($info[0]) && $info[0]["symptomsId"] > 0) {

            //print_r_pre($info) ; 
            //$this->rtnMessage["getEventInfo"]["info"] = $info;
            $this->rtnMessage["getNumberOfSymptoms"]["getSuccess"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getNumberOfSymptoms", "getSuccess");

            return $rtnMsg;
        }


        return $this->getRtnMessage("getNumberOfSymptoms", "getError");
    }

    public function getAllUserInformation($start, $end) {

        $attr = 'a.*';
        $parameter = array();
        $query = "SELECT %s ,b.name from Staff a left join role b on a.type=b.id"
                . " where a.status != '-2' order by a.createTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();
        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                $parameter1 .= $value;
                if ($count != ($i + 1)) {
                    $parameter1 .= ",";
                }
                $i++;
            }
            $query = vsprintf($query, $parameter1);
        } else {
            $query = vsprintf($query, array($attr));
        }

        $userInfo = $this->getDb()->getResult($query, $parameter);

        return $userInfo;
    }

    public function isUidValid() {
        $query = "SELECT * from Staff where id=? ";
        $parameter = array(
            $this->getQueryParam("id")
        );

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] != "-2") {

                $rtnMsg = $this->getRtnMessage("isUidValid", "userExists", $this->getQueryParam("id"));
                $rtnMsg["uid"] = $userInfo[0]["id"];
                $rtnMsg["data"] = $userInfo[0];
                return $rtnMsg;
            }

            return $this->getRtnMessage("isUidValid", "accDelete", array($userInfo[0]["status"]));
        }


        return $this->getRtnMessage("getUserInfo", "userNotExists", array("-1"));
    }

    public function setUserStatus() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set status = ? where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"),
            $this->getQueryParam("status")
        );

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);

        /*
          if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
          return $this->getRtnMessage("setUserStatus", "updateSuccess");
          } */

        return $this->getRtnMessage("setUserStatus", "updateSuccess");
    }

    public function isUserExisits($username, $password) {
        $encryptPassword = $this->getEncryptPassword($password);

        $query = "SELECT `id`, `status`,`jobTitle` from Staff where userName=? and password = ?";
        $parameter = array($username, $encryptPassword);

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] == "1") {
                $this->updateLoginTime($userInfo[0]["id"]);

                return array("status" => "1", "uid" => $userInfo[0]["id"], "jobTitle" => $userInfo[0]["jobTitle"]);
            }

            return $this->getRtnMessage("getUserInfo", "accBan", array($userInfo[0]["status"]));
        }

        return $this->getRtnMessage("isUserExisits", "userNotExists", array("-1"));
    }

    public function updateUserInfo() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set "
                . "firstName=?,"
                . "lastName=?,"
                . "sex=?,"
                . "email=?,"
                . "directLine=?,"
                . "address=?,"
                . "mobile=?,"
                . "jobTitle=?,"
                . "type=?,"
                . "team=?,"
                . "status=? "
                . " where id = @id limit 1; ";

        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"),
            $this->getQueryParam("firstName"),
            $this->getQueryParam("lastName"),
            $this->getQueryParam("sex"),
            $this->getQueryParam("email"),
            $this->getQueryParam("directLine"),
            $this->getQueryParam("address"),
            $this->getQueryParam("mobile"),
            $this->getQueryParam("jobTitle"),
            $this->getQueryParam("role"),
            $this->getQueryParam("team"),
            $this->getQueryParam("accStatus")
        );


        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);

        $verify = new Verify();
        $verify->setSession("jobTitle", $this->getQueryParam("jobTitle"));
        $count = $this->getDb()->getCount();


        return $this->getRtnMessage("updateUserInfo", "updateSuccess");
    }

    public function getUserInfoWithCustomQuery($start, $end) {

        $numericField = array("id", "status");
        $parameter = array();
        $query = "SELECT a.* ,b.name from Staff a left join role b on a.type=b.id"
                . " where %s a.status != '-2' order by a.createTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();

        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                if (in_array($key, $numericField)) {
                    $parameter1 .= "a." . $key . "='" . $value . "'";
                } else {
                    $parameter1 .= "a." . $key . " like '%" . $value . "%'";
                }

                $parameter1 .= " AND ";
                $i++;
            }
            $query = vsprintf($query, array($parameter1));
        } else {
            $query = vsprintf($query, array($attr));
        }
        //echo $query ; 
        $userInfo = $this->getDb()->getResult($query, $parameter);

        if ((isset($userInfo[0]) && $userInfo[0]["id"] > 0)) {
            return array("status" => "1", "userInfo" => $userInfo);
        }

        return array("status" => "-1");
    }

    public function getSymptom() {
        $query = "SELECT * from symptoms";

        $parameter = array(
                //$this->getQueryParam("userName")
        );

        $info = $this->getDb()->getResult($query, $parameter);


        if (isset($info[0]) && $info[0]["id"] > 0) {

            $this->rtnMessage["getSymptom"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getSymptom", "success");
            return $rtnMsg;
        }


        return $this->getRtnMessage("getSymptom", "fail");
    }

    public function getDevice() {
        $query = "SELECT a.mainCategory, a.subCategory subCategory,b.name from "
                . "symptoms a left join device b on a.subcategory = b.id "
                . "where a.status = '1' and b.status = '1' "
                . "group by (a.subCategory) ";

        $parameter = array(
                //$this->getQueryParam("userName")
        );
        $info = $this->getDb()->getResult($query, $parameter);

        if (isset($info[0]) && $info[0]["mainCategory"] > 0) {

            $this->rtnMessage["getDevice"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getDevice", "success");
            return $rtnMsg;
        }


        return $this->getRtnMessage("getDevice", "fail");
    }

    private function updateLoginTime($id) {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set loginTime = NOW() where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array($id);

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);


        if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
            return array("status" => "1", "msg" => "更改時間成功");
        }

        return array("status" => "-1", "msg" => "更改失敗");
    }

    private function getEncryptPassword($password) {

        return md5($password);
    }

    /*
      public function getRtnMessage($func, $status, $vsprintf = array()) {
      $message = $this->rtnMessage ["$func"]["$status"]["msg"];

      if (!empty($vsprintf)) {
      $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
      }


      return $this->rtnMessage["$func"]["$status"];
      } */
    /*
      private function displayResult($func, $status, $vsprintf = array()) {

      return $this->getRtnMessage($func, $status, $vsprintf);
      }
     */
}

/*
SELECT a.symptomsId, b.subCategory, c.name, COUNT( 1 ) 
FROM SymptomsIssue a
LEFT JOIN symptoms b ON a.symptomsId = b.id
LEFT JOIN device c ON b.subCategory = c.id
GROUP BY c.name
 */

/*
 * SELECT a.symptomsId, b.subCategory, c.logTime, d. * , COUNT( 1 ) 
FROM SymptomsIssue a
LEFT JOIN symptoms b ON a.symptomsId = b.id
LEFT JOIN Incident c ON c.id = a.IncidentId
LEFT JOIN device d ON b.subCategory = d.id
GROUP BY MONTH( c.logTime ) , d.name
 
 SELECT * from (
SELECT a.symptomsId, b.subCategory, c.logTime
FROM SymptomsIssue a
LEFT JOIN symptoms b ON a.symptomsId = b.id
LEFT JOIN Incident c ON c.id = a.IncidentId
GROUP BY MONTH( c.logTime ) 
) e left join device d on e.subCategory = d.id
 */
/*
Number of syptoms
SELECT a.symptomsId, d.name, c.logTime, COUNT( 1 ) 
FROM SymptomsIssue a
LEFT JOIN symptoms b ON a.symptomsId = b.id
LEFT JOIN Incident c ON c.id = a.IncidentId
LEFT JOIN device d ON b.subCategory = d.id
GROUP BY MONTH( c.logTime ) , d.name
 
 * 
 * 
 * 
 * Number of Incident by Model type 
 * 
 * 
 */
