<?php

//class Member extends VerifyWS {
class IncidentDB extends ParentDB {

    public $rtnMessage = array(
        "addIncident" => array(
            "addSuccess" => array("status" => "1","lastInsertId"=>"",  "msg" => "Incident add successfully"),
            "addError" => array("status" => "2003","lastInsertId"=>"", "msg" => "Incident add to db that have connection error"),
        ),
        "isIncidentExisits" => array(
            "userNotExists" => array("status" => "2001", "msg" => "UserName or UserPassword is wrong Status : %s"),
        ),
        "getIncidentInfo" => array(
            "success" => array("status" => "1", "info" => array(), "msg" => "Incident is Exisits."),
            "fail" => array("status" => "2005", "info" => array(), "msg" => "Incident does not Exists"),
        ),
        "isUidValid" => array(
            "userExists" => array("status" => "1", "uid" => "", "msg" => "User Exisits"),
            "accDelete" => array("status" => "2008", "msg" => "Account has been deleted status : %s"),
        ),
        "updateIncident" => array(
            "updateSuccess" => array("status" => "1", "msg" => "Update information successfully"),
            "updateFail" => array("status" => "2009", "msg" => "Please edit information before update information"),
        ),
        "setIncidentStatus" => array(
            "updateSuccess" => array("status" => "1", "msg" => "User delete successfully"),
            "updateError" => array("status" => "2007", "msg" => "Delete user that have connection error"),
        ),
        "getDevice" => array(
            "success" => array("status" => "1", "info" => array(), "msg" => "Device exists"),
            "fail" => array("status" => "2005", "msg" => "Device does not Exists"),
        ),
        "getSymptom" => array(
            "success" => array("status" => "1", "info" => array(), "msg" => "Symptoms exists"),
            "fail" => array("status" => "2005", "msg" => "Symptoms does not Exists"),
        ),
    );
    private $sex = array(
        "Mr" => "1",
        "Mrs" => "2"
    );
    private $serviceType = array(
        array("id" => "1", "name" => "Express"),
        array("id" => "2", "name" => "Standard"),
        array("id" => "3", "name" => "Per-Call"),
    );

    public function addIncident() {
        //$password = $this->getQueryParam("userPassword");
        //$encryptPassword = $this->getEncryptPassword($password);

        $query = "INSERT INTO `Incident` ("
                . "`firstName`,"
                . "`lastName`,"
                . "`sex`,"
                . "`email`,"
                . "`address`,"
                . "`mobile`,"
                . "`serialNo`,"
                . "`serviceType`,"
                . "`description`,"
                . "`status`,"
                . "`createBy`,"
                . "`logTime`)";
        $query.="VALUES (?,?,?,?,?,?,?,?,?,?,?,NOW());";

        $parameter = array(
            $this->getQueryParam("firstName"),
            $this->getQueryParam("lastName"),
            $this->sex[$this->getQueryParam("sex")],
            $this->getQueryParam("email"),
            addslashes($this->getQueryParam("address")),
            $this->getQueryParam("mobile"),
            $this->getQueryParam("serialNo"),
            $this->serviceType[$this->getQueryParam("serviceType") - 1]["id"],
            addslashes($this->getQueryParam("description")),
            "0", 
            $this->getQueryParam("uid")
        );
        
        //echo "parameter" ; 
        //print_r_pre($parameter) ; 

        $info = $this->getDb()->getResult($query, $parameter);

        $count = $this->getDb()->getCount();


        $lastInsertId = $this->getDb()->getLastInsertId();

        if ($count > 0) {
            $this->rtnMessage["addIncident"]["addSuccess"]["lastInsertId"] = $lastInsertId ; 
            return $this->getRtnMessage("addIncident", "addSuccess");
        }


        return $this->getRtnMessage("addIncident", "addError");
    }

    public function getAllUserInformation($start, $end) {
        
//                $query = "SELECT a.id eventId,a.title,  b.*   from calendar_newevent a left join Incident b on a.incidentId = b.id where a.id = ?";


        $attr = 'b.*';
        $parameter = array();

        $query = "SELECT a.id eventId,a.title ,%s from calendar_newevent a "
                . "left join Incident b on a.incidentId = b.id "
                . "left join Staff c on c.id = a.user_id "
                . "left join Staff d on d.id = b.createBy "
                . "  order by b.logTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();
        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                $parameter1 .= $value;
                if ($count != ($i + 1)) {
                    $parameter1 .= ",";
                }
                $i++;
            }
            $query = vsprintf($query, $parameter1);
        } else {
            $query = vsprintf($query, array($attr));
        }

        $userInfo = $this->getDb()->getResult($query, $parameter);

        return $userInfo;
    }

    
    public function autoSelect () {
        $attr = 'b.*';
        $parameter = array();

        $query = "SELECT a.id eventId,a.title ,%s from calendar_newevent a "
                . "left join Incident b on a.incidentId = b.id "
                . "left join Staff c on c.id = a.user_id "
                . "left join Staff d on d.id = b.createBy "
                . "  order by b.logTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();
        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                $parameter1 .= $value;
                if ($count != ($i + 1)) {
                    $parameter1 .= ",";
                }
                $i++;
            }
            $query = vsprintf($query, $parameter1);
        } else {
            $query = vsprintf($query, array($attr));
        }
    }
    
    public function isUidValid() {
        $query = "SELECT * from Staff where id=? ";
        $parameter = array(
            $this->getQueryParam("id")
        );

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] != "-2") {

                $rtnMsg = $this->getRtnMessage("isUidValid", "userExists", $this->getQueryParam("id"));
                $rtnMsg["uid"] = $userInfo[0]["id"];
                $rtnMsg["data"] = $userInfo[0];
                return $rtnMsg;
            }

            return $this->getRtnMessage("isUidValid", "accDelete", array($userInfo[0]["status"]));
        }


        return $this->getRtnMessage("getUserInfo", "userNotExists", array("-1"));
    }

    public function setUserStatus() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set status = ? where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"),
            $this->getQueryParam("status")
        );

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);

        /*
          if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
          return $this->getRtnMessage("setUserStatus", "updateSuccess");
          } */

        return $this->getRtnMessage("setUserStatus", "updateSuccess");
    }

    public function isUserExisits($username, $password) {
        $encryptPassword = $this->getEncryptPassword($password);

        $query = "SELECT `id`, `status`,`jobTitle` from Staff where userName=? and password = ?";
        $parameter = array($username, $encryptPassword);

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] == "1") {
                $this->updateLoginTime($userInfo[0]["id"]);

                return array("status" => "1", "uid" => $userInfo[0]["id"], "jobTitle" => $userInfo[0]["jobTitle"]);
            }

            return $this->getRtnMessage("getUserInfo", "accBan", array($userInfo[0]["status"]));
        }

        return $this->getRtnMessage("isUserExisits", "userNotExists", array("-1"));
    }

    public function updateIncident() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Incident where id = ? FOR UPDATE ; ";
        $query .= "Update Incident set "
                . "comment=?,"
                . "status=? "
                . " where id = @id limit 1; ";
        
        $query .= "Update calendar_newevent set "
                . "color=?"
                . " where incidentId = @id limit 1; ";

        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"), 
            $this->getQueryParam("comment"),
            $this->getQueryParam("incidentStatus"), 
            $this->getQueryParam("color")
        );
        //print_r_pre($parameter) ; 

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);




        return $this->getRtnMessage("updateIncident", "updateSuccess");
    }

    public function getUserInfoWithCustomQuery($start, $end) {

        $numericField = array("a.id");
        $parameter = array();
        
        $query = "SELECT a.id eventId,a.title ,b.description,d.userName Fromwho,c.userName Assigned,b.status,b.logTime from calendar_newevent a "
                . "left join Incident b on a.incidentId = b.id "
                . "left join Staff c on c.id = a.user_id "
                . "left join Staff d on d.id = b.createBy "
                . " where %s "
                . "  order by b.logTime desc limit $start,$end ; ";
        
        //$query = "SELECT a.* ,b.name from Staff a left join role b on a.type=b.id"
        //        . " where %s a.status != '-2' order by a.createTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();
        unset($queryParam["type"]) ; 
        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                if ($key == "c.userName Assigned"){
                    $key = "c.userName" ; 
                }
                if (in_array($key, $numericField)) {
                    $parameter1 .=  $key . "='" . $value . "'";
                } else {
                    $parameter1 .=  $key . " like '%" . $value . "%'";
                }
                
                if (count($queryParam) != "1" && $i != (count($queryParam)-1))
                    $parameter1 .= " AND ";
                
                
                $i++;
            }
            $query = vsprintf($query, array($parameter1));
        } else {
            $query = vsprintf($query, array($attr));
        }
        $userInfo = $this->getDb()->getResult($query, $parameter);

        if ((isset($userInfo[0]) && $userInfo[0]["eventId"] > 0)) {
            return array("status" => "1", "userInfo" => $userInfo);
        }

        return array("status" => "-1");
    }

    public function getIncidentInfoALL() {
        $query = "SELECT a.id eventId,a.title,  b.*   from calendar_newevent a left join Incident b on a.incidentId = b.id";

        $parameter = array(
            //$this->getQueryParam("id")
        );


        $info = $this->getDb()->getResult($query, $parameter);


        if (isset($info[0]) && $info[0]["id"] > 0) {

            $this->rtnMessage["getIncidentInfo"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getIncidentInfo", "success");
            return $rtnMsg;
        }

        return $this->getRtnMessage("getIncidentInfo", "fail");
    }
    
    
    public function getIncidentInfo() {
        $query = "SELECT a.id eventId,a.title,  b.*   from calendar_newevent a left join Incident b on a.incidentId = b.id where a.id = ?";

        $parameter = array(
            $this->getQueryParam("id")
        );


        $info = $this->getDb()->getResult($query, $parameter);


        if (isset($info[0]) && $info[0]["id"] > 0) {

            $this->rtnMessage["getIncidentInfo"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getIncidentInfo", "success");
            return $rtnMsg;
        }

        return $this->getRtnMessage("getIncidentInfo", "fail");
    }

    public function getIncidentInfoWithUid() {
        $query = "SELECT a.id eventId,a.title,  b.* , c.userName  from calendar_newevent a "
                . "left join Incident b on a.incidentId = b.id "
                . "left join Staff c on b.createBy = c.id "
                . "where a.user_id = ?";

        $parameter = array(
            $this->getQueryParam("uid")
        );


        $info = $this->getDb()->getResult($query, $parameter);


        if (isset($info[0]) && $info[0]["id"] > 0) {

            $this->rtnMessage["getIncidentInfo"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getIncidentInfo", "success");
            return $rtnMsg;
        }

        return $this->getRtnMessage("getIncidentInfo", "fail");
    }
    
    
    public function getSymptom() {
        $query = "SELECT * from symptoms";

        $parameter = array(
                //$this->getQueryParam("userName")
        );

        $info = $this->getDb()->getResult($query, $parameter);

        if (isset($info[0]) && $info[0]["id"] > 0) {

            $this->rtnMessage["getSymptom"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getSymptom", "success");
            return $rtnMsg;
        }


        return $this->getRtnMessage("getSymptom", "fail");
    }

    public function getDevice() {
        $query = "SELECT a.mainCategory, a.subCategory subCategory,b.name from "
                . "symptoms a left join device b on a.subcategory = b.id "
                . "where a.status = '1' and b.status = '1' "
                . "group by (a.subCategory) ";

        $parameter = array(
                //$this->getQueryParam("userName")
        );
        $info = $this->getDb()->getResult($query, $parameter);

        if (isset($info[0]) && $info[0]["mainCategory"] > 0) {

            $this->rtnMessage["getDevice"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getDevice", "success");
            return $rtnMsg;
        }


        return $this->getRtnMessage("getDevice", "fail");
    }

    private function updateLoginTime($id) {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set loginTime = NOW() where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array($id);

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);


        if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
            return array("status" => "1", "msg" => "更改時間成功");
        }

        return array("status" => "-1", "msg" => "更改失敗");
    }

    private function getEncryptPassword($password) {

        return md5($password);
    }

    /*
      public function getRtnMessage($func, $status, $vsprintf = array()) {
      $message = $this->rtnMessage ["$func"]["$status"]["msg"];

      if (!empty($vsprintf)) {
      $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
      }


      return $this->rtnMessage["$func"]["$status"];
      } */
    /*
      private function displayResult($func, $status, $vsprintf = array()) {

      return $this->getRtnMessage($func, $status, $vsprintf);
      }
     */
}
