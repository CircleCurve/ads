<?php

//class Member extends VerifyWS {
class SymptomsDB extends ParentDB {

    public $rtnMessage = array(
        "addSymptomsIssue" => array(
            "addSuccess" => array("status" => "1", "msg" => "Symptoms add successfully"),
            "addError" => array("status" => "2003", "msg" => "Symptoms add to db that have connection error"),
        ),
        "isIncidentExisits" => array(
            "userNotExists" => array("status" => "2001", "msg" => "UserName or UserPassword is wrong Status : %s"),
        ),
        "getSymptomsIssue" => array(
            "exists" => array("status" => "1", "info" => array(), "msg" => "Symptoms Issue is Exisits."),
            "notExists" => array("status" => "2005", "info" => array(), "msg" => "Symptoms Issue does not Exists"),
        ),
        "isUidValid" => array(
            "userExists" => array("status" => "1", "uid" => "", "msg" => "User Exisits"),
            "accDelete" => array("status" => "2008", "msg" => "Account has been deleted status : %s"),
        ),
        "updateIncidentInfo" => array(
            "updateSuccess" => array("status" => "1", "msg" => "User update information successfully"),
            "updateFail" => array("status" => "2009", "msg" => "Please edit information before update information"),
        ),
        "setIncidentStatus" => array(
            "updateSuccess" => array("status" => "1", "msg" => "User delete successfully"),
            "updateError" => array("status" => "2007", "msg" => "Delete user that have connection error"),
        ),
        "getDevice" => array(
            "success" => array("status" => "1", "info" => array(), "msg" => "Device exists"),
            "fail" => array("status" => "2005", "msg" => "Device does not Exists"),
        ),
        "getSymptoms" => array(
            "success" => array("status" => "1", "info" => array(), "msg" => "Symptoms exists"),
            "fail" => array("status" => "2005", "msg" => "Symptoms does not Exists"),
        ),
    );
    private $sex = array(
        "Mr" => "1",
        "Mrs" => "2"
    );
    private $serviceType = array(
        array("id" => "1", "name" => "Express"),
        array("id" => "2", "name" => "Standard"),
        array("id" => "3", "name" => "Per-Call"),
    );

    public function addSymptomsIssue() {
        //$password = $this->getQueryParam("userPassword");
        //$encryptPassword = $this->getEncryptPassword($password);
        $parameter = array() ; 
        $symptomsIds = $this->getQueryParam("symptoms") ; 
        
        $query = "INSERT INTO `SymptomsIssue` ("
                . "`incidentId`,"
                . "`symptomsId`,"
                . "`status`)";

        $query .="VALUES ";

        $delimiter = ",";
        $count = count($symptomsIds);

        for ($i = 0; $i < $count; $i++) {
            if ($i == $count - 1) {
                $delimiter = "";
            }
            $query.="(?,?,?)" . $delimiter;
            
            array_push($parameter, $this->getQueryParam("incidentId"),$symptomsIds[$i], "1") ; 
        }


        $info = $this->getDb()->getResult($query, $parameter);

        $count = $this->getDb()->getCount();


        if ($count > 0) {
            return $this->getRtnMessage("addSymptomsIssue", "addSuccess", "");
        }


        return $this->getRtnMessage("addSymptomsIssue", "addError");
    }

    public function getSymptomsIssue() {
        $query = "SELECT b.id,b.mainCategory,d.name,b.symptoms  from SymptomsIssue a "
                . "left join "
                . "symptoms b on a.symptomsId = b.id "
                . "left join Incident c "
                . "on a.incidentId=c.id  "
                . "left join device d "
                . "on b.subCategory = d.id"
                . " where a.incidentId=?";

        $parameter = array(
            $this->getQueryParam("incidentId")
        );

        $info = $this->getDb()->getResult($query, $parameter);
        

        if (isset($info[0]) && $info[0]["id"] > 0) {

            //print_r_pre($info) ; 
            //$this->rtnMessage["getEventInfo"]["info"] = $info;
            $this->rtnMessage["getSymptomsIssue"]["exists"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getSymptomsIssue", "exists");

            return $rtnMsg;
        }


        return $this->getRtnMessage("getSymptomsIssue", "notExists");
    }

    public function getAllUserInformation($start, $end) {

        $attr = 'a.*';
        $parameter = array();
        $query = "SELECT %s ,b.name from Staff a left join role b on a.type=b.id"
                . " where a.status != '-2' order by a.createTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();
        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                $parameter1 .= $value;
                if ($count != ($i + 1)) {
                    $parameter1 .= ",";
                }
                $i++;
            }
            $query = vsprintf($query, $parameter1);
        } else {
            $query = vsprintf($query, array($attr));
        }

        $userInfo = $this->getDb()->getResult($query, $parameter);

        return $userInfo;
    }

    public function isUidValid() {
        $query = "SELECT * from Staff where id=? ";
        $parameter = array(
            $this->getQueryParam("id")
        );

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] != "-2") {

                $rtnMsg = $this->getRtnMessage("isUidValid", "userExists", $this->getQueryParam("id"));
                $rtnMsg["uid"] = $userInfo[0]["id"];
                $rtnMsg["data"] = $userInfo[0];
                return $rtnMsg;
            }

            return $this->getRtnMessage("isUidValid", "accDelete", array($userInfo[0]["status"]));
        }


        return $this->getRtnMessage("getUserInfo", "userNotExists", array("-1"));
    }

    public function setUserStatus() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set status = ? where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"),
            $this->getQueryParam("status")
        );

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);

        /*
          if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
          return $this->getRtnMessage("setUserStatus", "updateSuccess");
          } */

        return $this->getRtnMessage("setUserStatus", "updateSuccess");
    }

    public function isUserExisits($username, $password) {
        $encryptPassword = $this->getEncryptPassword($password);

        $query = "SELECT `id`, `status`,`jobTitle` from Staff where userName=? and password = ?";
        $parameter = array($username, $encryptPassword);

        $userInfo = $this->getDb()->getResult($query, $parameter);

        if (isset($userInfo[0]) && $userInfo[0]["id"] > 0) {

            if ($userInfo[0]["status"] == "1") {
                $this->updateLoginTime($userInfo[0]["id"]);

                return array("status" => "1", "uid" => $userInfo[0]["id"], "jobTitle" => $userInfo[0]["jobTitle"]);
            }

            return $this->getRtnMessage("getUserInfo", "accBan", array($userInfo[0]["status"]));
        }

        return $this->getRtnMessage("isUserExisits", "userNotExists", array("-1"));
    }

    public function updateUserInfo() {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set "
                . "firstName=?,"
                . "lastName=?,"
                . "sex=?,"
                . "email=?,"
                . "directLine=?,"
                . "address=?,"
                . "mobile=?,"
                . "jobTitle=?,"
                . "type=?,"
                . "team=?,"
                . "status=? "
                . " where id = @id limit 1; ";

        $query.= "commit;";

        $parameter = array(
            $this->getQueryParam("id"),
            $this->getQueryParam("firstName"),
            $this->getQueryParam("lastName"),
            $this->getQueryParam("sex"),
            $this->getQueryParam("email"),
            $this->getQueryParam("directLine"),
            $this->getQueryParam("address"),
            $this->getQueryParam("mobile"),
            $this->getQueryParam("jobTitle"),
            $this->getQueryParam("role"),
            $this->getQueryParam("team"),
            $this->getQueryParam("accStatus")
        );


        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);

        $verify = new Verify();
        $verify->setSession("jobTitle", $this->getQueryParam("jobTitle"));
        $count = $this->getDb()->getCount();


        return $this->getRtnMessage("updateUserInfo", "updateSuccess");
    }

    public function getUserInfoWithCustomQuery($start, $end) {

        $numericField = array("id", "status");
        $parameter = array();
        $query = "SELECT a.* ,b.name from Staff a left join role b on a.type=b.id"
                . " where %s a.status != '-2' order by a.createTime desc limit $start,$end ; ";

        $queryParam = $this->getQueryParamALL();

        if (is_array($queryParam) && count($queryParam) != 0) {
            $parameter = array();
            $count = count($queryParam);
            $i = 0;
            foreach ($queryParam as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                if (in_array($key, $numericField)) {
                    $parameter1 .= "a." . $key . "='" . $value . "'";
                } else {
                    $parameter1 .= "a." . $key . " like '%" . $value . "%'";
                }

                $parameter1 .= " AND ";
                $i++;
            }
            $query = vsprintf($query, array($parameter1));
        } else {
            $query = vsprintf($query, array($attr));
        }
        //echo $query ; 
        $userInfo = $this->getDb()->getResult($query, $parameter);

        if ((isset($userInfo[0]) && $userInfo[0]["id"] > 0)) {
            return array("status" => "1", "userInfo" => $userInfo);
        }

        return array("status" => "-1");
    }

    public function getSymptom() {
        $query = "SELECT * from symptoms";

        $parameter = array(
                //$this->getQueryParam("userName")
        );

        $info = $this->getDb()->getResult($query, $parameter);


        if (isset($info[0]) && $info[0]["id"] > 0) {

            $this->rtnMessage["getSymptom"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getSymptom", "success");
            return $rtnMsg;
        }


        return $this->getRtnMessage("getSymptom", "fail");
    }

    public function getDevice() {
        $query = "SELECT a.mainCategory, a.subCategory subCategory,b.name from "
                . "symptoms a left join device b on a.subcategory = b.id "
                . "where a.status = '1' and b.status = '1' "
                . "group by (a.subCategory) ";

        $parameter = array(
                //$this->getQueryParam("userName")
        );
        $info = $this->getDb()->getResult($query, $parameter);

        if (isset($info[0]) && $info[0]["mainCategory"] > 0) {

            $this->rtnMessage["getDevice"]["success"]["info"] = $info;
            $rtnMsg = $this->getRtnMessage("getDevice", "success");
            return $rtnMsg;
        }


        return $this->getRtnMessage("getDevice", "fail");
    }

    private function updateLoginTime($id) {
        $query = "START TRANSACTION ; ";
        $query .= "set @id = 0 ;";
        $query .= "Select id into @id from Staff where id = ? FOR UPDATE ; ";
        $query .= "Update Staff set loginTime = NOW() where id = @id limit 1; ";
        $query.= "commit;";

        $parameter = array($id);

        $updateSuccessFul = $this->getDb()->getResult($query, $parameter);


        if (isset($updateSuccessFul[0]) && $updateSuccessFul[0]["updateSuccessful"] > 0) {
            return array("status" => "1", "msg" => "更改時間成功");
        }

        return array("status" => "-1", "msg" => "更改失敗");
    }

    private function getEncryptPassword($password) {

        return md5($password);
    }

    /*
      public function getRtnMessage($func, $status, $vsprintf = array()) {
      $message = $this->rtnMessage ["$func"]["$status"]["msg"];

      if (!empty($vsprintf)) {
      $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
      }


      return $this->rtnMessage["$func"]["$status"];
      } */
    /*
      private function displayResult($func, $status, $vsprintf = array()) {

      return $this->getRtnMessage($func, $status, $vsprintf);
      }
     */
}
