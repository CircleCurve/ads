<?php

class DepartmentDB extends ParentDB
{

    private $db = null;

    public $rtnMessage = array(
        "addDeparment" => array(
            "getSuccess" => array("status" => "1", "info" => array(), "msg" => "Success"),
            "getFail" => array("status" => "2003", "info" => array(), "msg" => "Fail"),
        )
    );

    public function updateDoc($array_string) {


        $filterArr = array() ;

        $whereClause = array(
            "_id" => [
                "_id"=> (isset($array_string["_id"])) ? new MongoId($array_string["_id"]) : ""

            ],

            "deptName" => [
                "Department.name"=> (isset($array_string["deptName"])) ? $array_string["deptName"] : ""
            ],
            "deptAddress" => [
                "Department.location"=>(isset($array_string["deptAddress"])) ? $array_string["deptAddress"] : ""
            ],

            "courseTitle" => [
                "Course.0.courseTitle" =>  (isset($array_string["courseTitle"])) ? $array_string["courseTitle"] : ""
            ],

            "courseLevel" => [
                "Course.0.courseLevel" =>  (isset($array_string["courseLevel"])) ? $array_string["courseLevel"] : ""
            ],


            "offerYear" => [
                "offerYear"=>(isset($array_string["offerYear"])) ? (new MongoDate(strtotime($array_string["offerYear"]))) : ""
            ],
            "classSize" => [
                "classSize"=>(isset($array_string["classSize"])) ? $array_string["classSize"] : ""
            ],
            "availablePlaces" => [
                "availablePlaces"=>(isset($array_string["availablePlaces"])) ? $array_string["availablePlaces"] : ""
            ],
            "enroll" => [
                'enroll' => [
                    'studentId' => (isset($array_string["enroll"]) && isset($array_string["enroll"]["studentId"])) ? $array_string["enroll"]["studentId"] : "",
                    'courseId' => (isset($array_string["enroll"]) && isset($array_string["enroll"]["courseId"])) ? $array_string["enroll"]["courseId"] : "",

                    'year' => (isset($array_string["enroll"]) && isset($array_string["enroll"]["year"])) ? $array_string["enroll"]["year"] : "",
                    'enrolledDate' => (isset($array_string["enroll"]) && isset($array_string["enroll"]["enrolledDate"])) ? $array_string["enroll"]["enrolledDate"] : "",
                ]
            ],

            "status" => [
                "status" => (isset($array_string["status"])) ? $array_string["status"] : ""
            ],
            "pullCourse" => [
                "enroll" => [ 'studentId'=>  (isset($array_string["pullCourse"]) && isset($array_string["pullCourse"]["studentId"])) ? new MongoId($array_string["pullCourse"]["studentId"]) : ""  ]

            ]
        ) ;


        $array_string = array_filter($array_string) ;

        foreach ($array_string as $key => $value){

            $filterArr = array_merge($filterArr , $whereClause["$key"]) ;

        }

        return $filterArr ;
    }


    public function selectDoc($array_string) {
        $filterArr = array() ;

        $whereClause = array(
            "_id" => [
                "_id"=> (isset($array_string["_id"])) ? new MongoId($array_string["_id"]) : ""

            ],

            "deptName" => [
                "Department.name"=>  ['$in' => (isset($array_string["deptName"])) ? $array_string["deptName"] : [] ]
                //"Department.name"=> (isset($array_string["deptName"])) ? $array_string["deptName"] : ""
            ],
            "deptAddress" => [
                "Department.location"=>(isset($array_string["deptAddress"])) ? $array_string["deptAddress"] : ""
            ],

            "courseTitle" => [
                "Course" => [ '$elemMatch' => [ 'courseTitle'=> (isset($array_string["courseTitle"])) ? $array_string["courseTitle"] : "" ] ]
            ],

            "courseLevel" => [
                "Course" => [ '$elemMatch' => [ 'courseLevel'=>  (isset($array_string["courseLevel"])) ? $array_string["courseLevel"] : ""  ] ]
            ],

            "offerYear" => [
                "offerYear"=>(isset($array_string["offerYear"])) ? [ '$gt' => $array_string["offerYear"][0] , '$lte' =>$array_string["offerYear"][1] ]   : ""
            ],
            "classSize" => [
                "classSize"=>(isset($array_string["classSize"])) ? $array_string["classSize"] : ""
            ],
            "availablePlaces" => [
                "availablePlaces"=>(isset($array_string["availablePlaces"])) ? (int)$array_string["availablePlaces"] : ""
            ],
            "status" => [
                "status" => (isset($array_string["status"])) ? $array_string["status"] : ""
            ],

            "getInfo" => [

                "Course.0.id" => (isset($array_string["getInfo"]) ) ? new MongoId($array_string["getInfo"]) : ""

            ],
            
            "studentId" => [
                "enroll" => [ '$elemMatch' => [ 'studentId'=>  (isset($array_string["studentId"])) ? new MongoId($array_string["studentId"]) : ""  ] ]
            ],
            
        ) ;

        $array_string = array_filter($array_string) ;

        foreach ($array_string as $key => $value){

            $filterArr = array_merge($filterArr , $whereClause["$key"]) ;

        }

        return $filterArr ;
    }


    public function __construct()
    {
        $setting = new MongoDBSetting();
        $m = $setting->getDBInfo();

        $this->db = $m->ads;
    }


    public function getAllInfo(){


        $collection = $this->db->department;

        $cursor = $collection->find([
            "status" => "1"
        ]) ;

        return $cursor ;

    }
    
    public function getInfoWithSearch($array_string ) {
        $collection = $this->db->department;

        $array_string["status"] = "1" ;
        $filterArr = $this->selectDoc($array_string) ;


        $cursor = $collection->find($filterArr) ;


        return $cursor ;
    }


    public function addDeparment($array_string)
    {

        $departmentInfo = [
            "offerYear" => new MongoDate(strtotime($array_string["offerYear"]))  ,
            "classSize" => $array_string["classSize"],
            "availablePlaces" => (int)$array_string["classSize"],
            
            "Department" => [
                "id" => new MongoId() ,
                "name" => $array_string["deptName"],
                "location" => $array_string["deptAddress"]
            ],
            "Course" => [
                [
                    "id" => new MongoId() ,
                    "courseTitle" => $array_string["courseTitle"],
                    "courseLevel" => $array_string["courseLevel"],
                ]
            ],
            "status" => "1"
        ];

        $collection = $this->db->department;

        $collection->insert($departmentInfo);

        if (isset($departmentInfo["_id"])) {
            $this->rtnMessage["addDeparment"]["getSuccess"]["info"] = $departmentInfo;
            return $this->getRtnMessage("addDeparment", "getSuccess");

        }

        return $this->getRtnMessage("addDeparment", "getFail");


    }

    public function updateInfo($id , $array_string, $project = '$set', $multi = array()){
        

        $collection = $this->db->department;

        $filterArr = $this->updateDoc($array_string) ;



        if (!empty($id )) {
            $id = ["_id" => new MongoId($id)] ;
        }

        $isUpdate = $collection->update(
            $id
            ,[ "$project" =>  $filterArr],
            $multi
        );




        if ($isUpdate["n"] === 1) {
            return array("status" => true, "msg" => "Update Success");
        }

        return array("status" => false, "msg" => "Update Fail");

    }



    public function updateAvailablePlace($array_string)
    {

        //return array("status" => true ) ;

        $collection = $this->db->department;

        $fliterData = $array_string["fliter"];
        $newData = $array_string["newData"];


        $isUpdate = $collection->update(
            $fliterData, $newData
        );

        if ($isUpdate["n"] === 1) {
            return array("status" => true);
        }

        return array("status" => false);
    }


    public function getAvailablePlace($array_string){
        $collection = $this->db->department;

        $ops = [
            ['$match' => [ "Course.0.id" => new MongoId ($array_string["courseId"]) ]] ,
            ['$unwind' => '$enroll'] ,
            ['$group' => [
                '_id' => '$_id',
                'availablePlaces' => ['$sum'=>1] ,
            ]]
        ];


        $cursor = $collection->aggregate($ops);

        $availablePlace = $cursor["result"][0]["availablePlaces"];

        return $availablePlace ;
    }

    public function sortByEnroll ($array_string){
        $collection = $this->db->department;



        $ops = [
            ['$unwind' => '$Course'] ,
            ['$unwind' => '$enroll'] ,
            ['$group' => [
                '_id' => '$_id',
                'count' => ['$sum'=>1] ,
            ]],
            ['$sort' => ['count' => -1 ]]
        ];


        $cursor = $collection->aggregate($ops);


        return $cursor ;
    }








}