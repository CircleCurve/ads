<?php

class StudentDB extends ParentDB
{

    private $db = null;

    public $rtnMessage = array(
        "addStudent" => array(
            "getSuccess" => array("status" => "1", "info" => array(), "msg" => "Success"),
            "getFail" => array("status" => "2003", "info" => array(), "msg" => "Fail"),
        )
    );

    public function updateDoc($array_string) {
        $filterArr = array() ;

        $whereClause = array(
            "_id" => [
                "_id"=> (isset($array_string["_id"])) ? new MongoId($array_string["_id"]) : ""

            ],

            "studName" => [
                "name"=> (isset($array_string["studName"])) ? $array_string["studName"] : ""
            ],
            "DOB" => [
                "DOB"=>(isset($array_string["DOB"])) ? $array_string["DOB"] : ""
            ],
            "status" => [
                "status" => (isset($array_string["status"])) ? $array_string["status"] : ""
            ]
        ) ;

        $array_string = array_filter($array_string) ;

        foreach ($array_string as $key => $value){

            if (isset($whereClause["$key"]))
                $filterArr = array_merge($filterArr , $whereClause["$key"]) ;

        }

        return $filterArr ;
    }


    public function selectDoc($array_string) {
        $filterArr = array() ;

        $whereClause = array(
            "_id" => [
                "_id"=> (isset($array_string["_id"])) ? new MongoId($array_string["_id"]) : ""

            ],


            "studName" => [
                "name"=> (isset($array_string["studName"])) ? $array_string["studName"] : ""
            ],
            "DOB" => [
                "DOB"=>(isset($array_string["DOB"])) ? $array_string["DOB"] : ""
            ],

            "status" => [
                "status" => (isset($array_string["status"])) ? $array_string["status"] : ""
            ]

        ) ;

        $array_string = array_filter($array_string) ;

        foreach ($array_string as $key => $value){

            $filterArr = array_merge($filterArr , $whereClause["$key"]) ;

        }

        return $filterArr ;
    }


    public function __construct()
    {
        $setting = new MongoDBSetting();
        $m = $setting->getDBInfo();

        $this->db = $m->ads;
    }


    public function getAllInfo(){


        $collection = $this->db->Student;

        $cursor = $collection->find([
            "status" => "1"
        ]) ;

        return $cursor ;

    }

    public function getInfoWithSearch($array_string ) {
        $collection = $this->db->Student;

        $array_string["status"] = "1" ;
        $filterArr = $this->selectDoc($array_string) ;


        $cursor = $collection->find($filterArr) ;

        return $cursor ;
    }


    public function addStudent($array_string)
    {

        $studentInfo = [
            "name" => $array_string["studName"] ,
            "DOB" => $array_string["DOB"],
            "status" => "1"
        ];


        $collection = $this->db->Student;

        $collection->insert($studentInfo);

        if (isset($studentInfo["_id"])) {

            $this->rtnMessage["addStudent"]["getSuccess"]["info"] = $studentInfo;
            return $this->getRtnMessage("addStudent", "getSuccess");

        }

        return $this->getRtnMessage("addStudent", "getFail");


    }

    public function updateInfo($id , $array_string){


        $collection = $this->db->Student;

        $filterArr = $this->updateDoc($array_string) ;

        $isUpdate = $collection->update(
            ["_id" => new MongoId($id)]
            ,[ '$set' =>  $filterArr]
        );



        if ($isUpdate["n"] === 1) {
            return array("status" => true, "msg" => "Update Success");
        }

        return array("status" => false, "msg" => "Update Fail");

    }

    public function enroll($id ,$studentId,  $array_string){

  
        $enroll["enroll"] = [
            'studentId' => new MongoId($studentId),
            'year' => (isset($array_string["offerYear"])) ? $array_string["offerYear"][0] : "",
            "courseId" => (isset($array_string["courseId"])) ? new MongoId($array_string["courseId"][0]) : "",
            'enrolledDate' => date("Y-m-d H:i:s"),


        ];

        $departmentDB = new DepartmentDB() ;

        return $departmentDB->updateInfo($id, $enroll, '$push') ;

    }




    public function updateAvailablePlace($array_string)
    {

        //return array("status" => true ) ;

        $collection = $this->db->Student;

        $fliterData = $array_string["fliter"];
        $newData = $array_string["newData"];


        $isUpdate = $collection->update(
            $fliterData, $newData
        );

        if ($isUpdate["n"] === 1) {
            return array("status" => true);
        }

        return array("status" => false);
    }


}