<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class db {

    private $db = null  ;
    private $link = null ; 

    private $dbInfo = null ; 
    
    //===========Count===========
    private $count = 0 ; 
    private $lastInsertId = 0 ; 
    
    public function setDBSetting ($dbInfo) {
        $this->dbInfo = $dbInfo ; 
    }
    
    public function setCount ($count) {
        $this->count = $count;  
    }
    
    public function getCount () {
        return $this->count ; 
    }
    
    public function setLastInsertId ($lastInsertId) {
        $this->lastInsertId = $lastInsertId ; 
        
    }
    
    public function getLastInsertId () {
        return $this->lastInsertId ; 
    }
    
    public function getResult ($query , $parameter = array()) {
        $hostName = $this->dbInfo["hostName"] ; 
        $userName = $this->dbInfo["userName"] ; 
        $password = $this->dbInfo["password"] ; 
        $dbName = $this->dbInfo["name"] ; 
        
        $this->connectToDb($hostName, $userName, $password, $dbName) ; 
        
        //$combineQuery = vsprintf($query, $parameter) ; 
        
        $statement = $this->link->prepare($query);
        //$statement->fetch(PDO::FETCH_ASSOC); 
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $result = $statement->execute($parameter);
        //======Set Count========
        $this->setCount($statement->rowCount()) ; 
        //======SetLastInsertId
        $this->setLastInsertId($this->link->lastInsertId()) ;

                
        $result = $statement->fetchAll(); 

        //print_r($result) ; 
        
        $this->link = null ; 
        
        return $result  ; 
    }
    
    
    private function connectToDb ($hostName , $userName , $password , $dbName ) {

        $dsn = "mysql:host=$hostName;dbname=$dbName;charset=utf8";

        try {
            $this->link  = new PDO($dsn , $userName, $password) ; 
            
        } catch (PDOException $ex) {
            
            //printf("Database Connection Error:%s " , $ex->getMessage()) ; 
            printf("DataBase connecton error %s", $ex->getMessage()) ; 
            die() ; 
        }
        
       

    }
    
   

    
}


