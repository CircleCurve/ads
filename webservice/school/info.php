<?php
require_once '../../config/init.php';


$department = new Department() ;
$data = array() ; 

$department->setFormValidation($department->getFormValidation()) ;
$fliterParam = array(
            "deptName" ,
            "deptAddress",
            "courseTitle",
            "courseLevel",
            "offerYear" ,
            "classSize",
            "availablePlaces"
        ) ;

$formatValid = $department->isFormatValid($_POST, "getUserInfoWithCustom") ;

$deptName  = explode("," ,$_POST['deptName'] );

if (!empty($deptName[0])){
   $_POST['deptName'] = $deptName ;              
}

if (isset($_POST["offerYear"]) && !empty($_POST["offerYear"])) {
    $startDate=  $_POST["offerYear"]."-01-01 00:00:00" ;
    $endDate=  $_POST["offerYear"]."-12-31 23:59:59" ;

    $offerYear = array() ;

    $offerYear[0] = new MongoDate(strtotime($startDate));
    $offerYear[1] = new MongoDate(strtotime($endDate));
    $_POST["offerYear"] = $offerYear ;


}



$data = $department->getInfoWithSearch($_POST ) ;


//$patientInfo = $staff->getPatientInfo($_POST) ; 
/*
$patientHistory = $patient->getPatientHistory($_POST) ;

$patientInfo["history"] = $patientHistory["result"] ; */


$department->displayRawResult($data) ;
//$patient->displaySuccessresultWithResult("success", "success", $patientInfo) ; 