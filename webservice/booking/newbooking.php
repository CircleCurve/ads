<?php
require_once '../../config/init.php';

$booking = new Booking() ; 

$member = new Member () ; 

$formatValid = $booking->isFormatValid($_POST, "booking") ; 

$isBookingBefore = $booking->isBookingBefore ($_POST);

$addBooking = $booking->addBooking($_POST) ; 

$bedAmount = $booking->getSession("bedAmount") ; 
$booking->setSession("bedAmount", ($bedAmount-1)) ; 

$booking->displayRawResult($addBooking) ; 
