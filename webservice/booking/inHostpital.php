<?php

require_once '../../config/init.php';

$booking = new Booking();


$formatValid = $booking->isFormatValid($_POST, "inHostpital");

$uid = $booking->getSession("uid");

if (isset($_POST["inPatientStatus"])) {
    $status = "1";
} else {
    $status = "-1";
}


$inPatientRecord = $booking->isInPatientNow(array(
    "status" => $status , 
    "bookingId" => $_POST["bookingIdForm"]
        ));

if ($inPatientRecord ["status"] == false) {
    $inPatientRecord = $booking->addInpatientRecord(array(
        "patientId" => $_POST["patientId"],
        "bookingId" => $_POST["bookingIdForm"],
        "staffId" => $uid,
        "incomedate" => $_POST["incomedate"],
        "outdate" => $_POST["outdate"],
        "status" => $status
            ));
}
$booking->displayRawResult($inPatientRecord);
//$patient->displaySuccessresultWithResult("success", "success", $patientInfo) ; 