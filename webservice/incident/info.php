<?php

require_once '../../config/init.php';


$incident = new Incident();
$data = array();

$incident->setFormValidation($incident->getFormValidation());

$postValue = $_POST;

$fliterParam = array(
    "incidentId" => "a.id",
    "title" => "a.title",
    "description" => "b.description",
    "fromWho" => "d.userName Fromwho",
    "assign" => "c.userName Assigned",
    "status" => "b.status",
    "logTime" => "b.logTime"
);

$fliterQueryParam = array(
    "incidentId" => "eventId",
    "title" => "title",
    "description" => "description",
    "fromWho" => "Fromwho",
    "assign" => "Assigned",
    "status" => "status",
    "logTime" => "logTime",
);



$queryParam = array();
foreach ($postValue as $key => $value) {
    if (isset($fliterParam["$key"]) && !empty($postValue["$key"])) {
        $queryParam[$fliterParam["$key"]] = $value;
    }
}



$formatValid = $incident->isFormatValid($_POST, "getUserInfoWithCustom");
$data = $incident->getUserInfoWithCustom($queryParam, $fliterQueryParam);


//$patientInfo = $staff->getPatientInfo($_POST) ; 
/*
  $patientHistory = $patient->getPatientHistory($_POST) ;

  $patientInfo["history"] = $patientHistory["result"] ; */


$incident->displayRawResult($data);
//$patient->displaySuccessresultWithResult("success", "success", $patientInfo) ; 