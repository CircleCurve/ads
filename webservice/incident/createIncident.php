<?php
require_once '../../config/init.php';

$incident = new Incident() ; 
$calendar = new Calendar() ; 
$symptoms = new Symptoms() ; 
$member = new Member () ; 

$formatValid = $incident->isFormatValid($_POST, "createIncident") ; 
$isAdd = $incident->addIncident($_POST) ; 

if ($isAdd["status"] != "1"){
    $incident->displayRawResult($isAdd) ;     
}
$postValue = $_POST; 
$postValue["incidentId"] = $isAdd["lastInsertId"] ; 

$postValue["symptoms"] = json_decode($postValue["symptoms"] , true) ; 

$symptomIssueInfo = $symptoms->addSymptomsIssue($postValue) ; 
$calendarInfo = $calendar->addEvent($postValue) ; 

$calendarInfo["msg"] = $calendarInfo["msg"] . "  Incident id:".$isAdd["lastInsertId"];  

$incident->displayRawResult($calendarInfo) ; 
//$staff->displaySuccessresult() ; 