<?php
require_once '../../config/init.php';

$staff = new Staff() ; 

$member = new Member () ; 

$formatValid = $staff->isFormatValid($_POST, "createUser") ; 
$isBirthDayValid = $staff->isBirthDayBiggerThanNOW($_POST["birthday"]) ; 
$isAdd = $staff->addUser($_POST) ; 
$staff->displayRawResult($isAdd) ; 
//$staff->displaySuccessresult() ; 