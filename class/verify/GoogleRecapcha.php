<?php

class GoogleRecapcha {
    //==============Regular Expression===========================
    const GOOGLE_RECAPTCHA_REGEX = "/^[A-Za-z0-9_-]+$/D";
    //=============Google Verify================
    const GOOGLE_RECAPTCHA = "https://www.google.com/recaptcha/api/siteverify";
    const GOOGLE_RECAPTCHA_SECRET = "6LfU9gMTAAAAAHm705CLR5gNuvKxkK23SoR_yQPS";
    const GOOGLE_RECAPTCHA_CLIENT_SECRET = "6LfU9gMTAAAAAOZZY4UeHQuaLcFUdn9BL0-5wINz" ; 
    
    
    
    public function isGoogleRecapchaValid($response) {
        
        $crossPlatform = new CrossPlatform();
        $postParam = array(
            "secret" => self::GOOGLE_RECAPTCHA_SECRET,
            "response" => $response,
        );
        $rtn = $crossPlatform->post_url_contents(self::GOOGLE_RECAPTCHA, $postParam);

        $decodeRtn = json_decode($rtn, true);

        if ($decodeRtn["success"] === true) {

            return array("status" => true);
        }
        return array("status" => "-1" , array("error"=> $decodeRtn["error-codes"][0])) ; 
    }
    
    
    public function getHtml () {
        echo '<div class="g-recaptcha" data-sitekey="'.self::GOOGLE_RECAPTCHA_CLIENT_SECRET.'"></div><br>' ; 
    }
    
}
