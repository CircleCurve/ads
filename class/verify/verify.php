<?php

class Verify {

    public function __construct() {
        
    }

    private $format = array(
    );
    private $formValidation = array(
    );
    private $jsErrorMessage = array(
    );
    private $rtnMessage = array(
    );
    private $successMessage = array(
        "status" => "1",
        "msg" => "Success to process",
    );

    public function getFormat($key) {
        return $this->format["$key"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function getRegex($key, $field) {
        return $this->getFormat($key, $field);
    }

    public function getRegexJsMessage($key) {
        $message = array();
        $format = $this->getFormat($key);
        $jsErrorMessage = $this->getJsErrorMessage($key);

        foreach ($format as $key => $regex) {
            $message["$key"] = array("regex" => substr($regex, 1, -2), "message" => $jsErrorMessage["$key"]["msg"]);
            if (isset($jsErrorMessage["$key"]["html"])) {
                $message["$key"]["html"] = $jsErrorMessage["$key"]["html"];
            }
        }

        return $message;
    }

    public function isSessionValid($sessionCode) {

        if (!isset($_SESSION["verifyCode"])) {
            
        }

        if (isset($_SESSION["verifyCode"]) && $sessionCode == $_SESSION["verifyCode"]) {
            return array("status" => true);
        }

        return $this->displayResult("isSessionValid", "sessionCodeNotEqual");
    }

    public function isFormatValid($postParam, $type) {
        foreach ($this->getFormat("$type") as $postKey => $regex) {
            if ($postKey == "g-recaptcha-response" && $this->isTurnOnGoogleRecapcha() == false) {
                continue;
            }

            if (!isset($postParam["$postKey"])) {

                return $this->displayResult("isFormatValid", "paramNotSubmut", array($postKey));
            }

            if (!preg_match($regex, $postParam["$postKey"])) {
                return $this->displayResult("isFormatValid", "formatNotValid", array($postKey));
            }
        }

        return array("status" => true);
    }

    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA;
    }

    public function isGoogleRecapchaValid($response) {

        if ($this->isTurnOnGoogleRecapcha() == false) {
            return array("status" => true);
        }

        $googleRecapcha = new GoogleRecapcha();
        $isValid = $googleRecapcha->isGoogleRecapchaValid($response);

        if ($isValid["status"] === true) {
            return $isValid;
        }
        //        return $this->displayResult("isGoogleRecapchaValid", "googleReturnError", array($decodeRtn["error-codes"][0]));

        return $this->displayResult("isGoogleRecapchaValid", "googleReturnError", array($isValid["error"]));
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->rtnMessage["$func"]["$status"];
    }

    private function getRtnMessage($func, $status) {
        $rtn = $this->getRtn($func, $status);
        return $rtn["msg"];
    }

    public function getErrorMessage($func, $status, $vsprintf = array()) {
        $message = $this->getRtnMessage($func, $status);

        if (!empty($vsprintf)) {
            $this->setRtnMessage($func, $status, $message, $vsprintf);
        }

        return $this->getRtn($func, $status);
    }

    public function issetSession($key) {
        if (isset($_SESSION["DSSystem"]) && isset($_SESSION["DSSystem"]["$key"])) {
            return true;
        }

        return false;
    }

    public function setSession($key, $value) {

        $_SESSION["DSSystem"]["$key"] = $value;
    }

    public function getSession($key) {
        return $_SESSION["DSSystem"]["$key"];
    }

    public function unsetSession() {

        unset($_SESSION["DSSystem"]);
    }

    private function unsetSessionWithKey($key) {
        unset($_SESSION["DSSystem"]["$key"]);
    }

    public function displayResult($func, $status, $vsprintf = array()) {

        return $this->getErrorMessage($func, $status, $vsprintf);
    }

    public function displaySuccessresult() {
        echo json_encode($this->successMessage);

        die();
    }

    public function displayAlertBox($message,$url) {
        echo '<script>alert ("Error Message : ' . $message . '"); </script>';
        echo '<META HTTP-EQUIV=REFRESH CONTENT="0; ' . $url . '">';
        die(); //echo "Error Message : " . $formatValid["msg"];
    }

}
