<?php

class VerifyWS extends Verify {

    private $formValidation = array() ; 
    
    public function setFormValidation ($formValidation) {
        $this->formValidation = $formValidation ; 
    }
    
    public function isFormOptionalValidation($key, $postKey, $postParam,$type = "optional") {
         
        $formValidation = $this->formValidation ; 
        
        if (!isset($formValidation["$key"])) {
            return false;
        }
        if (!isset($formValidation["$key"]["$type"])) {
            return false;
        }
        
        if (!isset($formValidation["$key"]["$type"]["$postKey"])) {
            return false ; 
        }
                
        if (!isset($postParam["$postKey"]) || empty($postParam["$postKey"])){
            return true ; 
        }
        
        $regex = $formValidation["$key"]["$type"]["$postKey"]; 
        
                               // echo "$postKey ";
        if (!preg_match($regex, $postParam["$postKey"])) {
            return $this->displayResult("$key", "$postKey", array($postKey));
        }


        return true;
    }



    public function isFormatValid($postParam, $type,$isReturn = false) {
        
        foreach ($this->getFormat("$type") as $postKey => $regex) {
            if ($postKey == "g-recaptcha-response" && $this->isTurnOnGoogleRecapcha() == false) {
                continue;
            }
            //echo "postKey: " . $postKey ; 
            
            if ($this->isFormOptionalValidation($type, $postKey,$postParam) == true) {
                continue;
            }
            if (!isset($postParam["$postKey"])) {
                return $this->displayResult("$type", "$postKey", array($postKey),$isReturn);
            }
            
            if (!preg_match($regex, $postParam["$postKey"])) {

                return $this->displayResult("$type", "$postKey", array($postKey),$isReturn);
            }
        }

        return array("status" => true);
    }

    public function displayResult($func, $status, $vsprintf = array(), $isReturn=false) {
        if ($isReturn == true){
            return $this->getErrorMessage($func, $status, $vsprintf) ; 
        }
        echo json_encode($this->getErrorMessage($func, $status, $vsprintf));
        die() ; 
    }

    public function displaySuccessresultWithResult($func, $status, $result, $vsprintf = array()) {
        $successMessage = $this->getErrorMessage($func, $status, $vsprintf);
        //$successMessage["result"] = $result ; 

        echo json_encode($successMessage);
        die();
    }

    public function displayRawResult($arr) {
        echo json_encode($arr);
        die();
    }

    public function isTurnOnGoogleRecapcha() {
        parent::isTurnOnGoogleRecapcha();
    }

}
