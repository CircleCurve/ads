<?php

class Route {

    private $login = array(
        "core" => array("error" , "login", "main"), 
        "non-login" => "login-login",
        "login" => "main-main"
    );
    
   
    const topic = "ADS/";
    //const topic = "DSSystem/" ; 
    
    public function requireThemeFile($themePath,  $data = array(), $BE_INCLUDE = 1) {
        $verify = new Verify();

        $path = "theme/".self::topic ; 
        

        require_once $path .  $themePath . ".php";


        die();
    }

    public function requireFile($BE_INCLUDE, $entirePath, $rootPath, $errorPage, $controllerPath, $route) {

        $fullPath = "$rootPath" . "$controllerPath" . $entirePath;
        
//echo "$rootPath"."$controllerPath"."/"."$entirePath" ; 
        if (is_file($fullPath)) {

            require_once $fullPath;
        } else {
            require_once "$rootPath" . "$controllerPath" . "/" . "$errorPage";
        }
    }

    public function getFileDirectory($act) {


        $route = explode("-", $act);
///print_r($route) ; 
        if  (count($route) != 2 ) {
            return "/error/error.php";
        }
        
        $fileDir = $route[0];
        $destFile = $route [1];
        
        if (in_array($fileDir, $this->login["core"])) {
            return "/".$fileDir . "/" . $destFile . ".php";
        }
        
        return "/".self::topic . $fileDir . "/" . $destFile . ".php";
    }

    public function isLogin($type) {
        $act = (!empty($_GET["act"])) ? $_GET["act"] : $this->login["$type"];
        if ($type == "login") {
            $dir = explode("-", $act);
            
            if ($dir[0] == "login") {
                $act = "main-main";
            }
        }

        return $act;
    }

}
