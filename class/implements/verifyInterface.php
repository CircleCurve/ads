<?php

interface verifyInterface {
    public function getFormat($key)  ; 
    public function setRtnMessage($func, $status, $message, $vsprintf); 
    public function getRtn($func, $status); 
    public function getJsErrorMessage($key) ; 
    //=========Turn on Google Recapcha Verification=====
    public function isTurnOnGoogleRecapcha () ; 
}
