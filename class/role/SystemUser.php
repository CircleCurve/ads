<?php

class SystemUser extends Verify implements verifyInterface {

    //=============Turn On Google Recapcha======================
    const TURN_ON_GOOGLE_RECAPCHA = false; 
    
    //==============Regular Expression===========================
    const userName = "/^[A-Za-z0-9]{3,20}$/D";
    const userPassword = "/^[A-Za-z0-9]{5,20}$/D";
    const GOOGLE_RECAPTCHA_REGEX = "/^[A-Za-z0-9_-]+$/D";

    //==============Regular Expression===========================


    private $rtnMessage = array(
        "isSessionValid" => array(
            "sessionCodeNotEqual" => array("status" => "1003", "msg" => "Please get inside this page from the normal method"),
        ),
        "isFormatValid" => array(
            "paramNotSubmut" => array("status" => "1001", "msg" => "Please input %s"),
            "formatNotValid" => array("status" => "1002", "msg" => "Format Error:%s. Please input again"),
        ),
        "isGoogleRecapchaValid" => array(
            "googleReturnError" => array("status" => "1004", "msg" => "VerifyCode is wrong.Please check again:%s"),
        ),
    );
    private $format = array(
        "loginConfirmed" => array(
            "userName" => self::userName,
            "password" => self::userPassword,
            "g-recaptcha-response" => self::GOOGLE_RECAPTCHA_REGEX,
        )
    );

    public function getUserName() {
        return self::userName;
    }

    public function getUserPassword() {
        return self::userPassword;
    }

    public function getFormat($key) {

        return $this->format["$key"];
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->rtnMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->rtnMessage["$func"]["$status"];
        
    }
    
    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA ; 
    }

}
