<?php

class Staff extends VerifyWS implements verifyInterface {

    //=============Turn on Google Recapcha=====================
    const TURN_ON_GOOGLE_RECAPCHA = false;
    //=============Staff Regex========================
    const firstNameRegex = "/^[A-Za-z]{1,20}$/D";
    const lastNameRegex = "/^[A-Za-z]{1,20}$/D";
    const userNameRegex = "/^[A-Za-z0-9]{3,20}$/D";
    const userPasswordRegex = "/^[A-Za-z0-9]{5,20}$/D";
    const HKIDCardNumberRegex = "/^[A-Za-z]{1}[0-9]{7}$/D";
    const phoneNumberRegex = "/^[0-9]{8}$/D";
    const addressRegex = "/^[A-Za-z0-9 ]+$/D";
    const birthdayRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const birthdayWithTimeRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const emailAddressRegex = "/^[A-Za-z0-9_]+@[A-Za-z0-9]+.[com|hk]+$/D";
    const sexRegex = "/^Mr|Ms|Mrs$/D";
    const jobTitleRegex = "/^[A-Za-z0-9]{1,20}$/D";
    const accStatusRegex = "/^1|-1|all$/D";
    const teamRegex = "/^[0-9]{1,3}$/D";
    const roleRegex = "/^[0-9]{1,3}|all$/D";
    const userIdRegex = "/^[0-9]+$/D";
    //============Staff Regex===========================
    const patientIdRegex = "/^[0-9]{1,20}$/D";
    const physicalCheckStartDate = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/D";
    const heightRegex = "/^[0-9]{1,20}.[0-9]{1,20}$/D";
    const weightRegex = "/^[0-9]{1,20}.[0-9]{1,20}$/D";
    const systolicRegex = "/^[0-9]{1,20}$/D";
    const diastolicRegex = "/^[0-9]{1,20}$/D";

    //=============Patient Registeration========================

    private $format = array(
        "createUser" => array(
            "firstName" => self::firstNameRegex,
            "lastName" => self::lastNameRegex,
            "userName" => self::userNameRegex,
            "userPassword" => self::userPasswordRegex,
            "sex" => self::sexRegex,
            "HKID" => self::HKIDCardNumberRegex,
            "mobile" => self::phoneNumberRegex,
            "address" => self::addressRegex,
            "birthday" => self::birthdayRegex,
            "email" => self::emailAddressRegex,
            "directLine" => self::phoneNumberRegex,
            "jobTitle" => self::jobTitleRegex,
            "address" => self::addressRegex,
            "accStatus" => self::accStatusRegex,
            "team" => self::teamRegex,
            "role" => self::roleRegex,
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "deleteUser" => array(
            "id" => self::userIdRegex,
        ),
        "getUserId" => array(
            "id" => self::userIdRegex
        ),
        "updateUserInfo" => array(
            "firstName" => self::firstNameRegex,
            "lastName" => self::lastNameRegex,
            "sex" => self::sexRegex,
            "mobile" => self::phoneNumberRegex,
            "address" => self::addressRegex,
            "email" => self::emailAddressRegex,
            "directLine" => self::phoneNumberRegex,
            "jobTitle" => self::jobTitleRegex,
            "address" => self::addressRegex,
            "accStatus" => self::accStatusRegex,
            "team" => self::teamRegex,
            "role" => self::roleRegex,
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "viewUser" => array(
            "userName" => self::userNameRegex,
            "roleName" => self::roleRegex,
            "id" => self::userIdRegex,
            "accStatus" => self::accStatusRegex,
            "jobTitle" => self::jobTitleRegex,
        ),
        "getUserInfoWithCustom" => array(
            "userName" => self::userNameRegex,
            "role" => self::roleRegex,
            "jobTitle" => self::jobTitleRegex,
            "id" => self::userIdRegex,
            "status" => self::accStatusRegex,
        )
    );
    private $jsErrorMessage = array(
        "createUser" => array(
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "userName" => array("status" => "2003", "msg" => "Please input your name 4-16 digit"),
            "userPassword" => array("status" => "2009", "msg" => "Please input your password"),
            "sex" => array("status" => "2010", "msg" => "Please input valid sex"),
            "HKID" => array("status" => "2004", "msg" => "Please input HKID card number A1234567 without () "),
            "mobile" => array("status" => "2005", "msg" => "Please input 8 digit phone number"),
            "directLine" => array("status" => "2019", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2006", "msg" => "Please input address"),
            "birthday" => array("status" => "2007", "msg" => "Please input birthday with yyyy-mm-dd hh:mm:ss"),
            "email" => array("status" => "2010", "msg" => "Please input valid email address : xxyy@xxx.com or hk"),
            "jobTitle" => array("status" => "2011", "msg" => "Please input job title"),
            "accStatus" => array("status" => "2012", "msg" => "Please input the valid accout status"),
            "team" => array("status" => "2013", "msg" => "Please input the valid team"),
            "role" => array("status" => "2014", "msg" => "Please input the valid role"),
            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),
        "deleteUser" => array(
            "id" => array("status" => "2015", "msg" => "Please remove valid user"),
        ),
        "getUserId" => array(
            "id" => array("status" => "2016", "msg" => "Please select valid user"),
        ),
        "updateUserInfo" => array(
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "sex" => array("status" => "2010", "msg" => "Please input valid sex"),
            "mobile" => array("status" => "2005", "msg" => "Please input 8 digit phone number"),
            "directLine" => array("status" => "2019", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2006", "msg" => "Please input address"),
            "email" => array("status" => "2010", "msg" => "Please input valid email address : xxyy@xxx.com or hk"),
            "jobTitle" => array("status" => "2011", "msg" => "Please input job title"),
            "accStatus" => array("status" => "2012", "msg" => "Please input the valid accout status"),
            "team" => array("status" => "2013", "msg" => "Please input the valid team"),
            "role" => array("status" => "2014", "msg" => "Please input the valid role"),
            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),
        "getUserInfoWithCustom" => array(
            "userName" => array("status" => "2003", "msg" => "Please input your name 4-16 digit"),
            "role" => array("status" => "2020", "msg" => "Please input your name 4-16 digit"),
            "id" => array("status" => "2016", "msg" => "Please select valid user"),
            "status" => array("status" => "2012", "msg" => "Please input the valid accout status"),
            "jobTitle" => array("status" => "2011", "msg" => "Please input job title"),
        ),
        "getPatientId" => array(
            "HKID" => array("status" => "2003", "msg" => "Please input HKID card number A1234567 without () "),
        ),
        "isBirthdayValid" => array(
            "isBirthdayValid" => array("status" => "2010", "msg" => "Birthday is bigger than NOW ! Please check it again \n Today is %s but your select date is %s"),
        ),
        "addPatient" => array(
            "isRegisterBefore" => array("status" => "2011", "msg" => "Patient is added before Please make it update \n link : %s \nor add the other patient"),
        ),
        "updatePatientInfo" => array(
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "phone" => array("status" => "2004", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2005", "msg" => "Please input address"),
            "emContactPerson" => array("status" => "2006", "msg" => "Please input Emergency Contact Person"),
            "emContactPhone" => array("status" => "2007", "msg" => "Please input Emergency Contact Person Phone Number with 8 digit"),
            "dateOfBirth" => array("status" => "2008", "msg" => "Please input birthday with yyyy-mm-dd"),
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "patientInfo" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "deletePatientId" => array(
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "addPhysicalCheck" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
            "startDate" => array("status" => "2010", "msg" => "Please input start date with yyyy-mm-dd"),
            "height" => array("status" => "2011", "msg" => "Please input height with decimal"),
            "weight" => array("status" => "2012", "msg" => "Please input weight with decimal "),
            "systolic" => array("status" => "2013", "msg" => "Please input systolic with digit"),
            "diastloic" => array("status" => "2014", "msg" => "Please input diastolic with digit"),
        ),
        "inpatientinfo" => array(
        ),
        "inportphycheck" => array(
        ),
        "conregister" => array(
        ),
        "reportcheck" => array(
        ),
        "editinfo" => array(
        ),
        "checkinvoice" => array(
        )
    );
    private $formValidation = array(
        "getUserInfoWithCustom" => array(
            "optional" => array(
                "userName" => self::userNameRegex,
                "role" => self::roleRegex,
                "jobTitle" => self::jobTitleRegex,
                "id" => self::userIdRegex,
                "status" => self::accStatusRegex
            )
        )
    );

    public function getFormValidation() {
        return $this->formValidation;
    }

    public function getFormat($key) {

        return $this->format["$key"];
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->jsErrorMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->jsErrorMessage["$func"]["$status"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA;
    }

    public function isBirthDayBiggerThanNOW($date) {
        $now = strtotime("now");
        $targetDate = strtotime($date);


        if ($now > $targetDate) {
            return array("status" => true);
        }
        $vsprintf = array(
            date("Y-m-d"), $date
        );

        //return $this->getErrorMessage("isBirthdayValid", "isBirthdayValid",$vsprintf) ; 
        return $this->displayResult("isBirthdayValid", "isBirthdayValid", $vsprintf);
    }

    //========================SQL===========================
    public function addUser($queryParam) {
        $rtnMsg = array();
        $member = new Member ();
        $member->setQueryParam($queryParam);

        $roleId = $this->getRoleInfoWithId($queryParam);
        if ($roleId["status"] != "1") {
            $rtnMsg = $roleId;
            return $rtnMsg;
        }

        $teamId = $this->getTeamInfoWithId($queryParam);
        if ($teamId["status"] != "1") {
            $rtnMsg = $teamId;
            return $rtnMsg;
        }

        $isUserExists = $member->getUserInfo();

        if ($isUserExists["status"] == "2005") {
            $rtnMsg = $member->addUser();
        } else {
            $rtnMsg = $isUserExists;
        }
        return $rtnMsg;
    }

    public function getAllStaff($queryParam) {

        $member = new Member ();
        $member->setQueryParam($queryParam);
        $userInfo = $member->getAllUserInformation(0, 100);

        //print_r_pre($isUserExists) ; 
        //die(); 
        return $userInfo;
    }
    

    public function getRoleInfo($type = "withoutAll") {
        $roleDB = new RoleDB ();
        //$roleDB->setQueryParam(); 
        $roleInfo = $roleDB->getRoleInfo();

        if ($roleInfo["status"] == "1") {

            if ($type == "all") {

                array_unshift($roleInfo["data"], array("id" => "all", "name" => "all"));

            }
            return $roleInfo;
        }

        return array("data" => array());
    }

    public function getRoleInfoWithId($queryParam) {
        $roleDB = new RoleDB ();
        $roleDB->setQueryParam($queryParam);
        $roleInfo = $roleDB->getRoleInfoWithId();

        return $roleInfo;
    }

    public function getTeamInfo() {
        $teamDB = new TeamDB ();
        //$roleDB->setQueryParam(); 
        $teamInfo = $teamDB->getTeamInfo();

        if ($teamInfo["status"] == "1") {
            return $teamInfo;
        }

        return array("data" => array());
    }

    public function getTeamInfoWithId($queryParam) {
        $teamDB = new TeamDB ();
        $teamDB->setQueryParam($queryParam);
        $teamInfo = $teamDB->getTeamInfoWithId();

        return $teamInfo;
    }

    public function setUserStatus($queryParam) {
        $member = new Member ();
        $member->setQueryParam($queryParam);
        $isUidValid = $member->isUidValid();

        if ($isUidValid["status"] != "1") {
            return $isUidValid;
        }
        $userStatus = $member->setUserStatus();
        //print_r_pre($isUserExists) ; 
        //die(); 
        return $userStatus;
    }

    public function getUserInfo($queryParam) {
        $member = new Member ();
        $member->setQueryParam($queryParam);
        $isUidValid = $member->isUidValid();

        return $isUidValid;
        /*
          if ($isUidValid["status"] != "1"){
          return $isUidValid ;
          }
          //print_r_pre($isUserExists) ;
          //die();
          return $userStatus; */
    }

    public function getUserInfoWithCustom($queryParam, $fliterParam) {
        $member = new Member ();
        $allArr = array(
            "status",
            "role"
        );

        foreach ($queryParam as $key => $value) {
            if (!isset($queryParam["$key"])) {
                $queryParam["$key"] = "";
            } elseif ($queryParam["$key"] == "all") {
                $queryParam["$key"] = "";
            }
        }


        $type = $queryParam["role"];
        unset($queryParam["role"]);


        $queryParam["type"] = $type;

        $member->setQueryParam($queryParam);
        $userInfo = $member->getUserInfoWithCustomQuery(0, 20);
        $userFliterInfo = array();

        if ($userInfo["status"] == "1") {

            for ($i = 0; $i < count($userInfo["userInfo"]); $i++) {
                for ($j = 0; $j < count($fliterParam); $j++) {
                    $key = $fliterParam[$j];
                    $userFliterInfo[$i][$key] = $userInfo["userInfo"][$i][$key];
                }
            }
        }

        return $userFliterInfo;
    }

    public function updateUserInfo($queryParam) {
        $member = new Member();
        $member->setQueryParam($queryParam);
        $isUidValid = $member->isUidValid();

        if ($isUidValid["status"] != "1") {
            return $isUidValid;
        }

        $userStatus = $member->updateUserInfo();
        return $userStatus;
    }

    public function getPatientId($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getPatientId();
    }

    public function isUserExists($queryParam) {
        $member = new Member();
        $member->setQueryParam($queryParam);
        return $member->isUserExisits();
    }

    public function updatePersonInformation($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->updatePersonInformation();
    }

    public function setPatientStatus($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->setPatientStatus();
    }

    public function addPhysicalCheck($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->addPhysicalCheck();
    }

    public function getPhysicalCheckInformation($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getPhysicalCheckInformation();
    }

    public function getPatientHistory($queryParam) {

        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);

        $patientHistory = $patientdb->getPatientHistory();

        for ($i = 0; $i < count($patientHistory["result"]); $i++) {
            $inPatientId = $patientHistory["result"]["$i"]["InPatientId"];
            $patientHistory["result"]["$i"]["details"] = "<a href='index.php?act=patient-inpatientinfo&inPatientId=" . $inPatientId . "'>Click here to get more details</a>";
        }


        return $patientHistory;
    }

    public function getHealthStatus() {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getHealthStatus();
    }

}
