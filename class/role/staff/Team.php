<?php

class Team {
    
    
    
    public function getInfo () {
        $teamDB = new TeamDB ();
        //$roleDB->setQueryParam(); 
        $teamInfo = $teamDB->getTeamInfo();

        if ($teamInfo["status"] == "1") {
            return $teamInfo;
        }

        return array("data" => array());
        
    }
    
    public function getInfoWithId ($queryParam) {
        $teamDB = new TeamDB ();
        $teamDB->setQueryParam($queryParam);
        $teamInfo = $teamDB->getTeamInfoWithId();

        return $teamInfo;
        
    }
    
   
    
    
    
}
