<?php

class Report extends VerifyWS implements verifyInterface {

    const dateRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const reportRegex = "/^1|2|3$/D";

    private $format = array(
        "viewReport" => array(
            "startDate" => self::dateRegex,
            "endDate" => self::dateRegex,
            "reportType" => self::reportRegex,
        )
    );
    private $jsErrorMessage = array(
        "viewReport" => array(
            "startDate" => array("status" => "4000", "msg" => "Please input valid start date"),
            "endDate" => array("status" => "4001", "msg" => "Please input valid end date"),
            "reportType" => array("status" => "4002", "msg" => "Please input valid report type"),
        ),
    );
    private $reportType = array(
        array("id" => "1", "name" => "Number Of Symptoms"),
        array("id" => "2", "name" => "SLA Report"),
        array("id" => "3", "name" => "Number of Incident by Model Type"),
    );
    
    private $numberOfSymptoms = array(
        "month" => "",
        "amount" => "",
    );
    private $numberOfSymptomsLine = array(
    );

    public function getReportType () {
        return $this->reportType ; 
    }
    
    public function getFormat($key) {

        return $this->format["$key"];
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->jsErrorMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->jsErrorMessage["$func"]["$status"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function getNumberOfSymptoms($queryParam) {

        $reportDB = new ReportDB();
        $reportDB->setQueryParam($queryParam);
        $info = $reportDB->getNumberOfSymptoms();

        $fliterInfo = array();
        $lineValue = array();
        $lineName = array();

        if ($info["status"] == "1") {
            $info = $info["info"];
            $date = $this->getDate($info);
            $temp = "";

            for ($i = 0, $k = 0; $i < count($info); $i++) {
                $temp = $info[$i]["name"];

                for ($j = 0; $j < count($info); $j++) {
                    if ($temp == $info[$j]["name"]) {
                        $fliterInfo[$temp][$k++] = array("2016-03-".$info[$j]["month"], $info[$j]["amount"]);
                    }
                }
                $k = 0;
                //unset($info[$i]) ; 
                //$fliterInfo[$i] = array($info[$i]["month"] , $info[$i]["amount"]);
            }
            $k = 0;
            foreach ($fliterInfo as $key => $value) {
                array_push($lineName, $key);
            }

            $valueInfo = array_values($fliterInfo);

            //print_r_pre($valueInfo) ; 
            //$info = $this->setDate($info["info"]);
            return array("lineName" => $lineName, "lineValue" => $valueInfo);
        }

        return array();
    }

    public function getDate() {
        $date = array();
        for ($i = 1; $i <= 12; $i++) {
            $date[$i - 1] = array("month" => $i);
        }

        return $date;
    }

}
