<?php

class HtmlElement {

    private $inputRegex = array();

    public function getTable($name, $result) {


        $count = count($result);
        $table = '<table name="' . $name . '" id="' . $name . '" class="table table-bordered table-striped">';

        $table.="<td> ID</td><td>Firmware</td><td>Device</td><td>Symptom</td>";

        $firmWare = $this->isDropDownElementExists($result, "firmWare");

        for ($i = 0; $i < $count; $i++) {
            $table .= '<tr class="success topbar">';

            foreach ($result[$i] as $key => $value) {

                if ($key == "mainCategory") {
                    for ($j = 0; $j < count($firmWare); $j++) {
                        if ($firmWare["$j"]["id"] == $value)
                            $table .= '<td>' . $firmWare[$j]["name"] . '</td>';
                    }
                } else {
                    $table .= '<td>' . $value . '</td>';
                }
            }

            $table.='</tr>';
        }

        $table .="</table>";
        return $table;
    }

    public function getDateField($name, $regex, $placeHolder = "", $value = "", $type) {
        $dateField = "";

        $dateField .= '<div class="input-group date" id="' . $name . 'datetimepicker2">';

        $dateField .= $this->getPartOfTextField($name, $regex, $placeHolder, $value, $type);

        $dateField .= '<span class="input-group-addon">';
        $dateField .= '<span class="glyphicon glyphicon-calendar"></span>';
        $dateField .= '</span>';

        $dateField .= '</div>';
        $dateField .= $this->getErrorHandling();
        //echo $dateField ; 
        //die() ; 

        return $dateField;
    }

    public function getTextArea($name, $regex, $value = "", $type) {
        $textArea = "";
        $textArea = '<textarea id="' . $name . '" name="' . $name . '" class="form-control" rows="2" pattern="' . $regex["regex"] . '" data-error="' . $regex["message"] . '"  ' . $type . '>' . $value . '</textarea>';
        $textArea .= '<span class="glyphicon form-control-feedback" aria-hidden="true"></span>';
        $textArea .= '<span class="help-block with-errors"></span>';

        return $textArea;
    }

    public function getTextField($name, $regex, $placeHolder = "", $value = "", $type) {
        $textField = "";
        $textField .= $this->getPartOfTextField($name, $regex, $placeHolder, $value, $type);

        $textField .= $this->getErrorHandling();

        return $textField;
    }

    public function getPartOfTextField($name, $regex, $placeHolder = "", $value = "", $type) {
        $textField = "";
        $textField .= '<input type="text" id="' . $name . '" name="' . $name . '" value="' . $value . '" class="form-control" placeholder="' . $placeHolder . '"  pattern="' . $regex["regex"] . '" data-error="' . $regex["message"] . '"  ' . $type . '>';

        return $textField;
    }

    public function getErrorHandling() {
        $error = "";
        $error .= '<span class="glyphicon form-control-feedback" aria-hidden="true"></span>';
        $error .= '<span class="help-block with-errors"></span>';

        return $error;
    }

    private $dropDownElement = array(
        "sex" => array(
            array("id" => "Mr", "name" => "Mr"),
            array("id" => "Ms", "name" => "Ms"),
            array("id" => "Mrs", "name" => "Mrs"),
        ),
        "accStatus" => array(
            array("id" => "1", "name" => "enable"),
            array("id" => "-1", "name" => "disable"),
        ),
        "status" => array(
            array("id" => "all", "name" => "all"),
            array("id" => "1", "name" => "enable"),
            array("id" => "-1", "name" => "disable"),
        ),
        "serviceType" => array(
            array("id" => "1", "name" => "Express"),
            array("id" => "2", "name" => "Standard"),
            array("id" => "3", "name" => "Per-Call"),
        ),
        "firmWare" => array(
            array("id" => "1", "name" => "Hardware"),
            array("id" => "2", "name" => "Software")
        ),
        "incidentStatus" => array(
            array("id" => "0", "color" => "14ADE0", "name" => "Pending"),
            array("id" => "1", "color" => "ED220C", "name" => "completed"),
            array("id"=>"2", "color" => "378006", "name" => "Assigned"),
            array("id" => "3", "color" => "ED220C", "name" => "In-Progress"),
            array("id" => "4", "color" => "06B9CC", "name" => "Wait for Customer"),
            array("id" => "5", "color" => "7C06CC", "name" => "Wait for parts"),
            array("id" => "6", "color" => "E87605", "name" => "closed"),
        )
    );

    public function isDropDownElementExists($result, $key) {
        if (isset($this->dropDownElement["$key"])) {
            return $this->dropDownElement["$key"];
        }
        return $result;
    }

    public function getDropDownListElement($key) {
        return $this->dropDownElement["$key"];
    }

    public function getDropDownList($selectName, $result, $regex, $isSelect = "0", $disabled = "disabled", $selectAmount = "") {

        $dropDownMenu = '<select ' . $selectAmount . ' id="' . $selectName . '" name="' . $selectName . '" class="form-control"  pattern="' . $regex["regex"] . '" data-error="' . $regex["msg"] . '"  ' . $disabled . ' >';

        $result = $this->isDropDownElementExists($result, $selectName);
        $dropDownMenu = $this->getOptionValue($dropDownMenu, $result, $isSelect);

        $dropDownMenu .= "</select>";
        $dropDownMenu .= "<span class='glyphicon form-control-feedback' aria-hidden='true'></span>";
        $dropDownMenu .= "<span class='help-block with-errors'></span>";

        return $dropDownMenu;
    }

    private function getOptionValue($dropDownMenu, $result, $isSelect) {
        $selected = "";
        for ($i = 0; $i < count($result); $i++) {
            if ($result[$i]["id"] == $isSelect) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            $dropDownMenu .= '<option value="' . $result[$i]["id"] . '" ' . $selected . '> ';

            $dropDownMenu .= $result[$i]["name"];
            $dropDownMenu .= "</option>";
        }

        return $dropDownMenu;
    }

}
