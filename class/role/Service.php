<?php

class Service extends VerifyWS implements verifyInterface {
    

    public function getService($param){
        $serviceType = $this->getServiceType($param) ; 
        $serviceName = $this->getServiceName($param) ;
        $serviceName = $serviceName["result"] ; 
        
        
        $service = $serviceType["result"] ; 
        
        for ($i = 0 ; $i<count($serviceName) ; $i++)  {
            for ($j = 0 ; $j < count ($service) ; $j++){
                if ($service[$j]["type"] == $serviceName[$i]["type"]){
                    $index = count ($service[$j]["info"]) ; 
                    $service[$j]["info"][$index] = array("id" => $serviceName[$i]["id"] , "name"=>$serviceName[$i]["name"]);
                    break ; 
                }
            }
        }
        return $service ; 
    }
    
    private function getServiceName($param) {
        $servicedb = new servicedb () ; 
        
        $servicedb->setQueryParam($param) ; 
        
        return $servicedb->getServiceName() ;
    }
    
    private function getServiceType ($param){
        
        $servicedb = new servicedb () ; 
        
        $servicedb->setQueryParam($param) ; 
        
        return $servicedb->getServiceType() ; 
    }
    
        
}