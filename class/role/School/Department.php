<?php

class Department extends VerifyWS implements verifyInterface {

    //=============Turn on Google Recapcha=====================
    const TURN_ON_GOOGLE_RECAPCHA = false;
    //=============Staff Regex========================
    const titleRegex = "/^[A-Za-z0-9]{1,30}$/D";
    const deptNameRegex = '/^[A-Za-z0-9,]{1,30}$/D';
    const firstNameRegex = "/^[A-Za-z]{1,20}$/D";
    const lastNameRegex = "/^[A-Za-z]{1,20}$/D";
    const userNameRegex = "/^[A-Za-z0-9]{3,20}$/D";
    const userPasswordRegex = "/^[A-Za-z0-9]{5,20}$/D";
    const HKIDCardNumberRegex = "/^[A-Za-z]{1}[0-9]{7}$/D";
    const phoneNumberRegex = "/^[0-9]{8}$/D";
    const addressRegex = "/^[A-Za-z0-9 ]+$/D";
    const birthdayRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const birthdayWithTimeRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const emailAddressRegex = "/^[A-Za-z0-9_]+@[A-Za-z0-9]+.[com|hk]+$/D";
    const sexRegex = "/^Mr|Ms|Mrs$/D";
    const jobTitleRegex = "/^[A-Za-z0-9]{1,20}$/D";
    const accStatusRegex = "/^1|-1|all$/D";
    const teamRegex = "/^[0-9]{1,3}$/D";
    const roleRegex = "/^[0-9]{1,3}|all$/D";
    const userIdRegex = "/^[0-9]+$/D";
    const incidentIdRegex = "/^[0-9]+$/D";
    const serialNoRegex = "/^[A-Za-z0-9]{1,30}+$/D";
    const serviceTypeRegex = "/^[0-9]+$/D";
    const symptomsRegex = "/^[0-9]+$/D";
    const descriptionRegex = "/^[A-Za-z0-9 ]+$/D";
    const firmWareRegex = "/^1|2$/D";
    const commentRegex = "/^[A-Za-z0-9 ]+$/D";

    const yearRegex = "/^[0-9]{4}$/D";

    const integerRegex = "/^[0-9]+$/D";

    private $format = array(
        "createDepartment" => array(
            "deptName" => self::titleRegex,
            "deptAddress" => self::addressRegex,
            "courseTitle" => self::addressRegex,
            "courseLevel" => self::integerRegex ,
            "offerYear" => self::birthdayWithTimeRegex ,
            "classSize" => self::integerRegex,
            //====category
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),


        "searchDepartment" => array(
            "deptName" => self::deptNameRegex,
            "deptAddress" => self::addressRegex,
            "courseTitle" => self::addressRegex,
            "courseLevel" => self::integerRegex ,
            "offerYear" => self::yearRegex ,
            "classSize" => self::integerRegex,
            "availablePlaces" => self::titleRegex,
            //====category
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),

        "updateDepartment" => array(
            "deptName" => self::deptNameRegex,
            "deptAddress" => self::addressRegex,
            "courseTitle" => self::addressRegex,
            "courseLevel" => self::integerRegex ,
            "classSize" => self::integerRegex,
            //====category
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),



        "deleteUser" => array(
            "id" => self::addressRegex,
        ),
        "getIncidentId" => array(
            "id" => self::incidentIdRegex
        ),
        "getUserId" => array(
            "id" => self::addressRegex
        ),
        "updateIncident" => array(
            "incidentStatus" => self::incidentIdRegex,
            "comment" => self::commentRegex,
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "viewUser" => array(
            "userName" => self::userNameRegex,
            "roleName" => self::roleRegex,
            "id" => self::userIdRegex,
            "accStatus" => self::accStatusRegex,
            "jobTitle" => self::jobTitleRegex,
        ),
        "searchIncidentId" => array(
            "title" => self::titleRegex,
            "description" => self::descriptionRegex,
            "incidentId" => self::userIdRegex,

            "fromWho"=>self::firstNameRegex,
            "assign"=>self::firstNameRegex
        ),
        "getUserInfoWithCustom" => array(
            "deptName" => self::deptNameRegex,
            "deptAddress" => self::addressRegex,
            "courseTitle" => self::addressRegex,
            "courseLevel" => self::integerRegex ,
            "offerYear" => self::yearRegex ,
            "classSize" => self::integerRegex,
            "availablePlaces" => self::titleRegex ,
        )
    );
    private $jsErrorMessage = array(
        "createDepartment" => array(
            "deptName" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "deptAddress" => array("status" => "2001", "msg" => "Please input department address", "html" => "textField"),
            "courseTitle" => array("status" => "2002", "msg" => "Please input valid course title", "html" => "textField"),
            "courseLevel" => array("status" => "2003", "msg" => "Please input course level", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "offerYear" => array("status" => "2004", "msg" => "Please input offer year", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "classSize" => array("status" => "2005", "msg" => "Please input class size", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),

            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),

        "searchDepartment" => array(
            "deptName" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "deptAddress" => array("status" => "2001", "msg" => "Please input department address", "html" => "textField"),
            "courseTitle" => array("status" => "2002", "msg" => "Please input valid course title", "html" => "textField"),
            "courseLevel" => array("status" => "2003", "msg" => "Please input course level", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "offerYear" => array("status" => "2004", "msg" => "Please input offer year", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "classSize" => array("status" => "2005", "msg" => "Please input class size", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "availablePlaces" => array( "status" =>  "2006", "msg" => "Please input Available Places", "html" => "textField"),

            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),


        "deleteUser" => array(
            "id" => array("status" => "2015", "msg" => "Please remove valid information"),
        ),
        "getUserId" => array(
            "id" => array("status" => "2016", "msg" => "Please select valid user"),
        ),
        "getIncidentId" => array(
            "id" => array("status" => "2017", "msg" => "Please select incident id"),
        ),
        "updateIncident" => array(
            "incidentStatus" => array("status" => "2009", "msg" => "Please input incident status"),
            "comment" => array("status" => "2010", "msg" => "Please input the comment"),
            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),
        "getUserInfoWithCustom" => array(
            "deptName" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "deptAddress" => array("status" => "2001", "msg" => "Please input department address", "html" => "textField"),
            "courseTitle" => array("status" => "2002", "msg" => "Please input valid course title", "html" => "textField"),
            "courseLevel" => array("status" => "2003", "msg" => "Please input course level", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "offerYear" => array("status" => "2004", "msg" => "Please input offer year", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "classSize" => array("status" => "2005", "msg" => "Please input class size", "html" => "textField"),"title" => array("status" => "2000", "msg" => "Please input valid department name", "html" => "textField"),
            "availablePlaces" => array("status" => "2006", "msg" => "Please input valid Available Places", "html" => "textField"),

        ),
        "getPatientId" => array(
            "HKID" => array("status" => "2003", "msg" => "Please input HKID card number A1234567 without () "),
        ),
        "isBirthdayValid" => array(
            "isBirthdayValid" => array("status" => "2010", "msg" => "Birthday is bigger than NOW ! Please check it again \n Today is %s but your select date is %s"),
        ),
        "addPatient" => array(
            "isRegisterBefore" => array("status" => "2011", "msg" => "Patient is added before Please make it update \n link : %s \nor add the other patient"),
        ),
        "updatePatientInfo" => array(
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "phone" => array("status" => "2004", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2005", "msg" => "Please input address"),
            "emContactPerson" => array("status" => "2006", "msg" => "Please input Emergency Contact Person"),
            "emContactPhone" => array("status" => "2007", "msg" => "Please input Emergency Contact Person Phone Number with 8 digit"),
            "dateOfBirth" => array("status" => "2008", "msg" => "Please input birthday with yyyy-mm-dd"),
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "patientInfo" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "deletePatientId" => array(
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "addPhysicalCheck" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
            "startDate" => array("status" => "2010", "msg" => "Please input start date with yyyy-mm-dd"),
            "height" => array("status" => "2011", "msg" => "Please input height with decimal"),
            "weight" => array("status" => "2012", "msg" => "Please input weight with decimal "),
            "systolic" => array("status" => "2013", "msg" => "Please input systolic with digit"),
            "diastloic" => array("status" => "2014", "msg" => "Please input diastolic with digit"),
        ),
        "inpatientinfo" => array(
        ),
        "inportphycheck" => array(
        ),
        "conregister" => array(
        ),
        "reportcheck" => array(
        ),
        "editinfo" => array(
        ),
        "checkinvoice" => array(
        )
    );
    private $formValidation = array(
        "getUserInfoWithCustom" => array(
            "optional" => array(
                "deptName" => self::deptNameRegex,
                "deptAddress" => self::addressRegex,
                "courseTitle" => self::addressRegex,
                "courseLevel" => self::integerRegex ,
                "offerYear" => self::yearRegex ,
                "classSize" => self::integerRegex,
                "availablePlaces" => self::titleRegex ,
            )
        )
    );

    public function getFormValidation() {
        return $this->formValidation;
    }

    public function getFormat($key) {

        return $this->format["$key"];
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->jsErrorMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->jsErrorMessage["$func"]["$status"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA;
    }

    //========================SQL===========================
    private function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    private function has_string_keys(array $array) {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }




    public function addDepartment($queryParam) {

        $departmentDB = new DepartmentDB ();
        $queryParam["uid"] = $this->getSession("uid");

        $departmentInfo = $departmentDB->addDeparment($queryParam);



        return $departmentInfo;

    }

    public function getAllInfo() {

        $departmentDB = new DepartmentDB ();

        $deptInfo = $departmentDB->getAllInfo();

        //print_r_pre($isUserExists) ;
        //die();
        return $deptInfo;
    }
    public function getInfoWithSearch($queryParam) {
        //print_r_pre($queryParam) ;

        $departmentDB = new DepartmentDB ();

        //print_r_pre($queryParam) ;
        $deptInfo = $departmentDB->getInfoWithSearch($queryParam);
        $sortByEnroll = $departmentDB->sortByEnroll($queryParam) ;
        $deptFilterInfo = array() ;


        //print_r_pre(iterator_to_array($deptInfo)) ;
        //print_r_pre($isUserExists) ;
        //die();
        $i = 0 ;
        foreach ($deptInfo as $doc) {
            $deptFilterInfo[$i]["_id"] = $doc["_id"];
            $deptFilterInfo[$i]["deptName"] = $doc["Department"]["name"];
            $deptFilterInfo[$i]["deptAddress"] = $doc["Department"]["location"];
            $deptFilterInfo[$i]["courseId"] = $doc["Course"][0]["id"];
            $deptFilterInfo[$i]["courseTitle"] = $doc["Course"][0]["courseTitle"];
            $deptFilterInfo[$i]["courseLevel"] = $doc["Course"][0]["courseLevel"];
            $deptFilterInfo[$i]["offerYear"] = date("Y" , $doc["offerYear"]->sec);
            $deptFilterInfo[$i]["classSize"] = $doc["classSize"];
            $deptFilterInfo[$i]["availablePlaces"] =$doc["classSize"];
            $deptFilterInfo[$i]["enroll"] = 0;
            $deptFilterInfo[$i]["percentage"] = "0%" ;
            $i++;
        }

        if ($sortByEnroll["ok"]==1){
            $sortByEnroll = $sortByEnroll["result"] ;
        }else{
            $sortByEnroll = [] ;
        }
        
        for ($j = 0 ; $j < count($deptFilterInfo) ; $j++){
            for ($k = 0 ; $k < count($sortByEnroll) ; $k ++){
                if ($deptFilterInfo[$j]["_id"] == $sortByEnroll[$k]["_id"]){
                    $deptFilterInfo[$j]["availablePlaces"] = $deptFilterInfo[$j]["classSize"] - $sortByEnroll[$k]["count"] ;
                    $deptFilterInfo[$j]["enroll"] = $sortByEnroll[$k]["count"] ;
                    $deptFilterInfo[$j]["percentage"] = (ceil(($sortByEnroll[$k]["count"] / $deptFilterInfo[$j]["classSize"]) * 100)) . "%";

                    break ;
                }
            }
        }



        //bubble sort
        $deptFilterInfo = $this->bubble_sort($deptFilterInfo, "percentage") ;


        return $deptFilterInfo;
    }

    public function updateDepartmentInfo ($queryParam){
        $departmentDB = new DepartmentDB ();

        $id = $queryParam["id"] ;
        $queryParam = array_filter($queryParam) ;
        unset($queryParam["id"]); 

        $deptInfo = $departmentDB->updateInfo($id, $queryParam );

        //print_r_pre($isUserExists) ;
        //die();
        return $deptInfo;
    }

    
    public function getAvailablePlaces($queryParam){

        $departmentDB = new DepartmentDB ();



        $deptInfo = $departmentDB->getAvailablePlace($queryParam );

        //print_r_pre($isUserExists) ;
        //die();
        return $deptInfo;
        
    }

    public function sortByEnroll($queryParam){

        $departmentDB = new DepartmentDB ();



        $deptInfo = $departmentDB->sortByEnroll($queryParam );

        //print_r_pre($isUserExists) ;
        //die();
        return $deptInfo;

    }


    public function bubble_sort($arr , $key) {
        $size = count($arr);
        for ($i=0; $i<$size; $i++) {
            for ($j=0; $j<$size-1-$i; $j++) {
                if ($arr[$j+1][$key] > $arr[$j][$key] ) {
                    $this->swap($arr, $j, $j+1);
                }
            }
        }
        return $arr;
    }

    public function swap(&$arr, $a, $b) {
        $tmp = $arr[$a];
        $arr[$a] = $arr[$b];
        $arr[$b] = $tmp;
    }


}
