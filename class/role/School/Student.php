<?php

class Student extends VerifyWS implements verifyInterface {

    //=============Turn on Google Recapcha=====================
    const TURN_ON_GOOGLE_RECAPCHA = false;
    //=============Staff Regex========================
    const titleRegex = "/^[A-Za-z0-9]{1,30}$/D";
    const firstNameRegex = "/^[A-Za-z]{1,20}$/D";
    const lastNameRegex = "/^[A-Za-z]{1,20}$/D";
    const userNameRegex = "/^[A-Za-z0-9]{3,20}$/D";
    const userPasswordRegex = "/^[A-Za-z0-9]{5,20}$/D";
    const HKIDCardNumberRegex = "/^[A-Za-z]{1}[0-9]{7}$/D";
    const phoneNumberRegex = "/^[0-9]{8}$/D";
    const addressRegex = "/^[A-Za-z0-9 ]+$/D";
    const birthdayRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const birthdayWithTimeRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const emailAddressRegex = "/^[A-Za-z0-9_]+@[A-Za-z0-9]+.[com|hk]+$/D";
    const sexRegex = "/^Mr|Ms|Mrs$/D";
    const jobTitleRegex = "/^[A-Za-z0-9]{1,20}$/D";
    const accStatusRegex = "/^1|-1|all$/D";
    const teamRegex = "/^[0-9]{1,3}$/D";
    const roleRegex = "/^[0-9]{1,3}|all$/D";
    const userIdRegex = "/^[0-9]+$/D";
    const incidentIdRegex = "/^[0-9]+$/D";
    const serialNoRegex = "/^[A-Za-z0-9]{1,30}+$/D";
    const serviceTypeRegex = "/^[0-9]+$/D";
    const symptomsRegex = "/^[0-9]+$/D";
    const descriptionRegex = "/^[A-Za-z0-9 ]+$/D";
    const firmWareRegex = "/^1|2$/D";
    const commentRegex = "/^[A-Za-z0-9 ]+$/D";

    const integerRegex = "/^[0-9]+$/D";

    private $format = array(
        "createStudent" => array(
            "studName" => self::addressRegex,
            "DOB" => self::birthdayRegex,
            //====category
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "enroll" => array(
            //"id" => self::addressRegex,
        ),

        "deleteUser" => array(
            "id" => self::addressRegex,
        ),

        "getUserId" => array(
            "id" => self::addressRegex
        ),


        "getUserInfoWithCustom" => array(
            "studName" => self::titleRegex,
            "DOB" => self::birthdayRegex,
        )
    );
    private $jsErrorMessage = array(
        "createStudent" => array(
            "studName" => array("status" => "2000", "msg" => "Please input valid student name", "html" => "textField"),
            "DOB" => array("status" => "2001", "msg" => "Please input date of birth", "html" => "textField"),
            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),

        "enroll" => array(
            "enroll" => array("status" => "2001", "msg" => "Please input enroll course", "html" => "textField"),

        ),

        "deleteUser" => array(
            "id" => array("status" => "2015", "msg" => "Please remove valid information"),
        ),
        "getUserId" => array(
            "id" => array("status" => "2016", "msg" => "Please select valid user"),
        ),
        "getIncidentId" => array(
            "id" => array("status" => "2017", "msg" => "Please select incident id"),
        ),
        "updateIncident" => array(
            "incidentStatus" => array("status" => "2009", "msg" => "Please input incident status"),
            "comment" => array("status" => "2010", "msg" => "Please input the comment"),
            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),
        "getUserInfoWithCustom" => array(
            "studName" => array("status" => "2000", "msg" => "Please input valid student name", "html" => "textField"),
            "DOB" => array("status" => "2001", "msg" => "Please input date of birth", "html" => "textField"),

        ),
        "getPatientId" => array(
            "HKID" => array("status" => "2003", "msg" => "Please input HKID card number A1234567 without () "),
        ),
        "isBirthdayValid" => array(
            "isBirthdayValid" => array("status" => "2010", "msg" => "Birthday is bigger than NOW ! Please check it again \n Today is %s but your select date is %s"),
        ),
        "addPatient" => array(
            "isRegisterBefore" => array("status" => "2011", "msg" => "Patient is added before Please make it update \n link : %s \nor add the other patient"),
        ),
        "updatePatientInfo" => array(
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "phone" => array("status" => "2004", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2005", "msg" => "Please input address"),
            "emContactPerson" => array("status" => "2006", "msg" => "Please input Emergency Contact Person"),
            "emContactPhone" => array("status" => "2007", "msg" => "Please input Emergency Contact Person Phone Number with 8 digit"),
            "dateOfBirth" => array("status" => "2008", "msg" => "Please input birthday with yyyy-mm-dd"),
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "patientInfo" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "deletePatientId" => array(
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "addPhysicalCheck" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
            "startDate" => array("status" => "2010", "msg" => "Please input start date with yyyy-mm-dd"),
            "height" => array("status" => "2011", "msg" => "Please input height with decimal"),
            "weight" => array("status" => "2012", "msg" => "Please input weight with decimal "),
            "systolic" => array("status" => "2013", "msg" => "Please input systolic with digit"),
            "diastloic" => array("status" => "2014", "msg" => "Please input diastolic with digit"),
        ),
        "inpatientinfo" => array(
        ),
        "inportphycheck" => array(
        ),
        "conregister" => array(
        ),
        "reportcheck" => array(
        ),
        "editinfo" => array(
        ),
        "checkinvoice" => array(
        )
    );
    private $formValidation = array(
        "getUserInfoWithCustom" => array(
            "optional" => array(
                "studName" => self::titleRegex,
                "DOB" => self::birthdayRegex,

            )
        ),
        "enroll" => array(
            "optional" => array(
                "enroll" => self::addressRegex,

            )
        ),
    );

    public function getFormValidation() {
        return $this->formValidation;
    }

    public function getFormat($key) {

        return $this->format["$key"];
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->jsErrorMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->jsErrorMessage["$func"]["$status"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA;
    }

    //========================SQL===========================
    private function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    private function has_string_keys(array $array) {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }




    public function addStudent($queryParam) {

        $studentDB = new StudentDB();

        $queryParam["uid"] = $this->getSession("uid");

        $studentInfo = $studentDB->addStudent($queryParam);


        return $studentInfo;

    }

    public function getAllInfo() {

        $studentDB = new StudentDB();

        $studentInfo = $studentDB->getAllInfo();

        //print_r_pre($isUserExists) ;
        //die();
        return $studentInfo;
    }
    public function getInfoWithSearch($queryParam) {
        $studentDB = new StudentDB();

        //print_r_pre($queryParam) ;
        $studentInfo = $studentDB->getInfoWithSearch($queryParam);
        $studentFilterInfo = array() ;

        //print_r_pre(iterator_to_array($deptInfo)) ;
        //print_r_pre($isUserExists) ;
        //die();
        $i = 0 ;
        foreach ($studentInfo as $doc){
            $studentFilterInfo[$i]["_id"] = $doc["_id"];
            $studentFilterInfo[$i]["studName"] = $doc["name"] ;
            $studentFilterInfo[$i]["DOB"] = $doc["DOB"] ;
            $i++ ;
        }

        
        return $studentFilterInfo;
    }

    public function updateStudentInfo ($queryParam){
        $studentDB = new StudentDB();
        $department = new Department() ;
        $departmentDB = new DepartmentDB() ;

        $id = $queryParam["id"] ;
        $queryParam = array_filter($queryParam) ;
        unset($queryParam["id"]);

        $pullParam = [
            "pullCourse" => [
                "studentId" => $id
            ]
        ];

        $departmentDB->updateInfo([], $pullParam , '$pull' , ['multiple' => true]);

        $studentInfo = $studentDB->updateInfo($id, $queryParam );

        /*
        $deptInfo = $departmentDB->getInfoWithSearch([
           "_id" =>  $id
        ]);

        for ($deptInfo as $doc){
            $classSize = $doc["classSize"] ;
        }
*/
        if (!isset($queryParam['enroll'])){
            $queryParam['enroll'] = array() ;
        }

        for ($i = 0 ; $i < count($queryParam['enroll'] ); $i++){
            $deptInfo = $department->getInfoWithSearch([
                "_id" =>   $queryParam['enroll'][$i]

            ]);
            $deptInfo = $deptInfo[0] ;

            $availablePlaces = $departmentDB->getAvailablePlace([
                "courseId" => $queryParam["courseId"][$i]
            ]);

            if (($deptInfo["classSize"] - $availablePlaces) >= 1)
                $studentDB->enroll($queryParam['enroll'][$i],$id,  $queryParam);
            else{
                return array("status" => "3001" , "msg" => $deptInfo["courseTitle"] ." has fulled");
            }
        }


        //print_r_pre($isUserExists) ;
        //die();
        return $studentInfo;
    }

    public function enrollCourse($queryParam){

    }


    public function getCourse($id) {
        $department = new Department() ;

        $startDate=  "2016-01-01 00:00:00" ;
        $endDate=  "2016-12-31 23:59:59" ;

        $offerYear = array() ;

        $offerYear[0] = new MongoDate(strtotime($startDate));
        $offerYear[1] = new MongoDate(strtotime($endDate));

        $deptInfo = $department->getInfoWithSearch([
            "offerYear" => $offerYear
        ]) ;
        $enrollCourseInfo = $department->getInfoWithSearch([
            "studentId" => $id
        ]) ;


        $courseInfo =  $deptInfo;
        for ($i = 0 ; $i < count($deptInfo) ; $i++){
            $courseInfo[$i]["availablePlaces"] = $deptInfo[$i]["classSize"] - $department->getAvailablePlaces([
                "courseId" => $deptInfo[$i]["courseId"]
            ]) ;

            for ($j = 0 ; $j < count($enrollCourseInfo) ; $j++){
                if ($deptInfo[$i]["_id"] == $enrollCourseInfo[$j]["_id"]){
                    $courseInfo[$i]["checked"] = "checked" ;
                    break ;
                }
            }

        }

        return $courseInfo ;

    }


}
