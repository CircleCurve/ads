<?php

class Booking extends VerifyWS implements verifyInterface {

    //=============Turn on Google Recapcha=====================
    const TURN_ON_GOOGLE_RECAPCHA = false;
    //=============Patient Registeration========================
    const bookingDateRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/D";
    const incomedateRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/D";
    const outdateRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/D";
    const HKIDCardNumberRegex = "/^[A-Za-z]{1}[0-9]{7}$/D";
    const serviceTypeRegex = "/^[0-9]{8}$/D";
    const bedTypeRegex = "/^[0-9]{1,20}$/D";
    const bookingIdRegex = "/^[0-9]{1,20}$/D";
    const patientIdRegex = "/^[0-9]{1,20}$/D";

    //=============format ========================

    private $format = array(
        
        "booking" => array(
            "HKID" => self::HKIDCardNumberRegex,
            "bookingdate" => self::bookingDateRegex,
            "incomedate" => self::incomedateRegex,
            "outdate" => self::outdateRegex,
            "patientId" => self::patientIdRegex,
            //"servicetype" => self::serviceTypeRegex,
            "bedType" => self::bedTypeRegex,
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "searchBookingId" => array(
            "bookingId" => self::bookingIdRegex
        ),
        "deleteBookingId" => array(
            "bookingId" => self::bookingIdRegex

        ),
        "inHostpital" => array(
            "bookingIdForm" => self::bookingIdRegex,
            "patientId" => self::patientIdRegex , 
        ),
        "income" => array(
            "bookingId" => self::bookingIdRegex,
            

        ),
        "bookinfo" => array(
            "bookingId" => self::bookingIdRegex,
        ),

        "reportcheck" => array(
        ),
        "checkinvoice" => array(
        )
    );

    private $formValidation = array(
        "booking" => array(
            "optional"=>array(
                "HKID"=>self::HKIDCardNumberRegex
            )
        )
    );
    //============form Required or Optional========================




    private $jsErrorMessage = array(
        "general" => array(
            "keyNotExists" => array("status" => "3000", "msg" => "key does not exists")
        ),
        "booking" => array(
            "bookingdate" => array("status" => "3001", "msg" => "Please using correct format of date"),
            "incomedate" => array("status" => "3002", "msg" => "Please using correct format of date"),
            "outdate" => array("status" => "3003", "msg" => "Please using correct format of date"),
            "patientId" => array("status" => "3004", "msg" => "Please input correctly patient id only digit"),
            "HKID" => array("status" => "3005", "msg" => "Please input HKID card number A1234567 without () "),
            "servicetype" => array("status" => "3006", "msg" => "Please input valid service"),
            "bedType" => array("status" => "3007", "msg" => "Please input valid bed type"),
            "g-recaptcha-response" => array("status" => "3008", "msg" => "Please input tap the verify code"),
        ),
        "searchBookingId" => array(
            "bookingId" => array("status" => "3009", "msg" => "Please input the correct booking id only digit"),
        ),
        "deleteBookingId" => array(
            "bookingId" => array("status" => "3010", "msg" => "Please input the correct booking id only digit"),
        ),
        "inHostpital" => array(
        
            "bookingIdForm" => array("status" => "3012", "msg" => "Please input the correct booking id only digit"),
            
            "patientId" => array("status" => "3013", "msg" => "Please input correctly patient id only digit"),
            
        ),
        
        "income" => array(
            "bookingId" => array("status" => "3011", "msg" => "Please input the correct booking id only digit"),

        ),
        
        "isBirthdayValid" => array(
            "isBirthdayValid" => array("status" => "2010", "msg" => "Birthday is bigger than NOW ! Please check it again \n Today is %s but your select date is %s"),
        ),

        "bookinfo" => array(
            "bookingId" => array("status" => "3100", "msg" => "Please input correct bookingid only digit")
        ),
        "conbooking" => array(
        ),
        "reportcheck" => array(
        ),
        "checkinvoice" => array(
        )
    );
    
   

    public function __construct() {
        //parent::__construct();
        $this->setFormValidation($this->formValidation) ; 
    }
    
    public function getFormat($key) {

        return $this->format["$key"];
    }
    
    public function setFormValidation($formValidation) {
        
        parent::setFormValidation($formValidation);
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->jsErrorMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->jsErrorMessage["$func"]["$status"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA;
    }

    public function isBirthDayBiggerThanNOW($date) {
        $now = strtotime("now");
        $targetDate = strtotime($date);


        if ($now > $targetDate) {
            return array("status" => true);
        }
        $vsprintf = array(
            date("Y-m-d"), $date
        );

        //return $this->getErrorMessage("isBirthdayValid", "isBirthdayValid",$vsprintf) ; 
        return $this->displayResult("isBirthdayValid", "isBirthdayValid", $vsprintf);
    }

    //========================SQL===========================


    //===========Bed Information========================

    public function getBedTypeInformation($queryParam) {
        $bookingdb = new Bookingdb ();
        $bookingdb->setQueryParam($queryParam);
        return $bookingdb->getBedTypeInformation();
    }
   //===========Bed Information========================
    public function getServiceInformation ($queryParam){
        $service = new Service() ; 
        $serviceName = $service->getService($queryParam) ; 
        
        //echo "serviceName : "; 
        //print_r_pre($serviceName) ; 
    }
    
    
    public function isBookingBefore($queryParam) {
        $bookingdb = new Bookingdb ();
        $bookingdb->setQueryParam($queryParam);
        return $bookingdb->isBookingBefore();
    }
    
    
    public function addBooking($queryParam) {
        $bookingdb = new Bookingdb ();
        $bookingdb->setQueryParam($queryParam);
        return $bookingdb->addBooking();
    }


    
    public function getBookingInfo($queryParam) {
        $bookingdb = new Bookingdb ();
        $bookingdb->setQueryParam($queryParam);
        $bookInfo = $bookingdb->getBookingInfo();
        
        for ($i = 0 ; $i < count($bookInfo["result"]) ; $i++){
            $bookInfo["result"][$i]["in_date"] = $queryParam["incomedate"] ; 
            $bookInfo["result"][$i]["out_date"] = $queryParam["outdate"] ; 
        }
        return $bookInfo ; 
    }
    
    
    public function getPatientBookingInformation ($queryParam){
        $bookingdb = new Bookingdb ();
        $bookingdb->setQueryParam($queryParam);
        return $bookingdb->getPatientBookingInformation();
    }
    
    
    public function setBookingStatus($queryParam){
        $bookingdb = new Bookingdb ();
        $bookingdb->setQueryParam($queryParam);
        return $bookingdb->setBookingStatus();
    }
    
    
    public function addInpatientRecord ($queryParam){
        $patiendb = new Patientdb ();
        $patiendb->setQueryParam($queryParam);
        return $patiendb->addInPatientRecord();
    }
    
    public function isInPatientNow ($queryParam){
        $patiendb = new Patientdb ();
        $patiendb->setQueryParam($queryParam);
        return $patiendb->isInPatientNow();
    }
    

}
