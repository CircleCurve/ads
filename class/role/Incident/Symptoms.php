<?php

class Symptoms extends VerifyWS implements verifyInterface {

    private $jsErrorMessage = array();
    private $serviceType = array(
        array("id" => "1", "name" => "Express", "time" => array("h" => "2")),
        array("id" => "2", "name" => "Standard", "time" => array("h" => "24")),
        array("id" => "3", "name" => "Per-Call", "time" => array("h" => "0")),
    );
    private $dateSetting = array(
        "start" => "",
        "end" => "",
        "color" => "", 
        "allDay" => ""
    );
    
    

    public function addSymptomsIssue($queryParam) {

        $symptomsDB = new SymptomsDB();
        $symptomsDB->setQueryParam($queryParam);
        return $symptomsDB->addSymptomsIssue();
    }
    
    public function getSymptomsIssue($queryParam) {

        $symptomsDB = new SymptomsDB ();
        $symptomsDB->setQueryParam($queryParam);
        $info = $symptomsDB->getSymptomsIssue();
        
        return $info ; 
        /*
        if ($info["status"] == "1"){
            
        }
        
        return  ;*/ 
    }

    public function getEvent($queryParam) {
        $queryParam["uid"] = $this->getSession("uid");

        $calendarDB = new CalendarDB();
        $calendarDB->setQueryParam($queryParam);
        $info = $calendarDB->getEvent();

        if ($info["status"] == "1") {

            $info = $this->setDate($info["info"]);

            return $info;
        }

        return array();
    }


}
