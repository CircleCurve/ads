<?php

class Calendar extends VerifyWS implements verifyInterface {

    private $jsErrorMessage = array(
        "title" => array("status" => "2000", "msg" => "Please input valid incident name", "html" => "textField"),
        "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words", "html" => "textField"),
        "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words", "html" => "textField"),
        "sex" => array("status" => "2010", "msg" => "Please input valid sex", "html" => "dropdown"),
        "mobile" => array("status" => "2005", "msg" => "Please input 8 digit phone number", "html" => "textField"),
        "address" => array("status" => "2006", "msg" => "Please input address", "html" => "textField"),
        "email" => array("status" => "2010", "msg" => "Please input valid email address : xxyy@xxx.com or hk", "html" => "textField"),
        "serialNo" => array("status" => "2011", "msg" => "Please input valid serial number within 30 digit", "html" => "textField"),
        "serviceType" => array("status" => "2012", "msg" => "Please input the valid sevice type", "html" => "dropdown"),
        "firmWare" => array("status" => "2015", "msg" => "Please input the valid firmware", "html" => "dropdown"),
        "symptoms" => array("status" => "2013", "msg" => "Please input the valid symptoms", "html" => "dropdown"),
        "description" => array("status" => "2014", "msg" => "Please input the valid description", "html" => "textArea"),
        "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
    );
    private $serviceType = array(
        array("id" => "1", "name" => "Express", "time" => array("h" => "2")),
        array("id" => "2", "name" => "Standard", "time" => array("h" => "24")),
        array("id" => "3", "name" => "Per-Call", "time" => array("h" => "0")),
    );
    private $dateSetting = array(
        "start" => "",
        "end" => "",
        "color" => "",
        "allDay" => ""
    );
    private $colorSetting = array(
        "0" => array("color" => "14ADE0", "name" => "Pending"),
        "1" => array("color" => "78B4B4", "name" => "completed"),
        "2" => array("color" => "378006", "name" => "Assigned"),
        "3" => array("color" => "ED220C", "name" => "In-Progress"),
        "4" => array("color" => "06B9CC", "name" => "Wait for Customer"),
        "5" => array("color" => "7C06CC", "name" => "Wait for parts"),
        "6" => array("color" => "E87605", "name" => "closed"),
    );

    public function getColorSettingWithKey($index, $key) {

        if (is_array($this->colorSetting["$index"]) && isset($this->colorSetting["$index"]["$key"])) {
            return $this->colorSetting["$index"]["$key"];
        }

        return "";
    }

    public function addEvent($queryParam) {

        $calendarDB = new CalendarDB();
        $queue = $calendarDB->lessIncidentFirst() ; 

        $serviceType = $queryParam["serviceType"];
        $incidentStatus = $queryParam["incidentStatus"];
        $serviceTypeArr = $this->serviceType[$serviceType - 1];
        $hour = $serviceTypeArr["time"]["h"];

        $date = new DateTime("now", new DateTimeZone('Asia/Taipei'));

        $queryParam["start"] = $date->format("Y-m-d H:i:s");
        $queryParam["end"] = $date->modify("+$hour hour")->format("Y-m-d H:i:s");
        $queryParam["color"] = $this->colorSetting["2"]["color"];
        $queryParam["uid"] = $queue["info"][0]["id"];
        $queryParam["public_own"] = "1";
        $queryParam["allDay"] = "0";


        $calendarDB->setQueryParam($queryParam);
        return $calendarDB->addEvent();
    }

    public function getEvent($queryParam) {
        $queryParam["uid"] = $this->getSession("uid");

        $calendarDB = new CalendarDB();
        $calendarDB->setQueryParam($queryParam);
        $info = $calendarDB->getEvent();

        if ($info["status"] == "1") {

            $info = $this->setDate($info["info"]);

            return $info;
        }

        return array();
    }

    public function setDate($info) {
        $fliterTime = $this->dateSetting;
        for ($i = 0; $i < count($info); $i++) {
            foreach ($fliterTime as $key => $value) {

                if ($key == "color") {
                    $info[$i]["$key"] = "#" . $info[$i]["$key"];
                }

                if ($key == "allDay") {
                    $info[$i]["$key"] = false;
                }

                if ($key == "start" || $key == "end") {


                    $datetime = explode(" ", $info[$i]["$key"]);
                    $info[$i]["$key"] = $datetime[0] . "T" . $datetime[1];
                }
            }
        }

        return $info;
    }

}
