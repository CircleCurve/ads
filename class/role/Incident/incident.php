<?php

class Incident extends VerifyWS implements verifyInterface {

    //=============Turn on Google Recapcha=====================
    const TURN_ON_GOOGLE_RECAPCHA = false;
    //=============Staff Regex========================
    const titleRegex = "/^[A-Za-z0-9 ]{1,30}+$/D";
    const firstNameRegex = "/^[A-Za-z]{1,20}$/D";
    const lastNameRegex = "/^[A-Za-z]{1,20}$/D";
    const userNameRegex = "/^[A-Za-z0-9]{3,20}$/D";
    const userPasswordRegex = "/^[A-Za-z0-9]{5,20}$/D";
    const HKIDCardNumberRegex = "/^[A-Za-z]{1}[0-9]{7}$/D";
    const phoneNumberRegex = "/^[0-9]{8}$/D";
    const addressRegex = "/^[A-Za-z0-9 ]+$/D";
    const birthdayRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const birthdayWithTimeRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const emailAddressRegex = "/^[A-Za-z0-9_]+@[A-Za-z0-9]+.[com|hk]+$/D";
    const sexRegex = "/^Mr|Ms|Mrs$/D";
    const jobTitleRegex = "/^[A-Za-z0-9]{1,20}$/D";
    const accStatusRegex = "/^1|-1|all$/D";
    const teamRegex = "/^[0-9]{1,3}$/D";
    const roleRegex = "/^[0-9]{1,3}|all$/D";
    const userIdRegex = "/^[0-9]+$/D";
    const incidentIdRegex = "/^[0-9]+$/D";
    const serialNoRegex = "/^[A-Za-z0-9]{1,30}+$/D";
    const serviceTypeRegex = "/^[0-9]+$/D";
    const symptomsRegex = "/^[0-9]+$/D";
    const descriptionRegex = "/^[A-Za-z0-9 ]+$/D";
    const firmWareRegex = "/^1|2$/D";
    const commentRegex = "/^[A-Za-z0-9 ]+$/D";

    private $format = array(
        "createIncident" => array(
            "title" => self::titleRegex,
            "firstName" => self::firstNameRegex,
            "lastName" => self::lastNameRegex,
            "sex" => self::sexRegex,
            "mobile" => self::phoneNumberRegex,
            "address" => self::addressRegex,
            "email" => self::emailAddressRegex,
            "serialNo" => self::serialNoRegex,
            "serviceType" => self::serviceTypeRegex,
            "firmWare" => self::firmWareRegex,
            //"symptoms"=>self::symptomsRegex , 
            "description" => self::descriptionRegex,
            //====category
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "deleteUser" => array(
            "id" => self::userIdRegex,
        ),
        "getIncidentId" => array(
            "id" => self::incidentIdRegex
        ),
        "updateIncident" => array(
            "incidentStatus" => self::incidentIdRegex,
            "comment" => self::commentRegex,
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "viewUser" => array(
            "userName" => self::userNameRegex,
            "roleName" => self::roleRegex,
            "id" => self::userIdRegex,
            "accStatus" => self::accStatusRegex,
            "jobTitle" => self::jobTitleRegex,
        ),
        "searchIncidentId" => array(
            "title" => self::titleRegex,
            "description" => self::descriptionRegex,
            "incidentId" => self::userIdRegex,

            "fromWho"=>self::firstNameRegex,
            "assign"=>self::firstNameRegex
        ),
        "getUserInfoWithCustom" => array(
            "userName" => self::userNameRegex,
            "role" => self::roleRegex,
            "jobTitle" => self::jobTitleRegex,
            "id" => self::userIdRegex,
            "status" => self::accStatusRegex,
        )
    );
    private $jsErrorMessage = array(
        "createIncident" => array(
            "title" => array("status" => "2000", "msg" => "Please input valid incident name", "html" => "textField"),
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words", "html" => "textField"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words", "html" => "textField"),
            "sex" => array("status" => "2010", "msg" => "Please input valid sex", "html" => "dropdown"),
            "mobile" => array("status" => "2005", "msg" => "Please input 8 digit phone number", "html" => "textField"),
            "address" => array("status" => "2006", "msg" => "Please input address", "html" => "textField"),
            "email" => array("status" => "2010", "msg" => "Please input valid email address : xxyy@xxx.com or hk", "html" => "textField"),
            "serialNo" => array("status" => "2011", "msg" => "Please input valid serial number within 30 digit", "html" => "textField"),
            "serviceType" => array("status" => "2012", "msg" => "Please input the valid sevice type", "html" => "dropdown"),
            "firmWare" => array("status" => "2015", "msg" => "Please input the valid firmware", "html" => "dropdown"),
            "symptoms" => array("status" => "2013", "msg" => "Please input the valid symptoms", "html" => "dropdown"),
            "description" => array("status" => "2014", "msg" => "Please input the valid description", "html" => "textArea"),
            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),
        "deleteUser" => array(
            "id" => array("status" => "2015", "msg" => "Please remove valid user"),
        ),
        "getUserId" => array(
            "id" => array("status" => "2016", "msg" => "Please select valid user"),
        ),
        "getIncidentId" => array(
            "id" => array("status" => "2017", "msg" => "Please select incident id"),
        ),
        "updateIncident" => array(
            "incidentStatus" => array("status" => "2009", "msg" => "Please input incident status"),
            "comment" => array("status" => "2010", "msg" => "Please input the comment"),
            "g-recaptcha-response" => array("status" => "2008", "msg" => "Please input tap the verify code"),
        ),
        "getUserInfoWithCustom" => array(
            "userName" => array("status" => "2003", "msg" => "Please input your name 4-16 digit"),
            "role" => array("status" => "2020", "msg" => "Please input your name 4-16 digit"),
            "id" => array("status" => "2016", "msg" => "Please select valid user"),
            "status" => array("status" => "2012", "msg" => "Please input the valid accout status"),
            "jobTitle" => array("status" => "2011", "msg" => "Please input job title"),
        ),
        "getPatientId" => array(
            "HKID" => array("status" => "2003", "msg" => "Please input HKID card number A1234567 without () "),
        ),
        "isBirthdayValid" => array(
            "isBirthdayValid" => array("status" => "2010", "msg" => "Birthday is bigger than NOW ! Please check it again \n Today is %s but your select date is %s"),
        ),
        "addPatient" => array(
            "isRegisterBefore" => array("status" => "2011", "msg" => "Patient is added before Please make it update \n link : %s \nor add the other patient"),
        ),
        "updatePatientInfo" => array(
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "phone" => array("status" => "2004", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2005", "msg" => "Please input address"),
            "emContactPerson" => array("status" => "2006", "msg" => "Please input Emergency Contact Person"),
            "emContactPhone" => array("status" => "2007", "msg" => "Please input Emergency Contact Person Phone Number with 8 digit"),
            "dateOfBirth" => array("status" => "2008", "msg" => "Please input birthday with yyyy-mm-dd"),
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "patientInfo" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "deletePatientId" => array(
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "addPhysicalCheck" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
            "startDate" => array("status" => "2010", "msg" => "Please input start date with yyyy-mm-dd"),
            "height" => array("status" => "2011", "msg" => "Please input height with decimal"),
            "weight" => array("status" => "2012", "msg" => "Please input weight with decimal "),
            "systolic" => array("status" => "2013", "msg" => "Please input systolic with digit"),
            "diastloic" => array("status" => "2014", "msg" => "Please input diastolic with digit"),
        ),
        "inpatientinfo" => array(
        ),
        "inportphycheck" => array(
        ),
        "conregister" => array(
        ),
        "reportcheck" => array(
        ),
        "editinfo" => array(
        ),
        "checkinvoice" => array(
        )
    );
    private $formValidation = array(
        "getUserInfoWithCustom" => array(
            "optional" => array(
                "userName" => self::userNameRegex,
                "role" => self::roleRegex,
                "jobTitle" => self::jobTitleRegex,
                "id" => self::userIdRegex,
                "status" => self::accStatusRegex
            )
        )
    );

    public function getFormValidation() {
        return $this->formValidation;
    }

    public function getFormat($key) {

        return $this->format["$key"];
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->jsErrorMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->jsErrorMessage["$func"]["$status"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA;
    }

    //========================SQL===========================
    private function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    private function has_string_keys(array $array) {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }

    public function addIncident($queryParam) {
        $rtnMsg = array();


        if (!$this->isJson($queryParam["symptoms"])) {
            $this->displayRawResult(array("status" => "3001", "msg" => "Please input valid symptoms"));
        }

        $queryParam["symptoms"] = json_decode($queryParam["symptoms"], true);

        if ($this->has_string_keys($queryParam["symptoms"])) {
            $this->displayRawResult(array("status" => "3002", "msg" => "Please input valid symptoms"));
        }

        $incidentDB = new IncidentDB ();
        $queryParam["uid"] = $this->getSession("uid");

        $incidentDB->setQueryParam($queryParam);

        $incidentInfo = $incidentDB->addIncident();


        return $incidentInfo;
        /*
          if ($incidentInfo["status"] != "1") {
          return $incidentInfo;
          } */

        //$calendarDB = new CalendarDB();
        //$calendarDB->setQueryParam($queryParam);
    }

    public function getAllStaff($queryParam) {
        
        $htmlElement = new HtmlElement() ; 
        $incidentStatus = $htmlElement->getDropDownListElement("incidentStatus") ; 
        
        $member = new IncidentDB ();
        $member->setQueryParam($queryParam);
        $userInfo = $member->getAllUserInformation(0, 100);
        
        for ($i = 0 ; $i< count($userInfo) ; $i++){
            $status = $userInfo[$i]["status"] ; 
            $userInfo[$i]["status"] = $incidentStatus[$status]["name"] ; 
            
        }

        //print_r_pre($isUserExists) ; 
        //die(); 
        return $userInfo;
    }

    public function getIncidentInfoInSearch($type = "withoutAll") {

        $roleDB = new RoleDB ();
        //$roleDB->setQueryParam(); 
        $roleInfo = $roleDB->getRoleInfo();



        if ($roleInfo["status"] == "1") {

            if ($type == "all") {

                array_unshift($roleInfo["data"], array("id" => "all", "name" => "all"));
            }
            return $roleInfo;
        }

        return array("data" => array());
    }

    public function getRoleInfoWithId($queryParam) {
        $roleDB = new RoleDB ();
        $roleDB->setQueryParam($queryParam);
        $roleInfo = $roleDB->getRoleInfoWithId();

        return $roleInfo;
    }

    public function getTeamInfo() {
        $teamDB = new TeamDB ();
        //$roleDB->setQueryParam(); 
        $teamInfo = $teamDB->getTeamInfo();

        if ($teamInfo["status"] == "1") {
            return $teamInfo;
        }

        return array("data" => array());
    }

    public function getTeamInfoWithId($queryParam) {
        $teamDB = new TeamDB ();
        $teamDB->setQueryParam($queryParam);
        $teamInfo = $teamDB->getTeamInfoWithId();

        return $teamInfo;
    }

    public function setUserStatus($queryParam) {
        $member = new Member ();
        $member->setQueryParam($queryParam);
        $isUidValid = $member->isUidValid();

        if ($isUidValid["status"] != "1") {
            return $isUidValid;
        }
        $userStatus = $member->setUserStatus();
        //print_r_pre($isUserExists) ; 
        //die(); 
        return $userStatus;
    }

    public function getDevice($queryParam) {
        $incidentDB = new IncidentDB ();
        $incidentDB->setQueryParam($queryParam);
        $device = $incidentDB->getDevice();


        return $device["info"];
    }

    public function getSymptom($queryParam) {
        $incidentDB = new IncidentDB ();
        $incidentDB->setQueryParam($queryParam);
        $symptom = $incidentDB->getSymptom();


        return $symptom["info"];
    }

    public function getIncidentInfoALL() {
        $incidentDB = new IncidentDB ();

        $incidentDB->setQueryParam(array());
        $info = $incidentDB->getIncidentInfoALL();


        return $info;
    }

    public function getIncidentInfo($queryParam) {
        $incidentDB = new IncidentDB ();

        $incidentDB->setQueryParam($queryParam);
        $info = $incidentDB->getIncidentInfo();


        return $info;
        /*
          if ($info["status"] == "1"){

          }

          return  ; */
    }

    public function getIncidentInfoWithUid($queryParam) {
        $incidentDB = new IncidentDB ();

        $incidentDB->setQueryParam($queryParam);
        $info = $incidentDB->getIncidentInfoWithUid();
        return $info;
        /*
          if ($info["status"] == "1"){

          }

          return  ; */
    }

    public function getUserInfoWithCustom($queryParam, $fliterParam) {
        $incidentDB = new IncidentDB  ();
        
        foreach ($queryParam as $key => $value) {
            if (!isset($queryParam["$key"])) {
                $queryParam["$key"] = "";
            } elseif ($queryParam["$key"] == "all") {
                $queryParam["$key"] = "";
            }
        }


        $type = $queryParam["role"];
        unset($queryParam["role"]);


        $queryParam["type"] = $type;

        $incidentDB->setQueryParam($queryParam);
        $userInfo = $incidentDB->getUserInfoWithCustomQuery(0, 20);
        $userFliterInfo = array();
        
        $fliterParam1 = array() ; 

        if ($userInfo["status"] == "1") {

            for ($i = 0; $i < count($userInfo["userInfo"]); $i++) {
                foreach ($fliterParam as $key=>$value) {
                    $userFliterInfo[$i][$key] = $userInfo["userInfo"][$i][$value];
                }
            }
        }
        
        return $userFliterInfo;
    }

    public function updateIncident($queryParam) {
        $incidentDB = new IncidentDB();
        $calendar = new Calendar ();

        $color = $calendar->getColorSettingWithKey($queryParam["incidentStatus"], "color");
        $queryParam["color"] = $color;

        $incidentDB->setQueryParam($queryParam);
        $isUpdated = $incidentDB->updateIncident();
        /*
          if ($isUpdated["status"] != "1") {
          return $isUidValid;
          }
         */
        //$userStatus = $member->updateIncident();
        return $isUpdated;
    }

    public function getPatientId($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getPatientId();
    }

    public function isUserExists($queryParam) {
        $member = new Member();
        $member->setQueryParam($queryParam);
        return $member->isUserExisits();
    }

    public function updatePersonInformation($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->updatePersonInformation();
    }

    public function setPatientStatus($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->setPatientStatus();
    }

    public function addPhysicalCheck($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->addPhysicalCheck();
    }

    public function getPhysicalCheckInformation($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getPhysicalCheckInformation();
    }

    public function getPatientHistory($queryParam) {

        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);

        $patientHistory = $patientdb->getPatientHistory();

        for ($i = 0; $i < count($patientHistory["result"]); $i++) {
            $inPatientId = $patientHistory["result"]["$i"]["InPatientId"];
            $patientHistory["result"]["$i"]["details"] = "<a href='index.php?act=patient-inpatientinfo&inPatientId=" . $inPatientId . "'>Click here to get more details</a>";
        }


        return $patientHistory;
    }

    public function getHealthStatus() {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getHealthStatus();
    }

}
