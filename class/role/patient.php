<?php

class Patient extends VerifyWS implements verifyInterface {

    //=============Turn on Google Recapcha=====================
    const TURN_ON_GOOGLE_RECAPCHA = false;
    //=============Patient Registeration========================
    const patientFirstNameRegex = "/^[A-Za-z]{1,20}$/D";
    const patientLastNameRegex = "/^[A-Za-z0-9]{1,20}$/D";
    const HKIDCardNumberRegex = "/^[A-Za-z]{1}[0-9]{7}$/D";
    const phoneNumberRegex = "/^[0-9]{8}$/D";
    const addressRegex = "/^[A-Za-z0-9 ]+$/D";
    const emergencyContactPersonRegex = "/^[A-Za-z0-9]{1,20}$/D";
    const emergencyContactPhoneRegex = "/^[0-9]{8}$/D";
    const birthdayRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/D";
    const birthdayWithTimeRegex = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/D";
    const patientIdRegex = "/^[0-9]{1,20}$/D";

    const physicalCheckStartDate = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/D"; 
    const heightRegex = "/^[0-9]{1,20}.[0-9]{1,20}$/D"; 
    const weightRegex = "/^[0-9]{1,20}.[0-9]{1,20}$/D"; 
    const systolicRegex = "/^[0-9]{1,20}$/D"; 
    const diastolicRegex = "/^[0-9]{1,20}$/D"; 

    //=============Patient Registeration========================

    private $format = array(
        "patientRegister" => array(
            "patientFirstName" => self::patientFirstNameRegex,
            "patientLastName" => self::patientLastNameRegex,
            "HKID" => self::HKIDCardNumberRegex,
            "phoneNumber" => self::phoneNumberRegex,
            "address" => self::addressRegex,
            "emContactPerson" => self::emergencyContactPersonRegex,
            "emContactPhone" => self::emergencyContactPhoneRegex,
            "birthday" => self::birthdayRegex,
            "g-recaptcha-response" => GoogleRecapcha::GOOGLE_RECAPTCHA_REGEX,
        ),
        "getPatientId" => array(
            "HKID" => self::HKIDCardNumberRegex
        ),
        "patientInfo" => array(
            "patientId" => self::patientIdRegex,
        ),
        "updatePatientInfo" => array(
            "firstName" => self::patientFirstNameRegex,
            "lastName" => self::patientLastNameRegex,
            "phone" => self::phoneNumberRegex,
            "address" => self::addressRegex,
            "emContactPerson" => self::emergencyContactPersonRegex,
            "emContactPhone" => self::emergencyContactPhoneRegex,
            "dateOfBirth" => self::birthdayWithTimeRegex,
            "id" => self::patientIdRegex,
        ),
        "deletePatientId" => array(
            "id" => self::patientIdRegex,
        ),
        "addPhysicalCheck" => array(
            "patientId" => self::patientIdRegex,
            "startDate" => self::physicalCheckStartDate,
            "height" => self::heightRegex, 
            "weight" => self::weightRegex, 
            "systolic" => self::systolicRegex , 
            "diastloic" => self::diastolicRegex 
        ),
        "inpatientinfo" => array(
        ),
        "inportphycheck" => array(
        ),
        "conregister" => array(
        ),
        "reportcheck" => array(
        ),
        "editinfo" => array(
        ),
        "checkinvoice" => array(
        )
    );
    private $jsErrorMessage = array(
        "patientRegister" => array(
            "patientFirstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "patientLastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "HKID" => array("status" => "2003", "msg" => "Please input HKID card number A1234567 without () "),
            "phoneNumber" => array("status" => "2004", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2005", "msg" => "Please input address"),
            "emContactPerson" => array("status" => "2006", "msg" => "Please input Emergency Contact Person"),
            "emContactPhone" => array("status" => "2007", "msg" => "Please input Emergency Contact Person Phone Number with 8 digit"),
            "birthday" => array("status" => "2008", "msg" => "Please input birthday with yyyy-mm-dd"),
            "g-recaptcha-response" => array("status" => "2009", "msg" => "Please input tap the verify code"),
        ),
        "getPatientId" => array(
            "HKID" => array("status" => "2003", "msg" => "Please input HKID card number A1234567 without () "),
        ),
        "isBirthdayValid" => array(
            "isBirthdayValid" => array("status" => "2010", "msg" => "Birthday is bigger than NOW ! Please check it again \n Today is %s but your select date is %s"),
        ),
        "addPatient" => array(
            "isRegisterBefore" => array("status" => "2011", "msg" => "Patient is added before Please make it update \n link : %s \nor add the other patient"),
        ),
        "updatePatientInfo" => array(
            "firstName" => array("status" => "2001", "msg" => "Please input first name 1 to 20 words"),
            "lastName" => array("status" => "2002", "msg" => "Please input last name 1 to 20 words"),
            "phone" => array("status" => "2004", "msg" => "Please input 8 digit phone number"),
            "address" => array("status" => "2005", "msg" => "Please input address"),
            "emContactPerson" => array("status" => "2006", "msg" => "Please input Emergency Contact Person"),
            "emContactPhone" => array("status" => "2007", "msg" => "Please input Emergency Contact Person Phone Number with 8 digit"),
            "dateOfBirth" => array("status" => "2008", "msg" => "Please input birthday with yyyy-mm-dd"),
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "patientInfo" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
        ),
        "deletePatientId" => array(
            "id" => array("status" => "2003", "msg" => "Please input patientId with integer "),
            
        ),
        "addPhysicalCheck" => array(
            "patientId" => array("status" => "2003", "msg" => "Please input patientId with integer "),
            "startDate" => array("status" => "2010", "msg" => "Please input start date with yyyy-mm-dd"),
            "height" => array("status" => "2011", "msg" => "Please input height with decimal"), 
            "weight" => array("status" => "2012", "msg" => "Please input weight with decimal "),
            "systolic" => array("status" => "2013", "msg" => "Please input systolic with digit"),
            "diastloic" => array("status" => "2014", "msg" => "Please input diastolic with digit"), 
        ),
        
        "inpatientinfo" => array(
        ),
        "inportphycheck" => array(
        ),
        "conregister" => array(
        ),
        "reportcheck" => array(
        ),
        "editinfo" => array(
        ),
        "checkinvoice" => array(
        )
    );

    public function getFormat($key) {

        return $this->format["$key"];
    }

    public function setRtnMessage($func, $status, $message, $vsprintf) {
        $this->jsErrorMessage ["$func"]["$status"]["msg"] = vsprintf($message, $vsprintf);
    }

    public function getRtn($func, $status) {
        return $this->jsErrorMessage["$func"]["$status"];
    }

    public function getJsErrorMessage($key) {
        return $this->jsErrorMessage["$key"];
    }

    public function isTurnOnGoogleRecapcha() {
        return self::TURN_ON_GOOGLE_RECAPCHA;
    }

    public function isBirthDayBiggerThanNOW($date) {
        $now = strtotime("now");
        $targetDate = strtotime($date);


        if ($now > $targetDate) {
            return array("status" => true);
        }
        $vsprintf = array(
            date("Y-m-d"), $date
        );

        //return $this->getErrorMessage("isBirthdayValid", "isBirthdayValid",$vsprintf) ; 
        return $this->displayResult("isBirthdayValid", "isBirthdayValid", $vsprintf);
    }

    //========================SQL===========================
    public function addPatient($queryParam) {
        $patientdb = new Patientdb ();
        $patientdb->setQueryParam($queryParam);
        $patientdb->isPatientExisits();
    }

    public function getPatientInfo($queryParam) {
        $patientdb = new Patientdb ();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getPatienInformation();
    }

    public function getPatientId($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getPatientId();
    }

    public function isPatientIdExists($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->isPatientIdExists();
    }

    public function updatePersonInformation($queryParam) {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->updatePersonInformation();
    }
    
    public function setPatientStatus($queryParam){
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->setPatientStatus();
    }
    
    public function addPhysicalCheck($queryParam){
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->addPhysicalCheck();
    }
    
    public function getPhysicalCheckInformation($queryParam){
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getPhysicalCheckInformation();
    }
    
    
    public function getPatientHistory ($queryParam){

        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        
        $patientHistory = $patientdb->getPatientHistory();

        for ($i = 0 ; $i< count($patientHistory["result"]) ; $i++){
            $inPatientId = $patientHistory["result"]["$i"]["InPatientId"];
            $patientHistory["result"]["$i"]["details"] = "<a href='index.php?act=patient-inpatientinfo&inPatientId=".$inPatientId."'>Click here to get more details</a>";

        }
        
        
        return $patientHistory;
        
    }
    
    public function getHealthStatus () {
        $patientdb = new Patientdb();
        $patientdb->setQueryParam($queryParam);
        return $patientdb->getHealthStatus();
    }

}
