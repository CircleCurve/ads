<?php
if (!defined('BEEN_INCLUDE') || !is_object($myerror))
    exit('Welcome to The Matrix');

$admin_right = '8001'; //the code of admin right for this page

if (!$admin_acc->check_login() || !$admin_acc->check_right($admin_right)) {
    require_once(modelExist('system-request-login'));
}
//include 'plugin/calendar/eventHandler.php';


$calendar_ajax_token = md5("calendar" . $form_prefixkey . uniqid(rand() . $form_postfixkey, TRUE));
setSession('calendar_ajax_token', $calendar_ajax_token);

function checkShare($status, $result) {

    if (($result == "1") && ($status == "all"))
        return 'document.getElementById("choiceShare").checked= true;';
    elseif (($result == "0") && ($status == "private") || ($result == ' '))
        return 'document.getElementById("choiceShare1").checked= true;';


    return "";
}
?>


<link href='testCalendarInterface/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='testCalendarInterface/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />

<link rel="stylesheet" type="text/css" href="testCalendarInterface/css/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="testCalendarInterface/ColorPicker/js/jquery.miniColors.css" />

<script src='js/jquery.js'></script>

<script src='testCalendarInterface/js/jquery-ui-1.10.2.custom.min.js'></script>
<script type="text/javascript" src="testCalendarInterface/js/jquery.qtip-1.0.0-rc3.min.js"></script>


<script src='testCalendarInterface/fullcalendar/fullcalendar.min.js'></script>
<script src='testCalendarInterface/ColorPicker/js/jquery.miniColors.js'></script>


<script src="testCalendarInterface/ui/jquery.ui.position.js"></script>
<script src="testCalendarInterface/ui/jquery.ui.resizable.js"></script>
<script src="testCalendarInterface/ui/jquery.ui.button.js"></script>
<script src="testCalendarInterface/ui/jquery.ui.dialog.js"></script>



<!-- ajaxCall = pass JSON type data to php file , deal with it -->
<script type='text/javascript' src='plugin/calendar/ajaxCallCalendar.js'></script>
<script src='js/gcal.js'></script>
<script type="text/javascript">
    jq162 = jQuery.noConflict(true);
    var event_id;
    shareMember = new Array();
    var choiceShare;
    sharedEditMember = new Array();

</script>



<script>


    var result_index = new Array();
    var result = function() {
        var result = $.ajax({url: "plugin/calendar/eventHandler.php", async: false, success: function(result) {
                //    console.log(result);
                return result;
            }
        });
        return result;
    }
    jq162(document).ready(function() {
        jq162('#dialog').hide();
        jq162('#reminder').hide();
        jq162('#dialog3').hide();

        var calendar = jq162('#calendar').fullCalendar({
            theme: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'

            },
            selectable: true,
            dragOpacity: 0.5,
            //selectHelper: true,
            weekNumbers: true,
            weekMode: 'variable',
            editable: true,
            events: 'plugin/calendar/FetchData.php',
            eventSources: [
                //jq162.fullCalendar.gcalFeed('https://www.google.com/calendar/feeds/patrickngnet%40gmail.com/public/basic'),
                //jq162.fullCalendar.gcalFeed('https://www.google.com/calendar/feeds/hong_kong__zh_tw%40holiday.calendar.google.com/public/basic')
				//zh-tw.hong_kong#holiday@group.v.calendar.google.com, AIzaSyAFHROnSTwFBCseKwh741Z9fJKCrpvGrws
                //jq162.fullCalendar.gcalFeed('https://www.google.com/calendar/feeds/zh-tw.hong_kong#holiday@group.v.calendar.google.com/public/basic')
                //zh-tw.hong_kong#holiday@group.v.calendar.google.com
            ],
            eventRender: function(event, element) {

                element.attr('title', event.description);
            },
            select: function(start, end, allDay) {
                jq162('#title').val("");
                jq162('#color').val("#378006");
<?= checkShare("private", "0"); ?>
                jq162("#reminder_event_main").val("true");
                event_id = calendar.id;
                var action = 1;
                var displayStart = jq162.fullCalendar.formatDate(start, "M月d日");
                var start = jq162.fullCalendar.formatDate(start, "yyyy-MM-dd H:mm");
                var end = jq162.fullCalendar.formatDate(end, "yyyy-MM-dd H:mm");
                jq162('#date').text(displayStart);

                promptBox(calendar.id, start, end, allDay, calendar, action);

            },
            eventClick: function(calEvent, jsEvent, view) {
                var data = [{status: "all", id: calEvent.id}];
                url = 'plugin/calendar/allFunction.php';
                event_id = calEvent.id;
                checkShare(calEvent.public_own);

                var action = 2;
                var start = jq162.fullCalendar.formatDate(calEvent.start, "yyyy-MM-dd H:mm");
                var displayStart = jq162.fullCalendar.formatDate(calEvent.start, "M月d日");
                var end = jq162.fullCalendar.formatDate(calEvent.end, "yyyy-MM-dd H:mm");

                jq162('#date').text(displayStart);
                jq162('#title').val(calEvent.title);
                var color = jq162('#color').val(calEvent.color);
                color = color.val().toString();
                jq162('#color').miniColors('value', color);
                jq162("#reminder_event_main").val("reminder_" + event_id);
                document.getElementById("reminder_event_main").checked = (calEvent.reminder == "prompt") ? true : false;

                promptBox(calEvent.id, start, end, calEvent.allDay, calendar, action);
                //     alert (title1); 
            },
            eventDrop: function(event, dayDelta, minuteDelta, allDay) {
                var color = jq162('#color');
                color = jq162('#color').val(event.color);
                color = color.val().toString();
                var id = event.id.toString();
                var start = jq162.fullCalendar.formatDate(event.start, "yyyy-MM-dd H:mm");
                var end;
                if (event.end === null)
                    end = start;
                else
                    end = jq162.fullCalendar.formatDate(event.end, "yyyy-MM-dd H:mm");
                url = 'plugin/calendar/SizeDrop.php';
                var data = new Array();
                data = [{id: id, start: start, end: end, allDay: allDay, 'color': color, 'protectKey': "<?= $calendar_ajax_token; ?>"}];
                callAjax('POST', 'JSON', url, data, calendar);
                //   alert (url);
                // added this line
            },
            eventResize: function(event, dayDelta, minuteDelta, revertFunc) {
                var color = jq162('#color');
                var id = event.id.toString();
                var start = jq162.fullCalendar.formatDate(event.start, "yyyy-MM-dd H:mm");
                var end;
                if (event.end === null)
                    end = start;
                else
                    end = jq162.fullCalendar.formatDate(event.end, "yyyy-MM-dd H:mm");
                var data = new Array();

                color = jq162('#color').val(event.color);   //getWhat color
                color = color.val().toString();
                url = 'plugin/calendar/SizeDrop.php';
                data = [{id: id, start: start, end: end, allDay: event.allDay, color: color, 'protectKey': "<?= $calendar_ajax_token; ?>"}];
                callAjax('POST', 'JSON', url, data, calendar);
                //   alert (url);
            },
            loading: function(bool) {
                if (bool)
                    ;
                else
                    jq162('#loading').hide();
            }
        });

    }
    );

    //For enter detection 
    jq162.extend(jq162.ui.dialog.prototype.options, {
        create: function() {
            var $this = jq162(this);

            // focus first button and bind enter to it
            $this.parent().find('.ui-dialog-buttonpane button:first').focus();
            $this.keypress(function(e) {
                if (e.keyCode == 13) {
                    //key 13 = enter 
                    $this.parent().find('.ui-dialog-buttonpane button:first').click();
                    return false;
                }
            });
        }
    });
    function addEvent(title, start, end, allDay, color, calendar) {
        if (title) {
            calendar.fullCalendar('renderEvent',
                    {
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay,
                        color: color
                    },
            false // make the event "stick"
                    );

            var data = new Array();
            // console.log(sharedEditMember)  ; 
            data = [{'title': title, 'start': start, 'end': end, 'allDay': allDay, 'color': color, 'shareMember': shareMember, 'shareEditMember': sharedEditMember, 'choiceShare': choiceShare, 'protectKey': "<?= $calendar_ajax_token; ?>"}];

            url = "plugin/calendar/Insert.php";
            callAjax('POST', 'JSON', url, data, calendar);

        }
        calendar.fullCalendar('unselect');
    }
    function updateEvent(title, id, color, calendar) {
        var data = new Array();
        data = [{'title': title, 'id': id, 'color': color, 'shareMember': shareMember, 'shareEditMember': sharedEditMember, 'choiceShare': choiceShare, 'protectKey': "<?= $calendar_ajax_token; ?>"}];
        url = "plugin/calendar/Update.php";
        callAjax('POST', 'JSON', url, data, calendar);
        // calendar.fullCalendar('refetchEvents');            

        //  calendar.fullCalendar('refetchEvents');  
        calendar.fullCalendar('unselect');
    }
    function addShareMember() {

    }

    function promptBox(id, start, end, allDay, calendar, action) {
        var title = jq162('#title');
        var color = jq162('#color');
        jq162('#dialog').dialog({
            autoOpen: true,
            height: 330,
            width: '340px',
            modal: true,
            buttons: {
                "Ok": function() {
                    title = title.val().toString();
                    color = color.val().toString();
                    switch (action) {
                        case 1:
                            addEvent(title, start, end, allDay, color, calendar, id);
                            addShareMember();
                            var myCheckboxes = getAllCheckBoxWithValue();
                            //myCheckboxes[1] .match("reminder_")!=null
                            if (myCheckboxes[1] == "true") {
                                setReminderReadable({"action": "addReminder", "type": "prompt"});
                            }
                            break;
                        case 2:
                            updateEvent(title, id, color, calendar);
                            var myCheckboxes = getAllCheckBoxWithValue(); 
                            if (myCheckboxes[1]!=undefined){
                                if (myCheckboxes[1] .match("reminder_")!=null) {
                                    setReminderReadable({"action": "addReminder", "event_id": event_id,"type":"prompt"});
                                }
                            }else {
                                    setReminderReadable({"action": "addReminder", "event_id": event_id,"type":"#"});
                            }
                           break;
                    }

                    jq162(this).dialog("close");
                },
                "Delete": function() {
                    var data = new Array();
                    data = [{'id': id, 'protectKey': "<?= $calendar_ajax_token; ?>"}];
                    callAjax('POST', 'JSON', 'plugin/calendar/delete.php', data, calendar);
                    jq162(this).dialog("close");
                },
                Cancel: function() {
                    jq162(this).dialog("close");
                }
            }
        });
        $("#type_all,#render_page").html("");

    }
    function moreSetting() {

        var title = jq162('#title').val().toString();
        var color = jq162('#color').val().toString();
        var id;
        if (event_id != undefined)
            id = event_id;
        else
            id = 0;
        //  alert (title+" "+color+ "  ");
        location.href = ("?act=testCalendar-event_profile&id=" + id + "&user_id=" + "<?= $_SESSION[SESSION_SITENAME]["login_id"] ?>");
    }

    jq162(function() {
        jq162(".color-picker").miniColors({
            letterCase: 'uppercase',
        });
    });


    function hideGoogle() {
        if (jq162('#hideGoogle').val() === "隱藏google日曆") {
            jq162('#calendar').fullCalendar('removeEventSource', 'https://www.google.com/calendar/feeds/hong_kong__zh_tw%40holiday.calendar.google.com/public/basic')
            jq162('#hideGoogle').val("顯示google日曆");
        }
        else {
            jq162('#calendar').fullCalendar('addEventSource', 'https://www.google.com/calendar/feeds/hong_kong__zh_tw%40holiday.calendar.google.com/public/basic');
            jq162('#hideGoogle').val("隱藏google日曆");
        }
    }
    function share() {

        jq162("#dialog3").dialog({width: '400px',
            modal: true,
            open: function() {
                var id;
                if (event_id != undefined)
                    id = event_id;
                else
                    id = 0;
                var data = [{'event_id': id, 'user_id': "<?= $_SESSION[SESSION_SITENAME]["login_id"] ?>", 'action': "share"}];

                var result = callAjax('POST', 'JSON', 'plugin/calendar/functionList.php', data, jq162('#calendar'));

                document.getElementById("dialog3").innerHTML = result.responseText;
                var data1 = new Array();
                data1 = [{'event_id': id, 'user_id': "<?= $_SESSION[SESSION_SITENAME]["login_id"] ?>", 'action': "ReturnUser"}];
                shareMember = callAjax('POST', 'JSON', 'plugin/calendar/functionList.php', data1, jq162('#calendar')).responseText;

                shareMember = JSON.parse(shareMember);


            }
        });
    }
    function sharebtm(user_id, username) {

        shareMember.push(user_id);

        $('#share').val(shareMember);

        $("#" + user_id).replaceWith(' ');

        $("#shared").append('<tr align=center id=' + user_id + '><td>' + username + '</td><td bgcolor=#C9BE62><a href="javascript:unsharebtm(\'' + user_id + '\',\'' + username + '\')">取消分享</a></td><td><input type="checkbox" name=sharedEdit value=' + user_id + ' onClick=checkBoxDetection(' + user_id + ')></td></tr>');
    }
    function unsharebtm(user_id, username) {

        shareMember.splice(Array.indexOf(user_id), 1);
        sharedEditMember.splice(Array.indexOf(user_id), 1);
        $('#share').val(shareMember);
        $('#shareEdit').val(sharedEditMember);
        $("#" + user_id).replaceWith(' ');
        $("#preshare").append('<tr align=center id=' + user_id + '><td>' + username + '</td><td bgcolor=#C9BE62><a href="javascript:sharebtm(\'' + user_id + '\',\'' + username + '\')">分享</a></td></tr>');

    }
    function checkBoxDetection(user_id) {
        if (($('input:checkbox[name=sharedEdit]').is(':checked'))) {
            $('input:checkbox:checked[name="sharedEdit"]').each(function(i) {
                sharedEditMember[i] = this.value;
            });
        }
        else {

            sharedEditMember.splice(Array.indexOf(user_id), 1);
        }
        //$('#shareEdit').val(sharedEditMember);
    }
    function checkShare(result) {
        if (result == 1) {
            document.getElementById("choiceShare").checked = true;
            choiceShare = 1;
        }
        if ((result == 0) || (result == ' ')) {
            document.getElementById("choiceShare1").checked = true;
            choiceShare = 0;
        }

    }
</script>


<style>

    body {
        /*margin-top: 40px;
        text-align: center;
        font-size: 14px;*/
        font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    }


    #loading {
        width: 900px;
        margin: 0 auto;
        border: 10px solid black;
    }

    #calendar {
        width: 900px;
        margin: 0 auto;

    }


</style>



<input type="submit" id="hideGoogle" value="隱藏google日曆" onClick="javascript:hideGoogle()" style="left: 385px; margin-bottom: 10px;">

<div id='calendar'></div>

<div id="dialog" title="活動">
    <table width="310">
        <tr valign=top align=left><td colspan=2 align=right><b><a href="javascript:moreSetting()">更多設定 >></a></b></td></tr>
        <tr valign=top height=30 align=left><td width=80>活動日期:</td><td id="date"></td></tr>
        <tr valign=top height=30 align=left><td>活動標題:</td>
            <td><input type="text" name="title" id="title" class="text ui-widget-content ui-corner-all" /></td></tr>
        <tr valign=top height=30 align=left><td width=80>活動顏色:</td>
            <td><input type="text" name="color1" class="color-picker"  size="11" autocomplete="on" id='color' value='#378006'/></td></tr>

        <tr valign=top height=30 align=left>
            <td width=80>活動分享:</td>
            <td><input type="radio" id ="choiceShare" name="choiceShare" value="1" />公開
                <input type="radio" id="choiceShare1" name="choiceShare" value="0"/>私人
                <a href="javascript:void(0);" onclick="share()">額外分享</a>
            </td>
        </tr>
        <tr valign=top height=30 align=left><td width=80>活動提醒:</td>
            <td><input type="checkbox" id="reminder_event_main" value="true"></td>
        </tr>

    </table>

</div>

<div id="dialog3" title="分享活動到..."> </div>
<input type="hidden"  name="shareEdit" id='shareEdit'>













