<?php
if (!defined('BEEN_INCLUDE') || !is_object($myerror))
    exit('Welcome to The Matrix');

$admin_right = '8001'; //the code of admin right for this page

if (!$admin_acc->check_login() || !$admin_acc->check_right($admin_right)) {
    require_once(modelExist('system-request-login'));
}
$event_id = $_GET['id'];
$user_id = $_GET['user_id'];
$event = new calendar_EventHandler ();

$user_id = $event->calendar_geUserId($user_id);
if (empty($user_id[0])) {
    $myerror->message('<font color=\'red\'><b>不明非法提交<br/>Error code :-3109</b></font>', 'INDEX', '', 'footer');
    require(modelExist('error'));
}



$list = $event->calendar_getSpecificFetchEvent($event_id);
$repeatList = $event->calendar_getSpecificRepeatEvent($event_id);
$invited_members = $event->calendar_getShareMemberEventID($event_id);
$javascript_members = $invited_members;

$allUser = $event->calendar_getAllUser($user_id[0]["uid"]);
$editor = $event->calendar_getShareMemberAllowEditor($event_id);

$data = array();
$preinvite_members = array();
if (empty($invited_members)) {
    $preinvite_members = $event->calendar_getAllUser($user_id[0]["uid"]);
} else {
    $allUser = checkPreInvitiedMember($allUser, "uid");
    $invited_members = checkPreInvitiedMember($invited_members, "user_id");
    $i = 0;
    foreach ($javascript_members as $value)
        foreach ($value as $value1) {
            $data[$i] = $javascript_members[$i]["user_id"];
            $i++;
        }
    $javascript_invited_members = $data;
    $allUser1 = packUserIDArray($allUser);
    $invited_members1 = packUserIDArray($invited_members);

    $preinvite_members = packToStandardMember($allUser, array_diff($allUser1, $invited_members1));

    $invited_members = packToStandardArray($invited_members, $editor);

    if (empty($javascript_invited_members))
        $javascript_invited_members = array();
}





if (!empty($list)) {
    $result = $event->calendar_getEvent($list, "");
    $action = "update";
} else {
    if ($event_id == 0) {
        $result1 = $event->calendar_getAllRowName("calendar_newevent");
        foreach ($result1 as $value)
            foreach ($value as $value1) {
                $result[$value1] = ' ';
            }
        $action = "add";
    } else {
        $myerror->message('<font color=\'red\'><b>不明非法提交<br/>Error code :-3109</b></font>', 'INDEX', '', 'footer');
        require(modelExist('error'));
    }
}

$editorID = $event->calendar_getShareMemberAllowEdit($event_id, $user_id[0]["uid"]);
$link = array();
$wholeLink = array();
$link = explode(" ", $result['description']);


for ($i = 0; $i < count($link); $i++) {
    if ((strpos($link[$i], 'http://') !== false) || (strpos($link[$i], 'https://') !== false)) {
        $link[$i] = substr($link[$i], (strpos($link[$i], "http://")));
        $wholeLink[$i] = '<a href="' . $link[$i] . '">' . $link[$i] . '</a>';
    }
}


if (($user_id[0]["uid"] == $result['user_id']) || ($event_id == "0") || ($editorID[0]["editEvent"] == 1))
    $disabled = " ";
else
    $disabled = "disabled";

if (!empty($repeatList)) {
    $repeatResult = $event->calendar_getEvent($repeatList, "");
    $repeated = $event->calendar_getEvent($event->calendar_getNumberofRepeatEvent($event_id), "str");
//print_r ($result);  //debug test
}
//time cal
else {
    $repeatResult["repeated_times"] = '';
    $repeated["repeated"] = '';
    $repeatResult["repeated_type"] = '';
}


/*  print_r_pre($result);
  if(getSession("loadValue")=="loadValue"){
  $result =    passValuetoPOST(getSession("passData")) ;
  $repeatResult = passValuetoPOSTRepeat(getSession("passData")) ;
  $repeated = passValuetoPOSTRepeated(getSession("passData")) ;
  unsetSession("passData");
  unsetSession("loadValue");
  }
  print_r_pre($result);
  print_r_pre($repeatResult);
  print_r_pre($repeated);
  print_r_pre(getSession("passData")); */

//  print_r_pre(getSession("passData"));


function checkPreInvitiedMember($user, $delimiter) {
    $event = new calendar_EventHandler ();
    $data = array();
    $i = 0;
    foreach ($user as $value) {
        foreach ($value as $key => $value1) {
            if (($key == $delimiter) && ($delimiter == "uid")) {
                $data[$i] = $value1;
                $data[$i + 1] = $value["nickname"];
            } else {
                if (($delimiter == "user_id")) {
                    $data[$i] = $value1;
                    $str = $event->calendar_getUserName($value1);

                    $data[$i + 1] = $str[0]["nickname"];
                }
            }
        }
        $i+=2;
    }


    return $data;
}

function packUserIDArray($in_array) {
    $user_id_array = array();
    for ($i = 0; $i < count($in_array); $i++) {
        if ($i % 2 == 0) {
            $user_id_array[$i] = $in_array[$i];
        }
    }

    return $user_id_array;
}

function packToStandardArray($in_array, $editor) {


    $i = 0;
    $data = array();
    //  $result = $in_array ; 


    $j = 1;
    foreach ($in_array as $value) {
        if ($i % 2 == "0") {
            $data[$i]["uid"] = $value;
        } else {
            $data[$i - 1]["nickname"] = $value;
            $data[$i - 1]["editEvent"] = $editor[($i - $j)]["editEvent"];
            $j++;
            //echo $value  ;
        }
        $i++;
    }

    return $data;
}

function packToStandardMember($in_array, $determine) {

    $result = array();
    for ($i = 0; $i < count($in_array); $i++)
        if ($i % 2 == 0) {
            $data[$i] = $in_array[$i];
        }
    $preinvited_members = array_diff($data, $determine);
    //print_r_pre($in_array);
    for ($i = 0; $i < count($in_array); $i++) {
        for ($j = $i; $j < count($in_array) - 1; $j++) {
            if (isset($preinvited_members[$j])) {
                if ($in_array[$j] == $preinvited_members[$j]) {
                    $i = $j + 1;
                    break;
                }
            } else {
                $result[$j]["uid"] = $in_array[$j];
                $result[$j]["nickname"] = $in_array[$j + 1];
                $i = $j++;
            }
        }
    }
    // print_r_pre($preinvited_members);


    return $result;
}

function checkAllDayorRepeated($result) {
    if (($result == '') || ($result == '0'))
        return '';
    else
        return "checked";
}

function isSelectWeekorDay($value, $repeated_type) {
    return ($value == $repeated_type) ? ("selected") : ('');
}

function checkReminder($value, $reminder) {

    return ($value == $reminder) ? ("selected") : ('');
}

function isSelectValue($date, $value) {
    if ($date === $value)
        return "selected";
    // else 
    //      return "";
}

function checkShare($status, $result) {

    if (($result == "1") && ($status == "all"))
        return 'checked';
    elseif (($result == "0") && ($status == "private") || ($result == ' '))
        return "checked";


    return '';
}

function explodeDate($date) {
    $wholedate = explode(" ", $date);
    $day = explode("-", $wholedate[0]);
    $time = explode(":", $wholedate[1]);
    return $wholedate;
}

$event_time_arr = timeCal();

function timeCal() {
    $event_time_arr = array();
    $h = 0;
    $hour1 = 0;
    $hour2 = 0;
    $am_pm = 0;

    for ($i = 0; $i <= 46; $i++) {

        $hour1 = ($h > 12) ? $h - 12 : $h;
        $hour1 = ($hour1 == 0) ? 12 : $hour1;
        $hour1 = ($hour1 < 10) ? "0" . @$hour1 : $hour1;
        $hour2 = $h;
        $hour2 = ($hour2 < 10) ? "0" . @$hour2 : $hour2;
        $am_pm = ($h > 11) ? " pm" : " am";

        if ($i % 2 == 0) {
            $event_time_arr[$i] = array(@$hour1 . ":00" . $am_pm, $hour2 . ":00:00");  //drop down list 
            $event_time_arr[$i + 1] = array(@$hour1 . ":30" . $am_pm, $hour2 . ":30:00"); //drop 
            $h+=1;
        }
    }

    return $event_time_arr;
}

//print_r ($evnet_time_arr);
//time cal
?>







<style type="text/css">
    #box {
        width: 400px;
        height:100px ; 
        margin: 0 auto;
        overflow: auto;
        border: 1px solid #0f0;
        padding: 2px;
        text-align: justify;
        background: transparent;
    }
</style>
<script src='js/jquery.js'></script>
<script src='testCalendarInterface/js/jquery-ui-1.10.2.custom.min.js'></script>
<script src="js/jquery.ui.datepicker.js"></script>

<script src="testCalendarInterface/ui/jquery.ui.position.js"></script>
<script src="testCalendarInterface/ui/jquery.ui.resizable.js"></script>
<script src="testCalendarInterface/ui/jquery.ui.button.js"></script>
<script src="testCalendarInterface/ui/jquery.ui.dialog.js"></script>

<script type="text/javascript">
    jq162 = jQuery.noConflict(true);

    shareMember = <?
if (!empty($javascript_invited_members))
    echo json_encode($javascript_invited_members);
else {
    ?>  new Array();
<? } ?>

    sharedEditMember = new Array();


</script>


<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog3').hide();
        $('#event_id').val("<?= $event_id ?>");
        $('#user_id').val("<?= $user_id[0]["uid"] ?>");
        $('#userName').val("<?= $_GET['user_id']; ?>");
        $('#action').val("<?= $action ?>");
        $('#title').val("<?= $result['title'] ?>");
        $('#start_date').val("<? $start = explodeDate($result ['start']);
echo $start[0];
?>");
        $('#end_date').val("<? $end = explodeDate($result ['end']);
echo $end[0];
?>")
        $('#allDay').val("<?= $result ['allDay'] ?>")
        $('#color').val("<?= $result ['color'] ?>")
        $('#repeat_times').val("<?= $repeatResult ['repeated_times'] ?>")
        $('#number').val("<?= $result ['reminder_number'] ?>")
        $('#share').val(shareMember);
        $('#shareEdit').val(sharedEditMember);
    });
    $(function() {
        jq162("#start_date").datepicker({dateFormat: 'yy-mm-dd'});
        jq162("#end_date").datepicker({dateFormat: 'yy-mm-dd'});
    });
    function dropDownList_time(time) {
        document.write("<select name=" + time + " <?= $disabled ?>>");
        document.write('<option value="#">--請選擇--</option>');
<? for ($i = 0; $i < count($event_time_arr); $i++) { ?>
            if (time === "start_time")
                document.write('<option value =<?= $event_time_arr[$i][1] . ' ' . isSelectValue($start[1], $event_time_arr[$i][1]) . '>' . $event_time_arr[$i][0]; ?>');
            else
                document.write('<option value =<?= $event_time_arr[$i][1] . ' ' . isSelectValue($end[1], $event_time_arr[$i][1]) . '>' . $event_time_arr[$i][0]; ?>');

<? } ?>
        document.write('</select>');

    }
    function calendar_reminder(action) {
        switch (action) {
            case "reminder":

                document.write('<select name="reminder" <?= $disabled ?>>');
                document.write('<option value="#">--請選擇--</option>');
                document.write('<option value="none" <?= " " . checkReminder("none", (isset($result["reminder"])) ? ($result["reminder"]) : ('')) ?>>無</option>');
                document.write('<option value="prompt" <?= " " . checkReminder("prompt", (isset($result["reminder"])) ? ($result["reminder"]) : ('')) ?>>彈出式視窗</option>');
                document.write('</select>');
                break;
            case "timer":

                document.write('<select name="timer" <?= $disabled ?>>');
                document.write('<option value="#">--請選擇--</option>');
                document.write('<option value="date" <?= " " . checkReminder("date", (isset($result["timer"])) ? ($result["timer"]) : ('')) ?>>日</option>');

                document.write('</select>');
                break;

            case "repeat_types":
                document.write('<select name="repeat_types" <?= $disabled ?>>');
                document.write('<option value="#">--請選擇--</option>');
                document.write('<option value="day" <?= " " . isSelectWeekorDay("day", $repeatResult["repeated_type"]) ?>>天</option>');
                document.write('<option value="week" <?= " " . isSelectWeekorDay("week", $repeatResult["repeated_type"]) ?>>星期</option>');
                document.write('</select>');
                break;

        }
    }
    function share() {
        jq162("#dialog3").dialog({width: '400px'});
    }
    function sharebtm(user_id, username) {

        shareMember.push(user_id);
        $('#share').val(shareMember);

        $("#" + user_id).replaceWith(' ');

        $("#shared").append('<tr align=center id=' + user_id + '><td>' + username + '</td><td bgcolor=#C9BE62><a href="javascript:unsharebtm(\'' + user_id + '\',\'' + username + '\')">取消分享</a></td><td><input type="checkbox" name=sharedEdit value=' + user_id + ' onClick=checkBoxDetection(' + user_id + ')></td></tr>');




    }
    function unsharebtm(user_id, username) {

        shareMember.splice(Array.indexOf(user_id), 1);
        sharedEditMember.splice(Array.indexOf(user_id), 1);
        $('#share').val(shareMember);
        $('#shareEdit').val(sharedEditMember);
        $("#" + user_id).replaceWith(' ');
        $("#preshare").append('<tr align=center id=' + user_id + '><td>' + username + '</td><td bgcolor=#C9BE62><a href="javascript:sharebtm(\'' + user_id + '\',\'' + username + '\')">分享</a></td></tr>');
    }
    function checkBoxDetection(user_id) {
        if (($('input:checkbox[name=sharedEdit]').is(':checked'))) {
            $('input:checkbox:checked[name="sharedEdit"]').each(function(i) {
                sharedEditMember[i] = this.value;
            });
        }
        else {

            sharedEditMember.splice(Array.indexOf(user_id), 1);
        }
        $('#shareEdit').val(sharedEditMember);
    }

</script>


<form action="?act=testCalendar-formData-dealData" method="POST">
    <table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td class='headertitle' align="center"></td>
        </tr>

        <tr>
            <td align="center">	
                <fieldset>
                    <legend class='legend'>基本資料</legend>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign=top height=50 >
                            <td width=100 colspan=2><font color=red>這部分是必填的資料 </font></td>  		
                        </tr>	
                        <input type="hidden"  name="event_id"  id="event_id">
                        <input type="hidden"  name="action" id ="action">
                        <input type="hidden"  name="user_id" id ="user_id">
                        <input type="hidden"  name="disabled" id ="disabled">
                        <input type="hidden"  name="userName" id ="userName">
                        <input type="hidden"  name="shareEdit" id='shareEdit'>

                        <tr valign=top height=50 >
                            <td width=100 >活動標題:</td>  
                            <td  align='left'><input type="text" name="title" id="title" <?= $disabled ?>/></td>			
                        </tr>	
                        <tr valign=top>
                            <td  align='left'>活動時間:</td>
                            <td  align='left'>
                                <table>
                                    <tr valign=top height=50>
                                        <td>從&nbsp;</td><td><input type="text" name="start" id="start_date"  <?= $disabled ?>/></td><td>&nbsp的&nbsp;</td>
                                        <td><script> dropDownList_time("start_time");</script> </td>
                                    </tr>
                                    <tr valign=top height=50>
                                        <td>到&nbsp;</td><td><input type="text" name="end" id="end_date"  <?= $disabled ?>/></td><td>&nbsp的&nbsp;</td>
                                        <td><script> dropDownList_time("end_time");</script> </td>
                                    </tr>
                                </table>

                            </td>			
                        </tr>
                        <tr valign=top height=30 >
                            <td  align='left'>&nbsp;</td>
                            <td  align='left'>
                                <table><tr valign=top><td><input type="checkbox" name="choice[]" id="choice" value="allDay" <?= checkAllDayorRepeated($result["allDay"]) ?> <?= $disabled ?>/>全天&nbsp;
                                            <input type="checkbox" name="choice[]" id="choice"  value="repeat" <?= checkAllDayorRepeated($repeated["repeated"]) ?> <?= $disabled ?>/>重覆&nbsp;</td>
                                        <td width=100 align=right>重覆次數: </td><td><input type="text" name="repeat" id="repeat_times"></td><td width=100 align=right>重覆類型: </td>
                                        <td><script>calendar_reminder("repeat_types")</script></td></tr></table>
                            </td>  		
                        </tr>	

                    </table>			

                </fieldset>	
            </td>	
        </tr>
        <tr>
            <td align="left">	
                <fieldset>  
                    <legend class='legend'>詳細資料</legend>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign=top height=50 >
                        <p  align='left'>活動說明: </p>
<? //if ()         ?>
                        <p  align='left'><textarea style="width: 532px; height: 80px;" name="description" <?= $disabled ?>><?= ($result ['description']) ? ($result ['description']) : (''); ?></textarea>


                            </tr>

                        <tr valign=top>
                            <td  align='left'>活動連結:</td>
                            <td  align='left'>
                                <?
                                foreach ($wholeLink as $linkArray) {
                                    echo $linkArray . "||";
                                }
                                ?>

                            </td>	
                        </tr>  

                        <tr valign=top>
                            <td  align='left'>活動提醒:</td>
                            <td  align='left'>
                                <table>
                                    <tr valign=top height=50>
                                        <td><script> calendar_reminder("reminder")</script> </td>
                                    </tr>
                                    <!--
                                   <tr>
                                    <td>&nbsp;在活動進行前的<input type="text" id="number" name="number" <?= $disabled ?>></td>
                                    <td><script> calendar_reminder("timer")</script> </td>
                                    </tr>-->
                                </table>

                            </td>	
                        </tr>    

                        <tr valign=top>

                            <td  align='left'>分享性質:</td>
                            <td  align='left'>
                                <table>
                                    <tr valign=top height=50>
                                        <td><input type="radio" name="choiceShare" value="1" <?= checkShare("all", $result["public_own"]) ?> <?= $disabled ?>>所有人</td>
                                        <td><input type="radio" name="choiceShare" value="0" <?= checkShare("private", ($result["public_own"])) ?> <?= $disabled ?>>私人</td>
                                        <td><a href="javascript:share()">額外分享</td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                    <tr height=50 >
                    <input type="hidden"  name="shareMember" id="share">
                    <td align=center><input type="submit" value="提交" name="submit" id="submit" <?= $disabled ?>>
                        <input type="submit" value="取消" name="cancel" id="cancel">
                    </td>  

        </tr>        
        </fieldset>
        </td>
        </tr>
    </table>                               
</form>


<div id="dialog3" title="分享活動到...">	
    <?
    echo '<table border=1 width=100%><tr valign=top>';
//left window
    echo '<td width=50% bgcolor=#fff8c6>';
    echo '<table border=1 id=preshare width=100%>';

    foreach ($preinvite_members as $value)
// foreach ($allUser as $value)
        echo '<tr align=center id=' . $value['uid'] . '><td>' . $value['nickname'] . '</td><td bgcolor=#C9BE62><a href="javascript:sharebtm(\'' . $value['uid'] . '\',\'' . $value['nickname'] . '\')">分享</a></td></tr>';


    echo '</table>';
    echo '</td>';

//right window
    echo '<td width=50% bgcolor=white>';
    echo '<table border=1 id=shared width=100%>';

    foreach ($invited_members as &$value1)
//         foreach ($value1 as &$value)
        echo '<tr align=center id=' . $value1['uid'] . '><td>' . $value1['nickname'] . '</td><td bgcolor=#C9BE62><a href="javascript:unsharebtm(\'' . $value1['uid'] . '\',\'' . $value1['nickname'] . '\')">取消分享</a></td>
                             <td><input type="checkbox" name=sharedEdit value=' . $value1['uid'] . ' onClick=checkBoxDetection(' . $value1['uid'] . ')  ' . checkAllDayorRepeated($value1["editEvent"]) . '></td></tr>';


    echo '</table>';
    echo '</td>';

    echo '</tr></table>';
    ?>
</div>

</body>
</html>

