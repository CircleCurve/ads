<?php

//include('../../../include/init.php');

if (!defined('BEEN_INCLUDE') || !is_object($myerror))
    exit('Welcome to The Matrix');

$admin_right = '6001'; //the code of admin right for this page

if (!$admin_acc->check_login()) {
    require_once(modelExist('system-request-login'));
}

function packPOSTvalue($POST) {
    foreach ($POST as $key => $value)
        echo $key . "=" . $value;
}

function packToArray($arg_list) {
    // print_r(get_defined_vars());
    $arg_list = explode(",", $arg_list);
    return $arg_list;
}

function explodeDate($date) {
    $wholedate = explode(" ", $date);
    $day = explode("-", $wholedate[0]);
    $time = explode(":", $wholedate[1]);
    return $wholedate;
}

function implodeDate($date, $time) {
    $array = array($date, $time);
    $date = implode(" ", $array);
    return $date;   //fusion date
}

$event = new calendar_EventHandler ();
if (isset($_POST['submit']) && ($_POST['disabled'] != "disabled")) {
    //  $event = new calendar_EventHandler () ;
    //check all data exists when exists CALL procedure
    //First check exists the basic information(基本資料)--->check format 
    $action = "loadValue";
    setSession("passData", $_POST);
    setSession("loadValue", $action);

    if ((strpos($_POST['title'], "<script>")) !== false || (strpos($_POST['title'], "</script>")) !== false || (strpos($_POST['title'], "\"")) !== false || (strpos($_POST['title'], "alert")) !== false) {
        $myerror->message('<font color=\'red\'><b>非法字元 請剔除 Error Code:5000</b></font>', '?act=testCalendar-event_profile&id=' . $_POST['event_id'] . '&user_id=' . $_POST['userName'], '回到上一頁', '');
        require(modelExist('error'));
        die();
    }
    if (!check_insert($_POST['title'], "remark")) {   //check format
        $myerror->message('<font color=\'red\'><b>事件標題格式錯誤 請修改   不超過合共40位中英文字 Error Code:5001</b></font>', '?act=testCalendar-event_profile&id=' . $_POST['event_id'] . '&user_id=' . $_POST['userName'], '回到上一頁', '');
        require(modelExist('error'));
        die();
    }
    if ((isset($_POST['choice']) && (in_array("allDay", $_POST['choice'], true)) == "allDay")) {
        if (!check_insert($_POST['start'], "date") || (!check_insert($_POST['end'], "date"))) {
            $myerror->message('<font color=\'red\'><b>請輸入日期  例子: 1997-07-01</b><br/></font>', '?act=testCalendar-event_profile&id=' . $_POST['event_id'] . '&user_id=' . $_POST['userName'], '回到上一頁', '');
            require(modelExist('error'));
            die();
        }
    }


    if (empty($_POST['choice'])) {
        if ((!check_insert($_POST['start'], "date")) || (!check_insert($_POST['start_time'], "time")) || (!check_insert($_POST['end'], "date")) || (!check_insert($_POST['end_time'], "time"))) {   //check format
            $myerror->message('<font color=\'red\'><b>日期/時間格式錯誤 請修改 1997-09-07 </b><br/></font>', '?act=testCalendar-event_profile&id=' . $_POST['event_id'] . '&user_id=' . $_POST['userName'], '回到上一頁', '');
            require(modelExist('error'));
            die();
        }
    }
    if ((isset($_POST['choice']) && (in_array("repeat", $_POST['choice'], true)) == "repeat")) {
        //check format
        if (($_POST['repeat'] <= 0) || ($_POST['repeat_types'] == "#")) {
            $myerror->message('<font color=\'red\'><b>重覆次數不能為0,小過0 / 請選擇重覆類型  請修改 </b><br/></font>', '?act=testCalendar-event_profile&id=' . $_POST['event_id'] . '&user_id=' . $_POST['userName'], '回到上一頁', '');
            require(modelExist('error'));
            die();
        }
    }



    $start_date = implodeDate($_POST['start'], $_POST['start_time']);
    $end_date = implodeDate($_POST['end'], $_POST['end_time']);
    $choice = (isset($_POST['choice']) && ((in_array("allDay", $_POST['choice'], true)) == "allDay")) ? ('1') : ('');


    $data = array(
        'event_id' => $_POST['event_id'],
        'title' => $_POST['title'],
        'start' => $start_date,
        'end' => $end_date,
        'allDay' => $choice,
        'description' => $_POST['description'],
        'reminder' => $_POST['reminder'],
        'timer' => "date",
        'reminder_numbers' => "3",
        'public_own' => $_POST['choiceShare'],
        'user_id' => $_POST['user_id']
    );
    //    print_r_pre($data);
    $event_id = ($_POST['action'] == "update") ? ($event->calendar_updateRepeatEvent($data)) : ($event->calendar_addEventInSetting($data));
    // $event_id = $event_id[0]["LAST_INSERT_ID"];
    // $event_id = ($_POST['event_id']!=0)?($_POST['event_id']):($event->calendar_getLastInsertID()) ; 

    if (isset($_POST['choice']) && ((in_array("repeat", $_POST['choice'], true)) == "repeat")) {
        $data = array('event_id' => $event_id, 'repeat_times' => $_POST['repeat'], 'repeat_types' => $_POST['repeat_types'], 'user_id' => $_POST['user_id']);
        $event->calendar_deleteRepeatEvent($event_id);
        $event->calendar_addRepeatEvent($data);
    }
    if ((isset($_POST['choice']) && (in_array("repeat", $_POST['choice'], true)) != "repeat") || (!isset($_POST['choice']))) {
        $event->calendar_deleteRepeatEvent($event_id);
    }

    if (isset($_POST['shareMember']) && ($_POST['choiceShare'] == '0') && (!empty($_POST['shareMember']))) {

        $shareMemberArray = packToArray($_POST['shareMember']);
        $shareMemberAllowEdit = packToArray($_POST['shareEdit']);
        $event->calendar_deleteShareMember($event_id);

        foreach ($shareMemberArray as $shareMember)
            $event->calendar_addShareMember($event_id, $shareMember);

        foreach ($shareMemberAllowEdit as $allowEditMember) {

            $event->calendar_addShareMemberAllowEdit($event_id, $allowEditMember, "1");
        }
    }
    if ((empty($_POST['shareMember'])) || ($_POST['choiceShare'] == '1')) {
        $event->calendar_deleteShareMember($event_id);
    }
    $myerror->message('修改成功，請耐心等候', '', '', '');
    require(modelExist('ok'));
    echo "<META HTTP-EQUIV='Refresh' CONTENT='1; URL=http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?act=testCalendar-main'>";
    die();
}

//check UnSuccessful format  (non-standard format / date/ no title....etc) 



/*
 */

$myerror->message('取消成功，請耐心等候', '', '', '');
require(modelExist('ok'));
echo "<META HTTP-EQUIV='Refresh' CONTENT='1; URL=http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?act=testCalendar-main'>";
die();


//  $event = new calendar_EventHandler () ;
//         $event->calendar_getRepeatEvent();
?>



