<?php
require_once 'Repository/Department.php' ;
//Add query
$department = new Department();

$courseId= new MongoId() ;

$department_CS = [
    "name" => "CS" ,
    "location" => "city",
    "Course" => [
        [
            "id" => $courseId,
            "title" => "Advance Database",
            "level" => "3",
            "offer" =>[
                [
                    "year" => "2016",
                    "classsize" => "150",
                    "availablePlaces" => ["AC1-LT7"]
                ],
                [
                    "year" => "2015",
                    "classsize" => "150",
                    "availablePlaces" => ["AC1-LT7"]
                ],

            ]
        ]
    ]
];

$expectedValue = array("status" => true) ;
$act = $department->addDeparment($department_CS) ;

echo 'Arrange Function : addDepartment ($department_CS) ;' ."<br />" ;

getAssertMessage($act, $expectedValue) ;


//Update

$avaliablePlace = [
    "fliter" => [
        "Course" => ['$elemMatch'=>['id'=> new MongoId('57b89adebe8a9bb0108b4569')]]
    ],

    "newData" => [
        '$set' => ['Course.$.offer.$.availablePlaces'=>"AC2"]
    ]
] ;

$expectedValue = array("status" => true) ;
$act = $department->updateAvailablePlace($avaliablePlace) ;

echo 'Arrange Function : updateAvailablePlace ($available) ;' ."<br />" ;

getAssertMessage($act, $expectedValue) ;


//Delete query



//Search



function getAssertMessage ( $act, $expected) {

    if ($act === $expected){
        echo "<font color = 'green'>Act value = Expected Value = true</font>" ;
    }else {

        echo "<font color = 'red'>Act value != Expected Value = fail</font>";
    }

    echo '<br />Act Result <br />' ;
    print_r_pre($act) ;

    echo 'Expected Result <br />' ;
    print_r_pre($expected) ;
    
    echo '<hr />' ; 
}


function print_r_pre ($arr){
    echo '<pre>' ;
    print_r($arr) ;
    echo '</pre>' ;
}
