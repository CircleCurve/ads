<?php

// connect
//Select Query
$m = new MongoClient();

$db = $m->comedy ;
$collection = $db->cartoons ;
$cursor = $collection->find() ;

foreach ($cursor as $document) {
    echo $document["_id"] . "<br />";
}
/*
$rtn = iterator_to_array($cursor) ;


foreach ($rtn as $id => $doc){

    foreach ($doc as $key =>$value){

        echo  "key:". $key. "  value : " .$value  . "<br />";
    }

    echo "<hr />";
}*/

//INSERT QUERY
$arr = [
    "userName" => "Patrick1" ,
    "password" => md5("patrick") ,
    "email" => "patrick@test.com"
];

//$collection->insert($arr) ;
//echo "isInsert : " . $arr["_id"] ;

//Update query
$newData = ['$set' => ["userName" => "Ruby" , "email" => "ruby@test.com"]] ;

$isUpdate = $collection->update([
    "userName" => "Patrick1"
],$newData
    );

if ($isUpdate["n"] === 1) {
    echo "update success" ;
}else{
    echo "update fail" ;
}


//DELETE QUERY
$err = $collection->remove([
    'set.userName' =>  "Ruby"
], ["justOne" => true]
    );

print_r($err) ;

//==========Document====================
/*
department
Courses
Offer
Students
Enrolled

1 student enrolled many class
1 course have one offer

Embedded document

Assume Department and Course is being litte and not greater anymore

1. always need to fliter query
Student want to know their offer , and what course they have attended
Teacher want to know how much student attend in their class
How much student enrolled this year

Advantages :
    Select easily , just only one document and it can be assigned whole document to program side
    no need to deal with nosql it'self , that means db 負荷減少

    For Super dependency with program
*/
//1 Department have multiple course
//1 course have 1 offer
Embedded Document to decrease joining

Departments
{
    "Departments" : {
            "id" : "1",
            "name" : "Name1" ,
            "location" : "shitty"
    },
    "Course": [
        {
            "id": "1",
            "Title" : "xxx" ,
            "Level" : "3",
            "offer" : {
                "year" : "" ,
                "classsize": "",
                "availablePlaces":""
            }
        }
    ]
}

Student table :
{
    "Student" : [
        {
            "id":"" ,
            "name"  : "" ,
            "DOB" : "" ,
            "Enrolled": [
                {                    
                    "year" : "" ,
                    "CourseID" :"" ,
                    "EnrolDate" :""
                }
            ]

        }
    ]
}

Query
a) Find the titles of courses offered by the CS department in 2016.

db.Departments.findOne ({ "course.id" : "1" }, {"course.$":1}) ;

b) List the information of courses offered by the CS or IS departments in 2016.

db.Departments.find({
    $or : [ "Departments.name": "CS" , "Departments.name": "IS" ],
    "Course.offer" : 2016
}, {
    "Course.id.$":1
})

db.Departments.aggregate(
    {$unwind:"$Course"},
    {$match : { $or : [ "Departments.name": "CS" , "Departments.name": "IS" ] }, {"Course.offer" : 2016} },
    {$project : { "Course":1}}
)

c) Find the information of the course which is the most popular course enrolled by students.

group by course id and count
db.Student.aggregate (
    {"$unwind" : {"$Student.Enrolled"}},
    {"$group" : {_id:"$Student.Enrolled.CourseID", count:{$sum:1}}},
    {"$sort": { count:  -1 } }
)


d) List the numbers of students for each course, who have enrolled the course offered by the CS department in 2016.
$courseId = db.Student.aggerate (
    {"$unwind" : {"$Student.Enrolled"}},
    {"$group" : {_id:"$Student.Enrolled.CourseID", count:{$sum:1}}},
)
                        
db.Departments.find(
    { "Departments.name":"CS" , "Course.id" : { $in: [$courseIds] } },
        {"_id" : 0 , "Course.Title.$": 1 }  
) ; 


e) List the courses offered by the CS department that the student Chan Tai Man has enrolled in 2016.





//===========Other======================










/*
echo '<pre>' ;
print_r($rtn) ;
echo '</pre>' ;
//var_dump(iterator_to_array($cursor));

/*
foreach ($cursor as $document) {
    echo $document["title"] . "\n";
}
*/
/*
// select a database
$db = $m->comedy;

// select a collection (analogous to a relational database's table)
$collection = $db->cartoons;

// add a record
$document = array( "title" => "Calvin and Hobbes", "author" => "Bill Watterson" );
$collection->insert($document);

// add another record, with a different "shape"
$document = array( "title" => "XKCD", "online" => true );
$collection->insert($document);

// find everything in the collection
$cursor = $collection->find();

// iterate through the results
foreach ($cursor as $document) {
    echo $document["title"] . "\n";
}
*/