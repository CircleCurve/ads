<?php
session_start();

$BE_INCLUDE = true ; 
$rootPath = dirname(dirname(__FILE__)) ."/" ; 

require_once $rootPath.'function/generalFunction.php';
require_once $rootPath.'config/const.php';
require_once $rootPath . 'config/setting.php';

require_once $rootPath.'model/db.php';

require_once $rootPath.'class/route/route.php';
require_once $rootPath.'class/verify/verify.php';
require_once $rootPath.'class/verify/verifyWS.php';

require_once $rootPath.'class/implements/verifyInterface.php';

//=====For Verify Object============
require_once $rootPath.'class/verify/GoogleRecapcha.php';

//====For Parent DB=================



//=======For role======
require_once $rootPath.'class/role/SystemUser.php';
require_once $rootPath.'class/role/staff/Staff.php';
require_once $rootPath.'class/role/Incident/incident.php';
require_once $rootPath.'class/role/Incident/Calendar.php';
require_once $rootPath.'class/role/Incident/Symptoms.php';
require_once $rootPath.'class/role/Report/Report.php';

//=======FOR School===
require_once $rootPath.'class/role/School/Department.php';
require_once $rootPath.'class/role/School/Student.php';
//=======FOR School===


require_once $rootPath.'class/role/patient.php';
require_once $rootPath.'class/role/booking.php';
require_once $rootPath.'class/role/Service.php';
//=======For role======
//=======FOR  DB=======
require_once $rootPath.'model/parent/ParentDB.php';
require_once $rootPath .'model/Member/member.php';
require_once $rootPath.'model/Member/RoleDB.php';
require_once $rootPath.'model/Member/TeamDB.php';
require_once $rootPath.'model/Incident/IncidentDB.php';
require_once $rootPath.'model/Incident/CalendarDB.php';
require_once $rootPath.'model/Incident/SymptomsDB.php';

require_once $rootPath.'model/Report/ReportDB.php';



require_once $rootPath.'model/patientdb.php';
require_once $rootPath.'model/Booking/bookingdb.php';
require_once $rootPath.'model/Service/servicedb.php';
//=======FOR  DB=======

//=======FOR School DB =======
require_once $rootPath.'model/School/DepartmentDB.php';
require_once $rootPath.'model/School/StudentDB.php';



//=======FOR LAYOUT=====
require_once $rootPath.'class/role/Layout/HtmlElement.php';
require_once $rootPath.'class/crossPlatform/CrossPlatform.php' ;


header('Content-Type: text/html; charset=utf-8');